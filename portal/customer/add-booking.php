<?php
session_start();
include_once('../../utils/VayaCleanCityUtility.php');
if (!isset($_SESSION['customer_id'])) {
    header("Location: ../../portal/customer-login");
    exit();
}

$iuserId = $_SESSION["customer_id"];
$customer_vphone = $_SESSION["customer_vphone"];
$vfirstName = $_SESSION["vfirstName"];
$vlastName = $_SESSION["vlastName"];

require_once('includes/header.php');
$cities = json_decode(getAllCities(), true);
$collection_types = json_decode(getAllCollectionTypes(), true);
$collection_sub_types = json_decode(getAllCollectionSubTypes(), true);
$households = json_decode(getSubTypeByCollectionType("HouseHoldCollection"), true);
$commercials = json_decode(getSubTypeByCollectionType("CommercialCollection"), true);
$householdadhocs = json_decode(getSubTypeByCollectionType("HouseHoldAdHocCollection"), true);
$CommercialAdhocCollections = json_decode(getSubTypeByCollectionType("CommercialAdhocCollection"), true);
$currencies = json_decode(getCurrencies(), true);
$wastes = json_decode(getAllWaste(), true);
// $subtypesById = json_decode(getSubTypesById(), true);


// $customers = getAllCustomers();  //causes page to be blank
// var_dump($households);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar " onload="onloadShowNumberOfBags()">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php'); ?>

            <!-- sidebar @e -->
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="html/index.html" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <!-- <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div>
                                    </a>
                                </div>
                            </div> -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                    <a class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-name dropdown-indicator"><?php echo $vfirstName; ?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text"><?php echo  $vfirstName; ?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <?php
                                                    if ($vlastName != "") {
                                                        echo "<li><a href='#' data-toggle='modal' data-target='#edit-customer" . $iuserId . "' ><em class='icon ni ni-user-alt'></em><span>View Profile</span></a></li>";
                                                    } else {
                                                        echo "<li><a href='#' data-toggle='modal' data-target='#edit-cocustomer" . $iuserId . "' ><em class='icon ni ni-user-alt'></em><span>View Profile</span></a></li>";
                                                        echo "<li><a href='#' data-toggle='modal' data-target='#upload-documents' ><em class='icon ni ni-activity-alt'></em><span>Upload Documents</span></a></li>";
                                                    }
                                                    ?>
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->

                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Add Booking</h3>

                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="card card-bordered">
                                        <div class="card-inner">


                                            <ul class="nav nav-tabs">
                                                <li class="nav-item">
                                                    <a class="nav-link active" data-toggle="tab" href="#tabItem5"><span>Household Scheduled Collection</span></a>
                                                </li>
                                                <li class="nav-item">
                                                    <a class="nav-link" data-toggle="tab" href="#tabItem6"><span>Commercial Scheduled Collection</span></a>
                                                </li>
                                                <!-- <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem7"></em><span>Once Off / Adhoc Collection</span></a> 
  </li>    -->
                                            </ul>

                                            <div class="tab-content">
                                                <!-- Household Scheduled Collection -->
                                                <div class="tab-pane active" id="tabItem5">
                                                    <form id="addhousebooking" action="#" class="mt-2" class="form-validate">
                                                        <div class="row gy-4">
                                                            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Collection Type</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="icollectionTypeId" data-placeholder="Select Collection Type" required>
                <?php foreach ($collection_types['eobjResponse']  as $collection_type) : ?>
                    <option value="">Select Collection Type</option>
                    <option value="<?= $collection_type["id"] ?>"><?= $collection_type["collectionType"] . "" ?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div> -->

                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Collection Sub Type</label>
                                                                    <div class="form-control-wrap">
                                                                        <select class="form-select" name="isubCollectionTypeId" data-placeholder="Select Collection Sub" required>
                                                                            <?php foreach ($households['eobjResponse']  as $household) : ?>
                                                                                <option value="">Select Collection Sub Type</option>
                                                                                <option value="<?= $household["id"] ?>"><?= $household["vsubType"] . "" ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>


                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="last-name">Collection Address</label>
                                                                    <input type="text" id="vcollectionsRequestHouseAddress" data-toggle="modal" data-target="#show_map" class="form-control" name="vcollectionsRequestAddress" readonly required>
                                                                    <input type="hidden" id="latitude" name="vcollectionsRequestLat" placeholder="latitude">
                                                                    <input type="hidden" id="longitude" name="vcollectionsRequestLong" placeholder="longitude">
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="last-name">Collection Date</label>
                                                                    <!-- <input type="date" class="form-control" name ="collectionDate"  placeholder=""> -->
                                                                    <input type="text" class="form-control date-picker" name="collectionDate" required>
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="phone-no">Collection Time</label>
                                                                    <input type="text" class="form-control time-picker" name="collectionTime" placeholder="" required>
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="last-name">Number Of Bags</label>
                                                                    <input type="number" id="one_month" class="form-control" min="2" max="12" name="iquantity" placeholder="" required>
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Unit</label>
            <div class="form-control-wrap">
                <select name ="vunit" class="form-select" data-placeholder="Select Unit" required>
                    <option value="">Select Unit</option>
                    <option value="BAGS">BAGS</option>
                </select>
            </div>
        </div>
    </div> -->
                                                            <!--col-->
                                                            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Reccurence</label>
            <div class="form-control-wrap">
                <select name ="reccurence" class="form-select" id="select_recurrence"  onchange="val()" data-placeholder="Select Recurrence" required>
                    <option value="">Select Recurrence </option>
                    <option value="OneMonth">1 Month</option>
                    <option value="TwoMonths">2 Months</option>
                    <option value="ThreeMonths">3 Months</option>
                </select>
            </div>
        </div>
    </div> -->
                                                            <!--col-->
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Currency</label>
                                                                    <div class="form-control-wrap">
                                                                        <select class="form-select" name="currencyId" data-placeholder="Select Currency" required>
                                                                            <?php foreach ($currencies['eobjResponse']  as $currency) : ?>
                                                                                <option value="">Select Currency</option>
                                                                                <option value="<?= $currency["currencyId"] ?>"><?= $currency["code"] . "" ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Waste Type</label>
                                                                    <div class="form-control-wrap">
                                                                        <select name="wasteType" class="form-select" data-placeholder="Select Waste Type" required>
                                                                            <option value="">Select Waste Type </option>
                                                                            <option value="Organic">Organic</option>
                                                                            <option value="Hazadous">Hazadous</option>
                                                                            <option value="Recyclables">Recyclables</option>
                                                                            <option value="EWaste">EWaste</option>
                                                                            <!-- <option value="Industrial">Industrial</option> -->
                                                                            <option value="EWaste">Agriculture</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--col-->

                                                            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Customer</label>
                                                                    <div class="form-control-wrap">
                                                                        <select class="form-control" name="iuserId" data-placeholder="Select Customer" required>
                                                                            <?php foreach ($customers['eobjResponse']  as $customer) : ?>
                                                                                <option value="<?= $customer["iUserId"] ?>">
                                                                                    <?php
                                                                                    if ($customer["partnerType"] == "Corporate") {
                                                                                        echo $customer['companyName'] . " - " . ($customer['vPhone']);
                                                                                    } else {
                                                                                        echo $customer['vFirstName'] . " " . $customer['vLastName'] . " - " . ($customer['vPhone']);
                                                                                    }
                                                                                    ?>
                                                                                </option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div> -->

                                                            <!--col-->
                                                            <input type="hidden" class="form-control" name="iuserId" value="<?php echo $iuserId; ?>">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <input type="hidden" name="create_house_booking" value="true">
                                                                    <button type="button" class="btn btn-primary" name="create_house_booking" onClick="addHouseBooking()">Add</button>
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                        </div>
                                                        <!--row-->
                                                    </form>
                                                </div>
                                                <!-- 
 Commercial Scheduled Collection -->
                                                <div class="tab-pane" id="tabItem6">

                                                    <form id="addcombooking" action="#" class="mt-2" class="form-validate">
                                                        <div class="row gy-4">
                                                            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Collection Type</label>
                <div class="form-control-wrap">
                    <select class="form-select" name ="icollectionTypeId" data-placeholder="Select Collection Type" required>
                    <?php foreach ($collection_types['eobjResponse']  as $collection_type) : ?>
                        <option value="">Select Collection Type</option>
                        <option value="<?= $collection_type["id"] ?>"><?= $collection_type["collectionType"] . "" ?></option>
                    <?php endforeach; ?> 
                    </select>
                </div>
            </div>
        </div> -->
                                                            <!--col-->

                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Collection Sub Type</label>
                                                                    <div class="form-control-wrap">
                                                                        <select class="form-select" name="isubCollectionTypeId" id="collectionUnit" onChange="getUnit();" data-placeholder="Select Collection Sub" required>
                                                                            <?php foreach ($commercials['eobjResponse']  as $commercial) : ?>
                                                                                <option value="">Select Collection Sub Type</option>
                                                                                <option value="<?= $commercial["id"] . '~' . $commercial["vunit"] . '~' . $commercial["message"] . '~' . $commercial["quantity"] ?>"><?= $commercial["vsubType"] . "" ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="last-name">Collection Address</label>
                                                                    <input type="text" id="vcollectionsRequestComAddress" data-toggle="modal" data-target="#show_map1" class="form-control" name="vcollectionsRequestAddress" readonly required>
                                                                    <input type="hidden" id="latitude1" name="vcollectionsRequestLat" placeholder="latitude">
                                                                    <input type="hidden" id="longitude1" name="vcollectionsRequestLong" placeholder="longitude">
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="last-name">Collection Date</label>
                                                                    <!-- <input type="date" class="form-control" name ="collectionDate"  placeholder=""> -->
                                                                    <input type="text" class="form-control date-picker" name="collectionDate" required>
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="phone-no">Collection Time</label>
                                                                    <input type="text" class="form-control time-picker" name="collectionTime" placeholder="" required>
                                                                </div>
                                                            </div>
                                                            <!-- col-->
                                                            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Unit</label>
                <div class="form-control-wrap">
                    <select name ="vunit" class="form-select"  id="select_unit"  onchange="Unitval()"  required>
                        <option selected="selected" value="BAGS">BAGS</option>
                        <option value="TONNAGE">TONNAGE</option>
                    </select>
                </div>
            </div>
        </div> -->
                                                            <!--col -->
                                                            <!--col-->

                                                            <div class="col-md-6 col-lg-4 col-xxl-3" id="hide_all">

                                                                <div class="form-group">
                                                                    <label class="form-label">Quantity</label>
                                                                    <div class="form-control-wrap">
                                                                        <input type="number" id="one_monthx" class="form-control" min="2" name="iquantity" placeholder="" required>
                                                                        <span style="color: red;" id="message"></span>
                                                                    </div>
                                                                </div>
                                                            </div>

                                                            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Reccurence</label>
                <div class="form-control-wrap">
                    <select name ="reccurence" class="form-select" id="select_recurrence_xx"  onchange="val_xx()" data-placeholder="Select Recurrence" required>
                    <option value="">Select Recurrence </option>
                    <option value="OneMonth">1 Month</option>
                    <option value="TwoMonths">2 Months</option>
                    <option value="ThreeMonths">3 Months</option>
                    <option value="OnceOff">Once Off</option>
                    </select>
                </div>
            </div>
        </div> -->
                                                            <!--col-->
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Currency</label>
                                                                    <div class="form-control-wrap">
                                                                        <select class="form-select" name="currencyId" data-placeholder="Select Currency" required>
                                                                            <?php foreach ($currencies['eobjResponse']  as $currency) : ?>
                                                                                <option value="">Select Currency</option>
                                                                                <option value="<?= $currency["currencyId"] ?>"><?= $currency["code"] . "" ?></option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Waste Type</label>
                                                                    <div class="form-control-wrap">
                                                                        <select name="wasteType" class="form-select" data-placeholder="Select Waste Type" required>
                                                                            <option value="">Select Waste Type </option>
                                                                            <option value="Organic">Organic</option>
                                                                            <option value="Hazadous">Hazadous</option>
                                                                            <option value="Recyclables">Recyclables</option>
                                                                            <option value="EWaste">EWaste</option>
                                                                            <option value="Industrial">Industrial</option>
                                                                            <option value="EWaste">Agriculture</option>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                <div class="form-group">
                                                                    <label class="form-label">Customer</label>
                                                                    <div class="form-control-wrap">
                                                                        <select class="form-control" name="iuserId" data-placeholder="Select Customer" required>
                                                                            <?php foreach ($customers['eobjResponse']  as $customer) : ?>
                                                                                <option value="<?= $customer["iUserId"] ?>">
                                                                                    <?php
                                                                                    if ($customer["partnerType"] == "Corporate") {
                                                                                        echo $customer['companyName'] . " - " . ($customer['vPhone']);
                                                                                    } else {
                                                                                        echo $customer['vFirstName'] . " " . $customer['vLastName'] . " - " . ($customer['vPhone']);
                                                                                    }
                                                                                    ?>
                                                                                </option>
                                                                            <?php endforeach; ?>
                                                                        </select>
                                                                    </div>
                                                                </div>
                                                            </div> -->
                                                            <input type="hidden" class="form-control" name="iuserId" value="<?php echo $iuserId; ?>">
                                                            <div class="col-sm-12">
                                                                <div class="form-group">
                                                                    <input type="hidden" name="create_com_booking" value="true">
                                                                    <button type="button" class="btn btn-primary" name="create_com_booking" onClick="addComBooking()">Add</button>
                                                                </div>
                                                            </div>
                                                            <!--col-->
                                                        </div>
                                                        <!--row-->
                                                    </form>
                                                </div>
                                                <!-- Adhoc/ Once Off Collection -->
                                                <div class="tab-pane" id="tabItem7">
                                                    <ul class="nav nav-tabs">
                                                        <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#tabItem14">Household Scheduled Collection</a></li>
                                                        <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tabItem15">Commercial Scheduled Collection</a> </li>
                                                    </ul>
                                                    <div class="tab-content">
                                                        <!-- Household Scheduled Adhoc Collection -->
                                                        <div class="tab-pane active" id="tabItem14">
                                                            <form id="addhouseadbooking" action="#" class="mt-2" class="form-validate">
                                                                <div class="row gy-4">
                                                                    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Collection Type</label>
                    <div class="form-control-wrap">
                        <select class="form-select" name ="icollectionTypeId" data-placeholder="Select Collection Type" required>
                        <?php foreach ($collection_types['eobjResponse']  as $collection_type) : ?>
                            <option value="">Select Collection Type</option>
                            <option value="<?= $collection_type["id"] ?>"><?= $collection_type["collectionType"] . "" ?></option>
                        <?php endforeach; ?> 
                        </select>
                    </div>
                </div>
            </div> -->
                                                                    <!--col-->

                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Collection Sub Type</label>
                                                                            <div class="form-control-wrap">
                                                                                <select class="form-select" name="isubCollectionTypeId" data-placeholder="Select Collection Sub" required>
                                                                                    <?php foreach ($householdadhocs['eobjResponse']  as $householdadhoc) : ?>
                                                                                        <option value="">Select Collection Sub Type</option>
                                                                                        <option value="<?= $householdadhoc["id"] ?>"><?= $householdadhoc["vsubType"] . "" ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="last-name">Collection Address</label>
                                                                            <input type="text" id="vcollectionsRequestHouseAdAddress" data-toggle="modal" data-target="#show_map2" class="form-control" name="vcollectionsRequestAddress" readonly required>
                                                                            <input type="hidden" id="latitude2" name="vcollectionsRequestLat" placeholder="latitude">
                                                                            <input type="hidden" id="longitude2" name="vcollectionsRequestLong" placeholder="longitude">
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="last-name">Collection Date</label>
                                                                            <!-- <input type="date" class="form-control" name ="collectionDate"  placeholder=""> -->
                                                                            <input type="text" class="form-control date-picker" name="collectionDate" required>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="phone-no">Collection Time</label>
                                                                            <input type="text" class="form-control time-picker" name="collectionTime" placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="last-name">Number Of Bags</label>
                                                                            <input type="number" id="one_month_x" class="form-control" min="2" name="iquantity" placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->

                                                                    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Unit</label>
                <div class="form-control-wrap">
                    <select name ="vunit" class="form-select" data-placeholder="Select Unit" required>
                        <option value="">Select Unit</option>
                        <option value="BAGS">BAGS</option>>
                    </select>
                </div>
            </div>
        </div> -->
                                                                    <!--col-->
                                                                    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Reccurence</label>
                    <div class="form-control-wrap">
                        <select name ="reccurence" class="form-select" data-placeholder="Select Recurrence" required>
                        <option value="">Select Recurrence </option>
                    <option value="OneMonth">1 Month</option>
                    <option value="TwoMonths">2 Months</option>
                    <option value="ThreeMonths">3 Months</option>
                    <option value="OnceOff">Once Off</option>
                        </select>
                    </div>
                </div>
            </div> -->
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Currency</label>
                                                                            <div class="form-control-wrap">
                                                                                <select class="form-select" name="currencyId" data-placeholder="Select Currency" required>
                                                                                    <?php foreach ($currencies['eobjResponse']  as $currency) : ?>
                                                                                        <option value="">Select Currency</option>
                                                                                        <option value="<?= $currency["currencyId"] ?>"><?= $currency["code"] . "" ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Waste Type</label>
                                                                            <div class="form-control-wrap">
                                                                                <select name="wasteType" class="form-select" data-placeholder="Select Waste Type" required>
                                                                                    <option value="">Select Waste Type </option>
                                                                                    <option value="Organic">Organic</option>
                                                                                    <option value="Hazadous">Hazadous</option>
                                                                                    <option value="Recyclables">Recyclables</option>
                                                                                    <option value="EWaste">EWaste</option>
                                                                                    <!-- <option value="Industrial">Industrial</option> -->
                                                                                    <option value="EWaste">Agriculture</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    
                                                                    <input type="hidden" class="form-control" name="iuserId" value="<?php echo $iuserId; ?>">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <input type="hidden" name="create_house_ad_booking" value="true">
                                                                            <button type="button" class="btn btn-primary" name="create_house_ad_booking" onClick="addHouseAdBooking()">Add</button>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                </div>
                                                                <!--row-->
                                                            </form>
                                                        </div>
                                                        <!-- Commercial Scheduled Adhoc Collection -->
                                                        <div class="tab-pane" id="tabItem15">
                                                            <form id="addcomadbooking" action="#" class="mt-2" class="form-validate">
                                                                <div class="row gy-4">
                                                                    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Collection Type</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="icollectionTypeId" data-placeholder="Select Collection Type" required>
                <?php foreach ($collection_types['eobjResponse']  as $collection_type) : ?>
                    <option value="">Select Collection Type</option>
                    <option value="<?= $collection_type["id"] ?>"><?= $collection_type["collectionType"] . "" ?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div> -->
                                                                    <!--col-->

                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Collection Sub Type</label>
                                                                            <div class="form-control-wrap">
                                                                                <select class="form-select" name="isubCollectionTypeId" data-placeholder="Select Collection Sub" required>
                                                                                    <?php foreach ($CommercialAdhocCollections['eobjResponse']  as $CommercialAdhocCollection) : ?>
                                                                                        <option value="">Select Collection Sub Type</option>
                                                                                        <option value="<?= $CommercialAdhocCollection["id"] ?>"><?= $CommercialAdhocCollection["vsubType"] . "" ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="last-name">Collection Address</label>
                                                                            <input type="text" id="vcollectionsRequestComAdAddress" data-toggle="modal" data-target="#show_map3" class="form-control" name="vcollectionsRequestAddress" readonly required>
                                                                            <input type="hidden" id="latitude3" name="vcollectionsRequestLat" placeholder="latitude">
                                                                            <input type="hidden" id="longitude3" name="vcollectionsRequestLong" placeholder="longitude">
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="last-name">Collection Date</label>
                                                                            <!-- <input type="date" class="form-control" name ="collectionDate"  placeholder=""> -->
                                                                            <input type="text" class="form-control date-picker" name="collectionDate" required>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="phone-no">Collection Time</label>
                                                                            <input type="text" class="form-control time-picker" name="collectionTime" placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Tonnage</label>
                                                                            <div class="form-control-wrap">
                                                                                <select name="iquantity" class="form-select" data-placeholder="Select Tonnage" required>
                                                                                    <option value="">Select Tonnage</option>
                                                                                    <option value="Two">2T</option>
                                                                                    <option value="ThreeToFive">3-5T</option>
                                                                                    <option value="SixToSeven">6-7T</option>
                                                                                    <option value="EightToTen">8-10T</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Unit</label>
                <div class="form-control-wrap">
                    <select name ="vunit" class="form-select" data-placeholder="Select Unit" required>
                        <option value="">Select Unit</option>
                        <option value="TONNAGE">TONNAGE</option>
                    </select>
                </div>
            </div>
        </div> -->
                                                                    <!--col-->
                                                                    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Reccurence</label>
            <div class="form-control-wrap">
                <select name ="reccurence" class="form-select" data-placeholder="Select Recurrence" required>
                  <option value="">Select Recurrence </option>
                    <option value="OneMonth">1 Month</option>
                    <option value="TwoMonths">2 Months</option>
                    <option value="ThreeMonths">3 Months</option>
                    <option value="OnceOff">Once Off</option>
                </select>
            </div>
        </div>
    </div> -->
                                                                    <!--col-->
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Currency</label>
                                                                            <div class="form-control-wrap">
                                                                                <select class="form-select" name="currencyId" data-placeholder="Select Currency" required>
                                                                                    <?php foreach ($currencies['eobjResponse']  as $currency) : ?>
                                                                                        <option value="">Select Currency</option>
                                                                                        <option value="<?= $currency["currencyId"] ?>"><?= $currency["code"] . "" ?></option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Waste Type</label>
                                                                            <div class="form-control-wrap">
                                                                                <select name="wasteType" class="form-select" data-placeholder="Select Waste Type" required>
                                                                                    <option value="">Select Waste Type </option>
                                                                                    <option value="Organic">Organic</option>
                                                                                    <option value="Hazadous">Hazadous</option>
                                                                                    <option value="Recyclables">Recyclables</option>
                                                                                    <option value="EWaste">EWaste</option>
                                                                                    <option value="Industrial">Industrial</option>
                                                                                    <option value="EWaste">Agriculture</option>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Customer</label>
                                                                            <div class="form-control-wrap">
                                                                                <select class="form-control" name="iuserId" data-placeholder="Select Customer" required>
                                                                                    <?php foreach ($customers['eobjResponse']  as $customer) : ?>
                                                                                        <option value="<?= $customer["iUserId"] ?>">
                                                                                            <?php
                                                                                            if ($customer["partnerType"] == "Corporate") {
                                                                                                echo $customer['companyName'] . " - " . ($customer['vPhone']);
                                                                                            } else {
                                                                                                echo $customer['vFirstName'] . " " . $customer['vLastName'] . "- - " . ($customer['vPhone']);
                                                                                            }
                                                                                            ?>
                                                                                        </option>
                                                                                    <?php endforeach; ?>
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div> -->
                                                                    <input type="hidden" class="form-control" name="iuserId" value="<?php echo $iuserId; ?>">
                                                                    <div class="col-sm-12">
                                                                        <div class="form-group">
                                                                            <input type="hidden" name="create_com_ad_booking" value="true">
                                                                            <button type="button" class="btn btn-primary" name="create_com_ad_booking" onClick="addHouseComBooking()">Add</button>
                                                                        </div>
                                                                    </div>
                                                                    <!--col-->
                                                                </div>
                                                                <!--row-->
                                                            </form>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div><!-- .card-inner-group -->
                                        </div><!-- .card -->
                                    </div><!-- .nk-block -->
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- content @e -->
                    <!-- footer @s -->
                    <div class="nk-footer">
                        <div class="container-fluid">
                            <div class="nk-footer-wrap">
                                <div class="nk-footer-copyright"> &copy; 2022 VAYA TECHNOLOGIES.<a href="https://softnio.com" target="_blank"></a>
                                </div>
                                <div class="nk-footer-links">
                                    <ul class="nav nav-sm">


                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- footer @e -->
                </div>
                <!-- wrap @e -->
            </div>
            <!-- main @e -->
        </div>
        <!-- app-root @e -->
        <!-- Success Commercial Adhoc Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successComAdAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">You’ve successfully created booking</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail  Commercial Adhoc Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failComAdAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='addBookingComAdResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <!-- Success House Hold  Adhoc Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successHouseAdAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">You’ve successfully created booking</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail  House Hold Adhoc Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failHouseAdAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='addBookingHouseAdResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="modal fade" tabindex="-1" id="successHouseAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">You’ve successfully created booking</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail  House Hold Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failHouseAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='addBookingHouseResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- Success Commercial Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successComAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                            <h4 class="nk-modal-title">Success!</h4>
                            <div class="nk-modal-text">
                                <div class="caption-text">You’ve successfully created booking</div>
                            </div>
                            <div class="nk-modal-action">
                                <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Fail  Commercial Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failComAlert">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                            <h4 class="nk-modal-title">Unable to Process!</h4>
                            <div class="nk-modal-text">
                                <div class='addBookingComResponse'></div>
                            </div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Map HouseHold Modal  -->
        <div class="modal fade" tabindex="-1" id="show_map">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="example-alert">
                        <div class="alert alert-primary alert-icon">
                            <em class="icon ni ni-alert-circle"></em> <strong>Search location or drag the marker on the map to the desired location.</strong>
                        </div>
                    </div>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <p><input class="postcode" id="Postcode" name="Postcode" type="text" value="">
                                <input type="submit" id="findbutton" value="Find" onclick="getInputHouseValue();" />
                            </p>
                            <div id="map" style="width:400px; height:350px"></div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Map Commercial Modal  -->
        <div class="modal fade" tabindex="-1" id="show_map1">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="example-alert">
                        <div class="alert alert-primary alert-icon">
                            <em class="icon ni ni-alert-circle"></em> <strong>Search location or drag the marker on the map to the desired location.</strong>
                        </div>
                    </div>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <p><input class="postcode" id="Postcode1" name="Postcode" type="text" value="">
                                <input type="submit" id="findbutton1" value="Find" onclick="getInputComValue();" />
                            </p>
                            <div id="map1" style="width:400px; height:350px"></div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Map HouseHold Adhoc Modal  -->
        <div class="modal fade" tabindex="-1" id="show_map2">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="example-alert">
                        <div class="alert alert-primary alert-icon">
                            <em class="icon ni ni-alert-circle"></em> <strong>Search location or drag the marker on the map to the desired location.</strong>
                        </div>
                    </div>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <p><input class="postcode" id="Postcode2" name="Postcode" type="text" value="">
                                <input type="submit" id="findbutton2" value="Find" onclick="getInputHouseAdValue();" />
                            </p>
                            <div id="map2" style="width:400px; height:350px"></div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!--Map Commercial Adhoc Modal  -->
        <div class="modal fade" tabindex="-1" id="show_map3">
            <div class="modal-dialog" role="document">
                <div class="modal-content">
                    <div class="example-alert">
                        <div class="alert alert-primary alert-icon">
                            <em class="icon ni ni-alert-circle"></em> <strong>Search location or drag the marker on the map to the desired location.</strong>
                        </div>
                    </div>
                    <div class="modal-body modal-body-lg text-center">
                        <div class="nk-modal">
                            <p><input class="postcode" id="Postcode3" name="Postcode" type="text" value="">
                                <input type="submit" id="findbutton3" value="Find" onclick="getInputComAdValue();" />
                            </p>
                            <div id="map3" style="width:400px; height:350px"></div>
                            <div class="nk-modal-action mt-5">
                                <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                            </div>
                        </div>
                    </div><!-- .modal-body -->
                    <div class="modal-footer bg-lighter">
                        <div class="text-center w-100">

                        </div>
                    </div>
                </div>
            </div>
        </div>

        <script>
            function getUnit() {
                const unit_value = document.getElementById("collectionUnit").value;
                const [id, vunit, message, quantity] = unit_value.split('~');
                console.log(id);
                console.log(vunit);
                console.log(message);
                console.log(quantity);

                if (quantity == "Two") {
                    document.getElementById('hide_all').style.display = 'none';
                } else if (quantity == "ThreeToFive") {
                    document.getElementById('hide_all').style.display = 'none';
                } else if (quantity == "SixToSeven") {
                    document.getElementById('hide_all').style.display = 'none';
                } else if (quantity == "EightToTen") {
                    document.getElementById('hide_all').style.display = 'none';
                } else {
                    document.getElementById("message").innerHTML = message;
                    document.getElementById('hide_all').style.display = 'inline-block';
                    document.getElementById("vunit_value").innerHTML = "BAGS";
                }
            }
        </script>
        <script>
            function val() {
                recurrency_value = document.getElementById("select_recurrence").value;
                if (recurrency_value == "OneMonth") {
                    $("#one_month").attr("min", "2");
                    $("#one_month").attr("max", "12");
                } else if (recurrency_value == "TwoMonths") {
                    $("#one_month").attr("min", "2");
                    $("#one_month").attr("max", "24");
                } else {
                    $("#one_month").attr("min", "2");
                    $("#one_month").attr("max", "36");
                }
            }
        </script>
        <script>
            function val_x() {
                recurrency_value = document.getElementById("select_recurrence_x").value;
                if (recurrency_value == "OneMonth") {
                    $("#one_month_x").attr("min", "2");
                } else if (recurrency_value == "TwoMonths") {
                    $("#one_month_x").attr("min", "2");
                } else {
                    $("#one_month_x").attr("min", "2");
                }
            }
        </script>


        <script>
            function val_xx() {
                recurrency_value = document.getElementById("select_recurrence_xx").value;
                if (recurrency_value == "OneMonth") {
                    $("#one_monthx").attr("min", "2");
                } else if (recurrency_value == "TwoMonths") {
                    $("#one_monthx").attr("min", "2");
                } else {
                    $("#one_monthx").attr("min", "2");
                }
            }
        </script>


        <script>
            function onloadShowNumberOfBags() {
                document.getElementById('one_monthx').style.display = 'inline-block';
                $('#tonnage_option').next(".select2-container").hide();
            }
        </script>
        <script>
            function Unitval() {
                unit_value = document.getElementById("select_unit").value;
                console.log(unit_value);
                if (unit_value == "BAGS") {
                    document.getElementById('one_monthx').style.display = 'inline-block';
                    $('#tonnage_option').next(".select2-container").hide();
                } else {
                    $('#tonnage_option').next(".select2-container").show();
                    document.getElementById('one_monthx').style.display = 'none';

                }
            }
        </script>
        <!-- BEGIN: AJAX CALLS-->
        <script>
            //Add House Hold Booking
            function addHouseBooking() {
                if ($("#addhousebooking").valid()) {
                    $('#spinner').show();
                    $.ajax({
                        type: "POST",
                        url: "portal/admin/controller/process.php",
                        data: $('form#addhousebooking').serialize(),
                        cache: false,
                        success: function(response) {
                            var json = $.parseJSON(response);
                            // console.log(json.eobjResponse.ibookingId);
                            // var test = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                            // console.log(test);
                            // return;
                            if (json.responseStatus == "SUCCESS") {
                                $('#spinner').hide();
                                $("#successHouseAlert").modal('show');
                                setTimeout(function() {
                                    // window.location = "portal/admin/bookings";
                                    window.location = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                                }, 2000);

                            } else {
                                $('.addBookingHouseResponse').empty();
                                $('.addBookingHouseResponse').append(json.responseMessage);
                                $('#spinner').hide();
                                $("#failHouseAlert").modal('show');

                            }

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $('.addBookingHouseResponse').empty();
                            $('.addBookingHouseResponse').append(errorThrown);
                            $('#spinner').hide();
                            $("#failHouseAlert").modal('show');

                        }
                    });
                }
            }
        </script>
        <script>
            //Add Commercial Booking
            function addComBooking() {
                if ($("#addcombooking").valid()) {
                    $('#spinner').show();
                    $.ajax({
                        type: "POST",
                        url: "portal/admin/controller/process.php",
                        data: $('form#addcombooking').serialize(),
                        cache: false,
                        success: function(response) {
                            var json = $.parseJSON(response);
                            // console.log(json.statusCode);
                            if (json.responseStatus == "SUCCESS") {
                                $('#spinner').hide();
                                $("#successComAlert").modal('show');
                                setTimeout(function() {
                                    // window.location = "portal/admin/bookings";
                                    window.location = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                                }, 2000);

                            } else {
                                $('.addBookingComResponse').empty();
                                $('.addBookingComResponse').append(json.responseMessage);
                                $('#spinner').hide();
                                $("#failComAlert").modal('show');

                            }

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $('.addBookingComResponse').empty();
                            $('.addBookingComResponse').append(errorThrown);
                            $('#spinner').hide();
                            $("#failComAlert").modal('show');

                        }
                    });
                }
            }
        </script>
        <script>
            //Add AdHoc House Booking
            function addHouseAdBooking() {
                if ($("#addhouseadbooking").valid()) {
                    $('#spinner').show();
                    $.ajax({
                        type: "POST",
                        url: "portal/admin/controller/process.php",
                        data: $('form#addhouseadbooking').serialize(),
                        cache: false,
                        success: function(response) {
                            var json = $.parseJSON(response);
                            // console.log(json.statusCode);
                            if (json.responseStatus == "SUCCESS") {
                                $('#spinner').hide();
                                $("#successHouseAdAlert").modal('show');
                                setTimeout(function() {
                                    // window.location = "portal/admin/bookings";
                                    window.location = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                                }, 2000);

                            } else {
                                $('.addBookingHouseAdResponse').empty();
                                $('.addBookingHouseAdResponse').append(json.responseMessage);
                                $('#spinner').hide();
                                $("#failHouseAdAlert").modal('show');

                            }

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $('.addBookingHouseAdResponse').empty();
                            $('.addBookingHouseAdResponse').append(errorThrown);
                            $('#spinner').hide();
                            $("#failHouseAdAlert").modal('show');

                        }
                    });
                }
            }
        </script>

        <script>
            //Add AdHoc Commercial Booking
            function addHouseComBooking() {
                if ($("#addcomadbooking").valid()) {
                    $('#spinner').show();
                    $.ajax({
                        type: "POST",
                        url: "portal/admin/controller/process.php",
                        data: $('form#addcomadbooking').serialize(),
                        cache: false,
                        success: function(response) {
                            var json = $.parseJSON(response);
                            // console.log(json.statusCode);
                            if (json.responseStatus == "SUCCESS") {
                                $('#spinner').hide();
                                $("#successComAdAlert").modal('show');
                                setTimeout(function() {
                                    // window.location = "portal/admin/bookings";
                                    window.location = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                                }, 2000);

                            } else {
                                $('.addBookingComAdResponse').empty();
                                $('.addBookingComAdResponse').append(json.responseMessage);
                                $('#spinner').hide();
                                $("#failComAdAlert").modal('show');

                            }

                        },
                        error: function(jqXHR, textStatus, errorThrown) {
                            $('.addBookingComAdResponse').empty();
                            $('.addBookingComAdResponse').append(errorThrown);
                            $('#spinner').hide();
                            $("#failComAdAlert").modal('show');

                        }
                    });
                }
            }
        </script>


        <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
        <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs"></script>
        <script>
            function getInputHouseValue() {
                // Selecting the input element and get its value 
                var inputVal = document.getElementById("Postcode").value;
                document.getElementById("vcollectionsRequestHouseAddress").value = inputVal;
                // Displaying the value
                // alert(inputVal);
            }

            var geocoder = new google.maps.Geocoder();
            var marker = null;
            var map = null;

            function initialize() {
                var $latitude = document.getElementById('latitude');
                var $longitude = document.getElementById('longitude');
                var latitude = -17.891120
                var longitude = 30.934920;
                var zoom = 16;

                var LatLng = new google.maps.LatLng(latitude, longitude);

                var mapOptions = {
                    zoom: zoom,
                    center: LatLng,
                    panControl: false,
                    zoomControl: false,
                    scaleControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                map = new google.maps.Map(document.getElementById('map'), mapOptions);
                if (marker && marker.getMap) marker.setMap(map);
                marker = new google.maps.Marker({
                    position: LatLng,
                    map: map,
                    title: 'Drag Me!',
                    draggable: true
                });

                google.maps.event.addListener(marker, 'dragend', function(marker) {
                    var latLng = marker.latLng;
                    $latitude.value = latLng.lat();
                    $longitude.value = latLng.lng();
                });

            }
            initialize();
            $('#findbutton').click(function(e) {
                var address = $("#Postcode").val();
                geocoder.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map.setCenter(results[0].geometry.location);
                        marker.setPosition(results[0].geometry.location);
                        $(latitude).val(marker.getPosition().lat());
                        $(longitude).val(marker.getPosition().lng());
                    } else {
                        alert("Please try again");
                    }
                });
                e.preventDefault();
            });
        </script>

        <script>
            function getInputComValue() {
                // Selecting the input element and get its value 
                var inputVal = document.getElementById("Postcode1").value;
                document.getElementById("vcollectionsRequestComAddress").value = inputVal;
                // Displaying the value
                // alert(inputVal);
            }

            var geocoder1 = new google.maps.Geocoder();
            var marker1 = null;
            var map1 = null;

            function initialize1() {
                var $latitude = document.getElementById('latitude1');
                var $longitude = document.getElementById('longitude1');
                var latitude = -17.891120
                var longitude = 30.934920;
                var zoom = 16;

                var LatLng = new google.maps.LatLng(latitude, longitude);

                var mapOptions = {
                    zoom: zoom,
                    center: LatLng,
                    panControl: false,
                    zoomControl: false,
                    scaleControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                map1 = new google.maps.Map(document.getElementById('map1'), mapOptions);
                if (marker1 && marker1.getMap) marker1.setMap(map1);
                marker1 = new google.maps.Marker({
                    position: LatLng,
                    map: map1,
                    title: 'Drag Me!',
                    draggable: true
                });

                google.maps.event.addListener(marker1, 'dragend', function(marker1) {
                    var latLng = marker1.latLng;
                    $latitude.value = latLng.lat();
                    $longitude.value = latLng.lng();
                });


            }
            initialize1();
            $('#findbutton1').click(function(e) {
                var address = $("#Postcode1").val();
                geocoder1.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map1.setCenter(results[0].geometry.location);
                        marker1.setPosition(results[0].geometry.location);
                        $(latitude).val(marker1.getPosition().lat());
                        $(longitude).val(marker1.getPosition().lng());
                    } else {
                        alert("Please try again");
                    }
                });
                e.preventDefault();
            });
        </script>

        <script>
            function getInputHouseAdValue() {
                // Selecting the input element and get its value 
                var inputVal = document.getElementById("Postcode2").value;
                document.getElementById("vcollectionsRequestHouseAdAddress").value = inputVal;
                // Displaying the value
                // alert(inputVal);
            }

            var geocoder2 = new google.maps.Geocoder();
            var marker2 = null;
            var map2 = null;

            function initialize2() {
                var $latitude = document.getElementById('latitude2');
                var $longitude = document.getElementById('longitude2');
                var latitude = -17.891120
                var longitude = 30.934920;
                var zoom = 16;

                var LatLng = new google.maps.LatLng(latitude, longitude);

                var mapOptions = {
                    zoom: zoom,
                    center: LatLng,
                    panControl: false,
                    zoomControl: false,
                    scaleControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                map2 = new google.maps.Map(document.getElementById('map2'), mapOptions);
                if (marker2 && marker2.getMap) marker2.setMap(map2);
                marker2 = new google.maps.Marker({
                    position: LatLng,
                    map: map2,
                    title: 'Drag Me!',
                    draggable: true
                });

                google.maps.event.addListener(marker2, 'dragend', function(marker2) {
                    var latLng = marker2.latLng;
                    $latitude.value = latLng.lat();
                    $longitude.value = latLng.lng();
                });


            }
            initialize2();
            $('#findbutton2').click(function(e) {
                var address = $("#Postcode2").val();
                geocoder2.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map2.setCenter(results[0].geometry.location);
                        marker2.setPosition(results[0].geometry.location);
                        $(latitude).val(marker2.getPosition().lat());
                        $(longitude).val(marker2.getPosition().lng());
                    } else {
                        alert("Please try again");
                    }
                });
                e.preventDefault();
            });
        </script>

        <script>
            function getInputComAdValue() {
                // Selecting the input element and get its value 
                var inputVal = document.getElementById("Postcode3").value;
                document.getElementById("vcollectionsRequestComAdAddress").value = inputVal;
                // Displaying the value
                // alert(inputVal);
            }

            var geocoder3 = new google.maps.Geocoder();
            var marker3 = null;
            var map3 = null;

            function initialize3() {
                var $latitude = document.getElementById('latitude3');
                var $longitude = document.getElementById('longitude3');
                var latitude = -17.891120
                var longitude = 30.934920;
                var zoom = 16;

                var LatLng = new google.maps.LatLng(latitude, longitude);

                var mapOptions = {
                    zoom: zoom,
                    center: LatLng,
                    panControl: false,
                    zoomControl: false,
                    scaleControl: true,
                    mapTypeId: google.maps.MapTypeId.ROADMAP
                }

                map3 = new google.maps.Map(document.getElementById('map3'), mapOptions);
                if (marker3 && marker3.getMap) marker3.setMap(map3);
                marker3 = new google.maps.Marker({
                    position: LatLng,
                    map: map3,
                    title: 'Drag Me!',
                    draggable: true
                });

                google.maps.event.addListener(marker3, 'dragend', function(marker3) {
                    var latLng = marker3.latLng;
                    $latitude.value = latLng.lat();
                    $longitude.value = latLng.lng();
                });

            }

            initialize3();
            $('#findbutton3').click(function(e) {
                var address = $("#Postcode3").val();
                geocoder3.geocode({
                    'address': address
                }, function(results, status) {
                    if (status == google.maps.GeocoderStatus.OK) {
                        map3.setCenter(results[0].geometry.location);
                        marker3.setPosition(results[0].geometry.location);
                        $(latitude).val(marker3.getPosition().lat());
                        $(longitude).val(marker3.getPosition().lng());
                    } else {
                        alert("Please try again");
                    }
                });
                e.preventDefault();
            });
        </script>

        <!-- JavaScript -->
        <?php require_once('includes/footer.php'); ?>
</body>

</html>