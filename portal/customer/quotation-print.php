<?php 
session_start();

if (!isset($_SESSION['customer_id'])) {
  header("Location: ../../portal/customer-login");
  exit();
}
include_once('../../utils/VayaCleanCityUtility.php');
$iuserId = $_SESSION["customer_id"];
$customer_vphone = $_SESSION["customer_vphone"];
$vfirstName = $_SESSION["vfirstName"];
$vlastName = $_SESSION["vlastName"];
$ibookingId = $_GET['ibookingId'];
$CurrencyCode = $_GET['CurrencyCode'];


require_once('includes/header.php');

// $details = getQuotationById($ibookingId);
// $details = json_decode(getBookingByBookingId($ibookingId),true);
// var_dump($ibookingId);
// var_dump($CurrencyCode);
// exit;


if($CurrencyCode == "USD"){
    $usd_amount = "USD" ."" .json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['fprice'];
    $onload_amount = $usd_amount;
    }else{
    $zwl_amount = "ZWL"."".json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['zwlprice'];
    $onload_amount = $zwl_amount;
    
    }
?>

<body class="bg-white" onload="printPromot()">
    <div class="nk-block">
        <div class="invoice invoice-print">
            <div class="invoice-wrap">
                <div class="invoice-brand text-center">
                <img src="./images/logo-dark.png" srcset="./images/vaya_logo.png 1x" alt="logo">
                </div>
                <div class="invoice-head">
                    <div class="invoice-contact">
                        <span class="overline-title">Quotation To</span>
                        <div class="invoice-contact-info">
                        <h4 class="title"><?php echo $vfirstName." ".$vlastName; ?></h4>
                        <ul class="list-plain">
                        <li><em class="icon ni ni-map-pin-fill"></em><span><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['vcollectionsRequestAddress'] ?></span></li>
                            <li><em class="icon ni ni-call-fill"></em><span><?php echo $customer_vphone; ?></span></li>
                        </ul>
                    </div>
                    </div>
                    <div class="invoice-desc">
                        <h3 class="title">Quotation</h3>
                        <ul class="list-plain">
                        <li class="invoice-id"><span>Quotation ID</span>:<span><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?></span></li>
                            <li class="invoice-date"><span>Date</span>:<span><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['requestedDate'] ?></span></li>
                        </ul>
                    </div>
                </div><!-- .invoice-head -->
                <div class="invoice-bills">
                    <div class="table-responsive">
                        <table class="table table-striped">
                            <thead>
                            <tr>
                               <th class="w-150px">Booking Number</th>
                                <th class="w-60">Waste Type</th>
                                <th>Recurrence</th>
                                <th>Qty</th>
                                <th>Amount</th>
                            </tr>
                            </thead>
                            <tbody>
                            <tr>
                            <td><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?></td>
                            <td><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['wasteType'] ?></td>
                            <td>
                            <?php
                            if(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['recurrence'] == "OneMonth"){
                            echo "1 Month";
                            }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['recurrence'] == "TwoMonths") {
                                echo "2 Months";
                                }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['recurrence'] == "ThreeMonths") {
                                echo "3 Months";
                            }else {
                                echo "Once Off";
                            }
                            ?>
                            </td>
                            <td>
                            <?php
                            if(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "OneToTwo"){
                            echo "1-2T";
                            }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "Three") {
                                echo "3T";
                                }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "FourToSix") {
                                echo "4-6T";
                            }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "Seven") {
                                echo "7T";
                            }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "TenToTwelve") {
                                echo "10-12T";
                            }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == 1) {
                                echo json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity']."BAG";
                            }else {
                                echo json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity']."BAGS";
                            }
                            ?>
                            </td>                                                           
                            <td><?php echo $onload_amount ?></td>
                            </tr>
                            </tbody>
                            <tfoot>
                            <tr>
                            <td colspan="2"></td>
                            <td colspan="2">Grand Total</td>
                            <td><?php echo $onload_amount ?></td>
                            </tr>
                            </tfoot>
                        </table>
                        <div class="nk-notes ff-italic fs-12px text-soft"> Quotation was generated by the system and is valid without the signature and seal. </div>
                    </div>
                </div><!-- .invoice-bills -->
            </div><!-- .invoice-wrap -->
        </div><!-- .invoice -->
    </div><!-- .nk-block -->
    <script>
        function printPromot() {
            window.print();
        }
    </script>
</body>

</html>