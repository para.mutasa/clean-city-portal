<?php 
session_start();

if (!isset($_SESSION['customer_id'])) {
  header("Location: ../../portal/customer-login");
  exit();
}

$iuserId = $_SESSION["customer_id"];

$customer_vphone = $_SESSION["customer_vphone"];
$vfirstName = $_SESSION["vfirstName"];
$vlastName = $_SESSION["vlastName"];

// var_dump($vlastName);
// exit;

include_once('../../utils/VayaCleanCityUtility.php');
?>

<div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
                <div class="nk-sidebar-element nk-sidebar-head">
                    <div class="nk-menu-trigger">
                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
                        <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                    </div>
                    <div class="nk-sidebar-brand">
                        <a href="portal/customer/bookings" class="logo-link nk-sidebar-logo">
                       <h4 style="color:#fff;"> <span class="nk-menu-text">CUSTOMER</span></h4>
                     </a>
                    </div>
                </div><!-- .nk-sidebar-element -->
                <div class="nk-sidebar-element nk-sidebar-body">
                    <div class="nk-sidebar-content">
                        <div class="nk-sidebar-menu" data-simplebar>
                            <ul class="nk-menu">
                                <!-- <li class="nk-menu-item">
                                    <a href="portal/customer/index" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-dashboard-fill"></em></span>
                                        <span class="nk-menu-text">Dashboard</span>
                                    </a>
                                </li> -->
                                <!-- .nk-menu-item -->
                                <li class="nk-menu-item has-sub">
                                    <a href="portal/customer/bookings" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-calendar-booking-fill"></em></span>
                                        <span class="nk-menu-text">Bookings</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <li class="nk-menu-item">
                                            <a href="portal/customer/bookings" class="nk-menu-link"><span class="nk-menu-text">All Bookings</span></a>
                                        </li>
                                    </ul>
                                 
                                </li><!-- .nk-menu-item -->
                                <?php
                                    if($vlastName != ""){
                                        echo "";
                                    }else {                       
                                        echo '<li class="nk-menu-item has-sub">
                                        <a href="#" class="nk-menu-link nk-menu-toggle">
                                            <span class="nk-menu-icon"><em class="icon ni ni-card-view"></em></span>
                                            <span class="nk-menu-text">Documents</span>
                                        </a>
                                        <ul class="nk-menu-sub">
                                        <li class="nk-menu-item"><a href="#" class="nk-menu-link" data-toggle="modal" data-target="#upload-documents" ><span class="nk-menu-text">Upload Documents</span></a></li>
                                        <li class="nk-menu-item"><a href="#" class="nk-menu-link" data-toggle="modal" data-target="#view-documents"><span class="nk-menu-text">View Documents</span></a></li>
                                        </ul>
                                     
                                    </li>';
                                    }  
                                    ?>
                         


                          
                        
                                <!-- .nk-menu-item -->
                            </ul><!-- .nk-menu -->
                        </div><!-- .nk-sidebar-menu -->
                    </div><!-- .nk-sidebar-content -->
                </div><!-- .nk-sidebar-element -->
            </div>