<?php
include_once('../../../utils/VayaCleanCityUtility.php');

//add waste type
if (isset($_POST['add_waste'])) {
        $bagColor = $_POST['bagColor'];
        $capacity = $_POST['capacity'];
        $description = $_POST['description'];
        $variableCost = $_POST['variableCost'];
        $variableCostDescription = $_POST['variableCostDescription'];
        $wasteTypeName = $_POST['wasteTypeName'];

    //  var_dump($bagColor);
    // var_dump($capacity);
    // var_dump($description);
    // var_dump($variableCost);
    // var_dump($variableCostDescription);
    // var_dump($wasteTypeName);
    // exit;

        $add_waste_result = addWaste($bagColor,$capacity,$description,$variableCost,$variableCostDescription,$wasteTypeName);
        $add_waste_data = json_decode($add_waste_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_waste_data);
        exit;
    }

    //edit waste type
if (isset($_POST['edit_waste_type'])) {
    $bagColor = $_POST['bagColor'];
    $capacity = $_POST['capacity'];
    $description = $_POST['description'];
    $id = $_POST['id'];
    $variableCost = $_POST['variableCost'];
    $variableCostDescription = $_POST['variableCostDescription'];
    $wasteTypeName = $_POST['wasteTypeName'];

//  var_dump($bagColor);
// var_dump($capacity);
// var_dump($description);
// var_dump($id);
// var_dump($variableCost);
// var_dump($variableCostDescription);
// var_dump($wasteTypeName);
// exit;


    $edit_waste_result = editWasteType($bagColor,$capacity,$description,$id,$variableCost,$variableCostDescription,$wasteTypeName);
    $edit_waste_data = json_decode($edit_waste_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_waste_data);
    exit;
}

//add currency
if (isset($_POST['add_currency'])) {
    $name = strtoupper($_POST['name']);
    $symbol = $_POST['symbol'];

    $add_currency_result = addCurrency($name,$symbol);
    $add_currency_data = json_decode($add_currency_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_currency_data);
    exit;
}

//edit currency
if (isset($_POST['edit_currency'])) {
    $name = strtoupper($_POST['name']);
    $symbol = $_POST['symbol'];
    $currencyId = $_POST['currencyId'];
    $status = $_POST['status'];

    // var_dump($name);
    // var_dump($symbol);
    // var_dump($currencyId);
    // var_dump($status);
    // exit;

    $edit_currency_result = editCurrency($name,$symbol,$currencyId,$status);
    $edit_currency_data = json_decode($edit_currency_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_currency_data);
    exit;
}
//quotation
if (isset($_POST['create_quotation'])) {
    $wasteType = $_POST['wasteType'];
    $customerId = $_POST['customerId'];
    $recurrence = $_POST['recurrence'];
    $ibookingId = $_POST['ibookingId'];

    $create_quotation_result = createQuotation($wasteType,$customerId,$recurrence);
    $create_quotation_data = json_decode($create_quotation_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($create_quotation_data);
    exit;
}



//assign driver
if (isset($_POST['assign_driver'])) {
    $ibookingId = $_POST['ibookingId'];
    $driverId = $_POST['driverId'];
    $ifranchiseId = $_POST['ifranchiseId'];

    // var_dump($ibookingId);
    // var_dump($driverId);
    //  var_dump($ifranchiseId);
    // exit;
   
    $assign_driver_result = assignDriver($ibookingId,$driverId,$ifranchiseId);
    $assign_driver_data = json_decode($assign_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($assign_driver_data);
    exit;
}

//re-assign driver
if (isset($_POST['reassign_driver'])) {
    $ibookingId = $_POST['ibookingId'];
    $driverId = $_POST['driverId'];


    // var_dump($ibookingId);
    // var_dump($driverId);
    // exit;
   
    $re_assign_driver_result = reAssignDriver($ibookingId,$driverId);
    $re_assign_driver_data = json_decode($re_assign_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($re_assign_driver_data);
    exit;
}

//add collection type
if (isset($_POST['add_collection_type'])) {
    $collectionType = $_POST['collectionType'];
    $unit = $_POST['unit'];
    $status = $_POST['status'];

    $add_collection_type_result = addCollectionType($collectionType,$unit,$status);
    $add_collection_type_data = json_decode($add_collection_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_collection_type_data);
    exit;
}

//edit collection type
if (isset($_POST['edit_collection'])) {
    $collectionType = $_POST['collectionType'];
    $unit = $_POST['unit'];
    $status = $_POST['status'];
    $id = $_POST['id'];

    // var_dump($collectionType);
    // var_dump($unit);
    // var_dump($status);
    // var_dump($id);
    // exit;

    $edit_collection_type_result = updateCollectionType($collectionType,$id,$status,$unit);
    $edit_collection_type_data = json_decode($edit_collection_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_collection_type_data);
    exit;
}
//add collection sub type
if (isset($_POST['add_sub_type'])) {
    $collectionType = $_POST['collectionType'];
    $costPerUnit = $_POST['costPerUnit'];
    $imaxQty = $_POST['imaxQty'];
    $ratePerKm = $_POST['ratePerKm'];
    $vshow = "vshow";
    $vsubType = $_POST['vsubType'];
    $vunit = $_POST['vunit'];

    // var_dump($collectionTypeId);
    // var_dump($costPerUnit);
    // var_dump($imaxQty);
    // var_dump($ratePerKm);
    // var_dump($vshow);
    // var_dump($vsubType);
    // var_dump($vunit);
    // exit;
 

    $add_sub_type_result = addSubType($collectionType,$costPerUnit,$imaxQty,$ratePerKm,$vshow,$vsubType,$vunit);
    $add_sub_type_data = json_decode($add_sub_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_sub_type_data);
    exit;
}

//edit collection sub type
if (isset($_POST['edit_sub_type'])) {
    $costPerUnit = $_POST['costPerUnit'];
    $estatus = $_POST['estatus'];
    $id = $_POST['id'];
    $imaxQty = $_POST['imaxQty'];
  
    $ratePerKm = $_POST['ratePerKm'];
    $typeId = $_POST['typeId'];
    $vshow = "vshow";
    $vsubType = $_POST['vsubType'];
    $vunit = $_POST['vunit'];

    // var_dump($costPerUnit);
    // var_dump($estatus);
    // var_dump($id);
    // var_dump($imaxQty);
    // var_dump($ratePerKm);
    // var_dump($typeId);
    // var_dump($vshow);
    // var_dump($vsubType);
    // var_dump($vunit);
    // exit;

    $edit_sub_type_result = editSubType($costPerUnit,$estatus,$id,$imaxQty,$ratePerKm,$typeId,$vshow,$vsubType,$vunit);
    $edit_sub_type_data = json_decode($edit_sub_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_sub_type_data);
    exit;
}

//add city
if (isset($_POST['add_city'])) {
    $cityName = $_POST['cityName'];
    $country = $_POST['country'];
    $province = $_POST['province'];
    $countryCode = "+263";
 
    $add_city_result = addCity($cityName,$country,$province,$countryCode);
    $add_city_data = json_decode($add_city_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_city_data);
    exit;
}
//edit city
if (isset($_POST['edit_city'])) {
    $cityName = $_POST['cityName'];
    $country = $_POST['country'];
    $countryCode = "+263";
    $province = $_POST['province'];
    $id = $_POST['id'];

    $edit_city_result = editCity($cityName,$country,$countryCode,$province,$id);
    $edit_city_data = json_decode($edit_city_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_city_data);
    exit;
}

//add franchise
if (isset($_POST['add_partner'])) {
    $address = $_POST['address'];
    $address2 = $_POST['address2'];
    $city = $_POST['cityId'];
    $code = "+263";
    $company = $_POST['company'];

    $country = $_POST['country'];
    $email = $_POST['email'];
    $language = $_POST['language'];

    $latitude = $_POST['franchiseLatitude'];
    $longitude = $_POST['franchiseLongitude'];


    $phoneNumber = $_POST['phoneNumber'];
    $province = $_POST['province'];
    $state = $_POST['state'];
    $zimraBpNumber = $_POST['zimraBpNumber'];

    // var_dump($address);
    // var_dump($address2);
    // var_dump($city);
    // var_dump($company);
    // var_dump($country);
    // var_dump($email);
    // var_dump($language);
    // var_dump($longitude);
    // var_dump($latitude);
    // var_dump($longitude);
    // var_dump($phoneNumber);
    // var_dump($province);
    // var_dump($state);
    // var_dump($zimraBpNumber);
    // exit;
 
    $add_partner_result = addPartner($address,$address2,$city,$code,$company,$country,$email,$language,$lattitude,$longitude,$phoneNumber,$province,$state,$zimraBpNumber);
    $add_partner_data = json_decode($add_partner_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_partner_data);
    exit;
}

//create House Hold booking 
if (isset($_POST['create_house_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "HouseHoldCollection";
    $iquantity = strval($_POST['iquantity']);

    $isubCollectionTypeId = "";
    $iuserId = $_POST['iuserId'];
    $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    $vunit = "BAGS";
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $add_house_booking_result = addHouseBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$reccurence,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$vunit,$wasteType);
    $add_house_booking_data = json_decode($add_house_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_house_booking_data);
    exit;
}
//create Commercial  booking 
if (isset($_POST['create_com_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "CommercialCollection";;
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $iuserId = $_POST['iuserId'];
    $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    $vunit = "TONNAGE";
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $add_com_booking_result = addComBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$reccurence,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$vunit,$wasteType);
    $add_com_booking_data = json_decode($add_com_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_com_booking_data);
    exit;
}

//create HouseHold Adhoc  booking 
if (isset($_POST['create_house_ad_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "HouseHoldAdHocCollection";
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $iuserId = $_POST['iuserId'];
    $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    $vunit = "BAGS";
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $add_house_ad_booking_result = addHouseAdBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$reccurence,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$vunit,$wasteType);
    $add_house_ad_booking_data = json_decode($add_house_ad_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_house_ad_booking_data);
    exit;
}


//create Commercial Adhoc  booking 
if (isset($_POST['create_com_ad_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "CommercialAdhocCollection";
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $iuserId = $_POST['iuserId'];
    $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    $vunit = "TONNAGE";
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $add_com_ad_booking_result = addComAdBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$reccurence,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$vunit,$wasteType);
    $add_com_ad_booking_data = json_decode($add_com_ad_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_com_ad_booking_data);
    exit;
}

//add vehicle
if (isset($_POST['add_vehicle'])) {
    $capacity = $_POST['capacity'];
    $franchiseId = $_POST['franchiseId'];
    $licensePlateNumber = $_POST['licensePlateNumber'];
    $make = $_POST['make'];

    $model = $_POST['model'];
    $vehicleStatus = $_POST['vehicleStatus'];
    $year = "2022-02-14T05:59:00.196Z";

    $add_vehicle_result = addVehicle($capacity,$franchiseId,$licensePlateNumber,$make,$model,$vehicleStatus,$year);
    $add_vehicle_data = json_decode($add_vehicle_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_vehicle_data);
    exit;
}

//eco Payment 
if (isset($_POST['eco_payment'])) {
    $ecocashNumber = $_POST['ecocashNumber'];
    $amount = $_POST['amount'];
    $currencyId = $_POST['currencyId'];
    $paidBy = $_POST['paidBy'];

    $paymentMethod = "ECOCASH";
    $bookingId = $_POST['bookingId'];

    // var_dump($ecocashNumber);
    // var_dump($ecocashNumber);
    // var_dump($amount);
    // var_dump($currencyId);
    // var_dump($paidBy);
    // var_dump($paymentMethod);
    // var_dump($bookingId);
    // exit;
  
    $eco_payment_result = ecoPayment($ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId);
    $eco_payment_data = json_decode($eco_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($eco_payment_data);
    exit;
}
//cash Payment 
if (isset($_POST['cash_payment'])) {
    $ecocashNumber = "";
    $amount = $_POST['amount'];
    $currencyId = $_POST['currencyId'];
    $paidBy = $_POST['paidBy'];

    $paymentMethod = "CASH";
    $bookingId = $_POST['bookingId'];

    // var_dump($ecocashNumber);
    // var_dump($amount);
    // var_dump($currencyId);
    // var_dump($paidBy);
    // exit;
  
    $cash_payment_result = cashPayment($ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId);
    $cash_payment_data = json_decode($cash_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($cash_payment_data);
    exit;
}
//approve cash Payment 
if (isset($_POST['approve_user_payment'])) {
    $paymentId = $_POST['paymentId'];
    $paymentStatus = "PAID";
   

    // var_dump($paymentId);
    // var_dump($paymentStatus);
    // exit;
  
    $approve_cash_payment_result = approvePayment($paymentId,$paymentStatus);
    $approve_cash_payment_data = json_decode($approve_cash_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($approve_cash_payment_data);
    exit;
}


//add driver
if (isset($_POST['add_driver'])) {
    $type = "franchiseDriverSignup";
    $vfirstName = $_POST['vfirstName'];
    $vlastName = $_POST['vlastName'];
    $ifranchiseId = $_POST['ifranchiseId'];

    $vemail = $_POST['vemail'];
    $vphone = $_POST['vphone'];
    $caddress = $_POST['caddress'];
    $caddress2 = $_POST['caddress2'];
    $egender = $_POST['egender'];
    $phoneCode = "+263";
    $countryCode = "ZWL";
    $vpassword = $_POST['vpassword'];

    $add_driver_result = addDriver($type,$vfirstName,$vlastName,$ifranchiseId,$vemail,$vphone,$caddress,$caddress2,$egender,$phoneCode,$countryCode,$vpassword);
    $add_driver_data = json_decode($add_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_driver_data);
    exit;
}


//add payment method
if (isset($_POST['add_payment_method'])) {
    $description = strtoupper($_POST['description']);

    $add_payment_method_result = addPaymentMethod($description);
    $add_payment_method_data = json_decode($add_payment_method_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_payment_method_data);
    exit;
}

//edit driver
if (isset($_POST['edit_driver'])) {
    $driverId = $_POST['driverId'];;
    $vname = $_POST['vname'];
    $vlastName = $_POST['vlastName'];
    $vemail = $_POST['vemail'];

    $vphone = $_POST['vphone'];
    $vcaddress = $_POST['vcaddress'];

    $vcadress2 = $_POST['vcadress2'];
    $egender = $_POST['egender'];
    $franchiseId = $_POST['franchiseId'];
    $status = $_POST['status'];
  
    $vpassword = $_POST['vpassword'];

    // var_dump($driverId);
    // var_dump($vname);
    // var_dump($vlastName);
    // var_dump($vemail);
    // var_dump($vphone);
    // var_dump($vcaddress);
    // var_dump($vcadress2);
    // var_dump($egender);
    // var_dump($franchiseId);
    // var_dump($status);
    // exit;

    $edit_driver_result = editDriver($driverId,$vname,$vlastName,$vemail,$vphone,$vcaddress,$vcadress2,$egender,$franchiseId,$status,$vpassword);
    $edit_driver_data = json_decode($edit_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_driver_data);
    exit;
}

//edit franchise
if (isset($_POST['edit_partner'])) {
    $address = $_POST['address'];;
    $address2 = $_POST['address2'];
    $city = $_POST['cityId'];
    $code = "+263";

    $company = $_POST['company'];
    $country = $_POST['country'];

    $email = $_POST['email'];
    $franchiseId = $_POST['franchiseId'];
    $language = $_POST['language'];


    $latitude = $_POST['franchiseLatitude'];
    $longitude = $_POST['franchiseLongitude'];

  
    $status = $_POST['status'];
  
    $vpassword = $_POST['vpassword'];
    $phoneNumber = $_POST['phoneNumber'];
  
    $province = $_POST['province'];

    $ratePerKM = $_POST['ratePerKM'];
  
    $registrationDate = $_POST['registrationDate'];
    $state = $_POST['state'];
    $status = $_POST['status'];
    $zimra_BP_Number = $_POST['zimra_BP_Number'];


    // var_dump($address);
    // var_dump($address2);
    // var_dump($city);
    // var_dump($code);
    // var_dump($company);
    // var_dump($country);
    // var_dump($email);
    // var_dump($franchiseId);
    // var_dump($language);
    // var_dump($latitude);
    // var_dump($longitude);
    // var_dump($phoneNumber);
    // var_dump($province);

    // var_dump($ratePerKM);
    // var_dump($registrationDate);
    // var_dump($state);
    // var_dump($status);
    // var_dump($zimra_BP_Number);
    // exit;

    $edit_partner_result = editPartner($address,$address2,$city,$code,$company,$country,$email,$franchiseId,$language,$latitude,$longitude,$phoneNumber,$province,$ratePerKM,$registrationDate,$state,$status,$zimra_BP_Number);
    $edit_partner_data = json_decode($edit_partner_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_partner_data);
    exit;
}

//edit vehicle
if (isset($_POST['edit_vehicle'])) {
    $capacity = $_POST['capacity'];;
    $franchiseId = $_POST['franchiseId'];
    $licensePlateNumber = $_POST['licensePlateNumber'];

    $make = $_POST['make'];
    $model = $_POST['model'];

    $vehicleId = $_POST['vehicleId'];
    $vehicleStatus = $_POST['vehicleStatus'];
    $originalDate = $_POST['year'];
    $year = date("d/m/Y", strtotime($originalDate));
    // $year = $_POST['year'];

  
    // var_dump($capacity);
    // var_dump($franchiseId);
    // var_dump($licensePlateNumber);
    // var_dump($make);
    // var_dump($model);
    // var_dump($vehicleId);
    // var_dump($vehicleStatus);
    // var_dump($year);
    // exit;

    $edit_vehicle_result = editVehicle($capacity,$franchiseId,$licensePlateNumber,$make,$model,$vehicleId,$vehicleStatus,$year);
    $edit_vehicle_data = json_decode($edit_vehicle_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_vehicle_data);
    exit;
}

//edit payment method
if (isset($_POST['edit_payment_method'])) {
    $description = strtoupper($_POST['description']);
    $paymentMethodId = $_POST['paymentMethodId'];
    $status = $_POST['status'];

    // var_dump($description);
    // var_dump($paymentMethodId);
    // var_dump($status);
    // exit;

    $edit_payment_method_result = editPaymentMethod($description,$paymentMethodId,$status);
    $edit_payment_method_data = json_decode($edit_payment_method_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_payment_method_data);
    exit;
}

//start trip
if (isset($_POST['start_trip'])) {
    $ibookingId = $_POST['ibookingId'];
    $start_trip_result = startTrip($ibookingId);
    $start_trip_data = json_decode($start_trip_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($start_trip_data);
    exit;
}
//approve booking cancellation by driver 
if (isset($_POST['approve_cancel_booking'])) {
    $bookingId = $_POST['bookingId'];
    $userId = $_POST['userId'];

    var_dump($bookingId);
    var_dump($userId);
    exit;

    $approve_booking_cancel_result = approveBookingCancellation($bookingId,$userId);
    $approve_booking_cancel_data = json_decode($approve_booking_cancel_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($approve_booking_cancel_data);
    exit;
}


//end trip
if (isset($_POST['end_trip'])) {
    $ibookingId = $_POST['ibookingId'];
  
    // var_dump($ibookingId);
    // exit;

    $end_trip_result = endTrip($ibookingId);
    $end_trip_data = json_decode($end_trip_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($end_trip_data);
    exit;
}
    //user login

if (isset($_POST['user_login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $user_login_result = userLogin($email,$password);
    $user_login_data_data = json_decode($user_login_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($user_login_data_data);
    exit;
}




   


    
    
    ?>
