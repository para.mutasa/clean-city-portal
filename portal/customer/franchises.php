<?php 
session_start();
include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
$partners = json_decode(getAllPartners(), true);
$cities = json_decode(getAllCities(), true);
// $customers = getAllCustomers();
// var_dump($customers);
// exit;
// var_dump($partners);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator">Abu Bin Ishityak</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text">Abu Bin Ishtiyak</span>
                                                        <span class="sub-text">info@softnio.com</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="#"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                                    <li class="dropdown notification-dropdown mr-n1">
                                        <a href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">
                                            <div class="icon-status icon-status-info"><em class="icon ni ni-bell"></em></div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-head">
                                                <span class="sub-title nk-dropdown-title">Notifications</span>
                                                <a href="#">Mark All as Read</a>
                                            </div>
                                            <div class="dropdown-body">
                                                <div class="nk-notification">
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                </div><!-- .nk-notification -->
                                            </div><!-- .nk-dropdown-body -->
                                            <div class="dropdown-foot center">
                                                <a href="#">View All</a>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Franchise Lists</h3>
                                        
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="portal/admin/add-franchise"><span>Add Franchise</span></a></li> 
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" id="franchise_table" data-order='[[5, "desc"]]'  data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                          <th class="nk-tb-col"><span class="sub-text">Name</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Phone</span></th>
                                                            <th class="nk-tb-col tb-col-mg"><span class="sub-text">Email</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Address</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Country</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Created Date</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($partners['responseStatus'] == "SUCCESS") : ?>
                                                    <?php
                                                    foreach ($partners['eobjResponse'] as $partner) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $partner['company'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $partner['phoneNumber'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $partner['email'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $partner['address'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $partner['country'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $partner['registrationDate'] ?></span></td>
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                                            
                                                                <li class="nk-tb-action-hidden">
                                                                    <a href="#" class="btn btn-trigger btn-icon" data-toggle="tooltip" data-placement="top" title="Send Email">
                                                                        <em class="icon ni ni-mail-fill"></em>
                                                                    </a>
                                                                </li>
                                                                <li>
                                                                    <div class="drodown">
                                                                        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <li><a href="#" data-toggle="modal" data-target="#editPartner<?= $partner['franchiseId'] ?>"><em class="icon ni ni-edit"></em><span>Edit</span></a></li>
   
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <!-- Edit Franchise-->
                                                    <div class="modal fade" tabindex="-1" role="dialog" class="edit_franchise"  id="editPartner<?= $partner['franchiseId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Edit Franchise</h5>
                                                                <div class="card-inner">
                                                    <form id ="ecoFranchise<?= $partner['franchiseId'] ?>" class="mt-2">
                                                <div class="row gy-4">
                                                    <div class="col-md-6 col-lg-6 col-xxl-3">
                                                        <div class="form-group">
                                                        <label class="form-label" for="first-name">Company Name</label>
                                                        <input type="text" class="form-control" name ="company"  value="<?= $partner['company'] ?>"  required>
                                                        </div>
                                                    </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">Address</label>
                                                       <input type="text" id="franchiseAddress" data-toggle="modal" data-target="#show_map" class="form-control" name ="address" value="<?= $partner['address'] ?>"  readonly required>
                                                        <input type="hidden" id="latitude" name ="franchiseLatitude" placeholder="latitude" value="<?= $partner['lattitude'] ?>">
                                                        <input type="hidden" id="longitude" name="franchiseLongitude" placeholder="longitude" value="<?= $partner['longitude'] ?>">
                                             
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">Address (2)</label>
                                                    <input type="text" class="form-control" name ="address2"  value="<?= $partner['address2'] ?>" >  
                                                   </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="phone-no">Phone Number</label>
                                                   <input type="text" class="form-control" name ="phoneNumber"  value="<?= $partner['phoneNumber'] ?>"  required>
                                                   </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">Email</label>
                                                    <input type="email" class="form-control" name ="email"  value="<?= $partner['email'] ?>" >
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">ZIMRA BP Numnber</label>
                                                     <input type="text" class="form-control" name ="zimra_BP_Number"  value="<?= $partner['zimra_BP_Number'] ?>" >
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">Rate Per Km</label>
                                                     <input type="text" class="form-control" name ="ratePerKM"  value="<?= $partner['ratePerKM'] ?>" >
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status"> <span class="sr-only">Loading...</span> </div></div>
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label">Country</label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select" name ="country" data-placeholder="Select Country"  value="<?= $partner['country'] ?>" required>
                                                                <option value="">Select Country</option>
                                                                <option value="Zimbabwe">Zimbabwe</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">Province</label>
                                                    <input type="text" class="form-control" name ="province"  value="<?= $partner['province'] ?>"  required>
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label">City</label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select" name ="cityId" data-placeholder="Select City" value="<?= $partner['city'] ?>"  required>
                                                            <?php foreach ($cities['eobjResponse']  as $city): ?>
                                                                <option value="">Select City</option>
                                                                <option value="<?= $city["id"] ?>"><?= $city["cityName"].""?></option>
                                                            <?php endforeach; ?> 
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">State</label>
                                                    <input type="text" class="form-control" name ="state"  value="<?= $partner['state'] ?>" >  
                                                  </div>
                                                </div>
                                                <!--col-->
                                            
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label">Language</label>
                                                        <div class="form-control-wrap">
                                                            <select name ="language" class="form-select" data-placeholder="Select language" value="<?= $partner['language'] ?>"  required>
                                                                <option value="English">English</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label">Status</label>
                                                        <div class="form-control-wrap">
                                                            <select name="status" class="form-select" data-placeholder="Select Status"  value="<?= $partner['status'] ?>" >
                                                                <option value="">Select Status</option>
                                                                <option value="ONLINE">ONLINE</option>
                                                                <option value="OFFLINE">OFFLINE</option>
                                                                <option value="DELETED">DELETED</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <input type="hidden" class="form-control" name="franchiseId"  value="<?= $partner['franchiseId'] ?>" >
                                                <input type="hidden" class="form-control" name="registrationDate"  value="<?= $partner['registrationDate'] ?>" >
                                                <div class="col-sm-12">
                                                <div class="form-group">
                                                <input type="hidden" name="edit_partner" value="true">
                                                 <button type="button" class="btn btn-primary" name ="edit_partner"  onClick="editPartner('<?= $partner["franchiseId"] ?>')" >Edit</button>
                                                    </div>
                                                </div>
                                                <!--col-->
                                            </div>
                                            <!--row-->
                                            </form>
                                          </div><!-- .card-inner-group -->
                                          <div class="modal-footer bg-light">
                                            <span class="sub-text"></span>
                                         </div>
                        
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->

                                                    <?php endforeach; ?>

                                                    <?php else : ?>

                                                    <?= $partners['responseMessage'] ?>

                                                    <?php endif; ?>

                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

            <!-- Success Modal Alert -->
            <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully updated franchise</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='editPartnerResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
                <!--Map  Modal  -->
 <div class="modal fade" tabindex="-1" id="show_map">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body modal-body-lg text-center">
            <div class="nk-modal">
            <p><input class="postcode" id="addressCode" name="addressCode" type="text" value="">
            <input type="submit" id="findbutton" value="Find" onclick="getInputAddress();" /></p>
            <div id="map" style="width:400px; height:350px"></div>
                <div class="nk-modal-action mt-5">
                    <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                </div>
            </div>
        </div><!-- .modal-body -->
        <div class="modal-footer bg-lighter">
            <div class="text-center w-100">
            
            </div>
        </div>
    </div>
</div>
</div>
 
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs"></script>
   <script>
  function getInputAddress(){
            // Selecting the input element and get its value 
            var inputAddress = document.getElementById("addressCode").value;
            document.getElementById("franchiseAddress").value = inputAddress;
            // Displaying the value
            // alert(inputAddress);
        }
  </script>
    <script>
 var geocoder = new google.maps.Geocoder();
var marker = null;
var map = null;
function initialize() {
      var $latitude = document.getElementById('latitude');
      var $longitude = document.getElementById('longitude');
      var latitude = -17.891120
      var longitude = 30.934920;
      var zoom = 16;

      var LatLng = new google.maps.LatLng(latitude, longitude);

      var mapOptions = {
        zoom: zoom,
        center: LatLng,
        panControl: false,
        zoomControl: false,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      if (marker && marker.getMap) marker.setMap(map);
      marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        title: 'Drag Me!',
        draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function(marker) {
        var latLng = marker.latLng;
        $latitude.value = latLng.lat();
        $longitude.value = latLng.lng();
      });


    }
    initialize();
    $('#findbutton').click(function (e) {
        var address = $("#Postcode").val();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                $(latitude).val(marker.getPosition().lat());
                $(longitude).val(marker.getPosition().lng());
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        e.preventDefault();
    });
    </script>

    <!-- BEGIN: AJAX CALLS-->
   <script>
    //Edit Partner
    function editPartner(franchiseId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#ecoFranchise'+franchiseId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('.edit_franchise').modal('hide');
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/franchises";
                    }, 2000);
                  
                } else {
                    $('.editPartnerResponse').empty();
                    $('.editPartnerResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('.edit_franchise').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".editPartnerResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.editPartnerResponse').empty();
                $('.editPartnerResponse').append(errorThrown);
                $('#spinner').hide();
                $('.edit_franchise').modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".editPartnerResponse").html("");
                });
            }
        });
    }
    </script>

   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>