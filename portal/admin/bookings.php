<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
  $admin_id =  $_SESSION["admin_id"];
  $admin_phone  =$_SESSION["admin_vphone"];
  $admin_email =  $_SESSION["admin_Email"];
  $admin_firstname =  $_SESSION["first_name"];
  $admin_lastname = $_SESSION["last_name"];
require_once('includes/header.php');
include_once('../../utils/VayaCleanCityUtility.php');
$bookings = json_decode(getBookings(), true);
$currencies = json_decode(getCurrencies(), true);
$partners = json_decode(getAllPartners(), true);
// var_dump($bookings);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="portal/admin/user-profile-regular"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="portal/admin/user-profile-setting"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="portal/admin/user-profile-activity"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                              
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Booking List</h3>
                                            
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                    <li class="nk-block-tools-opt"><a href="portal/admin/add-booking" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Add Booking</span></a></li>
                                                        <!-- <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="portal/admin/add-booking" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="portal/admin/add-booking"><span>Add Booking</span></a></li> 
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li> -->
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-order='[[0, "desc"]]' id="admin_bookings" data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                            <th class="nk-tb-col"><span class="sub-text">Booking Number</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Amount</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Franchise</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Collection Address</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Quantity</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Booking Status</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Payment Status</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Booking Date</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Proof Of Payment</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Action</span></th>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($bookings['responseStatus'] == "SUCCESS") : ?>

                                                    <?php
                                                    foreach ($bookings['eobjResponse'] as $booking) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['ibookingId'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($booking['currencyCode'] == "ZWL"){
                                                            echo "ZWL"."".$booking['zwlprice'];
                                                            }else {
                                                             echo "USD"."".$booking['fprice'];
                                                            }
                                                        ?>
                                                    </span>
                                                </td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= json_decode(getFranchiseById($booking['ifranchiseId']),true)['eobjResponse']['company']  ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['vcollectionsRequestAddress'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($booking['iquantity'] == "Two"){
                                                            echo "2T";
                                                            }elseif($booking['iquantity'] == "Three") {
                                                                echo "3T";
                                                            }elseif($booking['iquantity'] == "ThreeToFive") {
                                                                echo "3-5T";
                                                            }elseif($booking['iquantity'] == "SixToSeven") {
                                                                echo "6-7T";
                                                            }elseif($booking['iquantity'] == "EightToTen") {
                                                                echo "8-10T";
                                                            }elseif($booking['iquantity'] == 1) {

                                                                echo $booking['iquantity'].""."BAG";
                                                            }else {
                                                                echo $booking['iquantity'].""."BAGS";
                                                            }
                                                        ?>
                                                        </span>
                                                       </td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['estatus'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['paymentStatus'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['requestedDate'] ?></span></td>
                                                        <td class="tb-odr-action">
                                                        <div class="tb-odr-btns d-none d-sm-inline">
                                                        <?php
                                                        if($booking['proofOfPayment'] != ""){
                                                        echo "<a target='_blank' href='https://vayaafrica.com/clean_city_java_backend/download/".$booking['proofOfPayment']."' class='btn btn-dim btn-sm btn-primary' title='Proof Of Payment'>View</a>";
                                                        }
                                                        ?>
                                                          </div>
                                                        </td>
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                            
                                                                <li>
                                                                    <div class="drodown">
                                                                    <a  class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <?php
                                                                              if($booking['estatus'] == "ASSIGNED" || $booking['estatus'] == "REASSIGNED" && $booking['paymentStatus'] == "PAID"){
                                                                                echo"<li><a target='_blank'  href='portal/admin/quotation-details?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>View Quotation</span></a></li>";
                                                                                echo"<li><a  data-toggle='modal' data-target='#driver".$booking['ibookingId'] ."' ><em class='icon ni ni-edit'></em><span>View Driver</span></a></li>";
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";
                                                                                echo"<li><a  data-toggle='modal' data-target='#start_trip".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>Start Trip</span></a></li>";
                                                                                echo"<li><a target='_blank' href='portal/admin/edit-booking?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>Edit Booking</span></a></li>";
                                                                              }elseif($booking['estatus'] == "DISPATCHED") {
                                                                                echo"<li><a target='_blank'  href='portal/admin/quotation-details?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>View Quotation</span></a></li>";
                                                                                echo"<li><a  data-toggle='modal' data-target='#driver".$booking['ibookingId'] ."' ><em class='icon ni ni-edit'></em><span>View Driver</span></a></li>";
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";
                                                                                echo"<li><a  data-toggle='modal' data-target='#end_trip".$booking['ibookingId'] ."' ><em class='icon ni ni-edit'></em><span>End Trip</span></a></li>";
                                                                                echo"<li><a target='_blank'  href='portal/admin/edit-booking?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>Edit Booking</span></a></li>";
                                                                              
                                                                              }elseif($booking['estatus'] == "DRIVER_CANCELLATION_IN_PROGRESS") {
                                                                                echo"<li><a target='_blank' href='portal/admin/quotation-details?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>View Quotation</span></a></li>";
                                                                                echo"<li><a  data-toggle='modal' data-target='#driver".$booking['ibookingId'] ."' ><em class='icon ni ni-edit'></em><span>View Driver</span></a></li>";
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";
                                                                                echo"<li><a data-toggle='modal' data-target='#approve_cancel".$booking['ibookingId'] ."' ><em class='icon ni ni-edit'></em><span>Approve Cancellation</span></a></li>";
                                                                                echo"<li><a target='_blank'  href='portal/admin/edit-booking?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>Edit Booking</span></a></li>";
                                                                              }elseif($booking['estatus'] == "PENDING") {
                                                                                echo"<li><a target='_blank'  href='portal/admin/quotation-details?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>View Quotation</span></a></li>";
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";
                                                                                // echo"<li><a data-toggle='modal' data-target='#re_assign_franchise".$booking['ibookingId'] ."' ><em class='icon ni ni-edit'></em><span>Re-Assign Franchise</span></a></li>";
                                                                                echo"<li><a target='_blank'  href='portal/admin/edit-booking?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>Edit Booking</span></a></li>";
                                                                              }else {                                     
                                                                                echo"<li><a target='_blank'  href='portal/admin/quotation-details?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>View Quotation</span></a></li>";
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";
                                                                                echo"<li><a  data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";
                                                                                echo"<li><a target='_blank'  href='portal/admin/edit-booking?ibookingId=".$booking['ibookingId']."' ><em class='icon ni ni-eye'></em><span>Edit Booking</span></a></li>";
                                                                              }  
                                                                            ?>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                 <!-- Quotation-->
                                                 <div class="modal fade" tabindex="-1" role="dialog" class="quotation" id="quotation<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Booking</h5>
                                                                <form id ="ecoQuotation<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Collection Address</label>
                                                                                <input type="text" class="form-control"  value="<?= $booking['vcollectionsRequestAddress'] ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                     
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Booking Number</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['ibookingId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Customer</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['iuserId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Driver</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['idriverId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                            <div class="form-group">
                                                                                <label class="form-label">Currency</label>
                                                                                <div class="form-control-wrap">
                                                                                    <select class="form-select" name ="currencyId" data-placeholder="Select Currency" required>
                                                                                    <?php foreach ($currencies['eobjResponse']  as $currency): ?>
                                                                                        <option value="">Select Currency</option>
                                                                                        <option value="<?= $currency["currencyId"] ?>"><?= $currency["name"].""?></option>
                                                                                    <?php endforeach; ?> 
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" class="form-control" name ="wasteType" value="Plastics" >                  
                                                                        <input type="hidden" class="form-control" name ="customerId" value="5" >   
                                                                        <input type="hidden" class="form-control" name ="recurrence" value="<?= $booking['recurrence'] ?>" >  
                                                                        <input type="hidden" class="form-control" name ="ibookingId" value="<?= $booking['ibookingId'] ?>" >     
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="create_quotation">
                                                                                 <button type="button" class="btn btn-primary" name ="create_quotation" onClick="createQuotation('<?= $booking["ibookingId"] ?>')">Generate Quotation</button>
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                 <!-- Admin Approve Cancel-->
                                                 <div class="modal fade" tabindex="-1" role="dialog" class="approve_cancel" id="approve_cancel<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Approve Booking Cancellation</h5>
                                                                <form id ="approveCancel<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Collection Address</label>
                                                                                <input type="text" class="form-control"  value="<?= $booking['vcollectionsRequestAddress'] ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                     
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Booking Number</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['ibookingId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Customer</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['iuserId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Driver Name</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['name']." ".json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['surname']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Driver Phone</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['phoneNo']   ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Cancellation Reason</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['vcancelReason'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                     
                                                                        <input type="hidden" class="form-control" name ="bookingId" value="<?= $booking['ibookingId'] ?>" > 
                                                                        <input type="hidden" class="form-control" name ="userId" value="5" >        
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="approve_cancel_booking">
                                                                                 <button type="button" class="btn btn-primary" name ="approve_cancel_booking" onClick="approveCancelBooking('<?= $booking["ibookingId"] ?>')">Approve</button>
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                <!-- Re-Assign Franchise-->
                                                 <div class="modal fade" tabindex="-1" role="dialog"  id="re_assign_franchise<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Re-Assign Franchise</h5>
                                                                <form id ="assignfranchise<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Collection Address</label>
                                                                                <input type="text" class="form-control"  value="<?= $booking['vcollectionsRequestAddress'] ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                     
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Booking Number</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['ibookingId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Franchise</label>
                                                                            <div class="form-control-wrap">
                                                                            <select class="form-select" name ="iFranchiseId" data-placeholder="Select Franchise" >
                                                                                <?php foreach ($partners['eobjResponse']  as $partner): ?>
                                                                                    <option value="">Select Franchise</option>
                                                                                    <option value="<?= $partner["franchiseId"] ?>"><?= $partner["company"].""?></option>
                                                                                <?php endforeach; ?> 
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                        <input type="hidden" class="form-control" name ="bookingId" value="<?= $booking['ibookingId'] ?>" >      
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="re_assign_franchise">
                                                                                 <button type="button" class="btn btn-primary" name ="re_assign_franchise" onClick="reAssignFranchise('<?= $booking["ibookingId"] ?>')">Re-Assign Franchise</button>
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                 <!-- Start Trip-->
                                                 <div class="modal fade" tabindex="-1" role="dialog" class="start_trip" id="start_trip<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Start Trip</h5>
                                                                <form id ="ecoStartTrip<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Collection Address</label>
                                                                                <input type="text" class="form-control"  value="<?= $booking['vcollectionsRequestAddress'] ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                     
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Booking Number</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['ibookingId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Customer</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['iuserId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Driver Name</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['name']." ".json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['surname']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Driver Phone</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['phoneNo']   ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <input type="hidden" class="form-control" name ="ibookingId" value="<?= $booking['ibookingId'] ?>" >      
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="start_trip">
                                                                                 <button type="button" class="btn btn-primary" name ="start_trip" onClick="startTrip('<?= $booking["ibookingId"] ?>')">Start Trip</button>
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                 <!-- End Trip-->
                                                 <div class="modal fade" tabindex="-1" role="dialog" class="end_trip" id="end_trip<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">End Trip</h5>
                                                                <form id ="ecoEndTrip<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Collection Address</label>
                                                                                <input type="text" class="form-control"  value="<?= $booking['vcollectionsRequestAddress'] ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                     
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Booking Number</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['ibookingId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Customer</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['iuserId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Driver Name</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['name']." ".json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['surname']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Driver Phone</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['phoneNo']   ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                     
                                                                        <input type="hidden" class="form-control" name ="ibookingId" value="<?= $booking['ibookingId'] ?>" >      
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="end_trip">
                                                                                 <button type="button" class="btn btn-primary" name ="end_trip" onClick="endTrip('<?= $booking["ibookingId"] ?>')">Confirm</button>
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                <!-- Customer Details-->
                                                 <div class="modal fade" tabindex="-1" role="dialog" class="customer" id="customer<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Customer Details</h5>
                                                                <form id ="ecoAssignDriver<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Name</label>
                                         

                                                                                <input type="text" class="form-control" 
                                                                                 value="   <?php 
                                                                            if(json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['partnerType']== "Corporate"){
                                                                                echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['companyName'];
                                                                            }else {
                                                                            echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['vFirstName']." ".json_decode(getElogisticsCustomersById($quotation['iuserId']),true)['eobjResponse']['vLastName'];
                                                                            }
                                                                            ?>"
                                                                                 
                                                                            readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Phone</label>
                                                

                                                                                <input type="text" class="form-control" 
                                                                                 value="   <?php 
                                                                            if(json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['partnerType']== "Corporate"){
                                                                                echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['vPhone'];
                                                                            }else {
                                                                            echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['vPhone'];
                                                                            }
                                                                            ?>"
                                                                                 
                                                                            readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Email</label>
                                              

                                                                                <input type="text" class="form-control" 
                                                                                 value="   <?php 
                                                                            if(json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['partnerType']== "Corporate"){
                                                                                echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['VEmail'];
                                                                            }else {
                                                                            echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['VEmail'];
                                                                            }
                                                                            ?>"
                                                                                 
                                                                            readonly >
                                                                            </div>
                                                                        </div>
                                          
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                             
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                <!-- Driver Details-->
                                                <div class="modal fade" tabindex="-1" role="dialog" class="driver" id="driver<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Driver Details</h5>
                                                                <form id ="ecoAssignDriver<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">First Name</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['name']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Last Name</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['surname']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Phone Number</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['phoneNo']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Email</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['email']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                          
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                             
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->

                                                    <?php endforeach; ?>

                                                    <?php else : ?>

                                                    <?= $bookings['responseMessage'] ?>
                 
                                                    <?php endif; ?>

                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
        <!-- Success Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successCancelAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully approved booking cancellation</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failCancelAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='approveCancelBookingResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully started trip</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='tripResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Success Modal Alert -->
      <div class="modal fade" tabindex="-1" id="successEndTripAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully ended trip</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEndTripAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='endTripResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
          <!-- Success Quotation Modal Alert -->
          <div class="modal fade" tabindex="-1" id="successQuotationAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully generated quotation</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Quotation Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failQuotationAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='quotationResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Success Re-Assign Franchise Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successReAssignFranchiseAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully re-assigned franchise</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Re-Assign Franchise Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failReAssignFranchiseAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='reAssignFranchiseResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
   <script>
       $( document ).ready(function() {
	$(".export").click(function() {
		var export_type = $(this).data('export-type');		
		$('#example').tableExport({
			type : export_type,			
			escape : 'false',
			ignoreColumn: []
		});		
	});
});
   </script>
     <!-- <script>
    $(document).ready(function() {
      var table = $('#admin_bookings').DataTable();
      table.column(5).data().search('PENDING').draw();

    });
  </script> -->

  <script>
    //Re-Assign Franchise
    function reAssignFranchise(ibookingId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#assignfranchise'+ibookingId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#re_assign_franchise'+ibookingId).modal('hide');
                    $("#successReAssignFranchiseAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/bookings";
                    }, 2000);
                  
                } else {
                    $('.reAssignFranchiseResponse').empty();
                    $('.reAssignFranchiseResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('#re_assign_franchise'+ibookingId).modal('hide');
                    $("#failReAssignFranchiseAlert").modal('show');
                    $("#failReAssignFranchiseAlert").on("hidden.bs.modal", function() {
                        $(".quotationResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.reAssignFranchiseResponse').empty();
                $('.reAssignFranchiseResponse').append(errorThrown);
                $('#spinner').hide();
                $('#re_assign_franchise'+ibookingId).modal('hide');
                $("#failReAssignFranchiseAlert").modal('show');
                $("#failReAssignFranchiseAlert").on("hidden.bs.modal", function() {
                    $(".reAssignFranchiseResponse").html("");
                });
            }
        });
    }
    </script>

  <script>
    //Quotation
    function createQuotation(ibookingId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#ecoQuotation'+ibookingId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('.quotation').modal('hide');
                    $("#successQuotationAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/bookings";
                    }, 2000);
                  
                } else {
                    $('.quotationResponse').empty();
                    $('.quotationResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('.quotation').modal('hide');
                    $("#failQuotationAlert").modal('show');
                    $("#failQuotationAlert").on("hidden.bs.modal", function() {
                        $(".quotationResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.quotationResponse').empty();
                $('.quotationResponse').append(errorThrown);
                $('#spinner').hide();
                $('.quotation').modal('hide');
                $("#failQuotationAlert").modal('show');
                $("#failQuotationAlert").on("hidden.bs.modal", function() {
                    $(".quotationResponse").html("");
                });
            }
        });
    }
    </script>

     
<script>
          
          //Approve Driver Booking Cancellation 
          function approveCancelBooking(ibookingId) {
              $('#spinner').show();
              $.ajax({
                  type: "POST",
                  url: "portal/admin/controller/process.php",
                  data: $('form#approveCancel'+ibookingId).serialize(),
                  cache: false,
                  success: function(response) {
                      var json = $.parseJSON(response);
                      if (json.responseStatus == "SUCCESS") {
                          $('#spinner').hide();
                          $('.approve_cancel').modal('hide');
                          $("#successCancelAlert").modal('show');
                          setTimeout(function() {
                              window.location = "portal/admin/bookings";
                          }, 2000);
                        
                      } else {
                          $('.approveCancelBookingResponse').empty();
                          $('.approveCancelBookingResponse').append(json.responseMessage);
                          $('#spinner').hide();
                          $('.approve_cancel').modal('hide');
                          $("#failCancelAlert").modal('show');
                          $("#failCancelAlert").on("hidden.bs.modal", function() {
                              $(".approveCancelBookingResponse").html("");
                          });
                      }
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                      $('.approveCancelBookingResponse').empty();
                      $('.approveCancelBookingResponse').append(errorThrown);
                      $('#spinner').hide();
                      $('.approve_cancel').modal('hide');
                      $("#failCancelAlert").modal('show');
                      $("#failCancelAlert").on("hidden.bs.modal", function() {
                          $(".approveCancelBookingResponse").html("");
                      });
                  }
              });
          }
          </script>
    
      <script>
          
    //Start Trip
    function startTrip(ibookingId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#ecoStartTrip'+ibookingId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('.start_trip').modal('hide');
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/bookings";
                    }, 2000);
                  
                } else {
                    $('.tripResponse').empty();
                    $('.tripResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('.start_trip').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".tripResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.tripResponse').empty();
                $('.tripResponse').append(errorThrown);
                $('#spinner').hide();
                $('.start_trip').modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".tripResponse").html("");
                });
            }
        });
    }
    </script>
       <script>
    //End Trip
    function endTrip(ibookingId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#ecoEndTrip'+ibookingId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('.end_trip').modal('hide');
                    $("#successEndTripAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/bookings";
                    }, 2000);
                  
                } else {
                    $('.endTripResponse').empty();
                    $('.endTripResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('.end_trip').modal('hide');
                    $("#failEndTripAlert").modal('show');
                    $("#failEndTripAlert").on("hidden.bs.modal", function() {
                        $(".endTripResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.endTripResponse').empty();
                $('.endTripResponse').append(errorThrown);
                $('#spinner').hide();
                $('.end_trip').modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".endTripResponse").html("");
                });
            }
        });
    }
    </script>
</body>

</html>