<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
$admin_id =  $_SESSION["admin_id"];
$admin_phone  =$_SESSION["admin_vphone"];
$admin_email =  $_SESSION["admin_Email"];
$admin_firstname =  $_SESSION["first_name"];
$admin_lastname = $_SESSION["last_name"];

include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
$ibookingId = $_GET['ibookingId'];
$payment_methods = json_decode(getPaymentMethods(), true);
$currencies = json_decode(getCurrencies(), true);
$usd_amount = json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['fprice'];
$zwl_amount = json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['zwlprice'];
$previous_code = json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['currencyCode'];

if($previous_code == "USD"){
$usd_amount = json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['fprice'];
$onload_amount = $usd_amount;
}else{
$zwl_amount = json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['zwlprice'];
$onload_amount = $zwl_amount;
}


// echo $previous_code;
// exit;
// $currency = json_decode(getCurrencyById(277), true);

// $details = json_decode(getQuotationByBookingId($ibookingId),true);
// $details = json_decode(getBookingByBookingId($ibookingId),true);
// var_dump($previous_code);
// exit;

?>
 
<body class="nk-body bg-lighter npc-general has-sidebar " onload="onloadCurrencyCode()" >
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="html/index.html" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                   
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                          
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-name dropdown-indicator"><?php echo $vfirstName;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $vfirstName;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="html/user-profile-regular.html"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/user-profile-setting.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/user-profile-activity.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/customer/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                               
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head">
                                    <div class="nk-block-between g-3">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Quotation : <strong class="text-primary small"><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?></strong></h3>
                                            <div class="nk-block-des text-soft">
                                                <ul class="list-inline">
                                                <li>Created At: <span class="text-base"><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['requestedDate'] ?></span></li>      </ul>
                                            </div>
                                        </div>
                                        <div class="nk-block-head-content">
                                            <a href="portal/customer/bookings" class="btn btn-outline-light bg-white d-none d-sm-inline-flex"><em class="icon ni ni-arrow-left"></em><span>Back</span></a>
                                                     
                                                    <select class="form-select-sm" id="my-select"  name ="currencyCode" required>
                                                        <option value="ZWL" <?php if($previous_code == "ZWL") echo "SELECTED";?> > ZWL </option>
                                                        <option value="USD" <?php if($previous_code == "USD") echo "SELECTED";?> > USD </option>
                                                   
                                                    </select>
                                                  <button type="button" <?php if($previous_code == "USD" || $previous_code == "") echo 'style="display:none"';?> data-toggle="modal" data-target="#zwl_payment" class="btn btn-dim btn-outline-primary" id ="zwl_btn"><span id ="currency_code_pay_zwl">Pay</span></button>
                                                  <button type="button"  <?php if($previous_code == "ZWL" || $previous_code == "") echo 'style="display:none"';?> data-toggle="modal"  data-target="#usd_payment" class="btn btn-dim btn-outline-primary" id ="usd_btn"><span id ="currency_code_pay_usd">Pay</span</button>
                                               
                                             </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="invoice">
                                        <div class="invoice-action">
                                        <button id="myBtn" > <a  class="btn btn-icon btn-lg btn-white btn-dim btn-outline-primary" ><em class="icon ni ni-printer-fill"></em></a></button> 
                                        </div><!-- .invoice-actions -->
                                        <div class="invoice-wrap">
                                            <div class="invoice-brand text-center">
                                                <img src="./images/logo-dark.png" srcset="./images/vaya_logo.png 1x" alt="logo">
                                            </div>
                                            <div class="invoice-head">
                                                <div class="invoice-contact">
                                                    <span class="overline-title">Quotation To</span>
                                                    <div class="invoice-contact-info">
                                                        <h4 class="title"><?php echo $vfirstName." ".$vlastName; ?></h4>
                                                        <ul class="list-plain">
                                                            <li><em class="icon ni ni-map-pin-fill"></em><span><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['vcollectionsRequestAddress'] ?></span></li>
                                                            <li><em class="icon ni ni-call-fill"></em><span><?php echo $customer_vphone; ?></span></li>
                                                        </ul>
                                                    </div>
                                                </div>
                                                <div class="invoice-desc">
                                                    <h3 class="title">Quotation</h3>
                                                    <ul class="list-plain">
                                                        <li class="invoice-id"><span>Quotation ID</span>:<span><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?></span></li>
                                                        <li class="invoice-date"><span>Date</span>:<span><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['requestedDate'] ?></span></li>
                                                    </ul>
                                                </div>
                                            </div><!-- .invoice-head -->
                                            <div class="invoice-bills">
                                                <div class="table-responsive">
                                                    <table class="table table-striped">
                                                        <thead>
                                                            <tr>
                                                            <th class="w-150px">Booking Number</th>
                                                                <th class="w-65">Waste Type</th>
                                                                <th>Recurrence</th>
                                                                <th>Qty</th>
                                                                <th>Amount</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                        <tr>
                                                        <td><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?></td>
                                                        <td><?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['wasteType'] ?></td>
                                                        <td>
                                                        <?php
                                                        if(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['recurrence'] == "OneMonth"){
                                                        echo "1 Month";
                                                        }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['recurrence'] == "TwoMonths") {
                                                            echo "2 Months";
                                                         }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['recurrence'] == "ThreeMonths") {
                                                            echo "3 Months";
                                                        }else {
                                                            echo "Once Off";
                                                        }
                                                        ?>
                                                       </td>
                                                       <td>
                                                        <?php
                                                        if(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "OneToTwo"){
                                                        echo "1-2T";
                                                        }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "Three") {
                                                            echo "3T";
                                                         }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "FourToSix") {
                                                            echo "4-6T";
                                                        }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "Seven") {
                                                            echo "7T";
                                                        }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == "TenToTwelve") {
                                                            echo "10-12T";
                                                        }elseif(json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity'] == 1) {
                                                            echo json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity']."BAG";
                                                        }else {
                                                            echo json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iquantity']."BAGS";
                                                        }
                                                        ?>
                                                       </td>                                                            
                                                        <td>
                                                        <span id ="currency_code"></span>
                                                        <span id ="onload_currency_code"><?php echo $previous_code."".$onload_amount ?></span>
                                                        </td>
                                                        </tr>
                                                        </tbody>
                                                        <tfoot>
                                                   
                                                        <tr>
                                                            <td colspan="2"></td>
                                                            <td colspan="2">Grand Total</td>
                                                            <td>
                                                            <span id ="currency_code_grand"></span>
                                                            <span id ="onload_currency_code_grand"><?php echo $previous_code."".$onload_amount ?></span>
                                                        </td>
                                                        </tr>
                                                        </tfoot>
                                                    </table>
                                                    <div class="nk-notes ff-italic fs-12px text-soft"> Quotation was generated by the system and is valid without the signature and seal. </div>
                                                </div>
                                            </div><!-- .invoice-bills -->
                                        </div><!-- .invoice-wrap -->
                                    </div><!-- .invoice -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                        <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                            <div class="nk-footer-links">
                          
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
  <!-- Add ZWL Payment Method-->
  <div class="modal fade" tabindex="-1" role="dialog" id="zwl_payment">
<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
    <div class="modal-body modal-body-md">
        <h5 class="modal-title">Select ZWL Payment Method</h5>
                                     
    <ul class="nav nav-tabs"> 
    <li class="nav-item"> 
    <a class="nav-link active" data-toggle="tab" href="#tabItem1"><em class="icon ni ni-invest"></em><span>ECOCASH</span></a> 
   </li>  
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem2"><em class="icon ni ni-coin"></em><span>CASH</span></a> 
  </li>    
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem3"><em class="icon ni ni-wallet-alt"></em><span>Proof Of Payment</span></a> 
  </li>   
  <!-- <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem4"><em class="icon ni ni-repeat"></em><span>INTERNAL TRANSFER</span></a> 
  </li>  
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem9"><em class="icon ni ni-tranx"></em><span>RTGS</span></a> 
  </li>   -->
  </ul>

  <!-- ECOCASH ZWL PAYMENT -->
  <div class="tab-content">    
  <div class="tab-pane active" id="tabItem1">
  <form id = "ecoPayment" action="#" class="mt-2" class="form-validate">
        <div class="row g-4">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="phone-no-1">Mobile Number</label>
                    <div class="form-control-wrap">
                        <input type="number" name="ecocashNumber" class="form-control required" width="100" placeholder="e.g. 07XX XXX XXX" id="phone-no-1" required>
                    </div>
                </div>
            </div>
            <input type="hidden" name="zwlprice" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['zwlprice'] ?>">
            <input type="hidden" name="paidBy" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iuserId'] ?>"  required>
            <input type="hidden" name="bookingId" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?>" required>
            <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                <input type="hidden" class="form-control" name="eco_payment">
                 <button type="button" class="btn btn-primary" name ="eco_payment" onClick="ecoPayment()">Pay</button>
                </li>
                <!-- <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li> -->
            </ul>
        </div>
        </div>
    </form><!-- form -->
  </div>    
  <!-- CASH ZWL PAYMENT -->
  <div class="tab-pane" id="tabItem2"> 
  <form id = "userCashPayment" action="#" class="mt-2">
        <div class="row g-4">
    <!-- <div class="form-group">
            <div class="custom-control custom-control-xs custom-checkbox">
                <input type="checkbox" class="custom-control-input required"  id="checkbox"  name ="checkbox" required >
                <label class="custom-control-label" for="checkbox">I confirm to pay using cash</label>
            </div>
           
        </div> -->

        <input type="hidden" name="zwlprice" class="form-control"  value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['zwlprice'] ?>">
        <input type="hidden" name="paidBy" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iuserId'] ?>"  required>
        <input type="hidden" name="bookingId" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?>" required>
    
        <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                <input type="hidden" class="form-control" name="cash_payment">
                 <button type="button" class="btn btn-primary"  name ="cash_payment" onClick="userCashPayment()">Pay</button>
                </li>
                <!-- <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li> -->
            </ul>
        </div>
        </div>
    </form><!-- form --> 
  </div> 
    <!-- PROOF OF PAYMENT ZWL-->
    <div class="tab-pane" id="tabItem3"> 
    <form id = "userproof" action="#" class="mt-2"  method ="POST"  class="form-validate" enctype="multipart/form-data">
    <div class="example-alert">
    <div class="alert alert-primary alert-icon">
        <em class="icon ni ni-alert-circle"></em> <strong>Upload Images Only.</strong>
    </div>
</div>
        <div class="row g-4">
        <div class="form-group">
        <label class="form-label" for="default-06">Upload Proof Of Payment</label>
        <div class="form-control-wrap">
            <div class="custom-file">
            <input type="file" multiple class="custom-file-input required" name="myfile"  id="customFile" data-accepted-files="image/*" required>
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
    </div>
      <input type="hidden" name="reference" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iuserId'] ?>"  required>
      <input type="hidden" name="bookingId" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?>" required>
        <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                <input type="hidden" class="form-control" name="proof_of_payment">
                 <button type="button" class="btn btn-primary"  name ="proof_of_payment" onClick="userProof()">Upload</button>
                </li>
                <!-- <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li> -->
            </ul>
        </div>
        </div>
    </form><!-- form --> 
  </div> 
  </div>
 </div>
</div>
</div>
</div>



  <!-- Add USD Payment Method-->
  <div class="modal fade" tabindex="-1" role="dialog" id="usd_payment">
<div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
    <div class="modal-body modal-body-md">
        <h5 class="modal-title">Select USD Payment Method</h5>
                                     
    <ul class="nav nav-tabs"> 
    <!-- <li class="nav-item"> 
    <a class="nav-link active" data-toggle="tab" href="#tabItem4"><em class="icon ni ni-invest"></em><span>ECOCASH</span></a> 
   </li>   -->
  <li class="nav-item">
  <a class="nav-link active" data-toggle="tab" href="#tabItem5"><em class="icon ni ni-coin"></em><span>CASH</span></a> 
  </li>    
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem6"><em class="icon ni ni-wallet-alt"></em><span>Proof Of Payment</span></a> 
  </li>   
  <!-- <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem4"><em class="icon ni ni-repeat"></em><span>INTERNAL TRANSFER</span></a> 
  </li>  
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem9"><em class="icon ni ni-tranx"></em><span>RTGS</span></a> 
  </li>   -->
  </ul>

  <!-- ECOCASH USD PAYMENT -->
  <div class="tab-content">    
  <div class="tab-pane" id="tabItem4">
  <form id = "ecoUSDPayment" action="#" class="mt-2" class="form-validate">
        <div class="row g-4">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="phone-no-1">Mobile Number</label>
                    <div class="form-control-wrap">
                        <input type="number" name="ecocashNumber" class="form-control required" width="100" placeholder="e.g. 07XX XXX XXX" id="phone-no-1" required>
                    </div>
                </div>
            </div>
            <input type="hidden" name="fprice" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['fprice'] ?>">
            <input type="hidden" name="paidBy" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iuserId'] ?>"  required>
            <input type="hidden" name="bookingId" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?>" required>
            <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                <input type="hidden" class="form-control" name="eco_usd_payment">
                 <button type="button" class="btn btn-primary" name ="eco_usd_payment" onClick="ecoUSDPayment()">Pay</button>
                </li>
              
            </ul>
        </div>
        </div>
    </form>
  </div>    
  <!-- CASH USD PAYMENT -->
  <div class="tab-pane active"  id="tabItem5"> 
  <form id = "userCashUSDPayment" action="#" class="mt-2">
        <div class="row g-4">

            <!-- <div class="form-group">
            <div class="form-control-wrap">
            <div class="custom-control custom-control-xs custom-checkbox">
                <input type="checkbox" class="custom-control-input"  id="checkbox" required >
                <label class="custom-control-label" for="checkbox">I confirm to pay using cash</label>
            </div>
            </div>
        </div> -->
        <input type="hidden" name="fprice" class="form-control"  value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['fprice'] ?>">
        <input type="hidden" name="paidBy" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iuserId'] ?>"  required>
        <input type="hidden" name="bookingId" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?>" required>
    
        <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                <input type="hidden" class="form-control" name="cash_usd_payment">
                 <button type="button" class="btn btn-primary"  name ="cash_usd_payment" onClick="userCashUSDPayment()">Pay</button>
                </li>
                <!-- <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li> -->
            </ul>
        </div>
        </div>
    </form><!-- form --> 
  </div> 
    <!-- PROOF OF PAYMENT USD  -->
    <div class="tab-pane" id="tabItem6"> 
    <form id = "userUSDproof" action="#" class="mt-2"  method ="POST" enctype="multipart/form-data" class="form-validate">
    <div class="example-alert">
    <div class="alert alert-primary alert-icon">
        <em class="icon ni ni-alert-circle"></em> <strong>Upload Images Only.</strong>
    </div>
</div>
        <div class="row g-4">
        <div class="form-group">
        <label class="form-label" for="default-06">Upload Proof Of Payment</label>
        <div class="form-control-wrap">
            <div class="custom-file">
            <input type="file" multiple class="custom-file-input required" name="myfile" id="customFile" data-accepted-files="image/*" required>
                <label class="custom-file-label" for="customFile">Choose file</label>
            </div>
        </div>
    </div>
      <input type="hidden" name="reference" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['iuserId'] ?>"  required>
      <input type="hidden" name="bookingId" class="form-control" value="<?= json_decode(getBookingByBookingId($ibookingId),true)['eobjResponse']['ibookingId'] ?>" required>
        <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                <input type="hidden" class="form-control" name="proof_of_payment">
                 <button type="button" class="btn btn-primary"  name ="proof_of_payment" onClick="userUSDProof()">Upload</button>
                </li>
                <!-- <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li> -->
            </ul>
        </div>
        </div>
    </form><!-- form --> 
  </div> 
  </div>
 </div>
</div>
</div>
</div>



       <!--EcoCash ZWL Success Modal Alert -->
       <div class="modal fade" tabindex="-1" id="successEcoPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">EcoCash payment has been initiated successfully</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
          <!--EcoCash USD Success Modal Alert -->
          <div class="modal fade" tabindex="-1" id="successEcoUSDPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">EcoCash payment has been initiated successfully</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail EcoCash Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEcoPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='ecoPaymentResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
       <!--Fail EcoCash USD Modal Alert -->
       <div class="modal fade" tabindex="-1" id="failEcoUSDPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='ecoUSDPaymentResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
           <!--Cash Payment ZWL Success Modal Alert -->
           <div class="modal fade" tabindex="-1" id="successCashPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">Your cash payment is awaiting confirmation and approval</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
       <!--Cash Payment USD Success Modal Alert -->
       <div class="modal fade" tabindex="-1" id="successCashUSDPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">Your cash payment is awaiting confirmation and approval</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail Cash Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failCashPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='cashPaymentResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail Cash USD Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failCashUSDPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='cashUSDPaymentResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
          <!--Proof Of  Payment  Success Modal Alert -->
          <div class="modal fade" tabindex="-1" id="successProofPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">Proof Of Payment has been uploaded successfully</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail Proof Of Payment  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failproofPaymentAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='proofPaymentResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        function onloadCurrencyCode() {
            localStorage.clear();
        }
    </script>
    <script>
document.getElementById("myBtn").addEventListener("click", myFunction);

function myFunction() {
       // Get saved data from sessionStorage
       var storedCurrencyCode = sessionStorage.getItem('setCurrencyCode');
        if(storedCurrencyCode === null){
         var getCurrencyCode = "<?php echo $previous_code; ?>";
         var getibookingId = <?php echo $ibookingId; ?>;
        window.location = "portal/admin/quotation-print?ibookingId=" + getibookingId + "&CurrencyCode=" + getCurrencyCode;
        }else {
            var getCurrencyCode = storedCurrencyCode;
            var getibookingId = <?php echo $ibookingId; ?>;
            window.location = "portal/admin/quotation-print?ibookingId=" + getibookingId + "&CurrencyCode=" + getCurrencyCode;
        }
        
}
</script>
    <script>
        document.getElementById('my-select').addEventListener('change', function() {
            var currency_code_value = this.value;
            // Save data to sessionStorage
            sessionStorage.setItem('setCurrencyCode', currency_code_value);
            var usd_amount = <?php echo $usd_amount; ?>;
            var zwl_amount = <?php echo $zwl_amount; ?>;
            if (currency_code_value == "USD") {
                document.getElementById("currency_code").innerHTML = "USD" + usd_amount;
                document.getElementById("currency_code_grand").innerHTML = "USD" + usd_amount;
                document.getElementById("currency_code_pay_usd").innerHTML = "Pay";
                document.getElementById("currency_code_pay_zwl").innerHTML = "";
                document.getElementById('usd_btn').style.display = 'inline-block';
                document.getElementById('zwl_btn').style.display = 'none';
                document.getElementById("onload_currency_code").innerHTML = "";
                document.getElementById("onload_currency_code_grand").innerHTML = "";
       

            } else if (currency_code_value == "") {
                document.getElementById("currency_code").innerHTML = "";
                document.getElementById("currency_code_grand").innerHTML = "";
            } else {
                document.getElementById("currency_code").innerHTML = "ZWL" + zwl_amount;
                document.getElementById("currency_code_grand").innerHTML = "ZWL" + zwl_amount;
                document.getElementById("currency_code_pay_zwl").innerHTML = "Pay";
                document.getElementById("currency_code_pay_usd").innerHTML = "";
                document.getElementById('zwl_btn').style.display = 'inline-block';
                 document.getElementById('usd_btn').style.display = 'none';
                 document.getElementById("onload_currency_code").innerHTML = "";
                document.getElementById("onload_currency_code_grand").innerHTML = "";
    
            
            }
                    
});
    </script>
    <!-- BEGIN: AJAX CALLS-->
   <script>
    //eco ZWL Payment
    function ecoPayment() {
        if ($("#ecoPayment").valid()) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#ecoPayment').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#zwl_payment').modal('hide');
                    $("#successEcoPaymentAlert").modal('show');
                    // setTimeout(function() {
                    //     window.location = "portal/customer/bookings";
                    // }, 2000);
                } else {
                    $('.ecoPaymentResponse').empty();
                    $('.ecoPaymentResponse').html(json.responseMessage);
                    $('#spinner').hide();
                    $('#zwl_payment').modal('hide');
                    $("#failEcoPaymentAlert").modal('show');
                    $("#failEcoPaymentAlert").on("hidden.bs.modal", function() {
                        $(".ecoPaymentResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.ecoPaymentResponse').empty();
                $('.ecoPaymentResponse').append(errorThrown);
                $('#spinner').hide();
                $('#zwl_payment').modal('hide');
                $("#failEcoPaymentAlert").modal('show');
                $("#failEcoPaymentAlert").on("hidden.bs.modal", function() {
                    $(".ecoPaymentResponse").html("");
                });
            }
        });
    }
    }
    </script>
      <script>
    //eco USD Payment
    function ecoUSDPayment() {
        if ($("#ecoUSDPayment").valid()) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#ecoUSDPayment').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#usd_payment').modal('hide');
                    $("#successEcoUSDPaymentAlert").modal('show');
                    // setTimeout(function() {
                    //     window.location = "portal/customer/bookings";
                    // }, 2000);
                } else {
                    $('.ecoUSDPaymentResponse').empty();
                    $('.ecoUSDPaymentResponse').html(json.responseMessage);
                    $('#spinner').hide();
                    $('#usd_payment').modal('hide');
                    $("#failEcoUSDPaymentAlert").modal('show');
                    $("#failEcoUSDPaymentAlert").on("hidden.bs.modal", function() {
                        $(".ecoUSDPaymentResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.ecoUSDPaymentResponse').empty();
                $('.ecoUSDPaymentResponse').append(errorThrown);
                $('#spinner').hide();
                $('#usd_payment').modal('hide');
                $("#failEcoUSDPaymentAlert").modal('show');
                $("#failEcoUSDPaymentAlert").on("hidden.bs.modal", function() {
                    $(".ecoUSDPaymentResponse").html("");
                });
            }
        });
    }
    }
    </script>
       <script>
    //cash ZWL Payment
    function userCashPayment() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#userCashPayment').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#zwl_payment').modal('hide');
                    $("#successCashPaymentAlert").modal('show');
                    // setTimeout(function() {
                    //     window.location = "portal/customer/bookings";
                    // }, 2000);
                } else {
                    $('.cashPaymentResponse').empty();
                    $('.cashPaymentResponse').html(json.responseMessage);
                    $('#spinner').hide();
                    $('#zwl_payment').modal('hide');
                    $("#failCashPaymentAlert").modal('show');
                    $("#failCashPaymentAlert").on("hidden.bs.modal", function() {
                        $(".cashPaymentResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.cashPaymentResponse').empty();
                $('.cashPaymentResponse').append(errorThrown);
                $('#spinner').hide();
                $('#zwl_payment').modal('hide');
                $("#failCashPaymentAlert").modal('show');
                $("#failCashPaymentAlert").on("hidden.bs.modal", function() {
                    $(".cashPaymentResponse").html("");
                });
            }
        });
    }
    </script>
        <script>
    //cash USD Payment
    function userCashUSDPayment() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#userCashUSDPayment').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#usd_payment').modal('hide');
                    $("#successCashUSDPaymentAlert").modal('show');
                    // setTimeout(function() {
                    //     window.location = "portal/customer/bookings";
                    // }, 2000);
                } else {
                    $('.cashUSDPaymentResponse').empty();
                    $('.cashUSDPaymentResponse').html(json.responseMessage);
                    $('#spinner').hide();
                    $('#usd_payment').modal('hide');
                    $("#failCashUSDPaymentAlert").modal('show');
                    $("#failCashUSDPaymentAlert").on("hidden.bs.modal", function() {
                        $(".cashUSDPaymentResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.cashUSDPaymentResponse').empty();
                $('.cashUSDPaymentResponse').append(errorThrown);
                $('#spinner').hide();
                $('#usd_payment').modal('hide');
                $("#failCashUSDPaymentAlert").modal('show');
                $("#failCashUSDPaymentAlert").on("hidden.bs.modal", function() {
                    $(".cashUSDPaymentResponse").html("");
                });
            }
        });
    }
    </script>


<script>
    //Upload Proof Of  Payment ZWL
    function userProof() {
        if ($("#userproof").valid()) {
        var form = $('#userproof')[0];
		// Create an FormData object 
        var data = new FormData(form);

        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: data,
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#zwl_payment').modal('hide');
                    $("#successProofPaymentAlert").modal('show');
                    // setTimeout(function() {
                    //     window.location = "portal/customer/bookings";
                    // }, 2000);
                } else {
                    $('.proofPaymentResponse').empty();
                    $('.proofPaymentResponse').html(json.responseMessage);
                    $('#spinner').hide();
                    $('#zwl_payment').modal('hide');
                    $("#failproofPaymentAlert").modal('show');
                    $("#failproofPaymentAlert").on("hidden.bs.modal", function() {
                        $(".proofPaymentResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.proofPaymentResponse').empty();
                $('.proofPaymentResponse').append(errorThrown);
                $('#spinner').hide();
                $('#zwl_payment').modal('hide');
                $("#failproofPaymentAlert").modal('show');
                $("#failproofPaymentAlert").on("hidden.bs.modal", function() {
                    $(".proofPaymentResponse").html("");
                });
            }
        });
    }
    }
    </script>

<script>
    //Upload Proof Of  Payment USD
    function userUSDProof() {
        if ($("#userUSDproof").valid()) {
        var form = $('#userUSDproof')[0];
		// Create an FormData object 
        var data = new FormData(form);
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: data,
            processData: false,
            contentType: false,
            enctype: 'multipart/form-data',
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#usd_payment').modal('hide');
                    $("#successProofPaymentAlert").modal('show');
                    // setTimeout(function() {
                    //     window.location = "portal/customer/bookings";
                    // }, 2000);
                } else {
                    $('.proofPaymentResponse').empty();
                    $('.proofPaymentResponse').html(json.responseMessage);
                    $('#spinner').hide();
                    $('#usd_payment').modal('hide');
                    $("#failproofPaymentAlert").modal('show');
                    $("#failproofPaymentAlert").on("hidden.bs.modal", function() {
                        $(".proofPaymentResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.proofPaymentResponse').empty();
                $('.proofPaymentResponse').append(errorThrown);
                $('#spinner').hide();
                $('#usd_payment').modal('hide');
                $("#failproofPaymentAlert").modal('show');
                $("#failproofPaymentAlert").on("hidden.bs.modal", function() {
                    $(".proofPaymentResponse").html("");
                });
            }
        });
    }
    }
    </script>




    
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>