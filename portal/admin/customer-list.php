<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
  $admin_id =  $_SESSION["admin_id"];
  $admin_phone  =$_SESSION["admin_vphone"];
  $admin_email =  $_SESSION["admin_Email"];
  $admin_firstname =  $_SESSION["first_name"];
  $admin_lastname = $_SESSION["last_name"];
require_once('includes/header.php');
include_once('../../utils/VayaCleanCityUtility.php');
// $bookings = json_decode(getBookings(), true);
// $currencies = json_decode(getCurrencies(), true);
// $customers = json_decode(getAllLogisticsViaCleanCityCustomers(),true);  
// $customers = json_decode(getElogisticsCustomers(),true);
$customers = json_decode(getCleanCityCustomers(),true);

// $franchises = json_decode(getAllPartners(), true);
// var_dump($customers);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                      
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Customers List</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                    <li><a href="#" data-toggle="modal" data-target="#add-customer"><span>Add Customer</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                          <th class="nk-tb-col"><span class="sub-text">Customer Type</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Name</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Email</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Phone Number</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Address</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($customers['responseStatus'] == "SUCCESS") : ?>
                                                    <?php
                                                     foreach ($customers['eobjResponse'] as $customer) :
                                                        // var_dump($customer);
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $customer['partnerType'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($customer['partnerType'] == "Individual"){
                                                        echo  $customer['name']." ".$customer['surname'];
                                                        }else {
                                                        echo $customer['companyName'];
                                                        }
                                                        ?>
                                                        </span>
                                                       </td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $customer['email'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $customer['phoneNo'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $customer['address'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $customer['status'] ?></span></td>
                                                
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                                            
                                                                <li>
                                                                    <div class="drodown">
                                                                        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <?php
                                                                              if($customer['partnerType'] == "Individual"){
                                                                                 echo"<li><a href='#' data-toggle='modal' data-target='#edit-customer".$customer['elogisticsUserId'] ."' ><em class='icon ni ni-edit'></em><span>Edit Customer</span></a></li>";
                                                                                 echo"<li><a href='#' data-toggle='modal' data-target='#delete-customer".$customer['elogisticsUserId'] ."' ><em class='icon ni ni-edit'></em><span>Delete Customer</span></a></li>";
                                                                              }else {                       
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#edit-cocustomer".$customer['elogisticsUserId'] ."' ><em class='icon ni ni-edit'></em><span>Edit Customer</span></a></li>";
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#view-documents".$customer['elogisticsUserId'] ."' ><em class='icon ni ni-edit'></em><span>View Documents</span></a></li>";
                                                                                echo"<li><a data-toggle='modal' data-target='#approve_customer".$customer['elogisticsUserId'] ."' ><em class='icon ni ni-edit'></em><span>Approve</span></a></li>";
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#delete-cocustomer".$customer['elogisticsUserId'] ."' ><em class='icon ni ni-edit'></em><span>Delete Customer</span></a></li>";
                                                                              }  
                                                                            ?>
                                                                              </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                <!-- Edit Individual Customer-->
                                                <div class="modal fade" tabindex="-1" role="dialog" id="edit-customer<?= $customer['elogisticsUserId'] ?>" >
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Edit Individual Customer</h5>
                                                                <form id ="edit-customer<?= $customer['elogisticsUserId'] ?>"  class="form-validate">
                                                                <div class="row g-gs">
                                                                <div class="col-md-6">
                                                                <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">First Name</label>
                                                                            <input type="text" class="form-control"  name ="vFirstName" value="<?= $customer['name'] ?>"  placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">Last Name</label>
                                                                            <input type="text" class="form-control"  name ="vLastName" value="<?= $customer['surname'] ?>" placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="form-control-wrap">
                                                                    <label class="form-label" for="payment-name-add">Current Email</label>
                                                                    <div class="input-group">
                                                                    <input type="email" class="form-control"  value="<?= $customer['email'] ?>" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" readonly>
                                                                        <div class="input-group-append">
                                                                        <button type="button" class="btn btn-outline-primary btn-dim" data-toggle='modal' data-target="#edit-email<?= $customer['elogisticsUserId'] ?>">Change</button>
                                                                        </div>
                                                                    </div>
                                                                   </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                        <label class="form-label" for="currency-add">Phone Number</label>
                                                                        <input type="tel" class="form-control"  name="vPhone"  value="<?= $customer['phoneNo'] ?>"  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" placeholder="" required>
                                                                    </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Address</label>
                                                                        <input type="tel" class="form-control"  name="address"  placeholder="" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Password</label>
                                                                        <input type="password" class="form-control"   id="password" name="vPassword" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  placeholder="">
                                                                    </div>
                                                                    </div> -->
                                                                    <input type="hidden" class="form-control"  name="customerId"  value="<?= $customer['elogisticsUserId'] ?>">
                                                                    <input type="hidden" class="form-control"  name="vEmail" value="<?= $customer['email'] ?>">
                 
                                                                    <!-- <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Confirm Password</label>
                                                                        <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
                                                                    </div>
                                                                    </div> -->
                                                                    <div class="col-10">
                                                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                            <li>
                                                                                <input type="hidden" class="form-control" name="edit_customer">
                                                                                <button type="button" class="btn btn-primary" name ="edit_customer" onClick="editCustomer('<?= $customer["elogisticsUserId"] ?>')">Edit</button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                       <!-- Edit Email-->
                                                <div class="modal fade" tabindex="-1" role="dialog" id="edit-email<?= $customer['elogisticsUserId']?>" >
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <!-- <h5 class="modal-title">Change Email</h5> -->
                                                                <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status">    <span class="sr-only">Loading...</span>  </div></div> -->
                                                                <form id ="edit-email<?= $customer['elogisticsUserId']?>"  class="form-validate">
                                                                    <div class="row g-gs">
                                                                        <input type="hidden" class="form-control"  name="oldEmail"  value="<?= $customer['email'] ?>" required>
                                                                        <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">Current Email</label>
                                                                            <input type="text" class="form-control"   value="<?= $customer['email'] ?>" readonly>
                                                                        </div>
                                                                    </div>
                                                                    
                                                                        <div class="form-control-wrap">
                                                                        <label class="form-label" for="payment-name-add">New Email</label>
                                                                        <div  style ="color: #1C7ACD; text-align: center;" class='emailCustomer'></div>
                                                                            <div class="input-group">
                                                                            <input type="text" class="form-control" name ="newEmail" placeholder="Enter New Email">
                                                                                <div class="input-group-append">
                                                                                <input type="hidden" class="form-control" name="edit_email">
                                                                                <button type="button" class="btn btn-primary" name ="edit_email" onClick="editEmail('<?= $customer["elogisticsUserId"] ?>')">Confirm</button>
                                                                        
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                    
                                                                    
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                                <span class="sub-text"></span>
                                                            </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                </div><!-- .modal -->
                                                <!--  Customer Documents-->
                                                <div class="modal fade" tabindex="-1" role="dialog" id="view-documents<?= $customer['elogisticsUserId'] ?>" >
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Corporate Customer Documents</h5>
                                                             <br/>
                                                                <form id ="edit-customer<?= $customer['elogisticsUserId'] ?>"  class="form-validate">
                                                                <div class="row g-gs">
                                                                <div class="col-md-6">
                                                                <label class="form-label" for="payment-name-add">National ID</label>
                                                                <div class="tb-odr-btns d-none d-sm-inline">
                                                                        <?php
                                                                        if($customer['nationId'] == ""){
                                                                            echo "UNAVAILABLE";
                                                                            }else {
                                                                           echo "<a target='_blank' href='https://vayaafrica.com/clean_city_java_backend/download/".$customer['nationId']."' class='btn btn-dim btn-sm btn-primary' title='National ID'>View</a>";
                                                                        }
                                                                        ?>
                                                                     </div>
                                                            
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <label class="form-label" for="payment-name-add">Proof Of Residence</label>
                                                                    <div class="tb-odr-btns d-none d-sm-inline">
                                                                        <?php
                                                                        if($customer['proofOfResidence'] == ""){
                                                                            echo "UNAVAILABLE";
                                                                            }else {
                                                                           echo "<a target='_blank' href='https://vayaafrica.com/clean_city_java_backend/download/".$customer['proofOfResidence']."' class='btn btn-dim btn-sm btn-primary' title='Proof Of Residence'>View</a>";
                                                                        }
                                                                        ?>
                                                                     </div>
                                                                  
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <label class="form-label" for="payment-name-add">Bank Statement</label>
                                                                    <div class="tb-odr-btns d-none d-sm-inline">
                                                                        <?php
                                                                        if($customer['bankStatement'] == ""){
                                                                            echo "UNAVAILABLE";
                                                                            }else {
                                                                           echo "<a target='_blank' href='https://vayaafrica.com/clean_city_java_backend/download/".$customer['bankStatement']."' class='btn btn-dim btn-sm btn-primary' title='Bank Statement'>View</a>";
                                                                        }
                                                                        ?>
                                                                     </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <label class="form-label" for="payment-name-add">Trading License</label>
                                                                    <div class="tb-odr-btns d-none d-sm-inline">
                                                                        <?php
                                                                        if($customer['tradeLicense'] == ""){
                                                                            echo "UNAVAILABLE";
                                                                            }else {
                                                                           echo "<a target='_blank' href='https://vayaafrica.com/clean_city_java_backend/download/".$customer['tradeLicense']."' class='btn btn-dim btn-sm btn-primary' title='Trading License'>View</a>";
                                                                        }
                                                                        ?>
                                                                     </div>
                                                                    </div>
                                                                
                                                                </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                <!-- Edit Co Customer-->
                                                <div class="modal fade" tabindex="-1" role="dialog" id="edit-cocustomer<?= $customer['elogisticsUserId'] ?>" >
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Edit Corporate Customer</h5>
                                                                <form id ="edit-cocustomer<?= $customer['elogisticsUserId'] ?>"  class="form-validate">
                                                                <div class="row g-gs">
                                                                <div class="col-md-6">
                                                                <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">Trading Name</label>
                                                                            <input type="text" class="form-control"  name ="name" value="<?= $customer['companyName'] ?>"  placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                 
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                            <label class="form-label" for="currency-add">Current Email</label>
                                                                            <input type="email" class="form-control"  name="vEmail" value="<?= $customer['email'] ?>" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  placeholder="">
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                        <label class="form-label" for="currency-add">Phone Number</label>
                                                                        <input type="tel" class="form-control"  name="vPhone"  value="<?= $customer['phoneNo'] ?>"  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" placeholder="" required>
                                                                    </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Address</label>
                                                                        <input type="tel" class="form-control"  name="vCaddress" value="<?= $customer['address'] ?>"   placeholder="" required>
                                                                    </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Zimra BP Number</label>
                                                                        <input type="tel" class="form-control"  name="zimraBPNumber" value="<?= $customer['zimraBPNumber'] ?>"   placeholder="" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Password</label>
                                                                        <input type="password" class="form-control"   id="password" name="vPassword" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  placeholder="">
                                                                    </div>
                                                                    </div> -->
                                                                    <input type="hidden" class="form-control"  name="customerId"  value="<?= $customer['elogisticsUserId'] ?>">
                                                                    <!-- <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Confirm Password</label>
                                                                        <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
                                                                    </div>
                                                                    </div> -->
                                                                    <div class="col-10">
                                                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                            <li>
                                                                                <input type="hidden" class="form-control" name="edit_cocustomer">
                                                                                <button type="button" class="btn btn-primary" name ="edit_cocustomer" onClick="editCoCustomer('<?= $customer["elogisticsUserId"] ?>')">Edit</button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->

                                                <!-- Approve Customer-->
                                                <div class="modal fade" tabindex="-1" role="dialog" id="approve_customer<?= $customer['elogisticsUserId'] ?>" >
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Approve Customer</h5>
                                                                <form id ="approve_customer<?= $customer['elogisticsUserId'] ?>"  class="form-validate">
                                                                <div class="row g-gs">
                                                                <div class="col-md-6">
                                                                <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">Tranding Name</label>
                                                                            <input type="text" class="form-control"   value="<?= $customer['companyName'] ?>"  placeholder="" readonly>
                                                                        </div>
                                                                    </div>
                                                                 
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                            <label class="form-label" for="currency-add">Email</label>
                                                                            <input type="email" class="form-control"  value="<?= $customer['email'] ?>" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                        <label class="form-label" for="currency-add">Phone Number</label>
                                                                        <input type="tel" class="form-control"    value="<?= $customer['phoneNo'] ?>"  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$"  readonly>
                                                                    </div>
                                                                    </div>
                                                        
                                                                    <input type="hidden" class="form-control"  name="userId"  value="<?= $customer['elogisticsUserId'] ?>">
                                                               
                                                                    <div class="col-10">
                                                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                            <li>
                                                                                <input type="hidden" class="form-control" name="approve_customer">
                                                                                <button type="button" class="btn btn-primary" name ="approve_customer" onClick="approveCustomer('<?= $customer["elogisticsUserId"] ?>')">Confirm</button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->

                                                 <!-- Delete Co Customer-->
                                                <div class="modal fade" tabindex="-1" role="dialog" id="delete-cocustomer<?= $customer['elogisticsUserId'] ?>" >
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Delete Corporate Customer</h5>
                                                                <form id ="delete-cocustomer<?= $customer['elogisticsUserId'] ?>"  class="form-validate">
                                                                <div class="row g-gs">
                                                                <div class="col-md-6">
                                                                <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">Trading Name</label>
                                                                            <input type="text" class="form-control"  name ="name" value="<?= $customer['companyName'] ?>"  placeholder="" readonly>
                                                                        </div>
                                                                    </div>
                                                                 
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                            <label class="form-label" for="currency-add">Email</label>
                                                                            <input type="email" class="form-control"  name="vEmail" value="<?= $customer['email'] ?>" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  placeholder="" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                        <label class="form-label" for="currency-add">Phone Number</label>
                                                                        <input type="tel" class="form-control"  name="vPhone"  value="<?= $customer['phoneNo'] ?>"  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" placeholder="" readonly>
                                                                    </div>
                                                                    </div>
                                                                 
                                                                    <input type="hidden" class="form-control"  name="customerId"  value="<?= $customer['elogisticsUserId'] ?>">
                                                                    <!-- <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Confirm Password</label>
                                                                        <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
                                                                    </div>
                                                                    </div> -->
                                                                    <div class="col-10">
                                                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                            <li>
                                                                                <input type="hidden" class="form-control" name="delete_cocustomer">
                                                                                <button type="button" class="btn btn-primary" name ="delete_cocustomer" onClick="deleteCoCustomer('<?= $customer["elogisticsUserId"] ?>')">Delete</button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                <!-- Delete Ind Customer-->
                                                <div class="modal fade" tabindex="-1" role="dialog" id="delete-customer<?= $customer['elogisticsUserId'] ?>" >
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Delete Individual Customer</h5>
                                                                <form id ="delete-customer<?= $customer['elogisticsUserId'] ?>"  class="form-validate">
                                                                <div class="row g-gs">
                                                                <div class="col-md-6">
                                                                <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">First Name</label>
                                                                            <input type="text" class="form-control"  name ="name" value="<?= $customer['name'] ?>"  placeholder="" readonly>
                                                                        </div>
                                                                    </div>
                                                                 
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                            <label class="form-label" for="currency-add">Email</label>
                                                                            <input type="email" class="form-control"  name="vEmail" value="<?= $customer['email'] ?>" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  placeholder="" readonly>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                        <label class="form-label" for="currency-add">Phone Number</label>
                                                                        <input type="tel" class="form-control"  name="vPhone"  value="<?= $customer['phoneNo'] ?>"  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" placeholder="" readonly>
                                                                    </div>
                                                                    </div>
                                                                 
                                                                    <input type="hidden" class="form-control"  name="customerId"  value="<?= $customer['elogisticsUserId'] ?>">
                                                                    <!-- <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Confirm Password</label>
                                                                        <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
                                                                    </div>
                                                                    </div> -->
                                                                    <div class="col-10">
                                                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                            <li>
                                                                                <input type="hidden" class="form-control" name="delete_customer">
                                                                                <button type="button" class="btn btn-primary" name ="delete_customer" onClick="deleteCustomer('<?= $customer["elogisticsUserId"] ?>')">Delete</button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->




                                                    <?php endforeach; ?>

                                                    <?php else : ?>
                                                        <?= $customers['responseMessage'] ?>

                                                    <?php endif; ?>

                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
        <!-- Success Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successApproveAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully approved the customer</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failApproveAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='approveCustomerResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
     <!-- Success Modal Alert -->
     <div class="modal fade" tabindex="-1" id="successDeleteAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully deleted customer</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failDeleteAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addCustomerDeleteResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>

         <!-- Success Modal Alert -->
         <div class="modal fade" tabindex="-1" id="successCoDeleteAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully deleted customer</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failCoDeleteAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addCoCustomerDeleteResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>

       <!-- Success Modal Alert -->
       <div class="modal fade" tabindex="-1" id="successCoAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully added customer</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failCoAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addCoCustomerResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
      <!-- Success Modal Alert -->
      <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully added customer</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addCustomerResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>


    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully edited customer</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='editCustomerResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
       <!-- Success Edit Co Modal Alert -->
       <div class="modal fade" tabindex="-1" id="successCoEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully edited customer</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Edit Co Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEditCoAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='editCustomerCoResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Customer-->
    <div class="modal fade" tabindex="-1" role="dialog" id="add-customer">
        <div class="modal-dialog modal-mb" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <h5 class="modal-title">Add Customer</h5>

                    <ul class="nav nav-tabs">
                    <li class="nav-item"><a class="nav-link active" data-toggle="tab" href="#tabItem1">Individual</a></li>
                    <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tabItem2">Corporate</a> </li> 
                    </ul>

                    <div class="tab-content">
                    <div class="tab-pane active" id="tabItem1">
                        <form id = "addcustomer" action="#" class="mt-2">
                            <div class="row g-gs">
                            <div class="col-md-6">
                                <div class="form-group">
                                        <label class="form-label" for="payment-name-add">First Name</label>
                                        <input type="text" class="form-control"  name ="vFirstName" placeholder="" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                        <label class="form-label" for="payment-name-add">Last Name</label>
                                        <input type="text" class="form-control"  name ="vLastName" placeholder="" required>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                <div class="form-group">
                                        <label class="form-label" for="currency-add">Email</label>
                                        <input type="email" class="form-control"  name="vEmail" placeholder="">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="currency-add">Phone Number</label>
                                    <input type="tel" class="form-control"  name="vPhone"  placeholder="" pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" required>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="currency-add">Address</label>
                                    <input type="tel" class="form-control"  name="address"  placeholder="" required>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                <label class="form-label" for="currency-add">Password</label>
                            <input type="password" class="form-control passwordInput" name="vPassword" id="PassEntry" required>
                            <span id="StrengthDisp" class="badge displayBadge">Weak</span>
                                </div>
                                </div>
                                <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="currency-add">Confirm Password</label>
                                    <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
                                </div>
                                </div> -->
                                <div class="col-10">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                        <li>
                                            <input type="hidden" class="form-control" name="add_customer">
                                            <button type="button" class="btn btn-primary" name ="add_customer" onClick="addCustomer()">Add</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="tab-pane" id="tabItem2">
                        <form id = "addcocustomer" action="#" class="mt-2">
                            <div class="row g-gs">
                            <div class="col-md-6">
                                <div class="form-group">
                                        <label class="form-label" for="payment-name-add">Trading Name</label>
                                        <input type="text" class="form-control"  name ="name" placeholder="" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                        <label class="form-label" for="payment-name-add">Address</label>
                                        <input type="text" class="form-control"  name ="vCaddress" placeholder="" required>
                                    </div>
                                </div>
                            
                                <div class="col-md-6">
                                <div class="form-group">
                                        <label class="form-label" for="currency-add">Email</label>
                                        <input type="email" class="form-control"  name="vEmail" placeholder="" required>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="currency-add">Mobile Number</label>
                                    <input type="tel" class="form-control"  name="vPhone"  placeholder="" pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" required>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="currency-add">Zimra BP Number</label>
                                    <input type="tel" class="form-control"  name="zimraBPNumber"  placeholder="" required>
                                </div>
                                </div>
                                <div class="col-md-6">
                                <div class="form-group">
                                <label class="form-label" for="currency-add">Password</label>
                                <input type="password" class="form-control passwordInput" name="vPassword" id="PassEntryco" required>
                            <span id="StrengthDispco" class="badge displayBadge">Weak</span>
                                </div>
                                </div>
                                <!-- <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="currency-add">Confirm Password</label>
                                    <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
                                </div>
                                </div> -->
                                <div class="col-10">
                                    <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                        <li>
                                            <input type="hidden" class="form-control" name="add_cocustomer">
                                            <button type="button" class="btn btn-primary" name ="add_cocustomer" onClick="addCoCustomer()">Add</button>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-light">
                    <span class="sub-text"></span>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->
  

    <script src="./assets/validate-password-requirements/js/jquery.passwordRequirements.min.js"></script>
    
    <script>

let timeout;

    // traversing the DOM and getting the input and span using their IDs
let password = document.getElementById('PassEntry');
    let strengthBadge = document.getElementById('StrengthDisp');

    // The strong and weak password Regex pattern checker
let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthChecker(PasswordParameter){
        // We then change the badge's color and text based on the password strength
if(strongPassword.test(PasswordParameter)) {
            strengthBadge.style.backgroundColor = 'green';
            strengthBadge.textContent = 'Strong';
        } else if(mediumPassword.test(PasswordParameter)){
            strengthBadge.style.backgroundColor = 'blue';
            strengthBadge.textContent = 'Medium';
        } else{
            strengthBadge.style.backgroundColor = 'red';
            strengthBadge.textContent = 'Weak';
        }
    }

password.addEventListener("input", () => {
    strengthBadge.style.display = 'block';
    clearTimeout(timeout);
    timeout = setTimeout(() => StrengthChecker(password.value), 500);
    if(password.value.length !== 0) {
        strengthBadge.style.display != 'block';
    } else {
        strengthBadge.style.display = 'none';
    }
});



</script> 
<script>
//password coorperate
let timeoutco;

    // traversing the DOM and getting the input and span using their IDs
let passwordco = document.getElementById('PassEntryco');
    let strengthBadgeco = document.getElementById('StrengthDispco');

    // The strong and weak password Regex pattern checker
let strongPasswordco = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPasswordco = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthCheckerco(PasswordParameterco){
        // We then change the badge's color and text based on the password strength
if(strongPasswordco.test(PasswordParameterco)) {
    strengthBadgeco.style.backgroundColor = 'green';
    strengthBadgeco.textContent = 'Strong';
        } else if(mediumPasswordco.test(PasswordParameterco)){
            strengthBadgeco.style.backgroundColor = 'blue';
            strengthBadgeco.textContent = 'Medium';
        } else{
            strengthBadgeco.style.backgroundColor = 'red';
            strengthBadgeco.textContent = 'Weak';
        }
    }

    passwordco.addEventListener("input", () => {
        strengthBadgeco.style.display = 'block';
    clearTimeout(timeoutco);
    timeoutco = setTimeout(() => StrengthCheckerco(passwordco.value), 500);
    if(passwordco.value.length !== 0) {
        strengthBadgeco.style.display != 'block';
    } else {
        strengthBadgeco.style.display = 'none';
    }
});



</script> 
    <!-- BEGIN: AJAX CALLS-->
   <script>
    //Add Individual Customer
    function addCustomer() {
        var PasswordParameter = document.getElementById('PassEntry').value;

var vMsg = 'The minimum password length is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number, and 1 special character.';

if(!strongPassword.test(PasswordParameter)) {

alert(vMsg); return;

}
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addcustomer').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.Action == 1) {
                    $('#spinner').hide();
                    $('#add-customer').modal('hide');
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/customer-list";
                    }, 2000);
                } else {
                    $('.addCustomerResponse').empty();
                    $('.addCustomerResponse').html(json.Message);
                    $('#spinner').hide();
                    $('#add-customer').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".addCustomerResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addCustomerResponse').empty();
                $('.addCustomerResponse').append(errorThrown);
                $('#spinner').hide();
                $('#add-customer').modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".addCustomerResponse").html("");
                });
            }
        });
    }
    </script>
       <script>
    //Add Co Customer
    function addCoCustomer() {
        var PasswordParameterco = document.getElementById('PassEntryco').value;

var vMsg = 'The minimum password length is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number, and 1 special character.';

if(!strongPasswordco.test(PasswordParameterco)) {

alert(vMsg); return;

}
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addcocustomer').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.Action == 1) {
                    $('#spinner').hide();
                    $('#add-customer').modal('hide');
                    $("#successCoAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/customer-list";
                    }, 2000);
                } else {
                    $('.addCoCustomerResponse').empty();
                    $('.addCoCustomerResponse').html(json.Message);
                    $('#spinner').hide();
                    $('#add-customer').modal('hide');
                    $("#failCoAlert").modal('show');
                    $("#failCoAlert").on("hidden.bs.modal", function() {
                        $(".addCoCustomerResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addCoCustomerResponse').empty();
                $('.addCoCustomerResponse').append(errorThrown);
                $('#spinner').hide();
                $('#add-customer').modal('hide');
                $("#failCoAlert").modal('show');
                $("#failCoAlert").on("hidden.bs.modal", function() {
                    $(".addCoCustomerResponse").html("");
                });
            }
        });
    }
    </script>
    <script>
    //Edit Individual Customer
    function editCustomer(iUserId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#edit-customer'+iUserId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if(json.Action == 1) {
                    $('#spinner').hide();
                    $('#edit-customer'+iUserId).modal('hide');
                    $("#successEditAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/customer-list";
                    }, 2000);
                  
                } else {
                    $('.editCustomerResponse').empty();
                    $('.editCustomerResponse').append(json.Message);
                    $('#spinner').hide();
                    $('#edit-customer'+iUserId).modal('hide');
                    $("#failEditAlert").modal('show');
                    $("#failEditAlert").on("hidden.bs.modal", function() {
                        $(".editCustomerResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.editCustomerResponse').empty();
                $('.editCustomerResponse').append(errorThrown);
                $('#spinner').hide();
                $('#edit-customer'+iUserId).modal('hide');
                $("#failEditAlert").modal('show');
                $("#failEditAlert").on("hidden.bs.modal", function() {
                    $(".editCustomerResponse").html("");
                });
            }
        });
    }
    </script>
        <script>
    //Edit Co Customer
    function editCoCustomer(iUserId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#edit-cocustomer'+iUserId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if(json.Action == 1) {
                    $('#spinner').hide();
                    $('#edit-cocustomer'+iUserId).modal('hide');
                    $("#successCoEditAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/customer-list";
                    }, 2000);
                  
                } else {
                    $('.editCustomerCoResponse').empty();
                    $('.editCustomerCoResponse').append(json.Message);
                    $('#spinner').hide();
                    $('#edit-cocustomer'+iUserId).modal('hide');
                    $("#failEditCoAlert").modal('show');
                    $("#failEditCoAlert").on("hidden.bs.modal", function() {
                        $(".editCustomerCoResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.editCustomerCoResponse').empty();
                $('.editCustomerCoResponse').append(errorThrown);
                $('#spinner').hide();
                $('#edit-cocustomer'+iUserId).modal('hide');
                $("#failEditCoAlert").modal('show');
                $("#failEditCoAlert").on("hidden.bs.modal", function() {
                    $(".editCustomerCoResponse").html("");
                });
            }
        });
    }
    </script>
            <script>
    //Delete Co Customer
    function deleteCoCustomer(iUserId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#delete-cocustomer'+iUserId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if(json.Action == 1) {
                    $('#spinner').hide();
                    $('#delete-cocustomer'+iUserId).modal('hide');
                    $("#successCoDeleteAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/customer-list";
                    }, 2000);
                  
                } else {
                    $('.addCoCustomerDeleteResponse').empty();
                    $('.addCoCustomerDeleteResponse').append(json.Message);
                    $('#spinner').hide();
                    $('#delete-cocustomer'+iUserId).modal('hide');
                    $("#failCoDeleteAlert").modal('show');
                    $("#failCoDeleteAlert").on("hidden.bs.modal", function() {
                        $(".editCustomerCoResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addCoCustomerDeleteResponse').empty();
                $('.addCoCustomerDeleteResponse').append(errorThrown);
                $('#spinner').hide();
                $('#delete-cocustomer'+iUserId).modal('hide');
                $("#failCoDeleteAlert").modal('show');
                $("#failCoDeleteAlert").on("hidden.bs.modal", function() {
                    $(".addCoCustomerDeleteResponse").html("");
                });
            }
        });
    }
    </script>
            <script>
    //Delete Ind Customer
    function deleteCustomer(iUserId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#delete-customer'+iUserId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if(json.Action == 1) {
                    $('#spinner').hide();
                    $('#delete-customer'+iUserId).modal('hide');
                    $("#successDeleteAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/customer-list";
                    }, 2000);
                  
                } else {
                    $('.addCustomerDeleteResponse').empty();
                    $('.addCustomerDeleteResponse').append(json.Message);
                    $('#spinner').hide();
                    $('#delete-customer'+iUserId).modal('hide');
                    $("#failDeleteAlert").modal('show');
                    $("#failDeleteAlert").on("hidden.bs.modal", function() {
                        $(".addCustomerDeleteResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addCustomerDeleteResponse').empty();
                $('.addCustomerDeleteResponse').append(errorThrown);
                $('#spinner').hide();
                $('#delete-customer'+iUserId).modal('hide');
                $("#failDeleteAlert").modal('show');
                $("#failDeleteAlert").on("hidden.bs.modal", function() {
                    $(".addCustomerDeleteResponse").html("");
                });
            }
        });
    }
    </script>
     <script>
    //Edit Individual Customer Email
    function editEmail(iUserId) {
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#edit-email'+iUserId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('.emailCustomer').empty(); //clear apend
                    $('.emailCustomer').append("Enail Changed Successfully");
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".emailCustomer").html("");
                    });
                        setTimeout(function() {
                            window.location = "portal/admin/customer-list";
                    }, 2000);
                  
                } else {
                    $('.emailCustomer').empty();
                    $('.emailCustomer').append(json.Message);
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".emailCustomer").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.emailCustomer').empty();
                 $('.emailCustomer').append(errorThrown);
                $(".modal").on("hidden.bs.modal", function() {
                $(".emailCustomer").html("");
                });
            }
        });
    }
    </script>
    <script>
          
          //Approve Customer
          function approveCustomer(iUserId) {
              $('#spinner').show();
              $.ajax({
                  type: "POST",
                  url: "portal/admin/controller/process.php",
                  data: $('form#approve_customer'+ iUserId).serialize(),
                  cache: false,
                  success: function(response) {
                      var json = $.parseJSON(response);
                      if (json.responseStatus == "SUCCESS") {
                          $('#spinner').hide();
                          $('#approve_customer'+ iUserId).modal('hide');
                          $("#successApproveAlert").modal('show');
                          setTimeout(function() {
                              window.location = "portal/admin/customer-list";
                          }, 2000);
                        
                      } else {
                          $('.approveCustomerResponse').empty();
                          $('.approveCustomerResponse').append(json.responseMessage);
                          $('#approve_customer'+ iUserId).modal('hide');
                          $("#failApproveAlert").modal('show');
                          $("#failApproveAlert").on("hidden.bs.modal", function() {
                              $(".approveCustomerResponse").html("");
                          });
                      }
                  },
                  error: function(jqXHR, textStatus, errorThrown) {
                      $('.approveCustomerResponse').empty();
                      $('.approveCustomerResponse').append(errorThrown);
                      $('#spinner').hide();
                      $('#approve_customer'+ iUserId).modal('hide');
                      $("#failApproveAlert").modal('show');
                      $("#failApproveAlert").on("hidden.bs.modal", function() {
                          $(".approveCustomerResponse").html("");
                      });
                  }
              });
          }
          </script>
    
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>