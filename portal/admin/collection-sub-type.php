<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
$admin_id =  $_SESSION["admin_id"];
$admin_phone  =$_SESSION["admin_vphone"];
$admin_email =  $_SESSION["admin_Email"];
$admin_firstname =  $_SESSION["first_name"];
$admin_lastname = $_SESSION["last_name"];

include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
$collection_sub_types = json_decode(getAllCollectionSubTypes(), true);
$collection_types = json_decode(getAllCollectionTypes(), true);
// var_dump($collection_sub_types);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                             
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Collection Sub Type List</h3>
                                         
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                    <li><a href="#" data-toggle="modal" onclick="showBAGSADD()" data-target="#add-sub-type"><span>Add Sub Type</span></a></li>

                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                           <th class="nk-tb-col"><span class="sub-text">Name</span></th>
                                                            <th class="nk-tb-col tb-col-mg"><span class="sub-text">Unit</span></th>
                                                            <th class="nk-tb-col tb-col-mg"><span class="sub-text">Cost Per Unit(USD)</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Cost Per Unit(ZWL)</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Quantity</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Quotation Validity(days)</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Collection Description</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Collection Type</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Status</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($collection_sub_types['responseStatus'] == "SUCCESS") : ?>
                                                    <?php
                                                    foreach ($collection_sub_types['eobjResponse'] as $collection_sub_type) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $collection_sub_type['vsubType'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $collection_sub_type['vunit'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= "USD"."".$collection_sub_type['costPerUnitUSD'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= "ZWL"."".$collection_sub_type['costPerUnitZWL'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($collection_sub_type['quantity'] == "Two"){
                                                            echo "2T";
                                                            }elseif($collection_sub_type['quantity'] == "ThreeToFive") {
                                                                echo "3-5T";
                                                            }elseif($collection_sub_type['quantity'] == "SixToSeven") {
                                                                echo "6-7T";
                                                            }elseif($collection_sub_type['quantity'] == "EightToTen") {
                                                                echo "8-10T";
                                                            }elseif($collection_sub_type['quantity'] == 1) {
                                                                echo $collection_sub_type['quantity'].""."BAG";
                                                            }else {
                                                                echo $collection_sub_type['quantity'].""."BAGS";
                                                            }
                                                        ?>
                                                        </span>
                                                       </td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $collection_sub_type['quotationValidityDays'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $collection_sub_type['message'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($collection_sub_type['collectionType'] == "HouseHoldCollection"){
                                                            echo "HouseHold Collection";
                                                            }elseif($collection_sub_type['collectionType'] == "CommercialCollection") {
                                                                echo "Commercial Collection";
                                                            }elseif($collection_sub_type['collectionType'] == "HouseHoldAdHocCollection") {
                                                                echo "HouseHold AdHoc Collection";
                                                            }else {
                                                                echo "Commercial Adhoc Collection";
                                                            }
                                                        ?>
                                                        </span>
                                                       </td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $collection_sub_type['status'] ?></span></td>
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                                            
                                                          
                                                                <li>
                                                                    <div class="drodown">
                                                                        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <li><a href="#" data-toggle="modal"  data-target="#edit-sub-type<?= $collection_sub_type['id'] ?>"><em class="icon ni ni-edit"></em><span>Edit</span></a></li>

                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <!-- Edit Sub Type-->
                                                    <div class="modal fade" tabindex="-1" role="dialog" class ="edit_sub_type" id="edit-sub-type<?= $collection_sub_type['id'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Edit Collection Sub Type</h5>
                                                                <form id ="edit-sub-type<?= $collection_sub_type['id'] ?>" class="mt-2" class="form-validate">
                                                                    <div class="row g-gs">
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="payment-name-add">Sub Type</label>
                                                                            <input type="text" class="form-control" name ="vsubType" value="<?= $collection_sub_type['vsubType'] ?>" required>
                                                                        </div>
                                                                    </div>
                                                                
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="march-id-add">Cost Per Unit(USD)</label>
                                                                            <input type="text" class="form-control"  name ="costPerUnitUSD" value="<?= $collection_sub_type['costPerUnitUSD'] ?>" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label" for="march-id-add">Cost Per Unit(ZWL)</label>
                                                                            <input type="text" class="form-control"  name ="costPerUnitZWL" value="<?= $collection_sub_type['costPerUnitZWL'] ?>" required>
                                                                        </div>
                                                                    </div>
                                                                    <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status"><span class="sr-only">Loading...</span>  </div></div> -->
                                                                      <!-- col-->
                                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                            <div class="form-group">
                                                                                <label class="form-label">Unit</label>
                                                                                <div class="form-control-wrap">
                                                                                    <select name ="vunit" class="form-control"  id="edit_unit" onchange="EditUnit(this)"  required>
                                                                                        <!-- <option value="BAGS">BAG/S</option> -->
                                                                                        <option selected value="TONNAGE">TONNAGE</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>
                                                                        <!--col -->
                                                                        <!--col-->
                                                                        <div class="col-md-6 col-lg-4 col-xxl-3">
                                                                            <div class="form-group">
                                                                                <label class="form-label">Quantity</label>
                                                                                 <div class="form-control-wrap">
                                                                                 <input type="number" id="one_monthxe" name ="quantity_value[]" class="form-control" style="display: none;"  min ="2"   placeholder="" >
                                                                                     <select  id="tonnage_optione" name ="quantity_value[]"  class="form-control"  >
                                                                                    <option value="Two">2T</option>
                                                                                    <option value="ThreeToFive">3-5T</option>
                                                                                    <option value="SixToSeven">6-7T</option>
                                                                                    <option value="EightToTen">8-10T</option>
                                                                                    </select>
                                                                                </div>
                                                                            </div>
                                                                        </div>

                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                    <label class="form-label">Status</label>
                                                                        <div class="form-control-wrap">
                                                                            <select name ="estatus" class="form-control" data-placeholder="Select Status" required>
                                                                            <option value="ACTIVE">ACTIVE</option>
                                                                            <option value="INACTIVE">INACTIVE</option>
                                                                            <option value="DELETED">DELETED</option>
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                <div class="col-md-6">
                                                                <div class="form-group">
                                                                    <label class="form-label" for="march-id-add">Quotation Validity</label>
                                                                    <input type="number" class="form-control"  name ="quotationValidityDays" min="1" value="<?= $collection_sub_type['vunit'] ?>" placeholder="Number Of Days" required>
                                                                </div>
                                                                    </div>
                                                                    <div class="col-sm-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="default-textarea">Collection Description</label>
                                                                        <div class="form-control-wrap">
                                                                            <textarea class="form-control no-resize" maxlength="250" id="default-textarea" name ="message" value="<?php echo $collection_sub_type['message'] ?>"></textarea>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-group">
                                                                    <label class="form-label">Collection Type</label>
                                                                        <div class="form-control-wrap">
                                                                            <select name ="collectionType" class="form-select" data-placeholder="Select Collection Type" required>
                                                                                <option value="HouseHoldCollection">HouseHold Collection</option>
                                                                                <option value="CommercialCollection">Commercial Collection</option>
                                                                                <!-- <option value="HouseHoldAdHocCollection">HouseHold AdHoc Colledtion </option>
                                                                                <option value="CommercialAdhocCollection ">Commercial Adhoc Collection </option> -->
                                                                            </select>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                             
                                                                    <input type="hidden" class="form-control"  name="id" value="<?= $collection_sub_type['id'] ?>">
                                                                    <!-- <input type="hidden" class="form-control"  name="typeId" value="<?= $collection_sub_type['typeId'] ?>"> -->
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="edit_sub_type" value="true">
                                                                                <button type="button" class="btn btn-primary" name ="edit_sub_type"  onClick="editSubType('<?= $collection_sub_type["id"] ?>')" >Edit</button>
                                                                            
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->

                                                    <?php endforeach; ?>

                                                    <?php else : ?>

                                                    <?= $collection_sub_types['responseMessage'] ?>

                                                    <?php endif; ?>

                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
        <!-- Success Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully added collection sub type</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addSubTypeResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Success Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully updated sub type</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='editSubTypeResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
  <!-- Add  Sub Type-->
  <div class="modal fade" tabindex="-1" role="dialog" class="add_sub_type" id="add-sub-type">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-md">
                <h5 class="modal-title">Add Collection Sub Type</h5>
                <form id ="add-sub-type"  class="mt-2"  class="form-validate">
                    <div class="row g-gs">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="payment-name-add">Sub Type</label>
                                <input type="text" class="form-control" name ="vsubType" id="payment-name-add" placeholder="" required>
                            </div>
                        </div>
                      
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="march-id-add">Cost Per Unit(USD)</label>
                                <input type="text" class="form-control"  id="march-id-add" name ="costPerUnitUSD" placeholder="" required>
                            </div>
                        </div>
                          <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="march-id-add">Cost Per Unit(ZWL)</label>
                                <input type="text" class="form-control"  name ="costPerUnitZWL" placeholder="" required>
                            </div>
                        </div>
                        <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status"><span class="sr-only">Loading...</span>  </div></div> -->
                

                    <!-- col-->
                    <div class="col-md-6 col-lg-4 col-xxl-3">
                                <div class="form-group">
                                    <label class="form-label">Unit</label>
                                    <div class="form-control-wrap">
                                        <select name ="vunit" class="form-control"  id="select_unit"  onchange="Unitval(this)"  required>
                                            <option value="BAGS">BAG/S</option>
                                            <option value="TONNAGE">TONNAGE</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <!--col -->
                            <!--col-->
                            <div class="col-md-6 col-lg-4 col-xxl-3">
                                <div class="form-group">
                                    <label class="form-label">Quantity</label>
                                    <div class="form-control-wrap">
                                    <input type="number" id="one_monthx" name ="quantity[]" class="form-control"  min ="2"    placeholder="" required>
                                        <select    id="tonnage_option" name ="quantity[]" class="form-control" data-placeholder="Select Tonnage" required>
                                            <option value="">Select Tonnage</option>
                                            <option value="Two">2T</option>
                                            <option value="ThreeToFive">3-5T</option>
                                            <option value="SixToSeven">6-7T</option>
                                            <option value="EightToTen">8-10T</option>
                                        </select>
                                    </div>
                                </div>
                            </div>


         
                    <div class="col-md-6">
                        <div class="form-group">
                        <label class="form-label">Collection Type</label>
                            <div class="form-control-wrap">
                                <select name ="collectionType" class="form-select" data-placeholder="Select Collection Type" required>
                                    <option value="HouseHoldCollection">HouseHold Collection</option>
                                    <option value="CommercialCollection">Commercial Collection</option>
                                    <!-- <option value="HouseHoldAdHocCollection">HouseHold AdHoc Colledtion </option>
                                    <option value="CommercialAdhocCollection ">Commercial Adhoc Collection </option> -->
                                </select>
                            </div>
                        </div>
                    </div>
                    <!--col-->
                    <div class="col-md-6">
                    <div class="form-group">
                        <label class="form-label" for="march-id-add">Quotation Validity</label>
                        <input type="number" class="form-control"  name ="quotationValidityDays" min="1" placeholder="Number Of Days" required>
                    </div>
                        </div>
                        <div class="col-sm-6">
                    <div class="form-group">
                        <label class="form-label" for="default-textarea">Collection Description</label>
                        <div class="form-control-wrap">
                            <textarea class="form-control no-resize" id="default-textarea" name ="message"></textarea>
                        </div>
                    </div>
                </div>
                        <div class="col-12">
                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                <li>
                                <input type="hidden" class="form-control" name="add_sub_type">
                                    <button type ="button" name="add_sub_type" class="btn btn-primary" onClick="addSubType()">Add</button>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </form>
            </div><!-- .modal-body -->
            <div class="modal-footer bg-light">
            <span class="sub-text"></span>
        </div>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
</div><!-- .modal -->

   <!-- BEGIN: AJAX CALLS-->
<script>
   function showBAGSADD() {
    document.getElementById("one_monthx").style.display = "block";
$('#tonnage_option').hide();
   }
</script>
<!-- <script>
   function showBAGSEdit() {
    document.getElementById("one_monthxe").style.display = "block";
    $('#tonnage_optione').hide();
   }
</script> -->

<!-- <script>
function onloadShowBags() {
document.getElementById("one_monthx").style.display = "block";
document.getElementById("one_monthx").style.display = "block";
$('#tonnage_option').hide();
$('#tonnage_option').hide();

}
</script> -->
<script>
function Unitval(that) {
if (that.value == "BAGS") {
    document.getElementById("one_monthx").style.display = "block";
    $('#tonnage_option').hide();
} else {
    document.getElementById("one_monthx").style.display = "none";
    $('#tonnage_option').show();
}

}
 </script>

   <script>
    
function EditUnit(that) {
console.log(that.value);
    if (that.value == "BAGS") {
    document.getElementById("one_monthxe").style.display = "block";
    document.getElementById("tonnage_optione").style.display = "none";
    // $('#tonnage_optione').hide();
} else {
    document.getElementById("tonnage_optione").style.display = "block";
    document.getElementById("one_monthxe").style.display = "none";
    // $('#tonnage_optione').show();
}
}
    </script>
   <script>
    //Add Sub Type
    function addSubType() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#add-sub-type').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                // console.log(json.statusCode);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#add-sub-type').modal('hide');
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/collection-sub-type";
                    }, 2000);

                } else {
                    $('.addSubTypeResponse').empty();
                    $('.addSubTypeResponse').html(json.responseMessage);
                    $('#spinner').hide();
                    $('#add-sub-type').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".addSubTypeResponse").html("");
                    });
                    
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addSubTypeResponse').empty();
                $('.addSubTypeResponse').append(errorThrown);
                $('#spinner').hide();
                $('#add-sub-type').modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".addSubTypeResponse").html("");
                });
            }
        });
    }
    </script>
        <script>
    //Edit Sub Collection
    function editSubType(id) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#edit-sub-type'+id).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#edit-sub-type'+id).modal('hide');
                    $("#successEditAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/collection-sub-type";
                    }, 2000);
                  
                } else {
                    $('.editSubTypeResponse').empty();
                    $('.editSubTypeResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('#edit-sub-type'+id).modal('hide');
                    $("#failEditAlert").modal('show');
                    $("#failEditAlert").on("hidden.bs.modal", function() {
                        $(".editSubTypeResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.editSubTypeResponse').empty();
                $('.editSubTypeResponse').append(errorThrown);
                $('#spinner').hide();
                $('#edit-sub-type'+id).modal('hide');
                $("#failEditAlert").modal('show');
                $("#failEditAlert").on("hidden.bs.modal", function() {
                    $(".editSubTypeResponse").html("");
                });
            }
        });
    
    }
    </script>
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>