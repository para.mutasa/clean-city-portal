<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
$admin_id =  $_SESSION["admin_id"];
$admin_phone  =$_SESSION["admin_vphone"];
$admin_email =  $_SESSION["admin_Email"];
$admin_firstname =  $_SESSION["first_name"];
$admin_lastname = $_SESSION["last_name"];

include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
// $bookings = json_decode(getBookings(), true);
// $currencies = json_decode(getCurrencies(), true);
$admins = getAllLogisticsAdmins();
// $franchises = json_decode(getAllPartners(), true);
// var_dump($admins);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                      
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Admins List</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                    <li><a href="#" data-toggle="modal" data-target="#add-admin"><span>Add Admin</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                            <th class="nk-tb-col"><span class="sub-text">Name</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Email</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Phone Number</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($admins['Action'] == 1) : ?>
                                                    <?php
                                                     foreach ($admins['result'] as $admin) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $admin['firstname']." ".$admin['lastname'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $admin['vemail'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $admin['phoneNumber'] ?></span></td>
                                                
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                                            
                                                                <li>
                                                                    <div class="drodown">
                                                                        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <li><a href="#" data-toggle="modal" data-target="#edit-admin<?= $admin['id'] ?>"><em class="icon ni ni-edit"></em><span>Edit</span></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <!-- Edit Admin-->
                                                    <div class="modal fade" tabindex="-1" role="dialog" id="edit-admin<?= $admin['id'] ?>" class="editadmin">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Edit Admin</h5>
                                                                <form id ="edit-admin<?= $admin['id'] ?>"  class="form-validate">
                                                                <div class="row g-gs">
                                                                <div class="col-md-6">
                                                                <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">First Name</label>
                                                                            <input type="text" class="form-control"  name ="firstname" value="<?= $admin['firstname'] ?>"  placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                            <label class="form-label" for="payment-name-add">Last Name</label>
                                                                            <input type="text" class="form-control"  name ="lastname" value="<?= $admin['lastname'] ?>" placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                            <label class="form-label" for="currency-add">Email</label>
                                                                            <input type="email" class="form-control"  name="VEmail" value="<?= $admin['vemail'] ?>" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  placeholder="" required>
                                                                        </div>
                                                                    </div>
                                                                    <div class="col-md-6">
                                                                    <div class="form-control-wrap">
                                                                        <label class="form-label" for="currency-add">Phone Number</label>
                                                                        <input type="tel" class="form-control"  name="phoneNumber"  value="<?= $admin['phoneNumber'] ?>"  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" placeholder="" required>
                                                                    </div>
                                                                    </div>
                                                                    <!-- <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Password</label>
                                                                        <input type="password" class="form-control"   id="password" name="password" pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters"  placeholder="">
                                                                    </div>
                                                                    </div> -->
                                                                    <input type="hidden" class="form-control"  name="id"  value="<?= $admin['id'] ?>">
                                                                    <!-- <div class="col-md-6">
                                                                    <div class="form-group">
                                                                        <label class="form-label" for="currency-add">Confirm Password</label>
                                                                        <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
                                                                    </div>
                                                                    </div> -->
                                                                    <div class="col-10">
                                                                        <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                            <li>
                                                                                <input type="hidden" class="form-control" name="edit_admin">
                                                                                <button type="button" class="btn btn-primary" name ="edit_admin" onClick="editAdmin('<?= $admin["id"] ?>')">Edit</button>
                                                                            </li>
                                                                        </ul>
                                                                    </div>
                                                                </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                            <div class="modal-footer bg-light">
                                                        <span class="sub-text"></span>
                                                    </div>
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->

                                                    <?php endforeach; ?>

                                                    <?php else : ?>
                                                        <?= $admins['Message'] ?>

                                                    <?php endif; ?>

                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
      <!-- Success Modal Alert -->
      <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully added admin</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addAdminResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully updated admin</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='editAdminResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- Add Admin-->
    <div class="modal fade" tabindex="-1" role="dialog" id="add-admin">
        <div class="modal-dialog modal-mb" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <h5 class="modal-title">Add Admin</h5>
                    <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status">    <span class="sr-only">Loading...</span>  </div></div> -->
                    <form id = "addadmin" action="#" class="mt-2" class="form-validate">
                        <div class="row g-gs">
                        <div class="col-md-6">
                        <div class="form-control-wrap">
                                    <label class="form-label" for="payment-name-add">First Name</label>
                                    <input type="text" class="form-control"  name ="firstname" placeholder="" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-control-wrap">
                                    <label class="form-label" for="payment-name-add">Last Name</label>
                                    <input type="text" class="form-control"  name ="lastname" placeholder="" required>
                                </div>
                            </div>
                         
                         
                            <div class="col-md-6">
                            <div class="form-control-wrap">
                                    <label class="form-label" for="currency-add">Email</label>
                                    <input type="email" class="form-control"  name="VEmail" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  placeholder="" required>
                                </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-control-wrap">
                                <label class="form-label" for="currency-add">Phone Number</label>
                                <input type="tel" class="form-control"  name="phoneNumber"  placeholder=""  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" required>
                            </div>
                            </div>
                            <div class="col-md-6">
                            <div class="form-control-wrap">
                            <label class="form-label" for="currency-add">Password</label>
                            <input type="password" class="form-control passwordInput" name="password" id="PassEntry" required>
                            <span id="StrengthDisp" class="badge displayBadge">Weak</span>
                            </div>
                            </div>
                            <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label class="form-label" for="currency-add">Confirm Password</label>
                                <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
                            </div>
                            </div> -->
                            <div class="col-10">
                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                    <li>
                                        <input type="hidden" class="form-control" name="add_admin">
                                        <button type="button" class="btn btn-primary" name ="add_admin" onClick="addAdmin()">Add</button>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-light">
                    <span class="sub-text"></span>
                </div>
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->
    <!-- <script>
  var password = document.getElementById("password"),
    confirm_password = document.getElementById("confirm_password");

  function validatePassword() {
    if (password.value != confirm_password.value) {
      confirm_password.setCustomValidity("Passwords Don't Match");
    } else {
      confirm_password.setCustomValidity('');
    }
  }

  password.onchange = validatePassword;
  confirm_password.onkeyup = validatePassword;
</script> -->

<script src="./assets/validate-password-requirements/js/jquery.passwordRequirements.min.js"></script>
    
    <script>

let timeout;

    // traversing the DOM and getting the input and span using their IDs
let password = document.getElementById('PassEntry');
    let strengthBadge = document.getElementById('StrengthDisp');

    // The strong and weak password Regex pattern checker
let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthChecker(PasswordParameter){
        // We then change the badge's color and text based on the password strength
if(strongPassword.test(PasswordParameter)) {
            strengthBadge.style.backgroundColor = 'green';
            strengthBadge.textContent = 'Strong';
        } else if(mediumPassword.test(PasswordParameter)){
            strengthBadge.style.backgroundColor = 'blue';
            strengthBadge.textContent = 'Medium';
        } else{
            strengthBadge.style.backgroundColor = 'red';
            strengthBadge.textContent = 'Weak';
        }
    }

password.addEventListener("input", () => {
    strengthBadge.style.display = 'block';
    clearTimeout(timeout);
    timeout = setTimeout(() => StrengthChecker(password.value), 500);
    if(password.value.length !== 0) {
        strengthBadge.style.display != 'block';
    } else {
        strengthBadge.style.display = 'none';
    }
});



</script> 
    <!-- BEGIN: AJAX CALLS-->
   <script>
    //Add Admin
    function addAdmin() {
        var PasswordParameter = document.getElementById('PassEntry').value;

var vMsg = 'The minimum password length is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number, and 1 special character.';

if(!strongPassword.test(PasswordParameter)) {

alert(vMsg); return;

}
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addadmin').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.Action == 1) {
                    $('#spinner').hide();
                    $('#add-admin').modal('hide');
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/admins-list";
                    }, 2000);
                } else {
                    $('.addAdminResponse').empty();
                    $('.addAdminResponse').html(json.Message);
                    $('#spinner').hide();
                    $('#add-admin').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".addAdminResponse").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addAdminResponse').empty();
                $('.addAdminResponse').append(errorThrown);
                $('#spinner').hide();
                $('#add-admin').modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".addAdminResponse").html("");
                });
            }
        });
    }
    </script>
    <script>
    //Edit Admin
    function editAdmin(id) {
        if ($('form#edit-admin'+id).valid()) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#edit-admin'+id).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if(json.Action == 1) {
                    $('#spinner').hide();
                    $('#edit-admin'+id).modal('hide');
                    $("#successEditAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/admins-list";
                    }, 2000);
                  
                } else {
                    $('.editAdminResponse').empty();
                    $('.editAdminResponse').append(json.Message);
                    $('#spinner').hide();
                    $('#edit-admin'+id).modal('hide');
                    $("#failEditAlert").modal('show');
                    $("#failEditAlert").on("hidden.bs.modal", function() {
                        $(".editAdminResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.editAdminResponse').empty();
                $('.editAdminResponse').append(errorThrown);
                $('#spinner').hide();
                $('#edit-admin'+id).modal('hide');
                $("#failEditAlert").modal('show');
                $("#failEditAlert").on("hidden.bs.modal", function() {
                    $(".editAdminResponse").html("");
                });
            }
        });
    }else {
 return false;
}
    }
    </script>
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>