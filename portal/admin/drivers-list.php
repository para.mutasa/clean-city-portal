<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
$admin_id =  $_SESSION["admin_id"];
$admin_phone  =$_SESSION["admin_vphone"];
$admin_email =  $_SESSION["admin_Email"];
$admin_firstname =  $_SESSION["first_name"];
$admin_lastname = $_SESSION["last_name"];

include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
// $bookings = json_decode(getBookings(), true);
// $currencies = json_decode(getCurrencies(), true);
// $drivers = getAllElogisticsDrivers();
$drivers = json_decode(getAllLogisticsViaCleanCityDrivers(),true);
$franchises = json_decode(getAllPartners(), true);
// $franchises = json_decode(getAllPartners(), true);
// var_dump($drivers);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                      
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Drivers List</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <!-- <ul class="nk-block-tools g-3">
                                                        <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                    <li><a href="#" data-toggle="modal" data-target="#add-admin"><span>Add Admin</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li>
                                                    </ul> -->
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                        <th class="nk-tb-col tb-col-md"><span class="sub-text">Partner Type</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Name</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Email</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Phone Number</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Staus</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($drivers['responseStatus'] == "SUCCESS") : ?>
                                                    <?php
                                                     foreach ($drivers['eobjResponse'] as $driver) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $driver['partnerType'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($driver['partnerType'] == "Individual"){
                                                        echo  $driver['name']." ".$customer['surname'];
                                                        }else {
                                                        echo $driver['companyName'];
                                                        }
                                                        ?>
                                                        </span>
                                                       </td>
                                            
                                                        <td class="nk-tb-col tb-col-md"><span><?= $driver['VEmail'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $driver['phoneNumber'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $driver['creation_status'] ?></span></td>
                                                
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                                            
                                                                <li>
                                                                    <div class="drodown">
                                                                        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <?php
                                                                              if($driver['partnerType'] == "Individual"){
                                                                                // echo"<li><a href='#' data-toggle='modal' data-target='#customer".$partner['idriverId']."' ><em class='icon ni ni-edit'></em><span>Individual</span></a></li>";
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#assign_franchise".$driver['IDriverId']."' ><em class='icon ni ni-edit'></em><span>Assign Franchise</span></a></li>";
                                                                                echo"<li><a href='portal/admin/view-documents?IDriverId=".$driver['IDriverId']."' ><em class='icon ni ni-eye'></em><span>View Documents</span></a></li>";
                                                                              }else {                                     
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#assign_franchise".$driver['IDriverId']."' ><em class='icon ni ni-edit'></em><span>Assign Franchise</span></a></li>";
                                                                                echo"<li><a href='portal/admin/view-documents?IDriverId=".$driver['IDriverId']."' ><em class='icon ni ni-eye'></em><span>View Documents</span></a></li>";
                                                                              }  
                                                                            ?>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <!-- Assign Driver to franchise-->
                                                    <div class="modal fade" tabindex="-1" role="dialog" class="assign_franchise" id="assign_franchise<?= $driver['IDriverId'] ?>">
                                                            <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Assign Driver To Franchise</h5>
                                                                <form id ="assign_franchise<?= $driver['IDriverId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Name</label>
                                                                                <input type="text" class="form-control"  value="<?= $driver['name'] ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                     
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Phone Number</label>
                                                                                <input type="text" class="form-control" value="<?= $driver['phoneNumber'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                      
                                                                        <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Franchise</label>
                                                                            <div class="form-control-wrap">
                                                                            <select class="form-select" name ="franchiseId" data-placeholder="Select Franchise" >
                                                                                <?php foreach ($franchises['eobjResponse']  as $franchise): ?>
                                                                                    <option value="">Select Franchise</option>
                                                                                    <option value="<?= $franchise["franchiseId"] ?>"><?= $franchise["company"].""?></option>
                                                                                <?php endforeach; ?> 
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                    <input type="hidden" class="form-control" name ="idriverId" value="<?= $driver['IDriverId'] ?>" >      
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="assign_driver_franchise">
                                                                                 <button type="button" class="btn btn-primary" name ="assign_driver_franchise" onClick="assignFranchise('<?= $driver["IDriverId"] ?>')">Assign</button>
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->                                     
                                                
                                                    <?php endforeach; ?>

                                                    <?php else : ?>
                                                        <?= $drivers['responseMessage'] ?>

                                                    <?php endif; ?>

                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
      <!-- Success Modal Alert -->
      <div class="modal fade" tabindex="-1" id="successAssignFranchiseAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully assigned driver to franchise.</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAssignFranchiseAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='assignFranchiseResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
  
     
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>


<script>
          
//Assign Driver To Franchise 
function assignFranchise(idriverId) {
    $('#spinner').show();
    $.ajax({
        type: "POST",
        url: "portal/admin/controller/process.php",
        data: $('form#assign_franchise'+idriverId).serialize(),
        cache: false,
        success: function(response) {
            var json = $.parseJSON(response);
            if (json.responseStatus == "SUCCESS") {
                $('#spinner').hide();
                $('.assign_franchise').modal('hide');
                $("#successAssignFranchiseAlert").modal('show');
                setTimeout(function() {
                    window.location = "portal/admin/drivers-list";
                }, 2000);
            
            } else {
                $('.assignFranchiseResponse').empty();
                $('.assignFranchiseResponse').append(json.responseMessage);
                $('#spinner').hide();
                $('.assign_franchise').modal('hide');
                $("#failAssignFranchiseAlert").modal('show');
                $("#failAssignFranchiseAlert").on("hidden.bs.modal", function() {
                    $(".assignFranchiseResponse").html("");
                });
            }
        },
        error: function(jqXHR, textStatus, errorThrown) {
            $('.approveCancelBookingResponse').empty();
            $('.approveCancelBookingResponse').append(errorThrown);
            $('#spinner').hide();
            $('.assign_franchise').modal('hide');
            $("#failAssignFranchiseAlert").modal('show');
            $("#failAssignFranchiseAlert").on("hidden.bs.modal", function() {
                $(".assignFranchiseResponse").html("");
            });
        }
    });
}
          </script>
    
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>