<?php
include_once('../../../utils/VayaCleanCityUtility.php');

//add waste type
if (isset($_POST['add_waste'])) {
        $bagColor = $_POST['bagColor'];
        $capacity = $_POST['capacity'];
        $description = $_POST['description'];
        $variableCost = $_POST['variableCost'];
        $variableCostDescription = $_POST['variableCostDescription'];
        $wasteTypeName = $_POST['wasteTypeName'];

    //  var_dump($bagColor);
    // var_dump($capacity);
    // var_dump($description);
    // var_dump($variableCost);
    // var_dump($variableCostDescription);
    // var_dump($wasteTypeName);
    // exit;

        $add_waste_result = addWaste($bagColor,$capacity,$description,$variableCost,$variableCostDescription,$wasteTypeName);
        $add_waste_data = json_decode($add_waste_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_waste_data);
        exit;
    }

    //edit waste type
if (isset($_POST['edit_waste_type'])) {
    $bagColor = $_POST['bagColor'];
    $capacity = $_POST['capacity'];
    $description = $_POST['description'];
    $id = $_POST['id'];
    $variableCost = $_POST['variableCost'];
    $variableCostDescription = $_POST['variableCostDescription'];
    $wasteTypeName = $_POST['wasteTypeName'];

//  var_dump($bagColor);
// var_dump($capacity);
// var_dump($description);
// var_dump($id);
// var_dump($variableCost);
// var_dump($variableCostDescription);
// var_dump($wasteTypeName);
// exit;


    $edit_waste_result = editWasteType($bagColor,$capacity,$description,$id,$variableCost,$variableCostDescription,$wasteTypeName);
    $edit_waste_data = json_decode($edit_waste_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_waste_data);
    exit;
}

//Delete Ind Customer 
if (isset($_POST['delete_customer'])) {
    $type = 'deleteCustomer';
    $iUserId = $_POST['customerId'];


    // var_dump($iUserId);
    // exit;

    $delete_customer_result = deleteCustomer($type,$iUserId);
    echo json_encode($delete_customer_result);
    exit;
}

//Delete Co Customer 
if (isset($_POST['delete_cocustomer'])) {
    $type = 'deleteCustomer';
    $iUserId = $_POST['customerId'];


    // var_dump($iUserId);
    // exit;

    $delete_cocustomer_result = deleteCoCustomer($type,$iUserId);
    echo json_encode($delete_cocustomer_result);
    exit;
}

//add admin
if (isset($_POST['add_admin'])) {
    $type = 'createUser';
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $VEmail = $_POST['VEmail'];
    $phoneNumber = $_POST['phoneNumber'];
    $password = $_POST['password'];

    

    // var_dump($firstname);
    // var_dump($lastname);
    // var_dump($VEmail);
    // var_dump($phoneNumber);
    // var_dump($password);
    // exit;

    $add_admin_result = addAdmin($type,$firstname,$lastname,$VEmail,$phoneNumber,$password);
    echo json_encode($add_admin_result);
    exit;
}

//add individual customer
if (isset($_POST['add_customer'])) {
    $type = 'customerRegistration';
    $vEmail = $_POST['vEmail'];
    $vFirstName = $_POST['vFirstName'];
    $vPhone = $_POST['vPhone'];
    $vInviteCode = "invite";
    $vPassword = $_POST['vPassword'];
    $vLastName = $_POST['vLastName'];
    $phoneCode = "+263";
    $countryCode = "ZWL";
    $vLang = "Eng";
    $userType = "Customer";
    $address = $_POST['address'];


    // var_dump($vEmail);
    // var_dump($vFirstName);
    // var_dump($vPhone);
    // var_dump($phoneNumber);
    // var_dump($vPassword);

    // var_dump($vLastName);
    // var_dump($address);
    // exit;

    $add_customer_result = addCustomer($type,$vEmail,$vFirstName,$vPhone,$vInviteCode,$vPassword,$vLastName,$phoneCode,$countryCode,$vLang,$userType,$address);
    echo json_encode($add_customer_result);
    exit;
}

//edit individual customer
if (isset($_POST['edit_customer'])) {
    $type = 'updateCustomer';
    $customerId = $_POST['customerId'];
    $vEmail = $_POST['vEmail'];
    $vFirstName = $_POST['vFirstName'];
    $vPhone = $_POST['vPhone'];
    $vInviteCode = "invite";
    $vPassword = $_POST['vPassword'];
    $vLastName = $_POST['vLastName'];
    $phoneCode = "+263";
    $countryCode = "ZWL";
    $vLang = "Eng";
    $userType = "Customer";
    $address = $_POST['address'];
    $vCurrency = "ZWL";



    // var_dump($vEmail);
    // var_dump($vFirstName);
    // var_dump($vPhone);
    // var_dump($vPassword);

    // var_dump($vLastName);
    // var_dump($address);
    // exit;

    $edit_customer_result = editIndiCustomer($vCurrency,$customerId,$type,$vEmail,$vFirstName,$vPhone,$vInviteCode,$vPassword,$vLastName,$phoneCode,$countryCode,$vLang,$userType,$address);
    echo json_encode($edit_customer_result);
    exit;
}

//add co customer
if (isset($_POST['add_cocustomer'])) {
    $type = 'companyRegistration';
    $name = $_POST['name'];
    $vCaddress = $_POST['vCaddress'];
    $vEmail = $_POST['vEmail'];
    $vPhone = $_POST['vPhone'];
    $vPassword = $_POST['vPassword'];
    $phoneCode = "+263";
    $countryCode = "ZWL";
    $zimraBPNumber = $_POST['zimraBPNumber'];
    $userType = "Customer";


    // var_dump($vEmail);
    // var_dump($vFirstName);
    // var_dump($vPhone);
    // var_dump($phoneNumber);
    // var_dump($vPassword);

    // var_dump($vLastName);
    // var_dump($address);
    // exit;

    $add_cocustomer_result = addCoCustomer($type,$name,$vCaddress,$vEmail,$vPhone,$vPassword,$phoneCode,$countryCode,$zimraBPNumber,$userType);
    echo json_encode($add_cocustomer_result);
    exit;
}
//edit co customer
if (isset($_POST['edit_cocustomer'])) {
    $type = 'updateSME';
    $customerId = $_POST['customerId'];
    $name = $_POST['name'];
    $vCaddress = $_POST['vCaddress'];
    $vEmail = $_POST['vEmail'];
    $vPhone = $_POST['vPhone'];
    // $vPassword = $_POST['vPassword'];
    $phoneCode = "+263";
    $countryCode = "ZWL";
    $zimraBPNumber = $_POST['zimraBPNumber'];
    $userType = "Customer";
    // var_dump($customerId);
    // var_dump($vEmail);
    // var_dump($name);
    // var_dump($vPhone);
    // var_dump($vPassword);

    // var_dump($zimraBPNumber);
    // var_dump($vCaddress);
    // exit;

    $edit_cocustomer_result = editCoCustomer($customerId,$type,$name,$vCaddress,$vEmail,$vPhone,$phoneCode,$countryCode,$zimraBPNumber,$userType);
    echo json_encode($edit_cocustomer_result);
    exit;
}
//edit admin
if (isset($_POST['edit_admin'])) {
    $type = 'updateUser';
    $id = $_POST['id'];
    $firstname = $_POST['firstname'];
    $lastname = $_POST['lastname'];
    $VEmail = $_POST['VEmail'];
    $phoneNumber = $_POST['phoneNumber'];
    // $password = $_POST['password'];

    
    // var_dump($id);
    // var_dump($firstname);
    // var_dump($lastname);
    // var_dump($VEmail);
    // var_dump($phoneNumber);
    // var_dump($password);
    // exit;

    $edit_admin_result = editAdmin($type,$id,$firstname,$lastname,$VEmail,$phoneNumber);
    echo json_encode($edit_admin_result);
    exit;
}


//edit  Customer Email
if (isset($_POST['edit_email'])) {
    $oldEmail = $_POST['oldEmail'];
    $newEmail = $_POST['newEmail'];

    // var_dump($oldEmail);
    // var_dump($newEmail);
    // exit;


    $customer_edit_email_result = editCustomerEmail($oldEmail,$newEmail);
    $customer_edit_email_data = json_decode($customer_edit_email_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($customer_edit_email_data);
    exit;
}

//add currency
if (isset($_POST['add_currency'])) {
    $name = strtoupper($_POST['name']);
    $symbol = $_POST['symbol'];
    $code = $_POST['code'];


    $add_currency_result = addCurrency($code,$name,$symbol);
    $add_currency_data = json_decode($add_currency_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_currency_data);
    exit;
}

//edit currency
if (isset($_POST['edit_currency'])) {
    $name = strtoupper($_POST['name']);
    $symbol = $_POST['symbol'];
    $code = $_POST['code'];
    $currencyId = $_POST['currencyId'];
    $status = $_POST['status'];
    // $exchangeRate = $_POST['exchangeRate'];

    // var_dump($name);
    // var_dump($symbol);
    // var_dump($currencyId);
    // var_dump($status);
    // var_dump($exchangeRate);
    // exit;

    $edit_currency_result = editCurrency($code,$name,$symbol,$currencyId,$status);
    $edit_currency_data = json_decode($edit_currency_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_currency_data);
    exit;
}
//quotation
if (isset($_POST['create_quotation'])) {
    $wasteType = $_POST['wasteType'];
    $customerId = $_POST['customerId'];
    $recurrence = $_POST['recurrence'];
    $ibookingId = $_POST['ibookingId'];

    $create_quotation_result = createQuotation($wasteType,$customerId,$recurrence);
    $create_quotation_data = json_decode($create_quotation_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($create_quotation_data);
    exit;
}



//assign driver to booking
if (isset($_POST['assign_driver'])) {
    $ibookingId = $_POST['ibookingId'];
    $driverId = $_POST['driverId'];
    $ifranchiseId = $_POST['ifranchiseId'];

    // var_dump($ibookingId);
    // var_dump($driverId);
    //  var_dump($ifranchiseId);
    // exit;
   
    $assign_driver_result = assignDriver($ibookingId,$driverId,$ifranchiseId);
    $assign_driver_data = json_decode($assign_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($assign_driver_data);
    exit;
}

//assign driver to franchise
if (isset($_POST['assign_driver_franchise'])) {

    $idriverId = $_POST['idriverId'];
    $franchiseId = $_POST['franchiseId'];
  

    // var_dump($franchiseId);
    // var_dump($idriverId);
    // exit;
   
    $assign_driver_to_franchise_result = assignDriverToFranchise($franchiseId,$idriverId);
    $assign_driver_to_franchise_data = json_decode($assign_driver_to_franchise_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($assign_driver_to_franchise_data);
    exit;
}

//re-assign driver to booking
if (isset($_POST['reassign_driver'])) {
    $ibookingId = $_POST['ibookingId'];
    $driverId = $_POST['driverId'];


    // var_dump($ibookingId);
    // var_dump($driverId);
    // exit;
   
    $re_assign_driver_result = reAssignDriver($ibookingId,$driverId);
    $re_assign_driver_data = json_decode($re_assign_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($re_assign_driver_data);
    exit;
}

//add collection type
if (isset($_POST['add_collection_type'])) {
    $collectionType = $_POST['collectionType'];
    $unit = $_POST['unit'];
    $status = $_POST['status'];

    $add_collection_type_result = addCollectionType($collectionType,$unit,$status);
    $add_collection_type_data = json_decode($add_collection_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_collection_type_data);
    exit;
}

//edit collection type
if (isset($_POST['edit_collection'])) {
    $collectionType = $_POST['collectionType'];
    $unit = $_POST['unit'];
    $status = $_POST['status'];
    $id = $_POST['id'];

    // var_dump($collectionType);
    // var_dump($unit);
    // var_dump($status);
    // var_dump($id);
    // exit;

    $edit_collection_type_result = updateCollectionType($collectionType,$id,$status,$unit);
    $edit_collection_type_data = json_decode($edit_collection_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_collection_type_data);
    exit;
}
//add collection sub type
if (isset($_POST['add_sub_type'])) {
    $collectionType = $_POST['collectionType'];
    $costPerUnitUSD = $_POST['costPerUnitUSD'];
    $costPerUnitZWL = $_POST['costPerUnitZWL'];
    $message = htmlspecialchars($_POST['message']);
    if($_POST['quantity'][1] == ""){
        $quantity = $_POST['quantity'][0];
    }else{
        $quantity = $_POST['quantity'][1];
    }

    $imaxQty = 1;
    $ratePerKm = $_POST['ratePerKm'];
    $vshow = "vshow";
    $vsubType = $_POST['vsubType'];
    $vunit = $_POST['vunit'];
    $quotationValidityDays = $_POST['quotationValidityDays'];

    // var_dump($collectionType);
    // var_dump($costPerUnitUSD);
    // var_dump($costPerUnitZWL);
    // // var_dump($quotationValidityDays);
    // var_dump($quantity);
    // exit;
 

    $add_sub_type_result = addSubType($message,$quantity,$costPerUnitUSD,$costPerUnitZWL,$collectionType,$imaxQty,$ratePerKm,$vshow,$vsubType,$vunit,$quotationValidityDays);
    $add_sub_type_data = json_decode($add_sub_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_sub_type_data);
    exit;
}

//edit collection sub type
if (isset($_POST['edit_sub_type'])) {
    $costPerUnitUSD = $_POST['costPerUnitUSD'];
    $costPerUnitZWL = $_POST['costPerUnitZWL'];
    $message = htmlspecialchars($_POST['message']);
    if($_POST['quantity_value'][1] == ""){
        $quantity = $_POST['quantity_value'][0];
    }else{
        $quantity = $_POST['quantity_value'][1];
    }
    $estatus = $_POST['estatus'];
    $id = $_POST['id'];
    $imaxQty = 1;
  
    $ratePerKm = $_POST['ratePerKm'];
    $collectionType = $_POST['collectionType'];
    $vshow = "vshow";
    $vsubType = $_POST['vsubType'];
    $vunit = $_POST['vunit'];
    $quotationValidityDays = $_POST['quotationValidityDays'];

    // var_dump($costPerUnitUSD);
    // var_dump($costPerUnitZWL);
    // var_dump($id);
    // var_dump($message);
    // var_dump($estatus);
    // // var_dump($collectionType);
    // // var_dump($quotationValidityDays);
    // // var_dump($vsubType);
    // // var_dump($quantity);
    // exit;

    $edit_sub_type_result = editSubType($message,$quantity,$costPerUnitZWL,$costPerUnitUSD,$estatus,$id,$imaxQty,$ratePerKm,$collectionType,$vshow,$vsubType,$vunit,$quotationValidityDays);
    $edit_sub_type_data = json_decode($edit_sub_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_sub_type_data);
    exit;
}

//add city
if (isset($_POST['add_city'])) {
    $cityName = $_POST['cityName'];
    $country = $_POST['country'];
    $province = $_POST['province'];
    $countryCode = "+263";
 
    $add_city_result = addCity($cityName,$country,$province,$countryCode);
    $add_city_data = json_decode($add_city_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_city_data);
    exit;
}
//edit city
if (isset($_POST['edit_city'])) {
    $cityName = $_POST['cityName'];
    $country = $_POST['country'];
    $countryCode = "+263";
    $province = $_POST['province'];
    $id = $_POST['id'];

    $edit_city_result = editCity($cityName,$country,$countryCode,$province,$id);
    $edit_city_data = json_decode($edit_city_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_city_data);
    exit;
}

//add franchise
if (isset($_POST['add_partner'])) {
    $address = $_POST['address'];
    $address2 = $_POST['address2'];
    $city = $_POST['cityId'];
    $code = "+263";
    $company = $_POST['company'];

    $country = $_POST['country'];
    $email = $_POST['email'];
    $language = $_POST['language'];

    $latitude = $_POST['franchiseLatitude'];
    $longitude = $_POST['franchiseLongitude'];

    $password = $_POST['password'];

    $phoneNumber = $_POST['phoneNumber'];
    $province = $_POST['province'];
    $state = $_POST['state'];
    $zimraBpNumber = $_POST['zimraBpNumber'];

    // var_dump($address);
    // var_dump($address2);
    // var_dump($city);
    // var_dump($company);
    // var_dump($country);
    // var_dump($email);
    // var_dump($language);
    // var_dump($longitude);
    // var_dump($latitude);
    // var_dump($longitude);
    // var_dump($phoneNumber);
    // var_dump($province);
    // var_dump($state);
    // var_dump($zimraBpNumber);
    // exit;
 
    $add_partner_result = addPartner($address,$address2,$city,$code,$company,$country,$email,$language,$lattitude,$longitude,$password,$phoneNumber,$province,$state,$zimraBpNumber);
    $add_partner_data = json_decode($add_partner_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_partner_data);
    exit;
}

//create House Hold booking 
if (isset($_POST['create_house_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "HouseHoldCollection";
    $iquantity = strval($_POST['iquantity']);

    $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $iuserId = $_POST['iuserId'];
    // $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    // $vunit = "BAGS";
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $add_house_booking_result = addHouseBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType);
    $add_house_booking_data = json_decode($add_house_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_house_booking_data);
    exit;
}


//update House Hold booking 
if (isset($_POST['update_house_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "HouseHoldCollection";
    $iquantity = strval($_POST['iquantity']);

    $isubCollectionTypeId = "";
    $iuserId = $_POST['iuserId'];
    $ibookingId = $_POST['ibookingId'];
    // $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    // $vunit = $_POST['vunit'];
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $edit_house_booking_result = editHouseBooking($ibookingId,$collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType);
    $edit_house_booking_data = json_decode($edit_house_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_house_booking_data);
    exit;
}
//create Commercial  booking 
if (isset($_POST['create_com_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "CommercialCollection";;
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeIdString = $_POST['isubCollectionTypeId'];
    $myArray = explode('~', $isubCollectionTypeIdString);
    $isubCollectionTypeId =   $myArray[0];
    $iuserId = $_POST['iuserId'];
    // $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    // $vunit = "TONNAGE";
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($isubCollectionTypeId);
    // exit;
    $add_com_booking_result = addComBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType);
    $add_com_booking_data = json_decode($add_com_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_com_booking_data);
    exit;
}


//update Commercial  booking 
if (isset($_POST['update_com_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "CommercialCollection";;
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeIdString = $_POST['isubCollectionTypeId'];
    $myArray = explode('~', $isubCollectionTypeIdString);
    $isubCollectionTypeId =   $myArray[0];
    $iuserId = $_POST['iuserId'];
    $ibookingId = $_POST['ibookingId'];
    // $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    // $vunit = $_POST['vunit'];
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $edit_com_booking_result = editComBooking($ibookingId,$collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType);
    $edit_com_booking_data = json_decode($edit_com_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_com_booking_data);
    exit;
}
//create HouseHold Adhoc  booking 
if (isset($_POST['create_house_ad_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "HouseHoldAdHocCollection";
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $iuserId = $_POST['iuserId'];
    // $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    // $vunit = "BAGS";
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $add_house_ad_booking_result = addHouseAdBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType);
    $add_house_ad_booking_data = json_decode($add_house_ad_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_house_ad_booking_data);
    exit;
}

//update HouseHold Adhoc  booking 
if (isset($_POST['update_house_ad_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "HouseHoldAdHocCollection";
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $iuserId = $_POST['iuserId'];
    $ibookingId = $_POST['ibookingId'];
    // $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    // $vunit = $_POST['vunit'];
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $edit_house_ad_booking_result = editHouseAdBooking($ibookingId,$collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType);
    $edit_house_ad_booking_data = json_decode($edit_house_ad_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_house_ad_booking_data);
    exit;
}


//create Commercial Adhoc  booking 
if (isset($_POST['create_com_ad_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "CommercialAdhocCollection";
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $iuserId = $_POST['iuserId'];
    // $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    // $vunit = "TONNAGE";
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $add_com_ad_booking_result = addComAdBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType);
    $add_com_ad_booking_data = json_decode($add_com_ad_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_com_ad_booking_data);
    exit;
}

//update Commercial Adhoc  booking 
if (isset($_POST['update_com_ad_booking'])) {
    $originalDate = $_POST['collectionDate'];
    $newDate = date("d/m/Y", strtotime($originalDate));
    $time = $_POST['collectionTime'];
    $collectionDate =  $newDate ." ". date("H:i", strtotime($time));
    $currencyCode = $_POST['currencyCode'];
    $collectionType = "CommercialAdhocCollection";
    $iquantity = strval($_POST['iquantity']);

    // $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $isubCollectionTypeId = $_POST['isubCollectionTypeId'];
    $iuserId = $_POST['iuserId'];
    $ibookingId = $_POST['ibookingId'];
    // $reccurence = $_POST['reccurence'];
    $vcollectionsRequestAddress = $_POST['vcollectionsRequestAddress'];
    $vcollectionsRequestLat = $_POST['vcollectionsRequestLat'];
    $vcollectionsRequestLong = $_POST['vcollectionsRequestLong'];
    // $vunit = $_POST['vunit'];
    $wasteType = $_POST['wasteType'];

    // var_dump($collectionDate);
    // var_dump($currencyCode);
    // var_dump($collectionType);
    // var_dump($iquantity);
    // var_dump($isubCollectionTypeId);
    // var_dump($iuserId);
    // var_dump($reccurence);
    // var_dump($vcollectionsRequestAddress);
    // var_dump($vunit);
    // var_dump($vcollectionsRequestLat);
    // var_dump($vcollectionsRequestLong);
    //  var_dump($vcollectionsRequestLong);
    //  var_dump($wasteType);
    // exit;
    $edit_com_ad_booking_result = editComAdBooking($ibookingId,$collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType);
    $edit_com_ad_booking_data = json_decode($edit_com_ad_booking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_com_ad_booking_data);
    exit;
}

//add vehicle
if (isset($_POST['add_vehicle'])) {
    $capacity = $_POST['capacity'];
    $franchiseId = $_POST['franchiseId'];
    $licensePlateNumber = $_POST['licensePlateNumber'];
    $make = $_POST['make'];

    $model = $_POST['model'];
    $vehicleStatus = $_POST['vehicleStatus'];
    $year = "2022-02-14T05:59:00.196Z";

    $add_vehicle_result = addVehicle($capacity,$franchiseId,$licensePlateNumber,$make,$model,$vehicleStatus,$year);
    $add_vehicle_data = json_decode($add_vehicle_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_vehicle_data);
    exit;
}

//eco ZWL Payment 
if (isset($_POST['eco_payment'])) {
    $ecocashNumber = $_POST['ecocashNumber'];
    $amount = $_POST['zwlprice'];
    $currencyCode = "ZWL";
    $paidBy = $_POST['paidBy'];

    $paymentMethod = "ECOCASH";
    $bookingId = $_POST['bookingId'];

    // var_dump($ecocashNumber);
    // var_dump($ecocashNumber);
    // var_dump($amount);
    // var_dump($currencyId);
    // var_dump($paidBy);
    // var_dump($paymentMethod);
    // var_dump($bookingId);
    // exit;
  
    $eco_payment_result = ecoZWLPayment($currencyCode,$ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId);
    $eco_payment_data = json_decode($eco_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($eco_payment_data);
    exit;
}
//eco USD Payment 
if (isset($_POST['eco_usd_payment'])) {
    $ecocashNumber = $_POST['ecocashNumber'];
    $amount = $_POST['fprice'];
    $currencyCode = "USD";
    $paidBy = $_POST['paidBy'];

    $paymentMethod = "ECOCASH";
    $bookingId = $_POST['bookingId'];

    // var_dump($ecocashNumber);
    // var_dump($ecocashNumber);
    // var_dump($amount);
    // var_dump($currencyId);
    // var_dump($paidBy);
    // var_dump($paymentMethod);
    // var_dump($bookingId);
    // exit;
  
    $eco_payment_result = ecoUSDPayment($currencyCode,$ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId);
    $eco_payment_data = json_decode($eco_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($eco_payment_data);
    exit;
}
//cash ZWL Payment 
if (isset($_POST['cash_payment'])) {
    $ecocashNumber = "";
    $amount = $_POST['zwlprice'];
    $currencyCode = "ZWL";
    $paidBy = $_POST['paidBy'];

    $paymentMethod = "CASH";
    $bookingId = $_POST['bookingId'];

    // var_dump($ecocashNumber);
    // var_dump($amount);
    // var_dump($currencyId);
    // var_dump($paidBy);
    // exit;
  
    $cash_payment_result = cashZWLPayment($currencyCode,$ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId);
    $cash_payment_data = json_decode($cash_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($cash_payment_data);
    exit;
}

//cash USD Payment 
if (isset($_POST['cash_usd_payment'])) {
    $ecocashNumber = "";
    $amount = $_POST['fprice'];
    $currencyCode = "USD";
    $paidBy = $_POST['paidBy'];

    $paymentMethod = "CASH";
    $bookingId = $_POST['bookingId'];

    // var_dump($ecocashNumber);
    // var_dump($amount);
    // var_dump($currencyId);
    // var_dump($paidBy);
    // exit;
  
    $cash_payment_result = cashUSDPayment($currencyCode,$ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId);
    $cash_payment_data = json_decode($cash_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($cash_payment_data);
    exit;
}


//Upload Proof Of  Payment 
if (isset($_POST['proof_of_payment'])) {
    $reference = $_POST['reference'];
    $bookingId = $_POST['bookingId'];
    $file_name = $_FILES['myfile']['name'];
    $filedata = $_FILES['myfile']['tmp_name'];
    $filesize = $_FILES['myfile']['size'];
    $filetype = $_FILES['myfile']['type'];


    // var_dump($reference);
    // var_dump($bookingId);
    // var_dump($file_name);
    // exit;
  
    $proof_of_payment_result =  uploadProofOfPayment($reference,$bookingId,$filedata,$filetype,$file_name);
    $proof_of_payment_data = json_decode($proof_of_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($proof_of_payment_data);
    exit;
}

//Upload Documents 
if (isset($_POST['upload_documents'])) {
    $userId = $_POST['userId'];
    $nationalId_name = $_FILES['nationalId']['name'];
    $nationalId_data = $_FILES['nationalId']['tmp_name'];
    $nationalId_type = $_FILES['nationalId']['type'];

    $proofOfResidence_name = $_FILES['proofOfResidence']['name'];
    $proofOfResidence_data = $_FILES['proofOfResidence']['tmp_name'];
    $proofOfResidence_type = $_FILES['proofOfResidence']['type'];

    $bankStatement_name = $_FILES['bankStatement']['name'];
    $bankStatement_data = $_FILES['bankStatement']['tmp_name'];
    $bankStatement_type = $_FILES['bankStatement']['type'];

    $tradeLicense_name = $_FILES['tradeLicense']['name'];
    $tradeLicense_data = $_FILES['tradeLicense']['tmp_name'];
    $tradeLicense_type = $_FILES['tradeLicense']['type'];

    // var_dump($bankStatement_name);
    // var_dump($bankStatement_data);
    // var_dump($bankStatement_type);
    // exit;
  
    $upload_documents_result =  uploadDocuments(
    $userId,
   $nationalId_data,$nationalId_type,$nationalId_name,
   $proofOfResidence_data,$proofOfResidence_type,$proofOfResidence_name,
   $bankStatement_data,$bankStatement_type,$bankStatement_name,
   $tradeLicense_data,$tradeLicense_type,$tradeLicense_name
);

// var_dump($upload_documents_result);
// exit;
    $upload_documents_data = json_decode($upload_documents_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($upload_documents_data);
    exit;
}




//approve cash Payment 
if (isset($_POST['approve_user_payment'])) {
    $paymentId = $_POST['paymentId'];
    $paymentStatus = "PAID";
   

    // var_dump($paymentId);
    // var_dump($paymentStatus);
    // exit;
  
    $approve_cash_payment_result = approvePayment($paymentId,$paymentStatus);
    $approve_cash_payment_data = json_decode($approve_cash_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($approve_cash_payment_data);
    exit;
}

//approve driver 
if (isset($_POST['approve_driver'])) {
    $IDriverId = $_POST['IDriverId'];

    // var_dump($IDriverId);
    // exit;
    $approve_driver_result = approveDriver($IDriverId);
    $approve_driver_data = json_decode($approve_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($approve_driver_data);
    exit;
}

//download file 
if (isset($_POST['download_file'])) {
    $document_type = $_POST['document_type'];
    $userId = $_POST['userId'];
    $userType = "Driver";

    // var_dump($document_type);
    // var_dump($userId);
    // exit;
    $driver_file_result = downloadDriverFile($userId,$document_type,$userType);
    $base64 = base64_encode($driver_file_result);
    // echo json_encode(array('id' => $base64));
    echo json_encode($base64);

}




//add driver
if (isset($_POST['add_driver'])) {
    $type = "franchiseDriverSignup";
    $vfirstName = $_POST['vfirstName'];
    $vlastName = $_POST['vlastName'];
    $ifranchiseId = $_POST['ifranchiseId'];

    $vemail = $_POST['vemail'];
    $vphone = $_POST['vphone'];
    $caddress = $_POST['caddress'];
    $caddress2 = $_POST['caddress2'];
    $egender = $_POST['egender'];
    $phoneCode = "+263";
    $countryCode = "ZWL";
    $vpassword = $_POST['vpassword'];

    $add_driver_result = addDriver($type,$vfirstName,$vlastName,$ifranchiseId,$vemail,$vphone,$caddress,$caddress2,$egender,$phoneCode,$countryCode,$vpassword);
    $add_driver_data = json_decode($add_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_driver_data);
    exit;
}


//add payment method
if (isset($_POST['add_payment_method'])) {
    $description = strtoupper($_POST['description']);

    $add_payment_method_result = addPaymentMethod($description);
    $add_payment_method_data = json_decode($add_payment_method_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_payment_method_data);
    exit;
}

//assign dr
if (isset($_POST['add_payment_method'])) {
    $description = strtoupper($_POST['description']);

    $add_payment_method_result = addPaymentMethod($description);
    $add_payment_method_data = json_decode($add_payment_method_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_payment_method_data);
    exit;
}


//edit driver
if (isset($_POST['edit_driver'])) {
    $driverId = $_POST['driverId'];;
    $vname = $_POST['vname'];
    $vlastName = $_POST['vlastName'];
    $vemail = $_POST['vemail'];

    $vphone = $_POST['vphone'];
    $vcaddress = $_POST['vcaddress'];

    $vcadress2 = $_POST['vcadress2'];
    $egender = $_POST['egender'];
    $franchiseId = $_POST['franchiseId'];
    $status = $_POST['status'];
  
    $vpassword = $_POST['vpassword'];

    // var_dump($driverId);
    // var_dump($vname);
    // var_dump($vlastName);
    // var_dump($vemail);
    // var_dump($vphone);
    // var_dump($vcaddress);
    // var_dump($vcadress2);
    // var_dump($egender);
    // var_dump($franchiseId);
    // var_dump($status);
    // exit;

    $edit_driver_result = editDriver($driverId,$vname,$vlastName,$vemail,$vphone,$vcaddress,$vcadress2,$egender,$franchiseId,$status,$vpassword);
    $edit_driver_data = json_decode($edit_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_driver_data);
    exit;
}

//edit franchise
if (isset($_POST['edit_partner'])) {
    $address = $_POST['address'];;
    $address2 = $_POST['address2'];
    $city = $_POST['cityId'];
    $code = "+263";

    $company = $_POST['company'];
    $country = $_POST['country'];

    $email = $_POST['email'];
    $franchiseId = $_POST['franchiseId'];
    $language = $_POST['language'];


    $latitude = $_POST['franchiseLatitude'];
    $longitude = $_POST['franchiseLongitude'];
    $password = $_POST['password'];
    
    $status = $_POST['status'];
  
    $vpassword = $_POST['vpassword'];
    $phoneNumber = $_POST['phoneNumber'];
  
    $province = $_POST['province'];

    $ratePerKM = $_POST['ratePerKM'];
  
    $registrationDate = $_POST['registrationDate'];
    $state = $_POST['state'];
    $status = $_POST['status'];
    $zimra_BP_Number = $_POST['zimra_BP_Number'];


    // var_dump($address);
    // var_dump($address2);
    // var_dump($city);
    // var_dump($code);
    // var_dump($company);
    // var_dump($country);
    // var_dump($email);
    // var_dump($franchiseId);
    // var_dump($language);
    // var_dump($latitude);
    // var_dump($longitude);
    // var_dump($phoneNumber);
    // var_dump($province);

    // var_dump($ratePerKM);
    // var_dump($registrationDate);
    // var_dump($state);
    // var_dump($status);
    // var_dump($zimra_BP_Number);
    // exit;

    $edit_partner_result = editPartner($address,$address2,$city,$code,$company,$country,$email,$franchiseId,$language,$latitude,$longitude,$password,$phoneNumber,$province,$ratePerKM,$registrationDate,$state,$status,$zimra_BP_Number);
    $edit_partner_data = json_decode($edit_partner_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_partner_data);
    exit;
}

//edit vehicle
if (isset($_POST['edit_vehicle'])) {
    $capacity = $_POST['capacity'];;
    $franchiseId = $_POST['franchiseId'];
    $licensePlateNumber = $_POST['licensePlateNumber'];

    $make = $_POST['make'];
    $model = $_POST['model'];

    $vehicleId = $_POST['vehicleId'];
    $vehicleStatus = $_POST['vehicleStatus'];
    $originalDate = $_POST['year'];
    $year = date("d/m/Y", strtotime($originalDate));
    // $year = $_POST['year'];

  
    // var_dump($capacity);
    // var_dump($franchiseId);
    // var_dump($licensePlateNumber);
    // var_dump($make);
    // var_dump($model);
    // var_dump($vehicleId);
    // var_dump($vehicleStatus);
    // var_dump($year);
    // exit;

    $edit_vehicle_result = editVehicle($capacity,$franchiseId,$licensePlateNumber,$make,$model,$vehicleId,$vehicleStatus,$year);
    $edit_vehicle_data = json_decode($edit_vehicle_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_vehicle_data);
    exit;
}

//edit payment method
if (isset($_POST['edit_payment_method'])) {
    $description = strtoupper($_POST['description']);
    $paymentMethodId = $_POST['paymentMethodId'];
    $status = $_POST['status'];

    // var_dump($description);
    // var_dump($paymentMethodId);
    // var_dump($status);
    // exit;

    $edit_payment_method_result = editPaymentMethod($description,$paymentMethodId,$status);
    $edit_payment_method_data = json_decode($edit_payment_method_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_payment_method_data);
    exit;
}

//start trip
if (isset($_POST['start_trip'])) {
    $ibookingId = $_POST['ibookingId'];
    $start_trip_result = startTrip($ibookingId);
    $start_trip_data = json_decode($start_trip_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($start_trip_data);
    exit;
}
//approve booking cancellation by driver 
if (isset($_POST['approve_cancel_booking'])) {
    $bookingId = $_POST['bookingId'];
    $userId = $_POST['userId'];

    // var_dump($bookingId);
    // var_dump($userId);
    // exit;

    $approve_booking_cancel_result = approveBookingCancellation($bookingId,$userId);
    $approve_booking_cancel_data = json_decode($approve_booking_cancel_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($approve_booking_cancel_data);
    exit;
}

//approve customer
if (isset($_POST['approve_customer'])) {
    $status = "ACTIVE";
    $userId  = $_POST['userId'];

    // var_dump($bookingId);
    // var_dump($userId);
    // exit;

    $approve_customer_result = approveCustomer($userId,$status);
    $approve_customer_data = json_decode($approve_customer_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($approve_customer_data);
    exit;
}



//end trip
if (isset($_POST['end_trip'])) {
    $ibookingId = $_POST['ibookingId'];
  
    // var_dump($ibookingId);
    // exit;

    $end_trip_result = endTrip($ibookingId);
    $end_trip_data = json_decode($end_trip_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($end_trip_data);
    exit;
}
    //user login

if (isset($_POST['user_login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $user_login_result = userLogin($email,$password);
    $user_login_data_data = json_decode($user_login_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($user_login_data_data);
    exit;
}

//customer forgot password
if (isset($_POST['customer-forgot-password'])) {
    $vEmail  = trim($_POST['vEmail']);
    $userType  = "Customer";
    $type = 'forgotPassword';

    $forgot_password_result = customerForgotPassword($type,$vEmail,$userType);
    echo json_encode($forgot_password_result);
    exit;
}

//customer reset password
if (isset($_POST['customer-reset-password'])) {

    $token = trim($_POST['token']);
    $newPassword = trim($_POST['newPassword']);
    $userType  = "Customer";
    $type = 'resetPassword';

    // var_dump($token);
    // var_dump($newPassword);
    // exit;
    $reset_password_result = customerResetPassword($type,$token,$newPassword,$userType);
    echo json_encode($reset_password_result);
    exit;
}


//admin forgot password
if (isset($_POST['admin-forgot-password'])) {
    $vEmail  = trim($_POST['vEmail']);
    $userType  = "Admin";
    $type = 'forgotPassword';

    // var_dump($vEmail);
    //     exit;
        
    $forgot_password_result = adminForgotPassword($type,$vEmail,$userType);
    echo json_encode($forgot_password_result);
    exit;
}

//admin reset password
if (isset($_POST['admin-reset-password'])) {

    $token = trim($_POST['token']);
    $newPassword = trim($_POST['newPassword']);
    $userType  = "Admin";
    $type = 'resetPassword';

    // var_dump($token);
    // var_dump($newPassword);
    // exit;
    $reset_password_result = adminResetPassword($type,$token,$newPassword,$userType);
    echo json_encode($reset_password_result);
    exit;
}




//franchise forgot password
if (isset($_POST['franchise-forgot-password'])) {
    $email  = trim($_POST['email']);

    // var_dump($email);
    //     exit;
        
    $forgot_password_result = franchiseForgotPassword($email);
    $forgot_password_result_data = json_decode($forgot_password_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($forgot_password_result_data);
    exit;
}


//franchise forgot password
if (isset($_POST['franchise-reset-password'])) {
    $email  = trim($_POST['email']);
    $resetToken  = trim($_POST['resetToken']);
    $newpassword  = trim($_POST['newpassword']);

    // var_dump($email);
    // var_dump($resetToken);
    // var_dump($newpassword);
    // exit;
        
    $reset_password_result = franchiseResetPassword($email,$resetToken,$newpassword);
    $reset_password_result_data = json_decode($reset_password_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($reset_password_result_data);
    exit;
}

//Re-Assign Franchise
if (isset($_POST['re_assign_franchise'])) {
    $bookingId  = $_POST['bookingId'];
    $iFranchiseId  = $_POST['iFranchiseId'];

    var_dump($bookingId);
    var_dump($iFranchiseId);
    exit;
        
    $re_assign_franchise_result = reAssignFranchise($bookingId,$iFranchiseId);
    $re_assign_franchise_data = json_decode($re_assign_franchise_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($re_assign_franchise_data);
    exit;
}




    
    
    ?>
