<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
  $admin_id =  $_SESSION["admin_id"];
  $admin_phone  =$_SESSION["admin_vphone"];
  $admin_email =  $_SESSION["admin_Email"];
  $admin_firstname =  $_SESSION["first_name"];
  $admin_lastname = $_SESSION["last_name"];
require_once('includes/header.php');
include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
$bookings = json_decode(getBookingsByPaymentStatus("PAID"), true);
$currencies = json_decode(getCurrencies(), true);
// var_dump($bookings);
// exit;
?>
<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $company;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $company;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="portal/admin/user-profile-regular"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="portal/admin/user-profile-setting"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="portal/admin/user-profile-activity"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/partner/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->

                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Booking Report List</h3>
                                            
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                        <!-- <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="portal/partner/add-booking"><span>Add Booking</span></a></li> 
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li> -->
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                <div class="row">
                                <div class="col-md-6">
                                        <div class="form-group">
                                        <label class="form-label">Datepicker Range Filter</label>
                                        <div class="form-control-wrap">
                                        <div class="input-daterange date-picker-range input-group" >
                                        <input type="text"  id="datefilterfrom"  class="form-control" />
                                        <div class="input-group-addon">TO</div>
                                        <input type="text" id="datefilterto"  class="form-control"/>
                                        </div>
                                        </div>
                                        </div>
                                        </div>

                                        <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="form-label">Payment Method</label>
                                            <div class="form-control-wrap">
                                                <select  class="form-select"  id="dropdown1" data-placeholder="Select Payment Method" >
                                                <option value="">--All--</option>
                                                <option value="CASH" selected="selected">CASH</option>
                                                <option value="ECOCASH">ECOCASH</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="form-label">Collection Type</label>
                                            <div class="form-control-wrap">
                                                <select  class="form-select"  id="dropdown2" data-placeholder="Select Collection Type" >
                                                <option value="">--All--</option>
                                                <option value="HouseHold Collection" selected="selected">HouseHold Collection</option>
                                                <option value="Commercial Collection">Commercial Collection</option>
                                                <option value="HouseHold AdHoc Collection">HouseHold AdHoc Collection</option>
                                                <option value="Commercial Adhoc Collection">Commercial Adhoc Collection</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                     </div>
                                        </br>
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init-export table nowrap nk-tb-list nk-tb-ulist"  data-order='[[0, "desc"]]' id="admin_bookings"  data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                            <th class="nk-tb-col"><span class="sub-text">Booking Number</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Amount</span></th>
                                                            <!-- <th class="nk-tb-col"><span class="sub-text">Customer Name</span></th> -->
                                                            <!-- <th class="nk-tb-col tb-col-md"><span class="sub-text">Customer Phone</span></th> -->
                                                            <th class="nk-tb-col tb-col-mb"><span class="sub-text">Collection Address</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Quantity</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Booking Status</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Collection Type</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Payment Method</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Payment Status</span></th>
                                                            <th class="nk-tb-col tb-col-lg"><span class="sub-text">Collection Date</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($bookings['responseStatus'] == "SUCCESS") : ?>

                                                    <?php
                                                    foreach ($bookings['eobjResponse'] as $booking) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['ibookingId'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($booking['currencyCode'] == "ZWL"){
                                                            echo "ZWL"."".$booking['zwlprice'];
                                                            }else {
                                                             echo "USD"."".$booking['fprice'];
                                                            }
                                                        ?>
                                                    </span>
                                                </td>
                                                        <!-- <td class="nk-tb-col tb-col-md"><span><?= $booking['iuserId'] ?></span></td> -->
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['vcollectionsRequestAddress'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($booking['iquantity'] == "Two"){
                                                            echo "2T";
                                                            }elseif($booking['iquantity'] == "Three") {
                                                                echo "3T";
                                                            }elseif($booking['iquantity'] == "ThreeToFive") {
                                                                echo "3-5T";
                                                            }elseif($booking['iquantity'] == "SixToSeven") {
                                                                echo "6-7T";
                                                            }elseif($booking['iquantity'] == "EightToTen") {
                                                                echo "8-10T";
                                                            }elseif($booking['iquantity'] == 1) {

                                                                echo $booking['iquantity'].""."BAG";
                                                            }else {
                                                                echo $booking['iquantity'].""."BAGS";
                                                            }
                                                        ?>
                                                        </span>
                                                       </td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['estatus'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md">
                                                        <span>
                                                        <?php
                                                        if($booking['collectionType'] == "HouseHoldCollection"){
                                                            echo "HouseHold Collection";
                                                            }elseif($booking['collectionType'] == "CommercialCollection") {
                                                                echo "Commercial Collection";
                                                            }elseif($booking['collectionType'] == "HouseHoldAdHocCollection") {
                                                                echo "HouseHold AdHoc Collection";
                                                            }else {
                                                                echo "Commercial Adhoc Collection";
                                                            }
                                                        ?>
                                                        </span>
                                                       </td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['paymentMethod'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['paymentStatus'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $booking['requestedDate'] ?></span></td>
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                                            
                                                                <li>
                                                                    <div class="drodown">
                                                                        <a class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <?php
                                                                              if($booking['paymentStatus'] == "PAID"){
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";
                                                                                echo"<li><a  data-toggle='modal' data-target='#driver".$booking['ibookingId'] ."' ><em class='icon ni ni-edit'></em><span>View Driver</span></a></li>";

                                                                              }elseif($booking['paymentStatus'] == "PAID" && $booking['idriverId'] != "") {
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";
                                                                                echo"<li><a  data-toggle='modal' data-target='#driver".$booking['ibookingId'] ."' ><em class='icon ni ni-edit'></em><span>View Driver</span></a></li>";                         
                                                                               } else {                 
                                                                                echo"<li><a href='#' data-toggle='modal' data-target='#customer".$booking['ibookingId']."' ><em class='icon ni ni-edit'></em><span>View Customer</span></a></li>";                  

                                                                    
                                                                              }  
                                                                            ?>
                                                                         </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <!-- Re-Assign Booking-->
                                                    <div class="modal fade" tabindex="-1" role="dialog" class="assign_driver" id="assign<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Re-Assign Driver</h5>
                                                                <form id ="ecoAssignDriver<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Collection Address</label>
                                                                                <input type="text" class="form-control"  value="<?= $booking['vcollectionsRequestAddress'] ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Quantity</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['iquantity'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Booking Number</label>
                                                                                <input type="text" class="form-control" value="<?= $booking['ibookingId'] ?>" readonly>
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                        <div class="form-group">
                                                                            <label class="form-label">Driver</label>
                                                                            <div class="form-control-wrap">
                                                                            <select class="form-select" name ="driverId" data-placeholder="Select Driver" >
                                                                                <?php foreach ($franchiseDrivers['eobjResponse']  as $franchiseDriver): ?>
                                                                                    <option value="">Select Driver</option>
                                                                                    <option value="<?= $franchiseDriver["driverId"] ?>"><?= $franchiseDriver["name"]."". $franchiseDriver["surname"].""?></option>
                                                                                <?php endforeach; ?> 
                                                                                </select>
                                                                            </div>
                                                                        </div>
                                                                    </div>
                                                                     
                                                                     
                                                                        <input type="hidden" class="form-control" name ="ibookingId" value="<?= $booking['ibookingId'] ?>" >      
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                                <li>
                                                                                <input type="hidden" name="reassign_driver">
                                                                                 <button type="button" class="btn btn-primary" name ="reassign_driver" onClick="assignDriver('<?= $booking["ibookingId"] ?>')">Assign</button>
                                                                                </li>
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                <!-- Customer Details-->
                                                <div class="modal fade" tabindex="-1" role="dialog" class="customer" id="customer<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Customer Details</h5>
                                                                <form id ="ecoAssignDriver<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Name</label>
                                                      

                                                                                <input type="text" class="form-control" 
                                                                                 value="   <?php 
                                                                            if(json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['partnerType']== "Corporate"){
                                                                                echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['companyName'];
                                                                            }else {
                                                                            echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['vFirstName']." ".json_decode(getElogisticsCustomersById($quotation['iuserId']),true)['eobjResponse']['vLastName'];
                                                                            }
                                                                            ?>"
                                                                                 
                                                                            readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Phone</label>
                                              

                                                                                <input type="text" class="form-control" 
                                                                                 value="   <?php 
                                                                            if(json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['partnerType']== "Corporate"){
                                                                                echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['vPhone'];
                                                                            }else {
                                                                            echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['vPhone'];
                                                                            }
                                                                            ?>"
                                                                                 
                                                                            readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Email</label>
                                                                           

                                                                                <input type="text" class="form-control" 
                                                                                 value="   <?php 
                                                                            if(json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['partnerType']== "Corporate"){
                                                                                echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['VEmail'];
                                                                            }else {
                                                                            echo json_decode(getElogisticsCustomersById($booking['iuserId']),true)['eobjResponse']['VEmail'];
                                                                            }
                                                                            ?>"
                                                                                 
                                                                            readonly >
                                                                            </div>
                                                                        </div>
                                          
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                             
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                                <!-- Driver Details-->
                                                <div class="modal fade" tabindex="-1" role="dialog" class="driver" id="driver<?= $booking['ibookingId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Driver Details</h5>
                                                                <form id ="ecoAssignDriver<?= $booking['ibookingId'] ?>" class="mt-2">
                                                                    <div class="row g-gs">
                                                                   
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">First Name</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['name']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Last Name</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['surname']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Phone Number</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['phoneNo']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                                                        <div class="col-md-6">
                                                                            <div class="form-group">
                                                                                <label class="form-label" for="payment-name-add">Email</label>
                                                                                <input type="text" class="form-control"  value="<?= json_decode(getFranchiseDriversById($booking['idriverId']),true)['eobjResponse']['email']  ?>" readonly >
                                                                            </div>
                                                                        </div>
                                          
                                                                        <div class="col-12">
                                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                             
                                                                              
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </form>
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->
                                               
                            
                                                    <?php endforeach; ?>

                                                    <?php else : ?>

                                                    <?= $bookings['responseMessage'] ?>
                                                    <!-- <?php echo "No Bookings Available";?> -->

                                                    <?php endif; ?>

                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully re-assigned driver</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='assignDriverResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
   
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>


  <!--Start Datatable filter -->
   <script>
    $(document).ready(function() {
      var table = $('#admin_bookings').DataTable();

      table.column(6).data().search('CASH').draw();
      table.column(5).data().search('HouseHold Collection').draw();

      $('#dropdown1').on('change', function() {
        table.search(this.value).draw();
      });
      $('#dropdown2').on('change', function() {
        table.search(this.value).draw();
      });

      $('#datefilterfrom').on('change', function() {
        table.columns(8).search(this.value).draw();
      });

      $('#datefilterto').on('change', function() {
        table.columns(8).search(this.value).draw();
      });
    });
  </script>

  <script>
$('#datefilterfrom').datepicker({
format: 'dd/mm/yyyy',
clearBtn: true
});
$('#datefilterto').datepicker({
    format: 'dd/mm/yyyy',
    clearBtn: true
});
  </script>
  <script>
 $(function(){
        $("#datefilterto").datepicker({ dateFormat: 'dd/mm-yy' });
        $("#datefilterfrom").datepicker({ dateFormat: 'dd/mm-yy' }).bind("change",function(){
            var minValue = $(this).val();
            minValue = $.datepicker.parseDate("dd/mm-yy", minValue);
            minValue.setDate(minValue.getDate()+1);
            $("#to").datepicker( "option", "minDate", minValue );
        })
    });

  </script>
<!--End Datatable filter -->

   <script>
       $( document ).ready(function() {
	$(".export").click(function() {
		var export_type = $(this).data('export-type');		
		$('#example').tableExport({
			type : export_type,			
			escape : 'false',
			ignoreColumn: []
		});		
	});
});
   </script>

<script>
    //Assign Driver
    function assignDriver(ibookingId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#ecoAssignDriver'+ibookingId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('.assign_driver').modal('hide');
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/partner/all-bookings";
                    }, 2000);
                  
                } else {
                    $('.assignDriverResponse').empty();
                    $('.assignDriverResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('.assign_driver').modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".assignDriverResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.assignDriverResponse').empty();
                $('.assignDriverResponse').append(errorThrown);
                $('#spinner').hide();
                $('.assign_driver').modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".assignDriverResponse").html("");
                });
            }
        });
    }
    </script>
  
</body>

</html>