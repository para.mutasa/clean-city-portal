<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
$admin_id =  $_SESSION["admin_id"];
$admin_phone  =$_SESSION["admin_vphone"];
$admin_email =  $_SESSION["admin_Email"];
$admin_firstname =  $_SESSION["first_name"];
$admin_lastname = $_SESSION["last_name"];

include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');

$IDriverId = $_GET['IDriverId'];
$documents = json_decode(getDriverDocumentsById($IDriverId), true);
// var_dump($documents);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                      
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Documents Lists</h3>
                                            <div class="nk-block-des text-soft">
                                                <!-- <p>You have total 1 Currency.</p> -->
                                            </div>
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                    <li class="nk-block-tools-opt"><a href="#" data-toggle='modal'  data-target="#approve-driver" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Approve Driver</span></a></li>
 
                                                        <li class="nk-block-tools-opt">
                                                            <!-- <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                    <li><a href="#" data-toggle="modal" data-target="#add-currency"><span>Add Document</span></a></li>
                                                                    </ul>
                                                                </div>
                                                            </div> -->
                                                        </li>
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                            <th class="nk-tb-col"><span class="sub-text">Document Type</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Status</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                        </tr>
                                                    </thead>
 
                                                    <tbody>
                                                    <?php if ($documents['responseStatus'] == "SUCCESS") : ?>
                                                    <?php
                                                    foreach ($documents['eobjResponse'] as $document) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $document['document_type'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $document['creation_status'] ?></span></td>
                                                        
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                                            
                                                                <li>
                                                                    <div class="drodown">
                                                                        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <li><a href="#" data-toggle="modal" data-target="#download<?= $document['documentId'] ?>"><em class="icon ni ni-edit"></em><span>View Document</span></a></li>
                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                            <!-- Download File-->
                                            <div class="modal fade" tabindex="-1" role="dialog" id="download<?= $document['documentId'] ?>">
                                            <div class="modal-dialog modal-sm" role="document">
                                                <div class="modal-content">
                                                    <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                    <div class="modal-body modal-body-md">
                                                       
                                                        <div class="alert alert-icon alert-primary" role="alert"><em class="icon ni ni-alert-circle"></em><strong>View Driver Document</strong></div>
        
                                                        <form id ="download-file<?= $document['documentId'] ?>" class="mt-2">
                                                    <div class="row g-gs">
                                                        <input type="hidden" class="form-control"  name="document_type"  value="<?= $document['document_type'] ?>">
                                                        <input type="hidden" class="form-control"  name="userId"  value="<?= $document['userId'] ?>">
                                                        <div class="col-10">
                                                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                                                <li>
                                                                <input type="hidden" name="download_file" value="true">
                                                                <button type="button" class="btn btn-primary" name ="download_file"  onClick="downloadFile('<?= $document["documentId"] ?>')"  >Yes</button>
                                                                </li>
                                                                
                                                            </ul>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div><!-- .modal-body -->
                                            <div class="modal-footer bg-light">
                                        <span class="sub-text"></span>
                                    </div>
                                        </div><!-- .modal-content -->
                                    </div><!-- .modal-dialog -->
                                    </div><!-- .modal -->

                                    <?php endforeach; ?>

                                    <?php else : ?>

                                    <?= $documents['responseMessage'] ?>

                                    <?php endif; ?>

                                        
                                    </tbody>
                                </table>
                            </div>
                        </div><!-- .card-preview -->
                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->


          <!--Bar Code-->
          <div class="modal fade text-left" 
     id ="getImageModal" tabindex="-1" role="dialog"
        >
        <div class="modal-dialog modal-sm" role="document">
            <div class="modal-content">
                <div class="modal-header bg-blue white">
                    <label class="modal-title white"
                        id="myModalLabel33">Driver Document</label>
                    <button type="button" class="close" data-dismiss="modal"
                        aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <form id="generateqr">
                    <div class="modal-body">
                      <img width="380px" height="400px" id="myimage"/>
                    
                     </div>

                    <div class="modal-footer">
                        <button type="button"
                            class="btn grey btn-outline-secondary"
                            data-dismiss="modal">Close</button>
                        <input type="hidden" class="form-control"
                            name="generate_qr">
                    
                    </div>

                </form>
            </div>
        </div>
    </div>



     <!-- Approve Driver-->
     <div class="modal fade" tabindex="-1" role="dialog" id="approve-driver">
    <div class="modal-dialog modal-sm" role="document">
        <div class="modal-content">
            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
            <div class="modal-body modal-body-sm">
            <div class="alert alert-icon alert-primary" role="alert"><em class="icon ni ni-alert-circle"></em><strong>Approve Driver</strong></div>
                <!-- <h5 class="modal-title">Approve Driver</h5> -->
                <form id ="approve-driver-form" class="mt-2">
                    <div class="row g-gs">
                         <input type="hidden" class="form-control"  name="IDriverId"  value="<?php echo $IDriverId?>">
                        <div class="col-10">
                            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                <li>
                                <input type="hidden" name="approve_driver" value="true">
                                <button type="button" class="btn btn-primary" name ="approve_driver"  onClick="approveDriver('<?= $IDriverId ?>')" >Confirm</button>
                                </li>
                                
                            </ul>
                        </div>
                    </div>
                </form>
            </div><!-- .modal-body -->
            <div class="modal-footer bg-light">
        <span class="sub-text"></span>
    </div>
        </div><!-- .modal-content -->
    </div><!-- .modal-dialog -->
    </div><!-- .modal -->

      <!-- Success File Modal Alert -->
      <div class="modal fade" tabindex="-1" id="successFileAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully download file</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>

        <!--Fail  File  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="faileFileAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='downloadFileResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
   
 
    <!-- Success Edit Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully approved driver</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Edit Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failEditAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='approveDriverResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
   
 
       <!-- BEGIN: AJAX CALLS-->

    <script>
    //Approve Driver
    function approveDriver() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#approve-driver-form').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#approve-driver').modal('hide');
                    $("#successEditAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/drivers-list";
                    }, 2000);
                  
                } else {
                    $('.approveDriverResponse').empty();
                    $('.approveDriverResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('#approve-driver').modal('hide');
                    $("#failEditAlert").modal('show');
                    $("#failEditAlert").on("hidden.bs.modal", function() {
                        $(".approveDriverResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.approveDriverResponse').empty();
                $('.approveDriverResponse').append(errorThrown);
                $('#spinner').hide();
                $('#approve-driver').modal('hide');
                $("#failEditAlert").modal('show');
                $("#failEditAlert").on("hidden.bs.modal", function() {
                    $(".approveDriverResponse").html("");
                });
            }
        });
    }
    </script>

<script>
    //Download File
    function downloadFile(documentId) {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#download-file'+documentId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                console.log(response);
                // $(".getCode").html(response);
                $("#myimage").attr("src","data:image/png;base64," + json)
                // $('#myimage').append('<img src="data:image/png;base64,' + $response  '" />');
                $("#getImageModal").modal('show');
                $('#download'+documentId).modal('hide');
                
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $(".myimage").html("Please try again.");
            }
        });
    }
    </script>
   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>