<?php 
session_start();
include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
$cities = json_decode(getAllCities(), true);
$collection_types = json_decode(getAllCollectionTypes(), true);
$collection_sub_types = json_decode(getAllCollectionSubTypes(), true);
$currencies = json_decode(getCurrencies(), true);
$wastes = json_decode(getAllWaste(), true);
$customers = getAllCustomers();
// var_dump($customers);
// exit;
?>
<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
        
            <!-- sidebar @e -->
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="html/index.html" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <!-- <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div>
                                    </a>
                                </div>
                            </div> -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator">Abu Bin Ishityak</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text">Abu Bin Ishtiyak</span>
                                                        <span class="sub-text">info@softnio.com</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li>
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="#"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                                    <li class="dropdown notification-dropdown mr-n1">
                                        <a href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">
                                            <div class="icon-status icon-status-info"><em class="icon ni ni-bell"></em></div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-head">
                                                <span class="sub-title nk-dropdown-title">Notifications</span>
                                                <a href="#">Mark All as Read</a>
                                            </div>
                                            <div class="dropdown-body">
                                                <div class="nk-notification">
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                </div><!-- .nk-notification -->
                                            </div><!-- .nk-dropdown-body -->
                                            <div class="dropdown-foot center">
                                                <a href="#">View All</a>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Add Booking</h3>
                                
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
<div class="card card-bordered">
<div class="card-inner">
                                                         
                       
    <!-- <ul class="nav nav-tabs"> 
    <li class="nav-item"> 
       <a class="nav-link active" data-toggle="tab" href="#tabItem5"><span>Household Scheduled Collection</span></a> 
   </li>  
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem6"><span>Commercial Scheduled Collection</span></a> 
  </li>    
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem7"></em><span>Adhoc/ Once Off Collection</span></a> 
  </li>   
  </ul> -->
  <ul class="nav nav-tabs">
 <?php foreach ($collection_types['eobjResponse']  as $collection_type): ?>
    <li class="nav-item">
    <a class="nav-link" data-toggle="tab" href="#tabItem<?php echo $collection_type['id']; ?>"><span><?php echo $collection_type["collectionType"]; ?></span></a> 
   
</li>
<?php endforeach; ?> 
</ul>

  <div id="tabItem<?php echo $collection_type['id']; ?>" class="tab-content">    
  <!-- Household Scheduled Collection -->
<div class="tab-pane active" id="tabItem5">
<form id ="addhousebooking" action="#" class="mt-2">
<div class="row gy-4">

<div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Collection Type</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="icollectionTypeId" data-placeholder="Select Collection Type" required>
                <?php foreach ($collection_types['eobjResponse']  as $collection_type): ?>
                    <option value="">Select Collection Type</option>
                    <option value="<?= $collection_type["id"] ?>"><?= $collection_type["collectionType"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div>
    <!--col-->

    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Collection Sub Type</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="isubCollectionTypeId" data-placeholder="Select Collection Sub" required>
                <?php foreach ($collection_sub_types['eobjResponse']  as $collection_sub_type): ?>
                    <option value="">Select Collection Sub Type</option>
                    <option value="<?= $collection_sub_type["id"] ?>"><?= $collection_sub_type["vsubType"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div> -->
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="last-name">Collection Address</label>
            <input type="text" id="vcollectionsRequestHouseAddress" data-toggle="modal" data-target="#show_map" class="form-control" name ="vcollectionsRequestAddress"  readonly required>
            <input type="hidden" id="latitude" name ="vcollectionsRequestLat" placeholder="latitude">
            <input type="hidden" id="longitude" name="vcollectionsRequestLong" placeholder="longitude">
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="last-name">Collection Date</label>
            <!-- <input type="date" class="form-control" name ="collectionDate"  placeholder=""> -->
            <input type="text" class="form-control date-picker" name ="collectionDate"> 
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="phone-no">Collection Time</label>
            <input type="text" class="form-control time-picker" name ="collectionTime"  placeholder="">  
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="last-name">Number Of Bags</label>
            <input type="text" class="form-control" name ="iquantity"  placeholder="">
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Unit</label>
            <div class="form-control-wrap">
                <select name ="vunit" class="form-select" data-placeholder="Select Unit" required>
                    <option value="">Select Unit</option>
                    <option value="BAGS">BAGS</option>
                </select>
            </div>
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Reccurence</label>
            <div class="form-control-wrap">
                <select name ="reccurence" class="form-select" data-placeholder="Select Recurrence" required>
                        <option value="">Select Recurrence </option>
                    <option value="OneMonth">OneMonth</option>
                    <option value="TwoMonths">TwoMonths</option>
                    <option value="ThreeMonths">ThreeMonths</option>
                    <option value="OnceOff">OnceOff</option>
                </select>
            </div>
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Currency</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="currencyId" data-placeholder="Select Currency" required>
                <?php foreach ($currencies['eobjResponse']  as $currency): ?>
                    <option value="">Select Currency</option>
                    <option value="<?= $currency["currencyId"] ?>"><?= $currency["symbol"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Waste Type</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="wasteTypeId" data-placeholder="Select Waste Type" required>
                <?php foreach ($wastes['eobjResponse']  as $waste): ?>
                    <option value="">Select Waste Type</option>
                    <option value="<?= $waste["id"] ?>"><?= $waste["wasteTypeName"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div>
    <!--col-->
    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Franchise</label>
            <div class="form-control-wrap">
            <select class="form-select" name ="vfirstName" data-placeholder="Select Customer" required>
            <?php foreach ($customers['result']  as $customer) : ?>
            <option selected="selected" value="<?= $customer["iuserId"] ?>"><?= $customer["vfirstName"] . "" ?></option>
            <?php endforeach; ?>
            </select>
            </div>
        </div>
    </div> -->
    <input type="hidden" class="form-control" name ="iuserId" value="5">
    <div class="col-sm-12">
        <div class="form-group">
        <input type="hidden" name="create_house_booking" value="true">
        <button type="button" class="btn btn-primary" name ="create_house_booking" onClick="addHouseBooking()">Add</button>
        </div>
    </div>
    <!--col-->
</div>
<!--row-->
</form>
 </div>    
<!-- 
 Commercial Scheduled Collection -->
  <div class="tab-pane" id="tabItem6"> 
      
  <form id ="addcombooking" action="#" class="mt-2">
    <div class="row gy-4">
    <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Collection Type</label>
                <div class="form-control-wrap">
                    <select class="form-select" name ="icollectionTypeId" data-placeholder="Select Collection Type" required>
                    <?php foreach ($collection_types['eobjResponse']  as $collection_type): ?>
                        <option value="">Select Collection Type</option>
                        <option value="<?= $collection_type["id"] ?>"><?= $collection_type["collectionType"].""?></option>
                    <?php endforeach; ?> 
                    </select>
                </div>
            </div>
        </div>
        <!--col-->

        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Collection Sub Type</label>
                <div class="form-control-wrap">
                    <select class="form-select" name ="isubCollectionTypeId" data-placeholder="Select Collection Sub" required>
                    <?php foreach ($collection_sub_types['eobjResponse']  as $collection_sub_type): ?>
                        <option value="">Select Collection Sub Type</option>
                        <option value="<?= $collection_sub_type["id"] ?>"><?= $collection_sub_type["vsubType"].""?></option>
                    <?php endforeach; ?> 
                    </select>
                </div>
            </div>
        </div>
        <!--col-->
        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label" for="last-name">Collection Address</label>
                <input type="text" id="vcollectionsRequestComAddress" data-toggle="modal" data-target="#show_map" class="form-control" name ="vcollectionsRequestAddress"  readonly required>
                <input type="hidden" id="latitude" name ="vcollectionsRequestLat" placeholder="latitude">
                <input type="hidden" id="longitude" name="vcollectionsRequestLong" placeholder="longitude">
            </div>
        </div>
        <!--col-->
        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label" for="last-name">Collection Date</label>
                <!-- <input type="date" class="form-control" name ="collectionDate"  placeholder=""> -->
                <input type="text" class="form-control date-picker" name ="collectionDate"> 
            </div>
        </div>
        <!--col-->
        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label" for="phone-no">Collection Time</label>
                <input type="text" class="form-control time-picker" name ="collectionTime"  placeholder="">  
            </div>
        </div>
        <!--col-->
        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label" for="last-name">Tonnage</label>
                <input type="text" class="form-control" name ="iquantity"  placeholder="">
            </div>
        </div>
        <!--col-->
        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Unit</label>
                <div class="form-control-wrap">
                    <select name ="vunit" class="form-select" data-placeholder="Select Unit" required>
                        <option value="">Select Unit</option>
                        <option value="1-2 tonne">1-2 tonne</option>
                        <option value="3 tonne">3 tonne</option>
                        <option value="4-6 tonne">4-6 tonne</option>
                        <option value="7 tonne">7 tonne</option>
                        <option value="10-12 tonne">10-12 tonne</option>
                    </select>
                </div>
            </div>
        </div>
        <!--col-->
        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Reccurence</label>
                <div class="form-control-wrap">
                    <select name ="reccurence" class="form-select" data-placeholder="Select Recurrence" required>
                            <option value="">Select Recurrence </option>
                        <option value="OneMonth">OneMonth</option>
                        <option value="TwoMonths">TwoMonths</option>
                        <option value="ThreeMonths">ThreeMonths</option>
                        <option value="OnceOff">OnceOff</option>
                    </select>
                </div>
            </div>
        </div>
        <!--col-->
        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Currency</label>
                <div class="form-control-wrap">
                    <select class="form-select" name ="currencyId" data-placeholder="Select Currency" required>
                    <?php foreach ($currencies['eobjResponse']  as $currency): ?>
                        <option value="">Select Currency</option>
                        <option value="<?= $currency["currencyId"] ?>"><?= $currency["symbol"].""?></option>
                    <?php endforeach; ?> 
                    </select>
                </div>
            </div>
        </div>
        <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Waste Type</label>
                <div class="form-control-wrap">
                    <select class="form-select" name ="wasteTypeId" data-placeholder="Select Waste Type" required>
                    <?php foreach ($wastes['eobjResponse']  as $waste): ?>
                        <option value="">Select Waste Type</option>
                        <option value="<?= $waste["id"] ?>"><?= $waste["wasteTypeName"].""?></option>
                    <?php endforeach; ?> 
                    </select>
                </div>
            </div>
        </div>
        <!--col-->
        <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
            <div class="form-group">
                <label class="form-label">Franchise</label>
                <div class="form-control-wrap">
                <select class="form-select" name ="vfirstName" data-placeholder="Select Customer" required>
                <?php foreach ($customers['result']  as $customer) : ?>
                <option selected="selected" value="<?= $customer["iuserId"] ?>"><?= $customer["vfirstName"] . "" ?></option>
                <?php endforeach; ?>
                </select>
                </div>
            </div>
        </div> -->
        <input type="hidden" class="form-control" name ="iuserId" value="5">
        <div class="col-sm-12">
            <div class="form-group">
            <input type="hidden" name="create_com_booking" value="true">
            <button type="button" class="btn btn-primary" name ="create_com_booking" onClick="addComBooking()">Add</button>
            </div>
        </div>
        <!--col-->
    </div>
    <!--row-->
    </form>
  </div> 
  <!-- Adhoc/ Once Off Collection -->
  <div class="tab-pane" id="tabItem7"> 
  <ul class="nav nav-tabs"> 
   <li class="nav-item"> <a class="nav-link active" data-toggle="tab" href="#tabItem14">Household Scheduled Collection</a></li> 
   <li class="nav-item"><a class="nav-link" data-toggle="tab" href="#tabItem15">Commercial Scheduled Collection</a> </li>
</ul>
<div class="tab-content"> 
<!-- Household Scheduled Adhoc Collection -->
<div class="tab-pane active" id="tabItem14"> 
    <form id ="addhouseadbooking" action="#" class="mt-2">
        <div class="row gy-4">
        <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Collection Type</label>
                    <div class="form-control-wrap">
                        <select class="form-select" name ="icollectionTypeId" data-placeholder="Select Collection Type" required>
                        <?php foreach ($collection_types['eobjResponse']  as $collection_type): ?>
                            <option value="">Select Collection Type</option>
                            <option value="<?= $collection_type["id"] ?>"><?= $collection_type["collectionType"].""?></option>
                        <?php endforeach; ?> 
                        </select>
                    </div>
                </div>
            </div>
            <!--col-->
        
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Collection Sub Type</label>
                    <div class="form-control-wrap">
                        <select class="form-select" name ="isubCollectionTypeId" data-placeholder="Select Collection Sub" required>
                        <?php foreach ($collection_sub_types['eobjResponse']  as $collection_sub_type): ?>
                            <option value="">Select Collection Sub Type</option>
                            <option value="<?= $collection_sub_type["id"] ?>"><?= $collection_sub_type["vsubType"].""?></option>
                        <?php endforeach; ?> 
                        </select>
                    </div>
                </div>
            </div>
            <!--col-->
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label" for="last-name">Collection Address</label>
                    <input type="text" id="vcollectionsRequestHouseAdAddress" data-toggle="modal" data-target="#show_map" class="form-control" name ="vcollectionsRequestAddress"  readonly required>
                    <input type="hidden" id="latitude" name ="vcollectionsRequestLat" placeholder="latitude">
                    <input type="hidden" id="longitude" name="vcollectionsRequestLong" placeholder="longitude">
                </div>
            </div>
            <!--col-->
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label" for="last-name">Collection Date</label>
                    <!-- <input type="date" class="form-control" name ="collectionDate"  placeholder=""> -->
                    <input type="text" class="form-control date-picker" name ="collectionDate"> 
                </div>
            </div>
            <!--col-->
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label" for="phone-no">Collection Time</label>
                    <input type="text" class="form-control time-picker" name ="collectionTime"  placeholder="">  
                </div>
            </div>
            <!--col-->
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label" for="last-name">Number Of Bags</label>
                    <input type="text" class="form-control" name ="iquantity"  placeholder="">
                </div>
            </div>
            <!--col-->
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Unit</label>
                    <div class="form-control-wrap">
                        <select name ="vunit" class="form-select" data-placeholder="Select Unit" required>
                            <option value="">Select Unit</option>
                            <option value="1-2 tonne">1-2 tonne</option>
                            <option value="3 tonne">3 tonne</option>
                            <option value="4-6 tonne">4-6 tonne</option>
                            <option value="7 tonne">7 tonne</option>
                            <option value="10-12 tonne">10-12 tonne</option>
                        </select>
                    </div>
                </div>
            </div>
            <!--col-->
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Reccurence</label>
                    <div class="form-control-wrap">
                        <select name ="reccurence" class="form-select" data-placeholder="Select Recurrence" required>
                                <option value="">Select Recurrence </option>
                            <option value="OneMonth">OneMonth</option>
                            <option value="TwoMonths">TwoMonths</option>
                            <option value="ThreeMonths">ThreeMonths</option>
                            <option value="OnceOff">OnceOff</option>
                        </select>
                    </div>
                </div>
            </div>
            <!--col-->
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Currency</label>
                    <div class="form-control-wrap">
                        <select class="form-select" name ="currencyId" data-placeholder="Select Currency" required>
                        <?php foreach ($currencies['eobjResponse']  as $currency): ?>
                            <option value="">Select Currency</option>
                            <option value="<?= $currency["currencyId"] ?>"><?= $currency["symbol"].""?></option>
                        <?php endforeach; ?> 
                        </select>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Waste Type</label>
                    <div class="form-control-wrap">
                        <select class="form-select" name ="wasteTypeId" data-placeholder="Select Waste Type" required>
                        <?php foreach ($wastes['eobjResponse']  as $waste): ?>
                            <option value="">Select Waste Type</option>
                            <option value="<?= $waste["id"] ?>"><?= $waste["wasteTypeName"].""?></option>
                        <?php endforeach; ?> 
                        </select>
                    </div>
                </div>
            </div>
            <!--col-->
            <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
                <div class="form-group">
                    <label class="form-label">Franchise</label>
                    <div class="form-control-wrap">
                    <select class="form-select" name ="vfirstName" data-placeholder="Select Customer" required>
                    <?php foreach ($customers['result']  as $customer) : ?>
                    <option selected="selected" value="<?= $customer["iuserId"] ?>"><?= $customer["vfirstName"] . "" ?></option>
                    <?php endforeach; ?>
                    </select>
                    </div>
                </div>
            </div> -->
            <input type="hidden" class="form-control" name ="iuserId" value="5">
            <div class="col-sm-12">
                <div class="form-group">
                <input type="hidden" name="create_house_ad_booking" value="true">
                <button type="button" class="btn btn-primary" name ="create_house_ad_booking" onClick="addHouseAdBooking()">Add</button>
                </div>
            </div>
            <!--col-->
        </div>
        <!--row-->
        </form>
</div>
<!-- Commercial Scheduled Adhoc Collection -->
<div class="tab-pane" id="tabItem15">  
<form id ="addcomadbooking" action="#" class="mt-2">
<div class="row gy-4">
<div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Collection Type</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="icollectionTypeId" data-placeholder="Select Collection Type" required>
                <?php foreach ($collection_types['eobjResponse']  as $collection_type): ?>
                    <option value="">Select Collection Type</option>
                    <option value="<?= $collection_type["id"] ?>"><?= $collection_type["collectionType"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div>
    <!--col-->

    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Collection Sub Type</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="isubCollectionTypeId" data-placeholder="Select Collection Sub" required>
                <?php foreach ($collection_sub_types['eobjResponse']  as $collection_sub_type): ?>
                    <option value="">Select Collection Sub Type</option>
                    <option value="<?= $collection_sub_type["id"] ?>"><?= $collection_sub_type["vsubType"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="last-name">Collection Address</label>
            <input type="text" id="vcollectionsRequestComAdAddress" data-toggle="modal" data-target="#show_map" class="form-control" name ="vcollectionsRequestAddress"  readonly required>
            <input type="hidden" id="latitude" name ="vcollectionsRequestLat" placeholder="latitude">
            <input type="hidden" id="longitude" name="vcollectionsRequestLong" placeholder="longitude">
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="last-name">Collection Date</label>
            <!-- <input type="date" class="form-control" name ="collectionDate"  placeholder=""> -->
            <input type="text" class="form-control date-picker" name ="collectionDate"> 
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="phone-no">Collection Time</label>
            <input type="text" class="form-control time-picker" name ="collectionTime"  placeholder="">  
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="last-name">Tonnage</label>
            <input type="text" class="form-control" name ="iquantity"  placeholder="">
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Unit</label>
            <div class="form-control-wrap">
                <select name ="vunit" class="form-select" data-placeholder="Select Unit" required>
                    <option value="">Select Unit</option>
                    <option value="1-2 tonne">1-2 tonne</option>
                    <option value="3 tonne">3 tonne</option>
                    <option value="4-6 tonne">4-6 tonne</option>
                    <option value="7 tonne">7 tonne</option>
                    <option value="10-12 tonne">10-12 tonne</option>
                </select>
            </div>
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Reccurence</label>
            <div class="form-control-wrap">
                <select name ="reccurence" class="form-select" data-placeholder="Select Recurrence" required>
                        <option value="">Select Recurrence </option>
                    <option value="OneMonth">OneMonth</option>
                    <option value="TwoMonths">TwoMonths</option>
                    <option value="ThreeMonths">ThreeMonths</option>
                    <option value="OnceOff">OnceOff</option>
                </select>
            </div>
        </div>
    </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Currency</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="currencyId" data-placeholder="Select Currency" required>
                <?php foreach ($currencies['eobjResponse']  as $currency): ?>
                    <option value="">Select Currency</option>
                    <option value="<?= $currency["currencyId"] ?>"><?= $currency["symbol"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div>
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Waste Type</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="wasteTypeId" data-placeholder="Select Waste Type" required>
                <?php foreach ($wastes['eobjResponse']  as $waste): ?>
                    <option value="">Select Waste Type</option>
                    <option value="<?= $waste["id"] ?>"><?= $waste["wasteTypeName"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div>
    <!--col-->
    <!-- <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label">Franchise</label>
            <div class="form-control-wrap">
            <select class="form-select" name ="vfirstName" data-placeholder="Select Customer" required>
            <?php foreach ($customers['result']  as $customer) : ?>
            <option selected="selected" value="<?= $customer["iuserId"] ?>"><?= $customer["vfirstName"] . "" ?></option>
            <?php endforeach; ?>
            </select>
            </div>
        </div>
    </div> -->
    <input type="hidden" class="form-control" name ="iuserId" value="5">
    <div class="col-sm-12">
        <div class="form-group">
        <input type="hidden" name="create_com_ad_booking" value="true">
        <button type="button" class="btn btn-primary" name ="create_com_ad_booking" onClick="addHouseComBooking()">Add</button>
        </div>
    </div>
    <!--col-->
</div>
<!--row-->
</form>
</div>
</div>
</div>
</div><!-- .card-inner-group -->
</div><!-- .card -->
</div><!-- .nk-block -->
</div>
</div>
</div>
</div>
    <!-- content @e -->
    <!-- footer @s -->
    <div class="nk-footer">
        <div class="container-fluid">
            <div class="nk-footer-wrap">
                <div class="nk-footer-copyright"> &copy; 2022 VAYA TECHNOLOGIES.<a href="https://softnio.com" target="_blank"></a>
                </div>
                <div class="nk-footer-links">
                    <ul class="nav nav-sm">
            
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <!-- footer @e -->
</div>
<!-- wrap @e -->
</div>
<!-- main @e -->
</div>
    <!-- app-root @e -->
        <!-- Success Commercial Adhoc Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successComAdAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully created booking</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Commercial Adhoc Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failComAdAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addBookingComAdResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>

    <!-- Success House Hold  Adhoc Modal Alert -->
    <div class="modal fade" tabindex="-1" id="successHouseAdAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully created booking</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  House Hold Adhoc Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failHouseAdAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addBookingHouseAdResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="modal fade" tabindex="-1" id="successHouseAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully created booking</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  House Hold Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failHouseAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addBookingHouseResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- Success Commercial Modal Alert -->
        <div class="modal fade" tabindex="-1" id="successComAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully created booking</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Commercial Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failComAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addBookingComResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--Map  HouseHold Modal  -->
    <div class="modal fade" tabindex="-1" id="show_map">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body modal-body-lg text-center">
            <div class="nk-modal">
            <p><input class="postcode" id="Postcode" name="Postcode" type="text" value="">
            <input type="submit" id="findbutton" value="Find" onclick="getInputHouseValue();" /></p>
            <div id="map" style="width:400px; height:350px"></div>
                <div class="nk-modal-action mt-5">
                    <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                </div>
            </div>
        </div><!-- .modal-body -->
        <div class="modal-footer bg-lighter">
            <div class="text-center w-100">
            
            </div>
        </div>
    </div>
</div>
</div>
    <!--Map  Commercial Modal  -->
    <div class="modal fade" tabindex="-1" id="show_map">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body modal-body-lg text-center">
            <div class="nk-modal">
            <p><input class="postcode" id="Postcode" name="Postcode" type="text" value="">
            <input type="submit" id="findbutton" value="Find" onclick="getInputComValue();" /></p>
            <div id="map" style="width:400px; height:350px"></div>
                <div class="nk-modal-action mt-5">
                    <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                </div>
            </div>
        </div><!-- .modal-body -->
        <div class="modal-footer bg-lighter">
            <div class="text-center w-100">
            
            </div>
        </div>
    </div>
</div>
</div>
    <!--Map  HouseHold Adhoc Modal  -->
    <div class="modal fade" tabindex="-1" id="show_map">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body modal-body-lg text-center">
            <div class="nk-modal">
            <p><input class="postcode" id="Postcode" name="Postcode" type="text" value="">
            <input type="submit" id="findbutton" value="Find" onclick="getInputHouseAdValue();" /></p>
            <div id="map" style="width:400px; height:350px"></div>
                <div class="nk-modal-action mt-5">
                    <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                </div>
            </div>
        </div><!-- .modal-body -->
        <div class="modal-footer bg-lighter">
            <div class="text-center w-100">
            
            </div>
        </div>
    </div>
</div>
</div>
    <!--Map  Commercial Adhoc Modal  -->
    <div class="modal fade" tabindex="-1" id="show_map">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body modal-body-lg text-center">
            <div class="nk-modal">
            <p><input class="postcode" id="Postcode" name="Postcode" type="text" value="">
            <input type="submit" id="findbutton" value="Find" onclick="getInputComAdValue();" /></p>
            <div id="map" style="width:400px; height:350px"></div>
                <div class="nk-modal-action mt-5">
                    <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                </div>
            </div>
        </div><!-- .modal-body -->
        <div class="modal-footer bg-lighter">
            <div class="text-center w-100">
            
            </div>
        </div>
    </div>
</div>
</div>
<!-- BEGIN: AJAX CALLS-->
<script>
    //Add House Hold Booking
    function addHouseBooking() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addhousebooking').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                // console.log(json.eobjResponse.ibookingId);
                // var test = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                // console.log(test);
                // return;
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $("#successHouseAlert").modal('show');
                    setTimeout(function() {
                        // window.location = "portal/admin/bookings";
                        window.location = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                    }, 2000);

                } else {
                    $('.addBookingHouseResponse').empty();
                    $('.addBookingHouseResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $("#failHouseAlert").modal('show');
                  
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addBookingHouseResponse').empty();
                $('.addBookingHouseResponse').append(errorThrown);
                $('#spinner').hide();
                $("#failHouseAlert").modal('show');
              
            }
        });
    }
    </script>
    <script>
    //Add Commercial Booking
    function addComBooking() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addcombooking').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                // console.log(json.statusCode);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $("#successComAlert").modal('show');
                    setTimeout(function() {
                        // window.location = "portal/admin/bookings";
                        window.location = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                    }, 2000);

                } else {
                    $('.addBookingComResponse').empty();
                    $('.addBookingComResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $("#failComAlert").modal('show');
                  
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addBookingComResponse').empty();
                $('.addBookingComResponse').append(errorThrown);
                $('#spinner').hide();
                $("#failComAlert").modal('show');
              
            }
        });
    }
    </script>
      <script>
    //Add AdHoc House Booking
    function addHouseAdBooking() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addhouseadbooking').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                // console.log(json.statusCode);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $("#successHouseAdAlert").modal('show');
                    setTimeout(function() {
                        // window.location = "portal/admin/bookings";
                        window.location = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                    }, 2000);

                } else {
                    $('.addBookingHouseAdResponse').empty();
                    $('.addBookingHouseAdResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $("#failHouseAdAlert").modal('show');
                  
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addBookingHouseAdResponse').empty();
                $('.addBookingHouseAdResponse').append(errorThrown);
                $('#spinner').hide();
                $("#failHouseAdAlert").modal('show');
              
            }
        });
    }
    </script>

<script>
    //Add AdHoc Commercial Booking
    function addHouseComBooking() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addcomadbooking').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                // console.log(json.statusCode);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $("#successComAdAlert").modal('show');
                    setTimeout(function() {
                        // window.location = "portal/admin/bookings";
                        window.location = "portal/admin/quotation-details?ibookingId=" + json.eobjResponse.ibookingId;
                    }, 2000);

                } else {
                    $('.addBookingComAdResponse').empty();
                    $('.addBookingComAdResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $("#failComAdAlert").modal('show');
                  
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addBookingComAdResponse').empty();
                $('.addBookingComAdResponse').append(errorThrown);
                $('#spinner').hide();
                $("#failComAdAlert").modal('show');
              
            }
        });
    }
    </script>
  
  
    
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs"></script>
    <script>
  function getInputComAdValue(){
            // Selecting the input element and get its value 
            var inputVal = document.getElementById("Postcode").value;
            document.getElementById("vcollectionsRequestComAdAddress").value = inputVal;
            // Displaying the value
            // alert(inputVal);
        }
  </script>
    <script>
 var geocoder = new google.maps.Geocoder();
var marker = null;
var map = null;
function initialize() {
      var $latitude = document.getElementById('latitude');
      var $longitude = document.getElementById('longitude');
      var latitude = -17.891120
      var longitude = 30.934920;
      var zoom = 16;

      var LatLng = new google.maps.LatLng(latitude, longitude);

      var mapOptions = {
        zoom: zoom,
        center: LatLng,
        panControl: false,
        zoomControl: false,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      if (marker && marker.getMap) marker.setMap(map);
      marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        title: 'Drag Me!',
        draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function(marker) {
        var latLng = marker.latLng;
        $latitude.value = latLng.lat();
        $longitude.value = latLng.lng();
      });


    }
    initialize();
    $('#findbutton').click(function (e) {
        var address = $("#Postcode").val();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                $(latitude).val(marker.getPosition().lat());
                $(longitude).val(marker.getPosition().lng());
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        e.preventDefault();
    });
    </script>
    <script>
  function getInputHouseAdValue(){
            // Selecting the input element and get its value 
            var inputVal = document.getElementById("Postcode").value;
            document.getElementById("vcollectionsRequestHouseAdAddress").value = inputVal;
            // Displaying the value
            // alert(inputVal);
        }
  </script>
    <script>
 var geocoder = new google.maps.Geocoder();
var marker = null;
var map = null;
function initialize() {
      var $latitude = document.getElementById('latitude');
      var $longitude = document.getElementById('longitude');
      var latitude = -17.891120
      var longitude = 30.934920;
      var zoom = 16;

      var LatLng = new google.maps.LatLng(latitude, longitude);

      var mapOptions = {
        zoom: zoom,
        center: LatLng,
        panControl: false,
        zoomControl: false,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      if (marker && marker.getMap) marker.setMap(map);
      marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        title: 'Drag Me!',
        draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function(marker) {
        var latLng = marker.latLng;
        $latitude.value = latLng.lat();
        $longitude.value = latLng.lng();
      });


    }
    initialize();
    $('#findbutton').click(function (e) {
        var address = $("#Postcode").val();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                $(latitude).val(marker.getPosition().lat());
                $(longitude).val(marker.getPosition().lng());
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        e.preventDefault();
    });
    </script>
   <script>
  function getInputHouseValue(){
            // Selecting the input element and get its value 
            var inputVal = document.getElementById("Postcode").value;
            document.getElementById("vcollectionsRequestHouseAddress").value = inputVal;
            // Displaying the value
            // alert(inputVal);
        }
  </script>
    <script>
 var geocoder = new google.maps.Geocoder();
var marker = null;
var map = null;
function initialize() {
      var $latitude = document.getElementById('latitude');
      var $longitude = document.getElementById('longitude');
      var latitude = -17.891120
      var longitude = 30.934920;
      var zoom = 16;

      var LatLng = new google.maps.LatLng(latitude, longitude);

      var mapOptions = {
        zoom: zoom,
        center: LatLng,
        panControl: false,
        zoomControl: false,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      if (marker && marker.getMap) marker.setMap(map);
      marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        title: 'Drag Me!',
        draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function(marker) {
        var latLng = marker.latLng;
        $latitude.value = latLng.lat();
        $longitude.value = latLng.lng();
      });


    }
    initialize();
    $('#findbutton').click(function (e) {
        var address = $("#Postcode").val();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                $(latitude).val(marker.getPosition().lat());
                $(longitude).val(marker.getPosition().lng());
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        e.preventDefault();
    });
    </script>
      <script>
  function getInputComValue(){
            // Selecting the input element and get its value 
            var inputVal = document.getElementById("Postcode").value;
            document.getElementById("vcollectionsRequestComAddress").value = inputVal;
            // Displaying the value
            // alert(inputVal);
        }
  </script>
    <script>
 var geocoder = new google.maps.Geocoder();
var marker = null;
var map = null;
function initialize() {
      var $latitude = document.getElementById('latitude');
      var $longitude = document.getElementById('longitude');
      var latitude = -17.891120
      var longitude = 30.934920;
      var zoom = 16;

      var LatLng = new google.maps.LatLng(latitude, longitude);

      var mapOptions = {
        zoom: zoom,
        center: LatLng,
        panControl: false,
        zoomControl: false,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      if (marker && marker.getMap) marker.setMap(map);
      marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        title: 'Drag Me!',
        draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function(marker) {
        var latLng = marker.latLng;
        $latitude.value = latLng.lat();
        $longitude.value = latLng.lng();
      });


    }
    initialize();
    $('#findbutton').click(function (e) {
        var address = $("#Postcode").val();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                $(latitude).val(marker.getPosition().lat());
                $(longitude).val(marker.getPosition().lng());
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        e.preventDefault();
    });
    </script>

    <script>
          function getEventTarget(e) {
        e = e || window.event;
        return e.target || e.srcElement; 
    }

    var ul = document.getElementById('div#tabItem'+vehicleId);
    ul.onclick = function(event) {
        var target = getEventTarget(event);
        alert(target.innerHTML);
    };
    </script>

 <!-- JavaScript -->
 <?php require_once('includes/footer.php');?>
</body>

</html>
