<!-- JavaScript -->
<script src="././assets/js/bundle.js?ver=2.9.0"></script>
<script src="././assets/js/scripts.js?ver=2.9.0"></script>
<script src="././assets/js/charts/chart-crm.js?ver=2.9.0"></script>
<link rel="stylesheet" href="./assets/css/editors/quill.css?ver=2.9.0">
<script src="./assets/js/libs/editors/quill.js?ver=2.9.0"></script>
<script src="./assets/js/editors.js?ver=2.9.0"></script>
<script src="./assets/js/libs/datatable-btns.js"></script>
<script src="././assets/js/libs/datatable-btns.js?ver=2.9.0"></script>

<script>
    if ( window.history.replaceState ) {
        window.history.replaceState( null, null, window.location.href );
    }
  </script>
<script>
    $('.time-picker').timepicker({
    timeFormat: 'HH:mm'
    // interval: 60,
    // minTime: '10',
    // maxTime: '6:00pm',
    // defaultTime: '11',
    // startTime: '10:00',
    // dynamic: false,
    // dropdown: true,
    // scrollbar: true
});
</script>
