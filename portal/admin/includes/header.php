<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="././images/favicon.png">
    <!-- Page Title  -->
    <title>Dashboard | VAYA TECHNOLOGIES</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="././assets/css/dashlite.css">
    <link id="skin-default" rel="stylesheet" href="././assets/css/theme.css">
    <link rel="stylesheet" href="././assets/validate-password-requirements/css/jquery.passwordRequirements.css" />
    <style>
        #spinner{
            display:none; 
        }
        .invoice-desc .title {
    text-transform: uppercase;
    color: #0169a6;
}
.invoice-bills .table th {
    color: #0169a6;
    font-size: 12px;
    text-transform: uppercase;
    border-top: 0;
}
.text-primary {
    color: #0169a6 !important;
}

.btn-primary {
    color: #fff;
    background-color: #8dc63b;
    border-color: #8dc63b;
}

.code_hide { display: none; }

.error{
color: #e85347;
font-size: 11px;
font-style: italic;
}
.passwordInput{          
       margin-top: 5%;           
      /* text-align :center; */   
         }     
         .displayBadge{ 
         margin-top: 5%; 
         display: none;            
     text-align :center;      
      }  
    </style>

    

</head>


