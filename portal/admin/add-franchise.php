<?php 
session_start();
if (!isset($_SESSION['admin_id'])) {
  header("Location: ../../portal/admin-login");
  exit();
}
$admin_id =  $_SESSION["admin_id"];
$admin_phone  =$_SESSION["admin_vphone"];
$admin_email =  $_SESSION["admin_Email"];
$admin_firstname =  $_SESSION["first_name"];
$admin_lastname = $_SESSION["last_name"];

include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
$cities = json_decode(getAllCities(), true);
// var_dump($cities);
// exit;
?>
<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
        
            <!-- sidebar @e -->
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <!-- <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div>
                                    </a>
                                </div>
                            </div> -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li>
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->

                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Add Franchise</h3>
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
                                    <div class="card card-bordered">
                                        <div class="card-inner">
                                            <form id ="addpartner" action="#" class="form-validate">
                                            <div class="row gy-4">
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="first-name">Company Name</label>
                                                        <input type="text" class="form-control" name ="company"  placeholder="" required>
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="last-name">Address</label>
                                                        <input type="text" id="vcollectionsRequestAddress" data-toggle="modal" data-target="#show_map" class="form-control" name ="address"  readonly required>
                                                        <input type="hidden" id="latitude" name ="franchiseLatitude" placeholder="latitude">
                                                        <input type="hidden" id="longitude" name="franchiseLongitude" placeholder="longitude">
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="last-name">Address (2)</label>
                                                        <input type="text" class="form-control" name ="address2"  placeholder="" required>
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="phone-no">Phone Number</label>
                                                        <input type="text" class="form-control" name ="phoneNumber"  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" required>
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="last-name">Email</label>
                                                        <input type="email" class="form-control" name ="email" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  placeholder="" required>
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="last-name">ZIMRA BP Numnber</label>
                                                        <input type="text" class="form-control" name ="zimraBpNumber"  placeholder="" required>
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status"> <span class="sr-only">Loading...</span> </div></div> -->
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label">Country</label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select" name ="country" data-placeholder="Select Country" required>
                                                                <option value="">Select Country</option>
                                                                <option value="Zimbabwe">Zimbabwe</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="last-name">Province</label>
                                                        <input type="text" class="form-control" name ="province"  placeholder="" required>
                                                    </div>
                                                </div>
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label">City</label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select" name ="cityId" data-placeholder="Select City" required>
                                                            <?php foreach ($cities['eobjResponse']  as $city): ?>
                                                                <option value="">Select City</option>
                                                                <option value="<?= $city["id"] ?>"><?= $city["cityName"].""?></option>
                                                            <?php endforeach; ?> 
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--col-->
               
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label" for="last-name">State</label>
                                                        <input type="text" class="form-control" name ="state"  placeholder="" required>
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                        <label class="form-label">Language</label>
                                                        <div class="form-control-wrap">
                                                            <select name ="language" class="form-select" data-placeholder="Select language" required>
                                                                <option value="English">English</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                             
                                                <!--col-->
                                                <div class="col-md-6 col-lg-4 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="currency-add">Password</label>
                            <input type="password" class="form-control passwordInput" name="password" id="PassEntry" required>
                            <span id="StrengthDisp" class="badge displayBadge">Weak</span>
                                                    </div>
                                                </div>
                                        
                                                <div class="col-sm-12">
                                                    <div class="form-group">
                                                    <input type="hidden" name="add_partner">
                                                 <button type="button" class="btn btn-primary" name ="add_partner" onClick="addPartner()">Add</button>
                                                
                                                    </div>
                                                </div>
                                                <!--col-->
                                            </div>
                                            <!--row-->
                                            </form>
                                        </div><!-- .card-inner-group -->
                                    </div><!-- .card -->
                                </div><!-- .nk-block -->
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022 VAYA TECHNOLOGIES.<a href="https://softnio.com" target="_blank"></a>
                            </div>
    
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
       <!-- Success Modal Alert -->
       <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully added franchise</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='addPartnerResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Map  Modal  -->
 <div class="modal fade" tabindex="-1" id="show_map">
<div class="modal-dialog" role="document">
    <div class="modal-content">
    <div class="example-alert">
    <div class="alert alert-primary alert-icon">
        <em class="icon ni ni-alert-circle"></em> <strong>Search location or drag the marker on the map to the desired location.</strong>
    </div>
</div>
        <div class="modal-body modal-body-lg text-center">
            <div class="nk-modal">
            <p><input class="postcode" id="Postcode" name="Postcode" type="text" value="">
            <input type="submit" id="findbutton" value="Find" onclick="getInputValue();" /></p>
            <div id="map" style="width:400px; height:350px"></div>
                <div class="nk-modal-action mt-5">
                    <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                </div>
            </div>
        </div><!-- .modal-body -->
        <div class="modal-footer bg-lighter">
            <div class="text-center w-100">
            
            </div>
        </div>
    </div>
</div>
</div>
 
<script src="./assets/validate-password-requirements/js/jquery.passwordRequirements.min.js"></script>
    
    <script>

let timeout;

    // traversing the DOM and getting the input and span using their IDs
let password = document.getElementById('PassEntry');
    let strengthBadge = document.getElementById('StrengthDisp');

    // The strong and weak password Regex pattern checker
let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthChecker(PasswordParameter){
        // We then change the badge's color and text based on the password strength
if(strongPassword.test(PasswordParameter)) {
            strengthBadge.style.backgroundColor = 'green';
            strengthBadge.textContent = 'Strong';
        } else if(mediumPassword.test(PasswordParameter)){
            strengthBadge.style.backgroundColor = 'blue';
            strengthBadge.textContent = 'Medium';
        } else{
            strengthBadge.style.backgroundColor = 'red';
            strengthBadge.textContent = 'Weak';
        }
    }

password.addEventListener("input", () => {
    strengthBadge.style.display = 'block';
    clearTimeout(timeout);
    timeout = setTimeout(() => StrengthChecker(password.value), 500);
    if(password.value.length !== 0) {
        strengthBadge.style.display != 'block';
    } else {
        strengthBadge.style.display = 'none';
    }
});



</script> 
<!-- BEGIN: AJAX CALLS-->
   <script>
    //Add Partner
    function addPartner() {
        var PasswordParameter = document.getElementById('PassEntry').value;

var vMsg = 'The minimum password length is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number, and 1 special character.';

if(!strongPassword.test(PasswordParameter)) {

alert(vMsg); return;

}
     
            
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addpartner').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                // console.log(json.statusCode);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/admin/franchises";
                    }, 2000);

                } else {
                    $('.addPartnerResponse').empty();
                    $('.addPartnerResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $("#failAlert").modal('show');
                  
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.addPartnerResponse').empty();
                $('.addPartnerResponse').append(errorThrown);
                $('#spinner').hide();
                $("#failAlert").modal('show');
              
            }
        });
    }
    </script>
      
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs"></script>
   <script>
  function getInputValue(){
            // Selecting the input element and get its value 
            var inputVal = document.getElementById("Postcode").value;
            document.getElementById("vcollectionsRequestAddress").value = inputVal;
            // Displaying the value
            // alert(inputVal);
        }
  </script>
    <script>
 var geocoder = new google.maps.Geocoder();
var marker = null;
var map = null;
function initialize() {
      var $latitude = document.getElementById('latitude');
      var $longitude = document.getElementById('longitude');
      var latitude = -17.891120
      var longitude = 30.934920;
      var zoom = 16;

      var LatLng = new google.maps.LatLng(latitude, longitude);

      var mapOptions = {
        zoom: zoom,
        center: LatLng,
        panControl: false,
        zoomControl: false,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      if (marker && marker.getMap) marker.setMap(map);
      marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        title: 'Drag Me!',
        draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function(marker) {
        var latLng = marker.latLng;
        $latitude.value = latLng.lat();
        $longitude.value = latLng.lng();
      });


    }
    initialize();
    $('#findbutton').click(function (e) {
        var address = $("#Postcode").val();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                $(latitude).val(marker.getPosition().lat());
                $(longitude).val(marker.getPosition().lng());
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        e.preventDefault();
    });
    </script>


 <!-- JavaScript -->
 <?php require_once('includes/footer.php');?>
</body>

</html>