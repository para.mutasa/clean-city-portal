<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="././images/favicon.png">
    <!-- Page Title  -->
    <title>Dashboard | VAYA TECHNOLOGIES</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="././assets/css/dashlite.css">
    <link id="skin-default" rel="stylesheet" href="././assets/css/theme.css">
    <style>
        #spinner{
            display:none; 
        }
        .btn-primary {
    color: #fff;
    background-color: #8dc63b;
    border-color: #8dc63b;
}

.error{
color: #e85347;
font-size: 11px;
font-style: italic;
}
    </style>
</head>


