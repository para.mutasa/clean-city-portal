<div class="nk-sidebar nk-sidebar-fixed is-dark " data-content="sidebarMenu">
                <div class="nk-sidebar-element nk-sidebar-head">
                    <div class="nk-menu-trigger">
                        <a href="#" class="nk-nav-toggle nk-quick-nav-icon d-xl-none" data-target="sidebarMenu"><em class="icon ni ni-arrow-left"></em></a>
                        <a href="#" class="nk-nav-compact nk-quick-nav-icon d-none d-xl-inline-flex" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                    </div>
                    <div class="nk-sidebar-brand">
                        <a href="portal/partner/bookings" class="logo-link nk-sidebar-logo">
                        <h4 style="color:#fff;"> <span class="nk-menu-text">FRANCHISE</span></h4>
                            <!-- <img class="logo-light logo-img"  src="./images/vaya_logo_white.png" srcset="./images/vaya_logo_white.png" alt="logo">
                            <img class="logo-dark logo-img" src="./images/vaya_logo_white.png" srcset="./images/vaya_logo_white.png" alt="logo-dark"> -->
                        </a>
                    </div>
                </div><!-- .nk-sidebar-element -->
                <div class="nk-sidebar-element nk-sidebar-body">
                    <div class="nk-sidebar-content">
                        <div class="nk-sidebar-menu" data-simplebar>
                            <ul class="nk-menu">
                                <!-- <li class="nk-menu-item">
                                    <a href="portal/partner/index" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-dashboard-fill"></em></span>
                                        <span class="nk-menu-text">Dashboard</span>
                                    </a>
                                </li> -->
                                <!-- .nk-menu-item -->
                                <li class="nk-menu-item has-sub">
                                    <a href="portal/partner/bookings" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-calendar-booking-fill"></em></span>
                                        <span class="nk-menu-text">Bookings</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <li class="nk-menu-item">
                                            <a href="portal/partner/bookings" class="nk-menu-link"><span class="nk-menu-text">Pending Bookings</span></a>
                                            <li class="nk-menu-item">
                                            <a href="portal/partner/all-bookings" class="nk-menu-link"><span class="nk-menu-text">All Bookings</span></a>
                                        </li>
                                        </li>
                                    </ul>
                                </li><!-- .nk-menu-item -->
                                <!-- <li class="nk-menu-item">
                                    <a href="portal/partner/trips" class="nk-menu-link">
                                    <span class="nk-menu-icon"><em class="icon ni ni-tranx"></em></span>
                                        <span class="nk-menu-text">Trips</span>
                                    </a>
                                </li> -->
                                <!-- .nk-menu-item -->
                             
                                <li class="nk-menu-item">
                                    <a href="portal/partner/vehicles" class="nk-menu-link">
                                    <span class="nk-menu-icon"><em class="icon ni ni-truck"></em></span>
                                        <span class="nk-menu-text">Vehicles</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                                <li class="nk-menu-item">
                                    <a href="portal/partner/drivers" class="nk-menu-link">
                                        <span class="nk-menu-icon"><em class="icon ni ni-user-list-fill"></em></span>
                                        <span class="nk-menu-text">Drivers</span>
                                    </a>
                                </li><!-- .nk-menu-item -->
                       
                            
                                <li class="nk-menu-item has-sub">
                                    <a href="portal/partner/client-report" class="nk-menu-link nk-menu-toggle">
                                        <span class="nk-menu-icon"><em class="icon ni ni-growth-fill"></em></span>
                                        <span class="nk-menu-text">Report</span>
                                    </a>
                                    <ul class="nk-menu-sub">
                                        <!-- <li class="nk-menu-item">
                                            <a href="html/crm/dealing-info.html" class="nk-menu-link"><span class="nk-menu-text">Dealing Info</span></a>
                                        </li> -->
                                        <li class="nk-menu-item">
                                            <a href="portal/partner/client-report" class="nk-menu-link"><span class="nk-menu-text">Client Report</span></a>
                                        </li>
                                        <!-- <li class="nk-menu-item">
                                            <a href="portal/partner/expense-report" class="nk-menu-link"><span class="nk-menu-text">Expense Report</span></a>
                                        </li> -->
                                    </ul><!-- .nk-menu-sub -->
                                </li><!-- .nk-menu-item -->
                          
                            </ul><!-- .nk-menu -->
                        </div><!-- .nk-sidebar-menu -->
                    </div><!-- .nk-sidebar-content -->
                </div><!-- .nk-sidebar-element -->
            </div>