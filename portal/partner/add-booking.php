<?php 
require_once('includes/header.php');
?>
<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
        
            <!-- sidebar @e -->
            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="html/index.html" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <!-- <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div>
                                    </a>
                                </div>
                            </div> -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator">Abu Bin Ishityak</div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                        <span class="lead-text">Abu Bin Ishtiyak</span>
                                                        <span class="sub-text">info@softnio.com</span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li>
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <li><a href="#"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                                    <li class="dropdown notification-dropdown mr-n1">
                                        <a href="#" class="dropdown-toggle nk-quick-nav-icon" data-toggle="dropdown">
                                            <div class="icon-status icon-status-info"><em class="icon ni ni-bell"></em></div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-xl dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-head">
                                                <span class="sub-title nk-dropdown-title">Notifications</span>
                                                <a href="#">Mark All as Read</a>
                                            </div>
                                            <div class="dropdown-body">
                                                <div class="nk-notification">
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-warning-dim ni ni-curve-down-right"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">You have requested to <span>Widthdrawl</span></div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                    <div class="nk-notification-item dropdown-inner">
                                                        <div class="nk-notification-icon">
                                                            <em class="icon icon-circle bg-success-dim ni ni-curve-down-left"></em>
                                                        </div>
                                                        <div class="nk-notification-content">
                                                            <div class="nk-notification-text">Your <span>Deposit Order</span> is placed</div>
                                                            <div class="nk-notification-time">2 hrs ago</div>
                                                        </div>
                                                    </div>
                                                </div><!-- .nk-notification -->
                                            </div><!-- .nk-dropdown-body -->
                                            <div class="dropdown-foot center">
                                                <a href="#">View All</a>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Add Booking</h3>
                                            <li><a href="#" data-toggle="modal" data-target="#add-pay-method"><span>Add Payment Method</span></a></li>
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->
                                <div class="nk-block">
<div class="card card-bordered">
<div class="card-inner">
                                                         
                       
                                 <ul class="nav nav-tabs"> 
    <li class="nav-item"> 
       <a class="nav-link active" data-toggle="tab" href="#tabItem5"><span>Household Scheduled Collection</span></a> 
   </li>  
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem6"><span>Commercial Scheduled Collection</span></a> 
  </li>    
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem7"></em><span>Adhoc/ Once Off Collection</span></a> 
  </li>   
  </ul>
  <div class="tab-content">    
<div class="tab-pane active" id="tabItem5">

  <form action="#" class="nk-wizard nk-wizard-simple is-alter" id="wizard-01">
    <div class="nk-wizard-head">
        <h5>Step 1</h5>
    </div>
    <div class="nk-wizard-content">
        <div class="row gy-3">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="fw-first-name">First Name</label>
                    <div class="form-control-wrap">
                        <input type="text" data-msg="Required" class="form-control required" id="fw-first-name" name="fw-first-name" required>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="fw-last-name">Last Name</label>
                    <div class="form-control-wrap">
                        <input type="text" data-msg="Required" class="form-control required" id="fw-last-name" name="fw-last-name" required>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="fw-email-address">Email Address</label>
                    <div class="form-control-wrap">
                        <input type="text" data-msg="Required" data-msg-email="Wrong Email" class="form-control required email" id="fw-email-address" name="fw-email-address">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="fw-mobile-number">Mobile Number</label>
                    <div class="form-control-wrap">
                        <input type="text" data-msg="Required" class="form-control required" id="fw-mobile-number" name="fw-mobile-number" required>
                    </div>
                </div>
            </div>
        
        </div>
    </div>
    <div class="nk-wizard-head">
        <h5>Step 2</h5>
    </div>
    <div class="nk-wizard-content">
        <div class="row gy-3">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="fw-username">Number Of Bags (Max-3)</label>
                    <div class="form-control-wrap">
                        <input type="text" data-msg="Required" class="form-control required" id="fw-username" name="fw-username" required>
                    </div>
                </div>
            </div><!-- .col -->
            <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="fw-nationality">Waste Type</label>
                <div class="form-control-wrap ">
                    <div class="form-control-select">
                        <select class="form-control required" data-msg="Required" id="fw-nationality" name="fw-nationality" required>
                            <option value="">Select Waste  Type</option>
                            <option value="us">Organic</option>
                         
                        </select>
                    </div>
                </div>
            </div>
        </div><!-- .col -->
        </div><!-- .row -->
        <div class="row gy-3">
            <div class="col-md-6">
                <div class="form-group">
                    <label class="form-label" for="fw-password">Collection Address</label>
                    <div class="form-control-wrap">
                        <input type="text" data-msg="Required" class="form-control required" id="fw-password" name="fw-password" required>
                    </div>
                </div>
            </div><!-- .col -->
            <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="fw-nationality">Request Frequency</label>
                <div class="form-control-wrap ">
                    <div class="form-control-select">
                        <select class="form-control required" data-msg="Required" id="fw-nationality" name="fw-nationality" required>
                            <option value="">1 Month</option>
                            <option value="us">2 Months</option>
                            <option value="us">3 Months</option>
                        </select>
                    </div>
                </div>
            </div>
        </div><!-- .col -->
         
        </div><!-- .row -->
    </div>
    <div class="nk-wizard-head">
        <h5>Step 3</h5>
    </div>
    <div class="nk-wizard-content">
    <h5>Waste Collection Quotation</h5>
        <div class="row gy-2">
        <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="fw-username">Waste Type</label>
            <div class="form-control-wrap">
                <input type="text" data-msg="Required" class="form-control required" id="fw-username" placeholder="Organic" name="fw-username" Readonly>
            </div>
        </div>
    </div><!-- .col -->
        <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="fw-username">Quantity</label>
            <div class="form-control-wrap">
                <input type="text" data-msg="Required" class="form-control required" id="fw-username" name="fw-username" placeholder="2" Readonly>
            </div>
        </div>
    </div><!-- .col -->
    <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="fw-username">Total Cost</label>
            <div class="form-control-wrap">
                <input type="text" data-msg="Required" class="form-control required" id="fw-username" name="fw-username" placeholder="USD50" Readonly>
            </div>
        </div>
    </div><!-- .col -->
    <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="fw-username">Recurrence</label>
            <div class="form-control-wrap">
                <input type="text" data-msg="Required" class="form-control required" id="fw-username" name="fw-username" placeholder="4" Readonly>
            </div>
        </div>
    </div><!-- .col -->
    <!-- <div class="col-md-6">
    <div class="form-group">
        <label class="form-label" for="fw-nationality">Payment Method</label>
        <div class="form-control-wrap ">
            <div class="form-control-select">
                <select class="form-control required" data-msg="Required" id="fw-nationality" name="fw-nationality" required>
                    <option value="">Ecocash</option>
                    <option value="us">Bank</option>
                </select>
            </div>
        </div>
    </div>
        </div> -->
        <div class="col-md-12">
                <div class="custom-control custom-checkbox">
                    <input type="checkbox" data-msg="Required" class="custom-control-input required" name="fw-policy" id="fw-policy" required>
                    <label class="custom-control-label" for="fw-policy">I agreed Terms and policy</label>
                </div>
            </div>
        </div><!-- .row -->
    </div>
    </form><!-- form -->
  </div>    


  <div class="tab-pane" id="tabItem6"> 
      
    <form action="#">
        <div class="row g-4">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Trading Name</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Work Address</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-12">
                <div class="form-group">
                    <label class="form-label" for="email-address-1">Email address</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="email-address-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="phone-no-1">Mobile Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="phone-no-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pay-amount-1">Collection Address</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="pay-amount-1">
                    </div>
                </div>
            </div>
   
            <div class="form-group">
            <div class="custom-control custom-control-xs custom-checkbox">
                <input type="checkbox" class="custom-control-input" class="checkbox">
                <label class="custom-control-label" for="checkbox">I agree to E-logistics <a tabindex="-1" href="#">Privacy Policy</a> &amp; <a tabindex="-1" href="#"> Terms.</a></label>
            </div>
        
        </div>
        
        <div class="form-group">
            <button class="btn btn-lg btn-primary btn-block">Register</button>
        </div>
        </div>
    </form><!-- form --> 
  </div> 
  <div class="tab-pane" id="tabItem7"> 
  <form action="#">
        <div class="row g-4">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">First Name</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Last Name</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="email-address-1">Email address</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="email-address-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="phone-no-1">Mobile Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="phone-no-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pay-amount-1">Home Address</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="pay-amount-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pay-amount-1">Collection Address</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="pay-amount-1">
                    </div>
                </div>
            </div>
  
            <div class="form-group">
            <div class="custom-control custom-control-xs custom-checkbox">
                <input type="checkbox" class="custom-control-input" id="checkbox">
                <label class="custom-control-label" for="checkbox">I agree to E-logistics <a tabindex="-1" href="#">Privacy Policy</a> &amp; <a tabindex="-1" href="#"> Terms.</a></label>
            </div>
        </div>
        <div class="form-group">
            <button class="btn btn-lg btn-primary btn-block">Register</button>
        </div>
        </div>
    </form><!-- form -->
</div>
</div><!-- .card-inner-group -->
</div><!-- .card -->
</div><!-- .nk-block -->
</div>
</div>
</div>
</div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022 VAYA TECHNOLOGIES.<a href="https://softnio.com" target="_blank"></a>
                            </div>
                            <div class="nk-footer-links">
                                <ul class="nav nav-sm">
                                    <li class="nav-item dropup">
                                        <a herf="#" class="dropdown-toggle dropdown-indicator has-indicator nav-link" data-toggle="dropdown" data-offset="0,10"><span>English</span></a>
                                        <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                            <ul class="language-list">
                                                <li>
                                                    <a href="#" class="language-item">
                                                        <span class="language-name">English</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="language-item">
                                                        <span class="language-name">Español</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="language-item">
                                                        <span class="language-name">Français</span>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="#" class="language-item">
                                                        <span class="language-name">Türkçe</span>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </li>
                                    <li class="nav-item">
                                        <a href="#" data-toggle="modal" data-target="#region" class="nav-link"><em class="icon ni ni-globe"></em><span class="ml-1">Select Region</span></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
  <!-- Add Payment Method-->
  <div class="modal fade" tabindex="-1" role="dialog" id="add-pay-method">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <h5 class="modal-title">Select Payment Method</h5>
                                           
                       
                    <ul class="nav nav-tabs"> 
    <li class="nav-item"> 
       <a class="nav-link active" data-toggle="tab" href="#tabItem1"><em class="icon ni ni-invest"></em><span>Ecocash</span></a> 
   </li>  
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem2"><em class="icon ni ni-coin"></em><span>Cash</span></a> 
  </li>    
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem3"><em class="icon ni ni-wallet-alt"></em><span>Zipit</span></a> 
  </li>   
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem4"><em class="icon ni ni-repeat"></em><span>Internal Transfer</span></a> 
  </li>  
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem9"><em class="icon ni ni-tranx"></em><span>Rtgs</span></a> 
  </li>  
  </ul>
  <div class="tab-content">    
  <div class="tab-pane active" id="tabItem1">
    <form action="#">
        <div class="row g-4">
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="phone-no-1">Mobile Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="phone-no-1" required>
                    </div>
                </div>
            </div>

            <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                    <button class="btn btn-primary">Pay</button>
                </li>
                <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li>
            </ul>
        </div>
        </div>
    </form><!-- form -->
  </div>    
  <div class="tab-pane" id="tabItem2"> 
    <form action="#">
        <div class="row g-4">

            <div class="form-group">
            <div class="custom-control custom-control-xs custom-checkbox">
                <input type="checkboxs" class="custom-control-input" class="checkbox">
                <label class="custom-control-label" for="checkboxs">I confirm to pay using cash</label>
            </div>
        
        </div>
        
        <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                    <button class="btn btn-primary">Pay</button>
                </li>
                <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li>
            </ul>
        </div>
        </div>
    </form><!-- form --> 
  </div> 
  <div class="tab-pane" id="tabItem3"> 
  <form action="#">
        <div class="row g-4">
        <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Customer Name</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Reference</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Amount</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="fw-nationality">Choose Bank</label>
                <div class="form-control-wrap ">
                    <div class="form-control-select">
                        <select class="form-control required" data-msg="Required" id="fw-nationality" name="fw-nationality" required>
                            <option value="">BancABC</option>
                            <option value="">CABS</option>
                            <option value="">CBZ</option>
                        </select>
                    </div>
                </div>
            </div>
        </div><!-- .col -->
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="phone-no-1">Phone Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="phone-no-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pay-amount-1">Customer Account Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="pay-amount-1">
                    </div>
                </div>
            </div>
       
            <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                    <button class="btn btn-primary">Pay</button>
                </li>
                <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li>
            </ul>
        </div>
        </div>
    </form><!-- form -->
</div>
<div class="tab-pane" id="tabItem4"> 
  <form action="#">
        <div class="row g-4">
        <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Customer Name</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Narration</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Amount</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="phone-no-1">Phone Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="phone-no-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pay-amount-1">Customer Account Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="pay-amount-1">
                    </div>
                </div>
            </div>
       
            <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                    <button class="btn btn-primary">Pay</button>
                </li>
                <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li>
            </ul>
        </div>
        </div>
    </form><!-- form -->
</div>
<div class="tab-pane" id="tabItem9"> 
  <form action="#">
        <div class="row g-4">
        <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Customer Name</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Destination Name</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Reason</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="full-name-1">Amount</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="full-name-1">
                    </div>
                </div>
            </div>
            <div class="col-md-6">
            <div class="form-group">
                <label class="form-label" for="fw-nationality">Choose Bank</label>
                <div class="form-control-wrap ">
                    <div class="form-control-select">
                        <select class="form-control required" data-msg="Required" id="fw-nationality" name="fw-nationality" required>
                            <option value="">BancABC</option>
                            <option value="">CABS</option>
                            <option value="">CBZ</option>
                        </select>
                    </div>
                </div>
            </div>
        </div><!-- .col -->
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="phone-no-1">Phone Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="phone-no-1">
                    </div>
                </div>
            </div>
            <div class="col-lg-6">
                <div class="form-group">
                    <label class="form-label" for="pay-amount-1">Customer Account Number</label>
                    <div class="form-control-wrap">
                        <input type="text" class="form-control" id="pay-amount-1">
                    </div>
                </div>
            </div>
       
            <div class="col-12">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                    <button class="btn btn-primary">Pay</button>
                </li>
                <li>
                    <a href="#" class="link" data-dismiss="modal">Cancel</a>
                </li>
            </ul>
        </div>
        </div>
    </form><!-- form -->
</div>
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->
    <!-- Edit Payment Method-->
    <div class="modal fade" tabindex="-1" role="dialog" id="edit-pay-method">
        <div class="modal-dialog modal-lg" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                <div class="modal-body modal-body-md">
                    <h5 class="modal-title">Edit Payment Method</h5>
                    <form action="#" class="mt-2">
                        <div class="row g-gs">
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="payment-name-edit">Name</label>
                                    <input type="text" class="form-control" id="payment-name-edit" value="Cash" placeholder="Payment Name">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="form-group">
                                    <label class="form-label" for="edit-status">Status</label>
                                    <select class="form-select" id="edit-status">
                                        <option>Active</option>
                                        <option>Inctive</option>
                                    </select>
                                </div>
                            </div>
                            <div class="col-12">
                                <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                                    <li>
                                        <button class="btn btn-primary">Update</button>
                                    </li>
                                    <li>
                                        <a href="#" class="link" data-dismiss="modal">Cancel</a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </form>
                </div><!-- .modal-body -->
            </div><!-- .modal-content -->
        </div><!-- .modal-dialog -->
    </div><!-- .modal -->
 <!-- JavaScript -->
 <?php require_once('includes/footer.php');?>
</body>

</html>