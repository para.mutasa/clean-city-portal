<?php 
session_start();
if (!isset($_SESSION['franchise_Id'])) {
    header("Location: ../../portal/franchise-login");
    exit();
  }
  $franchise_id =  $_SESSION["franchise_Id"];
  $phone_number =   $_SESSION["phone_number"];
  $email =   $_SESSION["email"];
  $company =  $_SESSION["company"];
  $franchise_status =  $_SESSION["status"];
  
include_once('../../utils/VayaCleanCityUtility.php');
require_once('includes/header.php');
$vehicles = json_decode(getVehiclesByFranchiseId($franchise_id), true);
$partners = json_decode(getAllPartners(), true);

// var_dump($vehicles);
// exit;
?>

<body class="nk-body bg-lighter npc-general has-sidebar ">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- sidebar @s -->
            <?php require_once('includes/sidebar.php');?>
            <!-- sidebar @e -->


            <!-- wrap @s -->
            <div class="nk-wrap ">
                <!-- main header @s -->
                <div class="nk-header nk-header-fixed is-light">
                    <div class="container-fluid">
                        <div class="nk-header-wrap">
                            <div class="nk-menu-trigger d-xl-none ml-n1">
                                <a href="#" class="nk-nav-toggle nk-quick-nav-icon" data-target="sidebarMenu"><em class="icon ni ni-menu"></em></a>
                            </div>
                            <div class="nk-header-brand d-xl-none">
                                <a href="portal/admin/index" class="logo-link">
                                    <img class="logo-light logo-img" src="./images/logo.png" srcset="./images/logo2x.png 2x" alt="logo">
                                    <img class="logo-dark logo-img" src="./images/logo-dark.png" srcset="./images/logo-dark2x.png 2x" alt="logo-dark">
                                </a>
                            </div><!-- .nk-header-brand -->
                            <div class="nk-header-news d-none d-xl-block">
                                <div class="nk-news-list">
                                    <a class="nk-news-item" href="#">
                                        <!-- <div class="nk-news-icon">
                                            <em class="icon ni ni-card-view"></em>
                                        </div>
                                        <div class="nk-news-text">
                                            <p>Do you know the latest update of 2021? <span> A overview of our is now available on YouTube</span></p>
                                            <em class="icon ni ni-external"></em>
                                        </div> -->
                                    </a>
                                </div>
                            </div><!-- .nk-header-news -->
                            <div class="nk-header-tools">
                                <ul class="nk-quick-nav">
                                    <li class="dropdown user-dropdown">
                                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">
                                            <div class="user-toggle">
                                                <div class="user-avatar sm">
                                                    <em class="icon ni ni-user-alt"></em>
                                                </div>
                                                <div class="user-info d-none d-md-block">
                                                    <div class="user-status">Administrator</div>
                                                    <div class="user-name dropdown-indicator"><?php echo $admin_firstname;?></div>
                                                </div>
                                            </div>
                                        </a>
                                        <div class="dropdown-menu dropdown-menu-md dropdown-menu-right dropdown-menu-s1">
                                            <div class="dropdown-inner user-card-wrap bg-lighter d-none d-md-block">
                                                <div class="user-card">
                                                    <div class="user-avatar">
                                                        <span>AB</span>
                                                    </div>
                                                    <div class="user-info">
                                                    <span class="lead-text"><?php echo  $admin_firstname;?></span>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                    <!-- <li><a href="#"><em class="icon ni ni-user-alt"></em><span>View Profile</span></a></li>
                                                    <li><a href="html/hotel/settings.html"><em class="icon ni ni-setting-alt"></em><span>Account Setting</span></a></li>
                                                    <li><a href="html/hotel/settings-activity-log.html"><em class="icon ni ni-activity-alt"></em><span>Login Activity</span></a></li> -->
                                                    <li><a class="dark-switch" href="#"><em class="icon ni ni-moon"></em><span>Dark Mode</span></a></li>
                                                </ul>
                                            </div>
                                            <div class="dropdown-inner">
                                                <ul class="link-list">
                                                <li><a href="portal/admin/logout"><em class="icon ni ni-signout"></em><span>Sign out</span></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </li><!-- .dropdown -->
                              
                                </ul><!-- .nk-quick-nav -->
                            </div><!-- .nk-header-tools -->
                        </div><!-- .nk-header-wrap -->
                    </div><!-- .container-fliud -->
                </div>
                <!-- main header @e -->
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="container-fluid">
                        <div class="nk-content-inner">
                            <div class="nk-content-body">
                                <div class="nk-block-head nk-block-head-sm">
                                    <div class="nk-block-between">
                                        <div class="nk-block-head-content">
                                            <h3 class="nk-block-title page-title">Vehicles List</h3>
                                         
                                        </div><!-- .nk-block-head-content -->
                                        <div class="nk-block-head-content">
                                            <div class="toggle-wrap nk-block-tools-toggle">
                                                <a href="#" class="btn btn-icon btn-trigger toggle-expand mr-n1" data-target="pageMenu"><em class="icon ni ni-menu-alt-r"></em></a>
                                                <div class="toggle-expand-content" data-content="pageMenu">
                                                    <ul class="nk-block-tools g-3">
                                                    <li class="nk-block-tools-opt"><a href="portal/partner/add-vehicle" class="btn btn-primary"><em class="icon ni ni-reports"></em><span>Add Vehicle</span></a></li>
                                                        <!-- <li class="nk-block-tools-opt">
                                                            <div class="drodown">
                                                                <a href="#" class="dropdown-toggle btn btn-icon btn-primary" data-toggle="dropdown"><em class="icon ni ni-plus"></em></a>
                                                                <div class="dropdown-menu dropdown-menu-right">
                                                                    <ul class="link-list-opt no-bdr">
                                                                        <li><a href="portal/admin/add-vehicle"><span>Add Vehicle</span></a></li> 
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                        </li> -->
                                                    </ul>
                                                </div>
                                            </div><!-- .toggle-wrap -->
                                        </div><!-- .nk-block-head-content -->
                                    </div><!-- .nk-block-between -->
                                </div><!-- .nk-block-head -->

                                <div class="nk-block nk-block-lg">
                                    
                                        <div class="card card-preview">
                                            <div class="card-inner">
                                            <table class="datatable-init nowrap nk-tb-list nk-tb-ulist" data-auto-responsive="false">
                                                    <thead>
                                                        <tr class="nk-tb-item nk-tb-head">
                                                            <th class="nk-tb-col"><span class="sub-text">Number Plate</span></th>
                                                            <th class="nk-tb-col"><span class="sub-text">Model</span></th>
                                                            <th class="nk-tb-col tb-col-md"><span class="sub-text">Capacity</span></th>
                                                            <th class="nk-tb-col tb-col-mg"><span class="sub-text">Franchise</span></th>
                                                            <th class="nk-tb-col tb-col-mg"><span class="sub-text">Status</span></th>
                                                            <th class="nk-tb-col nk-tb-col-tools text-right">
                                                            </th>
                                                    </thead>
                                                    <tbody>
                                                    <?php if ($vehicles['responseStatus'] == "SUCCESS") : ?>
                                                    <?php
                                                        $vehicles['eobjResponse'] = isset($vehicles['eobjResponse']) ?  $vehicles['eobjResponse'] : 0;
                                                        $vehicles['eobjResponse'] = is_array($vehicles['eobjResponse']) ? $vehicles['eobjResponse'] : array();
                                                                             
                                                    foreach ($vehicles['eobjResponse'] as $vehicle) :
                                                    ?>
                                                        <tr>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $vehicle['licensePlateNumber'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $vehicle['model'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $vehicle['capacity'] ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= json_decode(getFranchiseById($vehicle['franchiseId']),true)['eobjResponse']['company']  ?></span></td>
                                                        <td class="nk-tb-col tb-col-md"><span><?= $vehicle['vehicleStatus'] ?></span></td>
                                                        <td class="nk-tb-col nk-tb-col-tools">
                                                            <ul class="nk-tb-actions gx-1">
                                                             
                                                                <li>
                                                                    <div class="drodown">
                                                                        <a href="#" class="dropdown-toggle btn btn-icon btn-trigger" data-toggle="dropdown"><em class="icon ni ni-more-h"></em></a>
                                                                        <div class="dropdown-menu dropdown-menu-right">
                                                                            <ul class="link-list-opt no-bdr">
                                                                            <li><a href="#" data-toggle="modal" data-target="#edit-vehicle<?= $vehicle['vehicleId'] ?>" data-target="#edit-waste-type"><em class="icon ni ni-edit"></em><span>Edit</span></a></li>

                                                                            </ul>
                                                                        </div>
                                                                    </div>
                                                                </li>
                                                            </ul>
                                                        </td>
                                                    <!-- Edit Vehicle-->
                                                    <div class="modal fade" tabindex="-1" role="dialog"   id="edit-vehicle<?= $vehicle['vehicleId'] ?>">
                                                    <div class="modal-dialog modal-lg" role="document">
                                                        <div class="modal-content">
                                                            <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross-sm"></em></a>
                                                            <div class="modal-body modal-body-md">
                                                                <h5 class="modal-title">Edit Vehicle</h5>
                                                                <div class="card-inner">
                                                                <form id ="ecoVehicle<?= $vehicle['vehicleId'] ?>" class="mt-2">
                                                <div class="row gy-4">
                                                    <div class="col-md-6 col-lg-6 col-xxl-3">
                                                        <div class="form-group">
                                                        <label class="form-label" for="first-name">Model</label>
                                                        <input type="text" class="form-control" name="model" value="<?= $vehicle['model'] ?>"  >
                                                        </div>
                                                    </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">Registration Plate</label>
                                                    <input type="text" class="form-control" name="licensePlateNumber" value="<?= $vehicle['licensePlateNumber'] ?>" >
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label">Franchise</label>
                                                        <div class="form-control-wrap">
                                                        <select class="form-select" name ="franchiseId" data-placeholder="Select Franchise" value="<?= $vehicle['company'] ?>" >
                                                            <?php foreach ($partners['eobjResponse']  as $partner): ?>
                                                                <option value="">Select Franchise</option>
                                                                <option value="<?= $partner["franchiseId"] ?>"><?= $partner["company"].""?></option>
                                                            <?php endforeach; ?> 
                                                            </select>
                                                        </div> 
                                                   </div>
                                                </div>
                                                <!--col-->
                                                <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status"> <span class="sr-only">Loading...</span> </div></div>
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="phone-no">Capacity</label>
                                                    <input type="text" class="form-control" name="capacity" value="<?= $vehicle['capacity'] ?>" >
                                                   </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">Make</label>
                                                     <input type="text" class="form-control" name="make" value="<?= $vehicle['make'] ?>" >
                                                    </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label" for="last-name">Year</label>
                                                    <input type="date" class="form-control" name="year" lass="form-control date-picker" value="<?= $vehicle['year'] ?>" >    
                                                 </div>
                                                </div>
                                                <!--col-->
                                                <div class="col-md-6 col-lg-6 col-xxl-3">
                                                    <div class="form-group">
                                                    <label class="form-label">Status</label>
                                                        <div class="form-control-wrap">
                                                            <select class="form-select" name ="vehicleStatus" data-placeholder="Select Status" value="<?= $vehicle['vehicleStatus'] ?>" >
                                                                <option value="">Select Status</option>
                                                                <!-- <option value="AVAILABLE">AVAILABLE</option> -->
                                                                <option value="UNAVAILABLE">UNAVAILABLE</option>
                                                                <option value="DELETED">DELETED</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <!--col--> 
                                                <input type="hidden" class="form-control" name="vehicleId"  value="<?= $vehicle['vehicleId'] ?>" >
                                                <div class="col-sm-12">
                                                <div class="form-group">
                                                <input type="hidden" name="edit_vehicle" value="true">
                                                 <button type="button" class="btn btn-primary" name ="edit_vehicle"  onClick="editVehicle('<?= $vehicle["vehicleId"] ?>')" >Edit</button>
                                                    </div>
                                                </div>
                                                <!--col-->
                                            </div>
                                            <!--row-->
                                            </form>
                                          </div><!-- .card-inner-group -->
                                          <div class="modal-footer bg-light">
                                            <span class="sub-text"></span>
                                         </div>
                                       
                                                            </div><!-- .modal-body -->
                                                        </div><!-- .modal-content -->
                                                    </div><!-- .modal-dialog -->
                                                    </div><!-- .modal -->

                                                    <?php endforeach; ?>

                                                    <?php else : ?>

                                                    <?= $vehicles['responseMessage'] ?>

                                                    <?php endif; ?>

                                                     
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div><!-- .card-preview -->
                                    </div> <!-- nk-block -->

                               
                            </div>
                        </div>
                    </div>
                </div>
                <!-- content @e -->
                <!-- footer @s -->
                <div class="nk-footer">
                    <div class="container-fluid">
                        <div class="nk-footer-wrap">
                            <div class="nk-footer-copyright"> &copy; 2022  VAYA TECHNOLOGIES <a href="https://softnio.com" target="_blank"></a>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- footer @e -->
            </div>
            <!-- wrap @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
           <!-- Success Modal Alert -->
           <div class="modal fade" tabindex="-1" id="successAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <a href="#" class="close" data-dismiss="modal"><em class="icon ni ni-cross"></em></a>
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-check bg-success"></em>
                        <h4 class="nk-modal-title">Success!</h4>
                        <div class="nk-modal-text">
                        <div class="caption-text">You’ve successfully updated vehicle</div>
                        </div>
                        <div class="nk-modal-action">
                            <a href="#" class="btn btn-lg btn-mw btn-primary" data-dismiss="modal">OK</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!--Fail  Modal Alert -->
        <div class="modal fade" tabindex="-1" id="failAlert">
        <div class="modal-dialog" role="document">
            <div class="modal-content">
                <div class="modal-body modal-body-lg text-center">
                    <div class="nk-modal">
                        <em class="nk-modal-icon icon icon-circle icon-circle-xxl ni ni-cross bg-danger"></em>
                        <h4 class="nk-modal-title">Unable to Process!</h4>
                        <div class="nk-modal-text">
                        <div class='editVehicleResponse'></div>
                        </div>
                        <div class="nk-modal-action mt-5">
                            <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                        </div>
                    </div>
                </div><!-- .modal-body -->
                <div class="modal-footer bg-lighter">
                    <div class="text-center w-100">
                    
                    </div>
                </div>
            </div>
        </div>
    </div>
        <!-- BEGIN: AJAX CALLS-->
   <script>
    //Edit Vehicle
    function editVehicle(vehicleId) {
        // $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#ecoVehicle'+vehicleId).serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.responseStatus == "SUCCESS") {
                    $('#spinner').hide();
                    $('#edit-vehicle'+vehicleId).modal('hide');
                    $("#successAlert").modal('show');
                    setTimeout(function() {
                        window.location = "portal/partner/vehicles";
                    }, 2000);
                  
                } else {
                    $('.editVehicleResponse').empty();
                    $('.editVehicleResponse').append(json.responseMessage);
                    $('#spinner').hide();
                    $('#edit-vehicle'+vehicleId).modal('hide');
                    $("#failAlert").modal('show');
                    $("#failAlert").on("hidden.bs.modal", function() {
                        $(".editVehicleResponse").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.editVehicleResponse').empty();
                $('.editVehicleResponse').append(errorThrown);
                $('#spinner').hide();
                $('#edit-vehicle'+vehicleId).modal('hide');
                $("#failAlert").modal('show');
                $("#failAlert").on("hidden.bs.modal", function() {
                    $(".editVehicleResponse").html("");
                });
            }
        });
    }
    </script>

   <!-- JavaScript -->
   <?php require_once('includes/footer.php');?>
</body>

</html>