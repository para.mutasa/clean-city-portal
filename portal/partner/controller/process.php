<?php
include_once('../../../utils/VayaCleanCityUtility.php');

//add waste type
if (isset($_POST['add_waste'])) {
        $bagColor = $_POST['bagColor'];
        $capacity = $_POST['capacity'];
        $description = $_POST['description'];
        $variableCost = $_POST['variableCost'];
        $variableCostDescription = $_POST['variableCostDescription'];
        $wasteTypeName = $_POST['wasteTypeName'];

    //  var_dump($bagColor);
    // var_dump($capacity);
    // var_dump($description);
    // var_dump($variableCost);
    // var_dump($variableCostDescription);
    // var_dump($wasteTypeName);
    // exit;

        $add_waste_result = addWaste($bagColor,$capacity,$description,$variableCost,$variableCostDescription,$wasteTypeName);
        $add_waste_data = json_decode($add_waste_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_waste_data);
        exit;
    }

//add currency
if (isset($_POST['add_currency'])) {
    $name = $_POST['name'];
    $symbol = $_POST['symbol'];

    $add_currency_result = addCurrency($name,$symbol);
    $add_currency_data = json_decode($add_currency_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_currency_data);
    exit;
}

//add collection type
if (isset($_POST['add_collection_type'])) {
    $collectionType = $_POST['collectionType'];
    $unit = $_POST['unit'];
    $status = $_POST['status'];

    $add_collection_type_result = addCollectionType($collectionType,$unit,$status);
    $add_collection_type_data = json_decode($add_collection_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_collection_type_data);
    exit;
}

//add collection sub type
if (isset($_POST['add_sub_type'])) {
    $costPerUnit = $_POST['costPerUnit'];
    $imaxQty = $_POST['imaxQty'];
    $ratePerKm = $_POST['ratePerKm'];
    $vshow = "vshow";
    $vsubType = $_POST['vsubType'];
    $vunit = $_POST['vunit'];

    // var_dump($costPerUnit);
    // var_dump($imaxQty);
    // var_dump($ratePerKm);
    // var_dump($vshow);
    // var_dump($vsubType);
    // var_dump($vunit);
    // exit;
 

    $add_sub_type_result = addSubType($costPerUnit,$imaxQty,$ratePerKm,$vshow,$vsubType,$vunit);
    $add_sub_type_data = json_decode($add_sub_type_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_sub_type_data);
    exit;
}

//add city
if (isset($_POST['add_city'])) {
    $cityName = $_POST['cityName'];
    $country = $_POST['country'];
    $province = $_POST['province'];
    $countryCode = "+263";
 
    $add_city_result = addCity($cityName,$country,$province,$countryCode);
    $add_city_data = json_decode($add_city_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_city_data);
    exit;
}

//add franchise
if (isset($_POST['add_partner'])) {
    $address = $_POST['address'];
    $address2 = $_POST['address2'];
    $city = $_POST['cityId'];
    $code = "+263";
    $company = $_POST['company'];

    $country = $_POST['country'];
    $email = $_POST['email'];
    $language = $_POST['language'];


  $prepAddr = str_replace(' ','+',$address);
  $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key='.$GOOGLE_API_KEY.'');
  $output= json_decode($geocode);
  
  $latitude = $output->results[0]->geometry->location->lat;
  $longitude = $output->results[0]->geometry->location->lng;

    $phoneNumber = $_POST['phoneNumber'];
    $province = $_POST['province'];
    $state = $_POST['state'];
    $zimraBpNumber = $_POST['zimraBpNumber'];
 
    $add_partner_result = addPartner($address,$address2,$city,$code,$company,$country,$email,$language,$lattitude,$longitude,$phoneNumber,$province,$state,$zimraBpNumber);
    $add_partner_data = json_decode($add_partner_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_partner_data);
    exit;
}

//add vehicle
if (isset($_POST['add_vehicle'])) {
    $capacity = $_POST['capacity'];
    $franchiseId = $_POST['franchiseId'];
    $licensePlateNumber = $_POST['licensePlateNumber'];
    $make = $_POST['make'];

    $model = $_POST['model'];
    $vehicleStatus = $_POST['vehicleStatus'];
    $year = "2022-02-14T05:59:00.196Z";

    $add_vehicle_result = addVehicle($capacity,$franchiseId,$licensePlateNumber,$make,$model,$vehicleStatus,$year);
    $add_vehicle_data = json_decode($add_vehicle_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_vehicle_data);
    exit;
}
//add driver
if (isset($_POST['add_driver'])) {
    $type = "franchiseDriverSignup";
    $vfirstName = $_POST['vfirstName'];
    $vlastName = $_POST['vlastName'];
    $ifranchiseId = $_POST['ifranchiseId'];

    $vemail = $_POST['vemail'];
    $vphone = $_POST['vphone'];
    $caddress = $_POST['caddress'];
    $caddress2 = $_POST['caddress2'];
    $egender = $_POST['egender'];
    $phoneCode = "+263";
    $countryCode = "ZWL";
    $vpassword = $_POST['vpassword'];

    $add_driver_result = addDriver($type,$vfirstName,$vlastName,$ifranchiseId,$vemail,$vphone,$caddress,$caddress2,$egender,$phoneCode,$countryCode,$vpassword);
    $add_driver_data = json_decode($add_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_driver_data);
    exit;
}


//add payment method
if (isset($_POST['add_payment_method'])) {
    $description = $_POST['description'];

    $add_payment_method_result = addPaymentMethod($description);
    $add_payment_method_data = json_decode($add_payment_method_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_payment_method_data);
    exit;
}

//edit driver
if (isset($_POST['edit_driver'])) {
    $driverId = $_POST['driverId'];;
    $vname = $_POST['vname'];
    $vlastName = $_POST['vlastName'];
    $vemail = $_POST['vemail'];

    $vphone = $_POST['vphone'];
    $vcaddress = $_POST['vcaddress'];

    $vcadress2 = $_POST['vcadress2'];
    $egender = $_POST['egender'];
    $franchiseId = $_POST['franchiseId'];
    $status = $_POST['status'];
  
    $vpassword = $_POST['vpassword'];

    // var_dump($driverId);
    // var_dump($vname);
    // var_dump($vlastName);
    // var_dump($vemail);
    // var_dump($vphone);
    // var_dump($vcaddress);
    // var_dump($vcadress2);
    // var_dump($egender);
    // var_dump($franchiseId);
    // var_dump($status);
    // exit;

    $edit_driver_result = editDriver($driverId,$vname,$vlastName,$vemail,$vphone,$vcaddress,$vcadress2,$egender,$franchiseId,$status,$vpassword);
    $edit_driver_data = json_decode($edit_driver_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_driver_data);
    exit;
}

//edit franchise
if (isset($_POST['edit_partner'])) {
    $address = $_POST['address'];;
    $address2 = $_POST['address2'];
    $city = $_POST['cityId'];
    $code = "+263";

    $company = $_POST['company'];
    $country = $_POST['country'];

    $email = $_POST['email'];
    $franchiseId = $_POST['franchiseId'];
    $language = $_POST['language'];

    $prepAddr = str_replace(' ','+',$address);
    $geocode=file_get_contents('https://maps.google.com/maps/api/geocode/json?address='.$prepAddr.'&sensor=false&key='.$GOOGLE_API_KEY.'');
    $output= json_decode($geocode);
    
    $latitude = $output->results[0]->geometry->location->lat;
    $longitude = $output->results[0]->geometry->location->lng;
    $status = $_POST['status'];
  
    $vpassword = $_POST['vpassword'];
    $phoneNumber = $_POST['phoneNumber'];
  
    $province = $_POST['province'];

    $ratePerKM = $_POST['ratePerKM'];
  
    $registrationDate = $_POST['registrationDate'];
    $state = $_POST['state'];
    $status = $_POST['status'];
    $zimra_BP_Number = $_POST['zimra_BP_Number'];


    // var_dump($address);
    // var_dump($address2);
    // var_dump($city);
    // var_dump($code);
    // var_dump($company);
    // var_dump($country);
    // var_dump($email);
    // var_dump($franchiseId);
    // var_dump($language);
    // var_dump($latitude);
    // var_dump($longitude);
    // var_dump($phoneNumber);
    // var_dump($province);

    // var_dump($ratePerKM);
    // var_dump($registrationDate);
    // var_dump($state);
    // var_dump($status);
    // var_dump($zimra_BP_Number);
    // exit;

    $edit_partner_result = editPartner($address,$address2,$city,$code,$company,$country,$email,$franchiseId,$language,$latitude,$longitude,$phoneNumber,$province,$ratePerKM,$registrationDate,$state,$status,$zimra_BP_Number);
    $edit_partner_data = json_decode($edit_partner_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_partner_data);
    exit;
}

//edit vehicle
if (isset($_POST['edit_vehicle'])) {
    $capacity = $_POST['capacity'];;
    $franchiseId = $_POST['franchiseId'];
    $licensePlateNumber = $_POST['licensePlateNumber'];

    $make = $_POST['make'];
    $model = $_POST['model'];

    $vehicleId = $_POST['vehicleId'];
    $vehicleStatus = $_POST['vehicleStatus'];
    $wasteTypeId = $_POST['wasteTypeId'];
    $year = $_POST['year'];

  
    // var_dump($capacity);
    // var_dump($franchiseId);
    // var_dump($licensePlateNumber);
    // var_dump($make);
    // var_dump($model);
    // var_dump($vehicleId);
    // var_dump($vehicleStatus);
    // var_dump($wasteTypeId);
    // var_dump($year);
    // exit;

    $edit_vehicle_result = editVehicle($capacity,$franchiseId,$licensePlateNumber,$make,$model,$vehicleId,$vehicleStatus,$wasteTypeId,$year);
    $edit_vehicle_data = json_decode($edit_vehicle_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_vehicle_data);
    exit;
}

//edit payment method
if (isset($_POST['edit_payment'])) {
    $createdDate = $_POST['createdDate'];
    $description = $_POST['description'];
    $paymentMethodId = $_POST['paymentMethodId'];
    $status = $_POST['status'];

    // var_dump($createdDate);
    // var_dump($description);
    // var_dump($paymentMethodId);
    // var_dump($status);
    // exit;

    $edit_payment_method_result = editPaymentMethod($createdDate,$description,$paymentMethodId,$status);
    $edit_payment_method_data = json_decode($edit_payment_method_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_payment_method_data);
    exit;
}
    //user login

if (isset($_POST['user_login'])) {
    $email = $_POST['email'];
    $password = $_POST['password'];
    $user_login_result = userLogin($email,$password);
    $user_login_data_data = json_decode($user_login_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($user_login_data_data);
    exit;
}

    //delete role
if (isset($_POST['delete_role'])) {
    $id = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $delete_role_result = deleteRole($id,$lastModifiedBy);
    $delete_role_data = json_decode($delete_role_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($delete_role_data);
    exit;
}

    //delete equip
    if (isset($_POST['delete_equip'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_equip_result = deleteEquip($id,$lastModifiedBy);
        $delete_equip_data = json_decode($delete_equip_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_equip_data);
        exit;
    }

    //delete order
    if (isset($_POST['delete_order'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_delete_result = deleteOrder($id,$lastModifiedBy);
        $delete_delete_data = json_decode($delete_delete_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_delete_data);
        exit;
    }

    //delete invoice
    if (isset($_POST['delete_invoice'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_customer_result = deleteInvoice($id,$lastModifiedBy);
        $delete_customer_data = json_decode($delete_customer_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_customer_data);
        exit;
    }


    //delete incident report
    if (isset($_POST['delete_report'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_report_result = deleteReport($id,$lastModifiedBy);
        $delete_report_data = json_decode($delete_report_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_report_data);
        exit;
    }



    //delete  equip
    if (isset($_POST['delete_equip'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_equip_result = deleteEquip($id,$lastModifiedBy);
        $delete_equip_data = json_decode($delete_equip_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_equip_data);
        exit;
    }

    //delete cylinder checks
    if (isset($_POST['delete_cylinder_check'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_cylinder_check_result = deleteCylinderCheck($id,$lastModifiedBy);
        $delete_cylinder_check_data = json_decode($delete_cylinder_check_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_cylinder_check_data);
        exit;
    }

    //delete  admin
    if (isset($_POST['delete_admin'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_user_result = deleteUser($id,$lastModifiedBy);
        $delete_user_data = json_decode($delete_user_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_user_data);
        exit;
    }

     //delete  oxy plant
     if (isset($_POST['delete_plant'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_plant_result = deletePlant($id,$lastModifiedBy);
        $delete_delete_data = json_decode($delete_plant_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_delete_data);
        exit;
    }

       //delete label
       if (isset($_POST['delete_label'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_label_result = deleteLabel($id,$lastModifiedBy);
        $delete_label_data = json_decode($delete_label_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_label_data);
        exit;
    }

          //delete Cylinder
          if (isset($_POST['delete_cylinder'])) {
            $id = $_POST['id'];
            $lastModifiedBy = $_POST['lastModifiedBy'];
            $delete_cylinder_result = deleteCylinder($id,$lastModifiedBy);
            $delete_cylinder_data = json_decode($delete_cylinder_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($delete_cylinder_data);
            exit;
        }


    //add Admin
if (isset($_POST['add_admin'])) {
    $country = $_POST['country'];
    $franchiseeId = $_POST['franchiseeId'];
    $firstName = $_POST['firstName'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $lastName = $_POST['lastName'];
    $maker = $_POST['maker'];
    $oxygenPlantId = $_POST['oxygenPlantId'];
    $phoneNumber = $_POST['phoneNumber'];
    $recordStatus = "CREATED";
    $userType = $_POST['name'];
    $vemail = $_POST['email'];

    $add_user_result = addNewUser($franchiseeId,$country,$firstName,$lastModifiedBy,$lastName,$maker,$oxygenPlantId,$phoneNumber,$recordStatus,$userType,$vemail);
    $add_user_data = json_decode($add_user_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_user_data);
    exit;
}

    //approve user
    if (isset($_POST['approve_user'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $checkerEmail = $_POST['checkerEmail'];
        $approve_user_result = approveUser($id,$lastModifiedBy,$checkerEmail);
        $approve_user_data = json_decode($approve_user_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($approve_user_data);
        exit;
    }

    //add Customer Order
    if (isset($_POST['add_order'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];
        $authorization = $_POST['authorization'];
        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $customerId = $_POST['customerid'];
        $cylinderPrice = $_POST['cylinderPrice'];
        $cylinderSize = $_POST['cylinderSize'];
        $dateDelivereds = $_POST['dateDelivered'];
        $timeDelivered = $_POST['timeDelivered'];
        $dateDelivered = $dateDelivereds." ".$timeDelivered;
     
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];
        $orderChannel = $_POST['orderChannel'];
        $orderType = $_POST['orderType'];
        $recordStatus = "CREATED";
        $salesAgent = $_POST['salesAgent'];
        $unitsOrdered = $_POST['unitsOrdered'];
        $plantId = $_POST['plantId'];

        $add_order_result = addOrder($franchiseeId,
        $country,$authorization,$authorized,$checker,
        $customerId,$cylinderPrice,$cylinderSize,
        $dateDelivered,$lastModifiedBy,$maker,$orderChannel,
        $orderType,$recordStatus,$salesAgent,$unitsOrdered,$plantId);
        $add_order_data = json_decode($add_order_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_order_data);
        exit;
    }

 
   



        //edit Customer Order
        if (isset($_POST['edit_order'])) {
            $franchiseeId = $_POST['franchiseeId'];
            $country = $_POST['country'];
            $id = $_POST['cid'];
            $authorization = $_POST['authorization'];
            $authorized = $_POST['authorized'];
            $checker = $_POST['checker'];
            $customerId = $_POST['customerId'];
            $cylinderPrice = $_POST['cylinderPrice'];
            $cylinderSize = $_POST['cylinderSize'];
            $dateDelivered = $_POST['dateDelivered']." ".$_POST['timeDelivered'];
            $invoiceNumber = $_POST['invoiceNumber'];
            $lastModifiedBy = $_POST['lastModifiedBy'];
            $maker = $_POST['maker'];
            $orderChannel = $_POST['orderChannel'];
            $orderType = $_POST['orderType'];
            $recordStatus = "CREATED";
            $salesAgent = $_POST['salesAgent'];
            $unitsOrdered = $_POST['unitsOrdered'];

            $add_edit_result = editOrder($franchiseeId,$country,$authorization,$authorized,$checker,$customerId,$cylinderPrice,$cylinderSize,$dateDelivered,$invoiceNumber,$lastModifiedBy,$maker,$orderChannel,$orderType,$recordStatus,$salesAgent,$unitsOrdered,$id);
            $add_edit_data = json_decode($add_edit_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($add_edit_data);
            exit;
        }

    //add Incident Report
    if (isset($_POST['add_report'])) {

        
        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $authorized = $_POST['authorized'];
        $businessHead = $_POST['businessHead'];
        $engineer = $_POST['engineer'];

        $followUpRecommendation = $_POST['followUpRecommendation'];
        $incidentCause = $_POST['incidentCause'];
        $incidentDetails = $_POST['incidentDetails'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $locationOfIncident = $_POST['locationOfIncident'];
        $maker = $_POST['maker'];
        $recordStatus = "CREATED";
        $plantManager = $_POST['plantManager'];
        $resolution = $_POST['resolution'];

        $serviceRequired = $_POST['serviceRequired'];
        $shift = $_POST['shift'];
        $typeOfService = $_POST['typeOfService'];
        $add_report_result = addNewReport($franchiseeId,$country,$authorized,$businessHead,$engineer,$followUpRecommendation,$incidentCause,$incidentDetails,$lastModifiedBy,$locationOfIncident,$maker,$plantManager,$recordStatus,$resolution,$serviceRequired,$shift,$typeOfService);
        $add_report_data = json_decode($add_report_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_report_data);
        exit;
    }

        //update Incident Report
        if (isset($_POST['edit_report'])) {
            
        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

            $id = $_POST['id'];
            $authorized = $_POST['authorized'];
            $businessHead = $_POST['businessHead'];
            $engineer = $_POST['engineer'];

            $followUpRecommendation = $_POST['followUpRecommendation'];
            $incidentCause = $_POST['incidentCause'];
            $incidentDetails = $_POST['incidentDetails'];
            $lastModifiedBy = $_POST['lastModifiedBy'];
            $locationOfIncident = $_POST['locationOfIncident'];
            $maker = $_POST['maker'];
            $recordStatus = "CREATED";
            $plantManager = $_POST['plantManager'];
            $resolution = $_POST['resolution'];

            $serviceRequired = $_POST['serviceRequired'];
            $shift = $_POST['shift'];
            $typeOfService = $_POST['typeOfService'];
            $edit_report_result = editReport($franchiseeId,$country,$authorized,$businessHead,$engineer,$followUpRecommendation,$incidentCause,$incidentDetails,$lastModifiedBy,$locationOfIncident,$maker,$plantManager,$recordStatus,$resolution,$serviceRequired,$shift,$typeOfService,$id);
            $edit_report_data = json_decode($edit_report_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($edit_report_data);
            exit;
        }





    //add Oxy Production
    if (isset($_POST['add_admin'])) {
        $authorized = $_POST['authorized'];
        $batchNumber = $_POST['batchNumber'];
        $checker = $_POST['checker'];
        $engineer = $_POST['engineer'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];
        $numberOfCylindersFilled = $_POST['numberOfCylindersFilled'];
        $oxygenProduction = $_POST['oxygenProduction'];
        $plantManager = $_POST['plantManager'];
        $recordStatus = "CREATED";
        $shiftTime = "37:00:20";
        list($hour, $minute,$second) = explode(":", $shiftTime);

        $vemail = $_POST['vemail'];
        $add_oxy_prod_result = addOxyProd($authorized,$batchNumber,$checker,$engineer,$lastModifiedBy,$maker,$numberOfCylindersFilled,$oxygenProduction,$plantManager,$recordStatus,$userType,$vemail);
        $add_oxy_prod_data = json_decode($add_oxy_prod_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_oxy_prod_data);
        exit;
    }



    //add Cylinder Checks
    if (isset($_POST['add_cylinder_check'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $cylinderId = $_POST['cylinderId'];

        $cylinderStandardWeight = $_POST['cylinderStandardWeight'];
        $engineer = $_POST['engineer'];
        $issuesWithWeight = $_POST['issuesWithWeight'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];

        $plantManager = $_POST['plantManager'];
        $purityCheck = $_POST['purityCheck'];

        $plantManager = $_POST['plantManager'];
        $purityCheck = $_POST['purityCheck'];

        $purityIssues = $_POST['purityIssues'];
        $purityLevel = $_POST['purityLevel'];

        $recordStatus = "CREATED";
        $weightChecked = $_POST['weightChecked'];

        $add_cylinder_check_result = addNewCylinderCheckUp($franchiseeId,$country,$franchiseeId,$country,$authorized,$checker,$cylinderId,$cylinderStandardWeight,$engineer,$issuesWithWeight,$lastModifiedBy,$maker,$plantManager,$purityCheck,$purityIssues,$purityLevel,$recordStatus,$weightChecked);
        $add_cylinder_check_data = json_decode($add_cylinder_check_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_cylinder_check_data);
        exit;
    }



    //add Oxygen Plant
    if (isset($_POST['add_plant'])) {

        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $email = $_POST['email'];
        $lastModifiedBy = $_POST['lastModifiedBy'];

        $location = $_POST['location'];
        $maker = $_POST['maker'];
        $phoneNumber = $_POST['phoneNumber'];

        $plantName = $_POST['plantName'];

        $recordStatus = "CREATED";

        $add_oxy_plant_result = addOxyPlant($authorized,$checker,$email,$lastModifiedBy,$location,$maker,$phoneNumber,$plantName,$recordStatus);
        $add_oxy_plant_data = json_decode($add_oxy_plant_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_oxy_plant_data);
        exit;
    }



    //add payment method
    if (isset($_POST['add_payment'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $currency = $_POST['currency'];
        $lastModifiedBy = $_POST['lastModifiedBy'];


        $maker = $_POST['maker'];
        $name = $_POST['name'];


        $recordStatus = "CREATED";

        $add_payment_result = addPayment($franchiseeId,$country,$authorized,$checker,$currency,$lastModifiedBy,$maker,$name,$recordStatus);
        $add_payment_data = json_decode($add_payment_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_payment_data);
        exit;
    }

        //edit payment method
        if (isset($_POST['edit_payment'])) {
            $franchiseeId = $_POST['franchiseeId'];
            $country = $_POST['country'];
    
            $id = $_POST['id'];
            $authorized = $_POST['authorized'];
            $checker = $_POST['checker'];
            $currency = $_POST['currency'];
            $lastModifiedBy = $_POST['lastModifiedBy'];


            $maker = $_POST['maker'];
            $name = $_POST['name'];


            $recordStatus = "CREATED";

            $edit_payment_result = editPayment($franchiseeId,$country,$authorized,$checker,$currency,$lastModifiedBy,$maker,$name,$recordStatus,$id);
            $edit_payment_data = json_decode($edit_payment_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($edit_payment_data);
            exit;
        }




    //add Invoice
    if (isset($_POST['add_invoice'])) {

        $customerOrderId = $_POST['orderId'];
        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];
        $authorized = $_POST['authorized'];
        $checker = "";
        $description = $_POST['description'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];
        $paymentMethod = $_POST['paymentMethod'];
        $quantity = $_POST['quantity'];
        $recordStatus = "CREATED";
        $total = $_POST['total'];
        $transactionNumber = $_POST['transactionNumber'];
        $unitPrice = $_POST['unitPrice'];
        // $deliveryLogId = 0;
        // $invoiceNumber = "string";
        // $invoiceStatus="string";
        // $plantId = $_POST['plantId'];

     
        $add_invoice_result = addInvoice($customerOrderId,$franchiseeId,$country,$authorized,$checker,$description,$lastModifiedBy,$maker,$paymentMethod,$quantity,$recordStatus,$total,$transactionNumber,$unitPrice);
        $add_invoice_data = json_decode($add_invoice_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_invoice_data);
        exit;
    }

     //edit  Invoice
     if (isset($_POST['edit_invoice'])) {

        $customerOrderId = $_POST['customerOrderId'];
        $deliveryLogId = $_POST['deliveryLogId'];
        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $authorized = $_POST['authorized'];
        $id = $_POST['id'];
        $checker = $_POST['checker'];
      


        $description = $_POST['description'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];
        $paymentMethod = $_POST['name'];

        $quantity = $_POST['quantity'];

        $recordStatus = "CREATED";

        $total = $_POST['total'];
        $transactionNumber = $_POST['transactionNumber'];
        $unitPrice = $_POST['unitPrice'];


        $edi_invoice_result = editInvoice($customerOrderId,$deliveryLogId,$franchiseeId,$country,$authorized,$checker,$description,$lastModifiedBy,$maker,$paymentMethod,$quantity,$recordStatus,$total,$transactionNumber,$unitPrice,$id);
        $edi_invoice_data = json_decode($edi_invoice_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($edi_invoice_data);
        exit;
    }



    //add Equip
    if (isset($_POST['add_equip'])) {

        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $engineer = $_POST['engineer'];


        $equipmentMaintained = $_POST['equipmentMaintained'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maintenanceDescription = $_POST['maintenanceDescription'];
        $maintenancePerformedBy = $_POST['maintenancePerformedBy'];

        $maintenanceTeamContact = $_POST['maintenanceTeamContact'];


        $maintenanceTeamName = $_POST['maintenanceTeamName'];
        $maintenanceType = $_POST['maintenanceType'];

        $maker = $_POST['maker'];


        $partsUsed = $_POST['partsUsed'];
        $plantManager = $_POST['plantManager'];

        $recordStatus = "CREATED";

        $remarks = $_POST['remarks'];


        $add_equip_result = addEquip($authorized,$checker,$engineer,$equipmentMaintained,$lastModifiedBy,$maintenanceDescription,$maintenancePerformedBy,$maintenanceTeamContact,$maintenanceTeamName,$maintenanceType,$maker,$partsUsed,$plantManager,$recordStatus,$remarks);
        $add_equip_data = json_decode($add_equip_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_equip_data);
        exit;
    }

     //edit Equip
     if (isset($_POST['edit_equip'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];


        $id = $_POST['id'];
        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $engineer = $_POST['engineer'];


        $equipmentMaintained = $_POST['equipmentMaintained'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maintenanceDescription = $_POST['maintenanceDescription'];
        $maintenancePerformedBy = $_POST['maintenancePerformedBy'];

        $maintenanceTeamContact = $_POST['maintenanceTeamContact'];


        $maintenanceTeamName = $_POST['maintenanceTeamName'];
        $maintenanceType = $_POST['maintenanceType'];

        $maker = $_POST['maker'];


        $partsUsed = $_POST['partsUsed'];
        $plantManager = $_POST['plantManager'];

        $recordStatus = "CREATED";

        $remarks = $_POST['remarks'];


        $edit_equip_result = updateEquip($franchiseeId,$country,$authorized,$checker,$engineer,$equipmentMaintained,$lastModifiedBy,$maintenanceDescription,$maintenancePerformedBy,$maintenanceTeamContact,$maintenanceTeamName,$maintenanceType,$maker,$partsUsed,$plantManager,$recordStatus,$remarks,$id);
        $edit_equip_data = json_decode($edit_equip_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($edit_equip_data);
        exit;
    }


    //update Oxygen Plant
    if (isset($_POST['edit_plant'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $id = $_POST['id'];
        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $email = $_POST['email'];
        $lastModifiedBy = $_POST['lastModifiedBy'];

        $location = $_POST['location'];
        $maker = $_POST['maker'];
        $phoneNumber = $_POST['phoneNumber'];

        $plantName = $_POST['plantName'];

        $recordStatus = "CREATED";

        $edit_oxy_plant_result = editOxyPlant($franchiseeId,$country,$authorized,$checker,$email,$lastModifiedBy,$location,$maker,$phoneNumber,$plantName,$recordStatus,$id);
        $edit_oxy_plant_data = json_decode($edit_oxy_plant_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($edit_oxy_plant_data);
        exit;
    }




     //add Cylinder Tracking
     if (isset($_POST['add_cylinder_tracking'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $authorized = $_POST['authorized'];
        $checkedIn = $_POST['checkedIn'];
        $checkedOut = $_POST['checkedOut'];

        $checker = $_POST['checker'];
        $customerId = $_POST['id'];
        $cylindersDelivered = $_POST['cylindersDelivered'];
        // $lastModifiedBy = "blessingnyuwani@gmail.com";
        $cylindersReturned = $_POST['cylindersReturned'];

        $estimatedReturnDate = $_POST['estimatedReturnDate'];
        $invoicingId = $_POST['invoiceNumber'];

        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];


        $recordStatus = "CREATED";


        $add_cylinder_tracking_result = addNewCylinderTracking($franchiseeId,$country,$authorized,$checkedIn,$checkedOut,$checker,$customerId,$cylindersDelivered,$cylindersReturned,$estimatedReturnDate,$invoicingId,$lastModifiedBy,$maker,$recordStatus);
        $add_cylinder_tracking_data = json_decode($add_cylinder_tracking_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_cylinder_tracking_data);
        exit;
    }


     //edit Cylinder Tracking
     if (isset($_POST['edit_cylinder_tracking'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $id = $_POST['id'];
        $authorized = $_POST['authorized'];
        $checkedIn = $_POST['checkedIn'];
        $checkedOut = $_POST['checkedOut'];

        $checker = $_POST['checker'];
        $customerId = $_POST['id'];
        $cylindersDelivered = $_POST['cylindersDelivered'];

        $cylindersReturned = $_POST['cylindersReturned'];

        $estimatedReturnDate = $_POST['estimatedReturnDate'];
        $invoicingId = $_POST['invoiceNumber'];

        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];

        $recordStatus = "CREATED";


        $edit_cylinder_tracking_result = editCylinderTracking($franchiseeId,$country,$authorized,$checkedIn,$checkedOut,$checker,$customerId,$cylindersDelivered,$cylindersReturned,$estimatedReturnDate,$invoicingId,$lastModifiedBy,$maker,$recordStatus,$id);
        $edit_cylinder_tracking_data = json_decode($edit_cylinder_tracking_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($edit_cylinder_tracking_data);
        exit;
    }
     //update Cylinder Checks
     if (isset($_POST['edit_cylinder_check'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];


        $id = $_POST['id'];
        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $cylinderId = $_POST['cylinderId'];

        $cylinderStandardWeight = $_POST['cylinderStandardWeight'];
        $engineer = $_POST['engineer'];
        $issuesWithWeight = $_POST['issuesWithWeight'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];

        $plantManager = $_POST['plantManager'];
        $purityCheck = $_POST['purityCheck'];

        $plantManager = $_POST['plantManager'];
        $purityCheck = $_POST['purityCheck'];

        $purityIssues = $_POST['purityIssues'];
        $purityLevel = $_POST['purityLevel'];

        $recordStatus = "CREATED";
        $weightChecked = $_POST['weightChecked'];

        $update_cylinder_check_result = updateCylinderCheckUp($franchiseeId,$country,$authorized,$checker,$cylinderId,$cylinderStandardWeight,$engineer,$issuesWithWeight,$lastModifiedBy,$maker,$plantManager,$purityCheck,$purityIssues,$purityLevel,$recordStatus,$weightChecked,$id);
        $update_cylinder_check_data = json_decode($update_cylinder_check_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($update_cylinder_check_data);
        exit;
    }



    //add Cylinder
    if (isset($_POST['add_cylinder'])) {
        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $authorized = $_POST['authorized'];
        $batchNumber = $_POST['batchNumber'];
        $checker = $_POST['checker'];
        $color = $_POST['color'];
        $conditionStatus = $_POST['conditionStatus'];
        $cylinderSerialNumber = $_POST['cylinderSerialNumber'];
        $cylinderStandardWeight = $_POST['cylinderStandardWeight'];
        $cylinderStatus = $_POST['cylinderStatus'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];
        $recordStatus = "CREATED";
        $add_cylinder_result = addNewCylinder($franchiseeId,$country,$authorized,$batchNumber,$checker,$color,$conditionStatus,$cylinderSerialNumber,$cylinderStandardWeight,$cylinderStatus,$lastModifiedBy,$maker,$recordStatus);
        $add_cylinder_data = json_decode($add_cylinder_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_cylinder_data);
        exit;
    }

    //add Customer
    if (isset($_POST['add_current_customer'])) {

        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $customerName = $_POST['customerName'];
        $address = $_POST['address'];

        $agreedPrice = $_POST['agreedPrice'];
        $authorization = $_POST['authorization'];
        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $contactName = $_POST['contactName'];

        $contractType = $_POST['contractType'];
        $countryCode = $_POST['countryCode'];
        $dateEstablished = $_POST['dateEstablished'];
        $email = $_POST['email'];
        $expDeliverySchedule = $_POST['expDeliverySchedule'];

        $financeContactEmail = $_POST['financeContactEmail'];
        $financeContactName = $_POST['financeContactName'];
        $financeContactNumber = $_POST['financeContactNumber'];
        $lastModifiedBy = $_POST['lastModifiedBy'];

        $maker = $_POST['maker'];
        $natureOfBusiness = $_POST['natureOfBusiness'];
        $numberWk = $_POST['numberWk'];
        $o2ContactEmail = $_POST['o2ContactEmail'];
        $o2ContactName = $_POST['o2ContactName'];

        $o2ContactNumber = $_POST['o2ContactNumber'];
        $phoneCode = $_POST['phoneCode'];
        $phoneNumber = $_POST['phoneNumber'];
        $pinNumber = $_POST['pinNumber'];
        $recordStatus = "CREATED";

        $salesOfficer = $_POST['salesOfficer'];
        $vatNumber = $_POST['vatNumber'];
        $vcurrency = $_POST['vcurrency'];
        $vlang = $_POST['vlang'];
        $locationLatitude = $_POST['locationLatitude'];
        $locationLongitude = $_POST['locationLongitude'];


        $add_customer_result = addNewCustomer($franchiseeId,$country,$customerName,$address,
        $agreedPrice,
        $authorization,
        $authorized,
        $checker,
        $contactName,
        $contractType,
        $countryCode,
        $dateEstablished,
        $email,
        $expDeliverySchedule,
        $financeContactEmail,
        $financeContactName,
        $financeContactNumber,
        $lastModifiedBy,
        $maker,
        $natureOfBusiness,
        $numberWk,
        $o2ContactEmail,
        $o2ContactName,
        $o2ContactNumber,
        $phoneCode,
        $phoneNumber,
        $pinNumber,
        $recordStatus,
        $salesOfficer,
        $vatNumber,
        $vcurrency,
        $vlang,
        $locationLatitude,
        $locationLongitude
    );
        $add_customer_data = json_decode($add_customer_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_customer_data);
        exit;
    }


   //edit Customer
   if (isset($_POST['edit_current_customer'])) {

    $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

    $customerName = $_POST['customerName'];
    $address = $_POST['address'];

    $id = $_POST['id'];
    $agreedPrice = $_POST['agreedPrice'];
    $authorization = $_POST['authorization'];
    $authorized = $_POST['authorized'];
    $checker = $_POST['checker'];
    $contactName = $_POST['contactName'];

    $contractType = $_POST['contractType'];
    $countryCode = $_POST['countryCode'];
    $dateEstablished = $_POST['dateEstablished'];
    $email = $_POST['email'];
    $expDeliverySchedule = $_POST['expDeliverySchedule'];

    $financeContactEmail = $_POST['financeContactEmail'];
    $financeContactName = $_POST['financeContactName'];
    $financeContactNumber = $_POST['financeContactNumber'];
    $lastModifiedBy = $_POST['lastModifiedBy'];

    $maker = $_POST['maker'];
    $natureOfBusiness = $_POST['natureOfBusiness'];
    $numberWk = $_POST['numberWk'];
    // $o2ContactEmail = $_POST['o2ContactEmail'];
    // $o2ContactName = $_POST['o2ContactName'];

    // $o2ContactNumber = $_POST['o2ContactNumber'];
    $phoneCode = $_POST['phoneCode'];
    $phoneNumber = $_POST['phoneNumber'];
    $pinNumber = $_POST['pinNumber'];
    $recordStatus = "CREATED";

    $salesOfficer = $_POST['salesOfficer'];
    $vatNumber = $_POST['vatNumber'];
    $vcurrency = $_POST['vcurrency'];
    $vlang = $_POST['vlang'];
    $locationLatitude = $_POST['locationLatitude'];
    $locationLongitude = $_POST['locationLongitude'];


    $add_customer_result = editCustomer($franchiseeId,$country,$customerName,$address,
    $agreedPrice,
    $authorization,
    $authorized,
    $checker,
    $contactName,
    $contractType,
    $countryCode,
    $dateEstablished,
    $email,
    $expDeliverySchedule,
    $financeContactEmail,
    $financeContactName,
    $financeContactNumber,
    $lastModifiedBy,
    $maker,
    $natureOfBusiness,
    $numberWk,

    $phoneCode,
    $phoneNumber,
    $pinNumber,
    $recordStatus,
    $salesOfficer,
    $vatNumber,
    $vcurrency,
    $vlang,
    $id,
    $locationLatitude,
    $locationLongitude
);
    $add_customer_data = json_decode($add_customer_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_customer_data);
    exit;
}




    //add Oxy Plant
    if (isset($_POST['add_plant'])) {
        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $email = $_POST['email'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $location = $_POST['location'];
        $maker = $_POST['maker'];
        $phoneNumber = $_POST['phoneNumber'];
        $plantName = $_POST['plantName'];
        $recordStatus = "CREATED";
        $add_oxy_plant_result = addOxyPlant($franchiseeId,$country,$authorized,$checker,$email,$lastModifiedBy,$location,$maker,$phoneNumber,$plantName,$recordStatus);
        $add_oxy_plant_data = json_decode($add_oxy_plant_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_oxy_plant_data);
        exit;
    }



    //add customer log
    if (isset($_POST['add_customer_log'])) {
        $country = $_POST['country'];
        $franchiseeId = $_POST['franchiseeId'];

        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $contactDetails = $_POST['contactDetails'];

        $customerDesignation = $_POST['customerDesignation'];

        $timeofincident = $_POST['timeofincident'];
        $dateofincident = $_POST['dateofincident'];
        $dateOfIncident = date('Y-m-d H:i', strtotime("$dateofincident $timeofincident"));


        $timeOfResolution = $_POST['timeOfResolution'];
        $dateOfResolution = $_POST['dateOfResolution'];
        $dateTimeOfresolution = date('Y-m-d H:i', strtotime("$dateOfResolution $timeOfResolution"));

        $description = $_POST['description'];
        $issueLoggedBy = $_POST['issueLoggedBy'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];

        $recordStatus = "CREATED";
        $resolution = $_POST['resolution'];
        $signOff = $_POST['signOff'];
        $timeOfIncident = $_POST['timeOfIncident'];
        $add_log_result = addLog($franchiseeId,$country,$authorized,$checker,$contactDetails,$customerDesignation,$dateOfIncident,$dateTimeOfresolution,$description,$issueLoggedBy,$lastModifiedBy,$maker,$recordStatus,$resolution,$signOff,$timeOfIncident);
        $add_log_data = json_decode($add_log_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_log_data);
        exit;
    }


        //edit customer log
        if (isset($_POST['edit_customer_log'])) {

            $country = $_POST['country'];
            $franchiseeId = $_POST['franchiseeId'];
            $id = $_POST['id'];
            $authorized = $_POST['authorized'];
            $checker = $_POST['checker'];
            $contactDetails = $_POST['contactDetails'];

            $customerDesignation = $_POST['customerDesignation'];


            $timeofincident = $_POST['timeofincident'];
            $dateofincident = $_POST['dateofincident'];
            $dateOfIncident = date('Y-m-d H:i', strtotime("$dateofincident $timeofincident"));


            $timeOfResolution = $_POST['timeOfResolution'];
            $dateOfResolution = $_POST['dateOfResolution'];
            $dateTimeOfresolution = date('Y-m-d H:i', strtotime("$dateOfResolution $timeOfResolution"));

            $description = $_POST['description'];
            $issueLoggedBy = $_POST['issueLoggedBy'];
            $lastModifiedBy = $_POST['lastModifiedBy'];
            $maker = $_POST['maker'];

            $recordStatus = "CREATED";
            $resolution = $_POST['resolution'];
            $signOff = $_POST['signOff'];
            $timeOfIncident = $_POST['timeOfIncident'];
            $edit_log_result = editLog($franchiseeId,$country,$authorized,$checker,$contactDetails,$customerDesignation,$dateOfIncident,$dateTimeOfresolution,$description,$issueLoggedBy,$lastModifiedBy,$maker,$recordStatus,$resolution,$signOff,$timeOfIncident,$id);
            $edit_log_data = json_decode($edit_log_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($edit_log_data);
            exit;
        }


   //add delivery log
   if (isset($_POST['add_delivery_log'])) {

    $customerId = $_POST['customerId'];
    $customeOrderId = $_POST['customeOrderId'];

    $deliveredCylinderId = $_POST['deliveredCylinderId'];
    $franchiseeId = $_POST['franchiseeId'];
    $country = $_POST['country'];

    $authorized = $_POST['authorized'];
    $authorizedBy = $_POST['authorizedBy'];
    $beginningOdometer = $_POST['beginningOdometer'];

    $checker = $_POST['checker'];


    // $timeOfResolution = $_POST['timeOfResolution'];
    // $dateOfResolution = $_POST['dateOfResolution'];
    // $dateTimeOfresolution = date('Y-m-d H:i', strtotime("$dateOfResolution $timeOfResolution"));

  
    $cylinderSize = $_POST['cylinderSize'];

    $cylindersDelivered = $_POST['cylindersDelivered'];

    $deliveryDate = $_POST['deliveryDate'];
    // $dated = $_POST['dated'];
    // $deliveryDate = date('Y-m-d H:i', strtotime("$dated $timed"));



    $driverId = $_POST['driverId']; // roles all
    $dropOffLocation = $_POST['dropOffLocation'];
    $dropOffTime = $_POST['dropOffTime'];

    $endingOdometer = $_POST['endingOdometer'];
    $invoiceNumber = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $maker = $_POST['maker'];
    $paymentMethodId = $_POST['id']; //invoice getall
    $pickup = $_POST['pickup'];
    $recordStatus = "CREATED";
    $takeOffLocation = $_POST['takeOffLocation'];

    $takeOffTime = $_POST['takeOffTime'];
    $timeOfDelivery = $_POST['timeOfDelivery'];


    $add_delivery_result = addDelivery($customeOrderId,$deliveredCylinderId,$franchiseeId,$country,$authorized,$authorizedBy,$beginningOdometer,$checker,$customerId,$cylinderSize,$cylindersDelivered,$deliveryDate,$driverId,$dropOffLocation,$dropOffTime,$endingOdometer,$invoiceNumber,$lastModifiedBy,$maker,$paymentMethodId,$pickup,$recordStatus,$takeOffLocation,$takeOffTime,$timeOfDelivery);
    $add_delivery_data = json_decode($add_delivery_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_delivery_data);
    exit;
}


   //edit delivery log
   if (isset($_POST['edit_delivery_log'])) {

    $customerId = $_POST['customerId'];
    $customeOrderId = $_POST['customeOrderId'];

    $deliveredCylinderId = $_POST['deliveredCylinderId'];
    $franchiseeId = $_POST['franchiseeId'];
    $country = $_POST['country'];

    $id = $_POST['id'];
    $authorized = $_POST['authorized'];
    $authorizedBy = $_POST['authorizedBy'];
    $beginningOdometer = $_POST['beginningOdometer'];

    $checker = $_POST['checker'];


    // $timeOfResolution = $_POST['timeOfResolution'];
    // $dateOfResolution = $_POST['dateOfResolution'];
    // $dateTimeOfresolution = date('Y-m-d H:i', strtotime("$dateOfResolution $timeOfResolution"));


    $cylinderSize = $_POST['cylinderSize'];

    $cylindersDelivered = $_POST['cylindersDelivered'];

    $deliveryDate = $_POST['deliveryDate'];

    $driverId = $_POST['driverId']; // roles all
    $dropOffLocation = $_POST['dropOffLocation'];
    $dropOffTime = $_POST['dropOffTime'];

    $endingOdometer = $_POST['endingOdometer'];
    $invoiceNumber = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $maker = $_POST['maker'];
    $paymentMethodId = $_POST['id'];
    $pickup = $_POST['pickup'];
    $recordStatus = "CREATED";
    $takeOffLocation = $_POST['takeOffLocation'];

    $takeOffTime = $_POST['takeOffTime'];
    $timeOfDelivery = $_POST['timeOfDelivery'];
  

    $edit_delivery_result = editDelivery($customeOrderId,$deliveredCylinderId,$franchiseeId,$country,$authorized,$authorizedBy,$beginningOdometer,$checker,$customerId,$cylinderSize,$cylindersDelivered,$deliveryDate,$driverId,$dropOffLocation,$dropOffTime,$endingOdometer,$invoiceNumber,$lastModifiedBy,$maker,$paymentMethodId,$pickup,$recordStatus,$takeOffLocation,$takeOffTime,$timeOfDelivery,$id);
    $edit_delivery_data = json_decode($edit_delivery_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($edit_delivery_data);
    exit;
}



    //add Label
    if (isset($_POST['add_label'])) {
        $authorized = $_POST['authorized'];
        $checker = $_POST['checker'];
        $labelKey = $_POST['labelKey'];
        $labelValue = $_POST['labelValue'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];
        $recordStatus = "CREATED";
        $add_label_result = addNewLabel($authorized,$checker,$labelKey,$labelValue,$lastModifiedBy,$maker,$recordStatus);
        $add_label_data = json_decode($add_label_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($add_label_data);
        exit;
    }

        //update Label
        if (isset($_POST['edit_label'])) {
            $authorized = $_POST['authorized'];
            $checker = $_POST['checker'];
            $labelKey = $_POST['labelKey'];
            $labelValue = $_POST['labelValue'];
            $lastModifiedBy = $_POST['lastModifiedBy'];
            $maker = $_POST['maker'];
            $recordStatus = "CREATED";
            $id = $_POST['id'];
            $update_label_result = updateLabel($authorized,$checker,$labelKey,$labelValue,$lastModifiedBy,$maker,$recordStatus,$id);
            $update_label_data = json_decode($update_label_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($update_label_data);
            exit;
        }


// update role
    if (isset($_POST['edit_role'])) {
        $id = $_POST['id'];
        $authorized = $_POST['authorized'];
        $name = $_POST['name'];
        $maker = $_POST['maker'];
        $checker = $_POST['checker'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $recordStatus = "CREATED";
        $update_role_result = updateRole($authorized,$checker,$lastModifiedBy,$maker,$name,$id,$recordStatus);
        $update_role_data = json_decode($update_role_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($update_role_data);
        exit;
    }

     //update Cylinder
     if (isset($_POST['edit_cylinder'])) {
        $franchiseeId = $_POST['franchiseeId'];
        $country = $_POST['country'];

        $id = $_POST['id'];
        $authorized = $_POST['authorized'];
        $batchNumber = $_POST['batchNumber'];
        $checker = $_POST['checker'];
        $color = $_POST['color'];
        $conditionStatus = $_POST['conditionStatus'];
        $cylinderSerialNumber = $_POST['cylinderSerialNumber'];
        $cylinderStandardWeight = $_POST['cylinderStandardWeight'];
        $cylinderStatus = $_POST['cylinderStatus'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];
        $recordStatus = "CREATED";
        $update_cylinder_result = updateCylinder($franchiseeId,$country,$authorized,$batchNumber,$checker,$color,$conditionStatus,$cylinderSerialNumber,$cylinderStandardWeight,$cylinderStatus,$lastModifiedBy,$maker,$recordStatus,$id);
        $update_cylinder_data = json_decode($update_cylinder_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($update_cylinder_data);
        exit;
    }

  //Update  Admin
if (isset($_POST['edit_admin'])) {
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $id = $_POST['id'];
    $firstName = $_POST['firstName'];
    $lastName = $_POST['lastName'];
    $userType = $_POST['name'];
    $oxygenPlantId = $_POST['oxygenPlantId'];
    $phoneNumber = $_POST['phoneNumber'];

    $update_user_result = updateUser($firstName,$lastModifiedBy,$lastName,$oxygenPlantId,$phoneNumber,$userType,$id);
    $update_user_data = json_decode($update_user_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($update_user_data);
    exit;
}
/**Oxygen production */
 //add Oxygen Production
 if (isset($_POST['add_production'])) {
    $franchiseeId = $_POST['franchiseeId'];
    $country = $_POST['country'];
    $authorized = $_POST['authorized'];
    $checker = $_POST['checker'];
    $batchNumber = $_POST['batchNumber'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $numberOfCylindersFilled = $_POST['numberOfCylindersFilled'];
    $maker = "blessing";
    $oxygenProduction = $_POST['oxygenProduction'];
    $shiftTime = $_POST['shiftTime'];
    $startTime = $_POST['startTime'];
    $shutdownTime = $_POST['shutdownTime'];
    $engineer = $_POST['engineer'];
    $plantManager = $_POST['plantManager'];
    $recordStatus = "CREATED";
    $add_oxy_prod_result = addNewOxyProduction($franchiseeId,$country,$authorized,$checker,$batchNumber,$lastModifiedBy,$numberOfCylindersFilled,$maker,$oxygenProduction,$shiftTime,$startTime,$shutdownTime,$engineer,$plantManager,$recordStatus);
    $add_oxy_prod_data = json_decode($add_oxy_prod_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($add_oxy_prod_data);
    exit;
}
 //Edit Oxygen Production
 if (isset($_POST['edit_production'])) {
    $franchiseeId = $_POST['franchiseeId'];
    $country = $_POST['country'];

    $id= $_POST['id'];
    $authorized = $_POST['authorized'];
    $checker = $_POST['checker'];
    $batchNumber = $_POST['batchNumber'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $numberOfCylindersFilled = $_POST['numberOfCylindersFilled'];
    $maker = "blessing";
    $oxygenProduction = $_POST['oxygenProduction'];
    $shiftTime = $_POST['shiftTime'];
    $startTime = $_POST['startTime'];
    $shutdownTime = $_POST['shutdownTime'];
    $engineer = $_POST['engineer'];
    $plantManager = $_POST['plantManager'];
    $recordStatus = "CREATED";

    $update_oxy_prod_result = editOxyProduction($franchiseeId,$country,$authorized,$checker,$batchNumber,$lastModifiedBy,$numberOfCylindersFilled,$maker,$oxygenProduction,$shiftTime,$startTime,$shutdownTime,$engineer,$plantManager,$recordStatus,$id);
    $update_oxy_prod_data = json_decode($update_oxy_prod_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($update_oxy_prod_data);
    exit;
}
   //delete oxygen production
   if (isset($_POST['delete_production'])) {
    $id = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $delete_oxy_prod_result = deleteOxyProduction($id,$lastModifiedBy);
    $delete_oxy_prod_data = json_decode($delete_oxy_prod_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($delete_oxy_prod_data);
    exit;
}

   //delete delivery log
   if (isset($_POST['delete_delivery_log'])) {
    $id = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $delete_delivery_result = deleteDelivery($id,$lastModifiedBy);
    $delete_delivery_data = json_decode($delete_delivery_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($delete_delivery_data);
    exit;
}

  //delete payment Method
  if (isset($_POST['delete_payment'])) {
    $id = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $delete_payment_result = deletePayment($id,$lastModifiedBy);
    $delete_payment_data = json_decode($delete_payment_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($delete_payment_data);
    exit;
}

   //delete cylinder tracking
   if (isset($_POST['delete_cylinder_tracking'])) {
    $id = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $delete_tracking_result = deleteCylinderTracking($id,$lastModifiedBy);
    $delete_tracking_data = json_decode($delete_tracking_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($delete_tracking_data);
    exit;
}


   //delete delivery
   if (isset($_POST['delete_delivery'])) {
    $id = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $delete_delivery_result = deleteDelivery($id,$lastModifiedBy);
    $delete_delivery_data = json_decode($delete_delivery_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($delete_delivery_data);
    exit;
}

   //delete customer log
   if (isset($_POST['delete_customer_log'])) {
    $id = $_POST['id'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $delete_log_result = deleteLog($id,$lastModifiedBy);
    $delete_log_data = json_decode($delete_log_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($delete_log_data);
    exit;
}

  //forgot password
  if (isset($_POST['forgot_password'])) {
    $email = $_POST['email'];
    $forgot_password_result = forgotPassword($email);
    $forgot_password_data = json_decode($forgot_password_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($forgot_password_data);
    exit;
}
  //reset password
  if (isset($_POST['reset_password'])) {
    $resetToken = $_POST['resetToken'];
    $password = $_POST['password'];
    $reset_password_result = resetPassword($resetToken,$password);
    $reset_password_data = json_decode($reset_password_result, true, JSON_UNESCAPED_SLASHES);
    echo json_encode($reset_password_data);
    exit;
}
/**End Oxygen Production */



        //Delete Customer
        if (isset($_POST['delete_customer'])) {
            $id = $_POST['id'];
            $lastModifiedBy = $_POST['lastModifiedBy'];
            $delete_customer_result = deleteCustomer($id,$lastModifiedBy);
            $delete_customer_data = json_decode($delete_customer_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($delete_customer_data);
            exit;
        }
/**End of Customer */

/**Start QR */
  //delete franchise
  if (isset($_POST['generate_qr'])) {
    $cylinderSerialNumber =  $_POST['cylinderSerialNumber'];
 
    $generate_barcode_result = callWebApiGetBarCode($cylinderSerialNumber);
  $imageData = base64_encode($generate_barcode_result);

    echo '<img src="data:image/png;base64,' . $imageData . '" />';
 
}

/**End QR */
/**Franchise  */
 //add Franchise
     if (isset($_POST['add_franchise'])) {
        $franchiseeId = $_POST['franchiseeId'];
        $franchiseeName = $_POST['franchiseeName'];
        $address = $_POST['address'];
        $country = $_POST['country'];
        $email = $_POST['email'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $maker = $_POST['maker'];
        $phoneNumber = $_POST['phoneNumber'];
        $recordStatus = "CREATED";

        // var_dump($franchiseeId);
        // var_dump($franchiseeName);
        // var_dump($address);
        // var_dump($country);
        // var_dump($email);
        // var_dump($lastModifiedBy);
        // var_dump($maker);
        // var_dump($recordStatus);
        // exit;


        $add_franchise_result = addFranchise($franchiseeId,$franchiseeName,$address,$country,$email,$lastModifiedBy,$maker,$phoneNumber,$recordStatus);
        $add_franchise_data = json_decode($add_franchise_result, true, JSON_UNESCAPED_SLASHES);

        echo json_encode($add_franchise_data);
        exit;
    }
    
 //update Franchise
 if (isset($_POST['update_franchise'])) {
    $id = $_POST['id'];
    $authorized = $_POST['authorized'];
    $franchiseeId = $_POST['franchiseeId'];
    $franchiseeName = $_POST['franchiseeName'];
    $address = $_POST['address'];
    $country = $_POST['country'];
    $email = $_POST['email'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $maker = $_POST['maker'];
    $checker = $_POST['checker'];
    $phoneNumber = $_POST['phoneNumber'];
    $recordStatus = "CREATED";
    $edit_franchise_result = updateFranchise($authorized,$checker,$franchiseeId,$franchiseeName,$address,$country,$email,$lastModifiedBy,$maker,$phoneNumber,$recordStatus,$id);
    $edit_franchise_data = json_decode($edit_franchise_result, true, JSON_UNESCAPED_SLASHES);

    echo json_encode($edit_franchise_data);
    exit;
}

    //delete franchise
    if (isset($_POST['delete_franchise'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_franchise_result = deleteFranchise($id,$lastModifiedBy);
        $delete_franchise_data = json_decode($delete_franchise_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_franchise_data);
        exit;
    }

 /**End of Franchise */

 /**Delivered Cylinders  */

 //add D Cylinder
 if (isset($_POST['add_dcylineder'])) {
    $authorized = $_POST['authorized'];
    $checker = $_POST['checker'];
    $country = $_POST['country'];
    $cylinderSerialNumber = $_POST['cylinderSerialNumber'];
    $deliveryLogId = $_POST['deliveryLogId'];
    $franchiseeId = $_POST['franchiseeId'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $maker = $_POST['maker'];
    $recordStatus = "CREATED";

    $add_dcylinder_result = addDeliveredC($authorized,$checker,$country,$cylinderSerialNumber,$deliveryLogId,$franchiseeId,$lastModifiedBy,$maker,$recordStatus);
    $add_dcylinder_data = json_decode($add_dcylinder_result, true, JSON_UNESCAPED_SLASHES);

    echo json_encode($add_dcylinder_data);
    exit;
}

 //update D Cylinder
 if (isset($_POST['edit_dcylinder'])) {
    $id = $_POST['id'];
    $authorized = $_POST['authorized'];
    $checker = $_POST['checker'];
    $country = $_POST['country'];
    $cylinderSerialNumber = $_POST['cylinderSerialNumber'];
    $deliveryLogId = $_POST['deliveryLogId'];
    $franchiseeId = $_POST['franchiseeId'];
    $lastModifiedBy = $_POST['lastModifiedBy'];
    $maker = $_POST['maker'];
    $recordStatus = "CREATED";

    $update_dcylinder_result = updateDeliveredC($authorized,$checker,$country,$cylinderSerialNumber,$deliveryLogId,$franchiseeId,$lastModifiedBy,$maker,$recordStatus,$id);
    $update_dcylinder_data = json_decode($update_dcylinder_result, true, JSON_UNESCAPED_SLASHES);

    echo json_encode($update_dcylinder_data);
    exit;
}



    //delete D Cylinder
    if (isset($_POST['delete_deliveredC'])) {
        $id = $_POST['id'];
        $lastModifiedBy = $_POST['lastModifiedBy'];
        $delete_dcylinder_result = deleteDeliveredC($id,$lastModifiedBy);
        $delete_dcylinder_data = json_decode($delete_dcylinder_result, true, JSON_UNESCAPED_SLASHES);
        echo json_encode($delete_dcylinder_data);
        exit;
    }
  /**End of Delivered Cylinders */
        /**Equipment Maintanance */
         //add equipment Maintance
         if (isset($_POST['add_equip'])) {

            $franchiseeId = $_POST['franchiseeId'];
            $country = $_POST['country'];
    
            $authorized = $_POST['authorized'];
            $checker = $_POST['checker'];
            $maintenanceType = $_POST['maintenanceType'];
            $lastModifiedBy = $_POST['lastModifiedBy'];
            $equipmentMaintained = $_POST['equipmentMaintained'];
            $maker = "blessing";
            $maintenanceDescription = $_POST['maintenanceDescription'];
            $partsUsed = $_POST['partsUsed'];
            $maintenancePerformedBy = $_POST['maintenancePerformedBy'];
            $maintenanceTeamName = $_POST['maintenanceTeamName'];
            $maintenanceTeamContact = $_POST['maintenanceTeamContact'];
            $engineer = $_POST['engineer'];
            $plantManager = $_POST['plantManager'];
            $remarks = $_POST['remarks'];
            $recordStatus = "CREATED";
            $add_equip_main_result = addNewEquipmentMaintanance($franchiseeId,$country,$authorized,$checker,$maintenanceType,$lastModifiedBy,$equipmentMaintained,$maker,$maintenanceDescription,$partsUsed,$maintenancePerformedBy,$maintenanceTeamName,$maintenanceTeamContact,$engineer,$plantManager,$remarks,$recordStatus);
            $add_equip_main_data = json_decode($add_equip_main_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($add_equip_main_data);
            exit;
        }

           //delete oxygen production
           if (isset($_POST['delete_equip'])) {
            $id = $_POST['id'];
            $lastModifiedBy = $_POST['lastModifiedBy'];
            $delete_equip_main_result = deleteEquipmentMaintance($id,$lastModifiedBy);
            $delete_equip_main_data = json_decode($delete_equip_main_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($delete_equip_main_data);
            exit;
        }
        /**End Oxygen Production */
        //get one user
        if (isset($_POST['get_user'])) {
            $id = $_POST['id'];
            $get_user_result = userLogin($id);
            $get_user_data = json_decode($get_user_result, true, JSON_UNESCAPED_SLASHES);
            echo json_encode($get_user_data);
            exit;
        }
    
    
    ?>
