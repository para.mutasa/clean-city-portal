<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>VAYA TECHNOLOGIES</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <link rel="stylesheet" href="./assets/validate-password-requirements/css/jquery.passwordRequirements.css" />
    <style>
    .passwordInput{          
       margin-top: 5%;           
      /* text-align :center; */   
         }     
         .displayBadge{ 
         margin-top: 5%; 
         display: none;            
     text-align :center;      
      }
    </style>
</head>

<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-split nk-split-page nk-split-md">
                        <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container bg-white w-lg-45">
                            <div class="absolute-top-right d-lg-none p-3 p-sm-5">
                                <a href="#" class="toggle btn btn-white btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
                            </div>
                            <div class="nk-block nk-block-middle nk-auth-body">
                                <div class="brand-logo pb-5">
                                    <a href="portal/index" class="logo-link">
                                        <img class="logo-light logo-img logo-img-lg" src="./images/vaya_logo.png" srcset="./images/vaya_logo.png 1px" alt="logo">
                                        <img class="logo-dark logo-img logo-img-lg" src="./images/vaya_logo.png" srcset="./images/vaya_logo.png" alt="logo-dark">
                                    </a>
                                </div>
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title">Register</h5>
                                        <div class="nk-block-des">
                                            <p>Create New Account</p>
                                        </div>
                                        <strong><div  style ="color: #1C7ACD; text-align: center;" class='indCustomer'></div></strong> 
                                <strong>  <div  style ="color: #1C7ACD; text-align: center;" class='coCustomer'></div></strong>
                                    </div>
                                
                                    <ul class="nav nav-tabs"> 
    <li class="nav-item"> 
       <a class="nav-link active" data-toggle="tab" href="#tabItem5"><em class="icon ni ni-user"></em><span>Individual</span></a> 
   </li>  
  <li class="nav-item">
  <a class="nav-link" data-toggle="tab" href="#tabItem6"><em class="icon ni ni-users-fill"></em><span>Corporate</span></a> 
  </li>     
  </ul>
  <div class="tab-content">    
  <div class="tab-pane active" id="tabItem5">
  <form id = "addcustomer" action="#" class="mt-2" class="form-validate">
    <div class="row g-gs">
    <div class="col-md-6">
        <div class="form-group">
                <label class="form-label" for="payment-name-add">First Name</label>
                <input type="text" class="form-control"  name ="vFirstName" placeholder="" required>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
                <label class="form-label" for="payment-name-add">Last Name</label>
                <input type="text" class="form-control"  name ="vLastName" placeholder="" required>
            </div>
        </div>
    
        <div class="col-md-6">
        <div class="form-group">
                <label class="form-label" for="currency-add">Email</label>
                <input type="email" class="form-control"  name="vEmail" placeholder="">
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="currency-add">Mobile Number</label>
            <input type="tel" class="form-control"  name="vPhone"  placeholder="" required>
        </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="currency-add">Home Address</label>
            <input type="tel" class="form-control"  name="address"  placeholder="" required>
        </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label class="form-label" for="currency-add">Password</label>
                            <input type="password" class="form-control passwordInput" name="vPassword" id="PassEntry" required>
                            <span id="StrengthDisp" class="badge displayBadge">Weak</span>
        </div>
        </div>
        <!-- <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="currency-add">Confirm Password</label>
            <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
        </div>
        </div> -->
        <div class="col-10">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                    <input type="hidden" class="form-control" name="add_customer">
                    <button type="button" class="btn btn-primary" name ="add_customer" onClick="addCustomer()">Register</button>
                </li>
            </ul>
        </div>
    </div>
</form>
  </div>    
  <div class="tab-pane" id="tabItem6"> 
  <form id = "addcocustomer" action="#" class="mt-2" class="form-validate">
    <div class="row g-gs">
    <div class="col-md-6">
        <div class="form-group">
                <label class="form-label" for="payment-name-add">Tranding Name</label>
                <input type="text" class="form-control"  name ="name" placeholder="" required>
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
                <label class="form-label" for="payment-name-add">Work Address</label>
                <input type="text" class="form-control"  name ="vCaddress" placeholder="" required>
            </div>
        </div>
    
        <div class="col-md-6">
        <div class="form-group">
                <label class="form-label" for="currency-add">Email</label>
                <input type="email" class="form-control"  name="vEmail" placeholder="">
            </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="currency-add">Phone Number</label>
            <input type="tel" class="form-control"  name="vPhone"  placeholder="" required>
        </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="currency-add">Zimra BP Number</label>
            <input type="tel" class="form-control"  name="zimraBPNumber"  placeholder="">
        </div>
        </div>
        <div class="col-md-6">
        <div class="form-group">
        <label class="form-label" for="currency-add">Password</label>
        <input type="password" class="form-control passwordInput" name="vPassword" id="PassEntryco" required>
                            <span id="StrengthDispco" class="badge displayBadge">Weak</span>
        </div>
        </div>
        <!-- <div class="col-md-6">
        <div class="form-group">
            <label class="form-label" for="currency-add">Confirm Password</label>
            <input type="password" class="form-control"   id="confirm_password"  pattern="(?=.*\d)(?=.*[a-z])(?=.*[A-Z]).{8,}" title="Must contain at least one number and one uppercase and lowercase letter, and at least 8 or more characters" placeholder="">
        </div>
        </div> -->
        <div class="col-10">
            <ul class="align-center flex-wrap flex-sm-nowrap gx-4 gy-2">
                <li>
                    <input type="hidden" class="form-control" name="add_cocustomer">
                    <button type="button" class="btn btn-primary" name ="add_cocustomer" onClick="addCoCustomer()">Register</button>
                </li>
            </ul>
        </div>
    </div>
</form>
  </div> 
  </div>
                                                            
                                </div><!-- .nk-block-head -->
                          
                                <div class="form-note-s2 pt-4"> Already have an account ? <a href="portal/customer-login"><strong>Sign in instead</strong></a>
                                </div>
                                <!-- <div class="text-center pt-4 pb-3">
                                    <h6 class="overline-title overline-title-sap"><span>OR</span></h6>
                                </div>
                                <ul class="nav justify-center gx-8">
                                    <li class="nav-item"><a class="nav-link" href="#">Facebook</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Google</a></li>
                                </ul> -->
                            </div><!-- .nk-block -->
                            <div class="nk-block nk-auth-footer">
                            
                                <div class="mt-3">
                                    <p>&copy; 2022 VAYA TECHNOLOGIES. All Rights Reserved.</p>
                                </div>
                            </div><!-- nk-block -->
                        </div><!-- nk-split-content -->
                        <div class="nk-split-content nk-split-stretch bg-abstract"></div><!-- nk-split-content -->
                    </div><!-- nk-split -->
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
 <script src="./assets/js/bundle.js?ver=2.9.0"></script>
 <script src="./assets/js/scripts.js?ver=2.9.0"></script>
 <script src="./assets/validate-password-requirements/js/jquery.passwordRequirements.min.js"></script>
    
    <script>

let timeout;

    // traversing the DOM and getting the input and span using their IDs
let password = document.getElementById('PassEntry');
    let strengthBadge = document.getElementById('StrengthDisp');

    // The strong and weak password Regex pattern checker
let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthChecker(PasswordParameter){
        // We then change the badge's color and text based on the password strength
if(strongPassword.test(PasswordParameter)) {
            strengthBadge.style.backgroundColor = 'green';
            strengthBadge.textContent = 'Strong';
        } else if(mediumPassword.test(PasswordParameter)){
            strengthBadge.style.backgroundColor = 'blue';
            strengthBadge.textContent = 'Medium';
        } else{
            strengthBadge.style.backgroundColor = 'red';
            strengthBadge.textContent = 'Weak';
        }
    }

password.addEventListener("input", () => {
    strengthBadge.style.display = 'block';
    clearTimeout(timeout);
    timeout = setTimeout(() => StrengthChecker(password.value), 500);
    if(password.value.length !== 0) {
        strengthBadge.style.display != 'block';
    } else {
        strengthBadge.style.display = 'none';
    }
});



</script> 



<script>
//password coorperate
let timeoutco;

    // traversing the DOM and getting the input and span using their IDs
let passwordco = document.getElementById('PassEntryco');
    let strengthBadgeco = document.getElementById('StrengthDispco');

    // The strong and weak password Regex pattern checker
let strongPasswordco = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPasswordco = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthCheckerco(PasswordParameterco){
        // We then change the badge's color and text based on the password strength
if(strongPasswordco.test(PasswordParameterco)) {
    strengthBadgeco.style.backgroundColor = 'green';
    strengthBadgeco.textContent = 'Strong';
        } else if(mediumPasswordco.test(PasswordParameterco)){
            strengthBadgeco.style.backgroundColor = 'blue';
            strengthBadgeco.textContent = 'Medium';
        } else{
            strengthBadgeco.style.backgroundColor = 'red';
            strengthBadgeco.textContent = 'Weak';
        }
    }

    passwordco.addEventListener("input", () => {
        strengthBadgeco.style.display = 'block';
    clearTimeout(timeoutco);
    timeoutco = setTimeout(() => StrengthCheckerco(passwordco.value), 500);
    if(passwordco.value.length !== 0) {
        strengthBadgeco.style.display != 'block';
    } else {
        strengthBadgeco.style.display = 'none';
    }
});



</script> 
 <script>
    //Add Individual Customer
    function addCustomer() {
        var PasswordParameter = document.getElementById('PassEntry').value;

var vMsg = 'The minimum password length is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number, and 1 special character.';

if(!strongPassword.test(PasswordParameter)) {

alert(vMsg); return;

}
       
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addcustomer').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.Action == 1) {
                    $('.indCustomer').empty(); //clear apend
                    $('.indCustomer').append("Customer registered successfully");
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".indCustomer").html("");
                    });
                        setTimeout(function() {
                        window.location = "portal/customer-login";
                    }, 2000);
                } else {
                    $('.indCustomer').empty();
                    $('.indCustomer').append(json.Message);
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".indCustomer").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.indCustomer').empty();
                 $('.indCustomer').append(errorThrown);
                $(".modal").on("hidden.bs.modal", function() {
                $(".indCustomer").html("");
                });
            }
        });
    }
    
    </script>
       <script>
    //Add Co Customer
    function addCoCustomer() {
        var PasswordParameterco = document.getElementById('PassEntryco').value;

var vMsg = 'The minimum password length is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number, and 1 special character.';

if(!strongPasswordco.test(PasswordParameterco)) {

alert(vMsg); return;

}
        
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addcocustomer').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.Action == 1) {
                    $('.coCustomer').empty(); //clear apend
                    $('.coCustomer').append("Customer registered successfully");
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".coCustomer").html("");
                    });
                    setTimeout(function() {
                        window.location = "portal/customer-login";
                    }, 2000);
                } else {
                    $('.coCustomer').empty();
                    $('.coCustomer').append(json.Message);
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".coCustomer").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.coCustomer').empty();
                 $('.coCustomer').append(errorThrown);
                $(".modal").on("hidden.bs.modal", function() {
                $(".coCustomer").html("");
                });
            }
        });
    }
    
    </script>

</html>