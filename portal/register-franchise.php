
<?php 
include_once('../utils/VayaCleanCityUtility.php');
$cities = json_decode(getAllCities(), true);
// var_dump($cities);
// exit;
?>

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>VAYA TECHNOLOGIES</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <link rel="stylesheet" href="./assets/validate-password-requirements/css/jquery.passwordRequirements.css" />
    <style>
 .nk-auth-container {
  width: 45%;
  margin-right: 10%;
}


    .passwordInput{          
       margin-top: 5%;           
      /* text-align :center; */   
         }     
         .displayBadge{ 
         margin-top: 5%; 
         display: none;            
     text-align :center;      
      }  
    
    </style>
</head>

<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-split nk-split-page nk-split-md">
                        <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container bg-white w-lg-45">
                            <div class="absolute-top-right d-lg-none p-3 p-sm-5">
                                <a href="#" class="toggle btn btn-white btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
                            </div>
                            <div class="nk-block nk-block-middle nk-auth-body">
                                <div class="brand-logo pb-5">
                                    <a href="portal/index" class="logo-link">
                                        <img class="logo-light logo-img logo-img-lg" src="./images/vaya_logo.png" srcset="./images/vaya_logo.png 1px" alt="logo">
                                        <img class="logo-dark logo-img logo-img-lg" src="./images/vaya_logo.png" srcset="./images/vaya_logo.png" alt="logo-dark">
                                    </a>
                                </div>
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title">Register</h5>
                                        <div class="nk-block-des">
                                            <p>Create New Account</p>
                                        </div>
                                    </div>
                                    <div  style ="color: #1C7ACD; text-align: center;" class='franchise'></div>
                                    <ul class="nav nav-tabs"> 
    <li class="nav-item"> 
       <a class="nav-link active" data-toggle="tab" href="#tabItem5"><em class="icon ni ni-user"></em><span>Franchise</span></a> 
   </li>  
  </ul>
  <div class="tab-content" style="width: 120%;">    
  <div class="tab-pane active" id="tabItem5">
  <form id ="addpartner" action="#" class="form-validate is-alter">
    <div class="row gy-4">
    <div class="col-md-6 col-lg-4 col-xxl-3">
    <div class="form-control-wrap">
        <div class="form-group">
    
            <label class="form-label" for="first-name">Company Name</label>
            <input type="text" class="form-control" name ="company"  placeholder="" data-msg="Error message" required>
        </div>
    </div>
</div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label" for="last-name">Address</label>
            <input type="text" id="vcollectionsRequestAddress" data-toggle="modal" data-target="#show_map" class="form-control" name ="address"  readonly required>
            <input type="hidden" id="latitude" name ="franchiseLatitude" placeholder="latitude">
            <input type="hidden" id="longitude" name="franchiseLongitude" placeholder="longitude">
        </div>
    </div>
</div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label" for="last-name">Address (2)</label>
            <input type="text" class="form-control" name ="address2"  placeholder="" required>
        </div>
    </div>
</div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label" for="phone-no">Phone Number</label>
            <input type="text" class="form-control" name ="phoneNumber"  pattern="\+(9[976]\d|8[987530]\d|6[987]\d|5[90]\d|42\d|3[875]\d|2[98654321]\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\d{1,14}$" placeholder=" e.g. +26377X XXX XXX" required>
        </div>
    </div>
</div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label" for="last-name">Email</label>
            <input type="email" class="form-control" name ="email" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  placeholder="" required>
        </div>
    </div>
</div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
            <label class="form-label" for="last-name">ZIMRA BP Numnber</label>
            <input type="text" class="form-control" name ="zimraBpNumber"  placeholder="">
        </div>
    </div>
    <!--col-->
    <!-- <div class="d-flex justify-content-center">  <div class="spinner-border" id="spinner" style="width: 4rem; height: 4rem;" role="status"> <span class="sr-only">Loading...</span> </div></div> -->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label">Country</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="country" data-placeholder="Select Country" required>
                    <option value="">Select Country</option>
                    <option value="Zimbabwe">Zimbabwe</option>
                </select>
            </div>
        </div>
    </div>
</div>
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label" for="last-name">Province</label>
            <input type="text" class="form-control" name ="province"  placeholder="" required>
        </div>
    </div>
</div>
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label">City</label>
            <div class="form-control-wrap">
                <select class="form-select" name ="cityId" data-placeholder="Select City" required>
                <?php foreach ($cities['eobjResponse']  as $city): ?>
                    <option value="">Select City</option>
                    <option value="<?= $city["id"] ?>"><?= $city["cityName"].""?></option>
                <?php endforeach; ?> 
                </select>
            </div>
        </div>
    </div>
                </div>
    <!--col-->

    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label" for="last-name">State</label>
            <input type="text" class="form-control" name ="state"  placeholder="" required>
        </div>
    </div>
                </div>
    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
            <label class="form-label">Language</label>
            <div class="form-control-wrap">
                <select name ="language" class="form-select" data-placeholder="Select language" required>
                    <option value="English">English</option>
                </select>
            </div>
        </div>
    </div>
    </div>

    <!--col-->
    <div class="col-md-6 col-lg-4 col-xxl-3">
        <div class="form-group">
        <div class="form-control-wrap">
        <label class="form-label" for="currency-add">Password</label>
                            <input type="password" class="form-control passwordInput" name="password" id="PassEntry" required>
                            <span id="StrengthDisp" class="badge displayBadge">Weak</span>
        </div>
    </div>
                </div>

    <div class="col-sm-12">
        <div class="form-group">
        <input type="hidden" name="add_partner">
        <button type="button" class="btn btn-primary" name ="add_partner" onClick="addPartner()">Register</button>

        </div>
    </div>
    <!--col-->
    </div>
    <!--row-->
    </form>
  </div>    

  </div>
                                                            
                                </div><!-- .nk-block-head -->
                          
                                <div class="form-note-s2 pt-4"> Already have an account ? <a href="portal/franchise-login"><strong>Sign in instead</strong></a>
                                </div>
                                <!-- <div class="text-center pt-4 pb-3">
                                    <h6 class="overline-title overline-title-sap"><span>OR</span></h6>
                                </div>
                                <ul class="nav justify-center gx-8">
                                    <li class="nav-item"><a class="nav-link" href="#">Facebook</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Google</a></li>
                                </ul> -->
                            </div><!-- .nk-block -->
                            <div class="nk-block nk-auth-footer">
                            
                                <div class="mt-3">
                                    <p>&copy; 2022 VAYA TECHNOLOGIES. All Rights Reserved.</p>
                                </div>
                            </div><!-- nk-block -->
                        </div><!-- nk-split-content -->
                        <div class="nk-split-content nk-split-stretch bg-abstract"></div><!-- nk-split-content -->
                    </div><!-- nk-split -->
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

<!--Map  Modal  -->
 <div class="modal fade" tabindex="-1" id="show_map">
<div class="modal-dialog" role="document">
    <div class="modal-content">
        <div class="modal-body modal-body-lg text-center">
            <div class="nk-modal">
            <p><input class="postcode" id="Postcode" name="Postcode" type="text" value="">
            <input type="submit" id="findbutton" value="Find" onclick="getInputValue();" /></p>
            <div id="map" style="width:400px; height:350px"></div>
                <div class="nk-modal-action mt-5">
                    <a href="#" class="btn btn-lg btn-mw btn-light" data-dismiss="modal">Return</a>
                </div>
            </div>
        </div><!-- .modal-body -->
        <div class="modal-footer bg-lighter">
            <div class="text-center w-100">
            
            </div>
        </div>
    </div>
</div>
</div>
 
    <!-- JavaScript -->
 <script src="./assets/js/bundle.js?ver=2.9.0"></script>
 <script src="./assets/js/scripts.js?ver=2.9.0"></script>
 <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.1/jquery.min.js"></script>
 <script src="https://maps.googleapis.com/maps/api/js?key=AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs"></script>

 <script>
  function getInputValue(){
            // Selecting the input element and get its value 
            var inputVal = document.getElementById("Postcode").value;
            document.getElementById("vcollectionsRequestAddress").value = inputVal;
            // Displaying the value
            // alert(inputVal);
        }
  </script>
    <script>
 var geocoder = new google.maps.Geocoder();
var marker = null;
var map = null;
function initialize() {
      var $latitude = document.getElementById('latitude');
      var $longitude = document.getElementById('longitude');
      var latitude = -17.891120
      var longitude = 30.934920;
      var zoom = 16;

      var LatLng = new google.maps.LatLng(latitude, longitude);

      var mapOptions = {
        zoom: zoom,
        center: LatLng,
        panControl: false,
        zoomControl: false,
        scaleControl: true,
        mapTypeId: google.maps.MapTypeId.ROADMAP
      }

      map = new google.maps.Map(document.getElementById('map'), mapOptions);
      if (marker && marker.getMap) marker.setMap(map);
      marker = new google.maps.Marker({
        position: LatLng,
        map: map,
        title: 'Drag Me!',
        draggable: true
      });

      google.maps.event.addListener(marker, 'dragend', function(marker) {
        var latLng = marker.latLng;
        $latitude.value = latLng.lat();
        $longitude.value = latLng.lng();
      });


    }
    initialize();
    $('#findbutton').click(function (e) {
        var address = $("#Postcode").val();
        geocoder.geocode({ 'address': address }, function (results, status) {
            if (status == google.maps.GeocoderStatus.OK) {
                map.setCenter(results[0].geometry.location);
                marker.setPosition(results[0].geometry.location);
                $(latitude).val(marker.getPosition().lat());
                $(longitude).val(marker.getPosition().lng());
            } else {
                alert("Geocode was not successful for the following reason: " + status);
            }
        });
        e.preventDefault();
    });
    </script>

<script src="./assets/validate-password-requirements/js/jquery.passwordRequirements.min.js"></script>
    
    <script>

let timeout;

    // traversing the DOM and getting the input and span using their IDs
let password = document.getElementById('PassEntry');
    let strengthBadge = document.getElementById('StrengthDisp');

    // The strong and weak password Regex pattern checker
let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{8,})');
    let mediumPassword = new RegExp('((?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[^A-Za-z0-9])(?=.{6,}))|((?=.*[a-z])(?=.*[A-Z])(?=.*[^A-Za-z0-9])(?=.{8,}))');
    
    function StrengthChecker(PasswordParameter){
        // We then change the badge's color and text based on the password strength
if(strongPassword.test(PasswordParameter)) {
            strengthBadge.style.backgroundColor = 'green';
            strengthBadge.textContent = 'Strong';
        } else if(mediumPassword.test(PasswordParameter)){
            strengthBadge.style.backgroundColor = 'blue';
            strengthBadge.textContent = 'Medium';
        } else{
            strengthBadge.style.backgroundColor = 'red';
            strengthBadge.textContent = 'Weak';
        }
    }

password.addEventListener("input", () => {
    strengthBadge.style.display = 'block';
    clearTimeout(timeout);
    timeout = setTimeout(() => StrengthChecker(password.value), 500);
    if(password.value.length !== 0) {
        strengthBadge.style.display != 'block';
    } else {
        strengthBadge.style.display = 'none';
    }
});



</script> 
<!-- BEGIN: AJAX CALLS-->
<script>
    //Add Partner
    function addPartner() {
        var PasswordParameter = document.getElementById('PassEntry').value;

var vMsg = 'The minimum password length is 8 characters and must contain at least 1 lowercase letter, 1 capital letter, 1 number, and 1 special character.';

if(!strongPassword.test(PasswordParameter)) {

alert(vMsg); return;

}

        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addpartner').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                // console.log(json.statusCode);
                if (json.responseStatus == "SUCCESS") {
                    $('.franchise').empty(); //clear apend
                    $('.franchise').append("Account created  successfully");
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".franchise").html("");
                    });
                        setTimeout(function() {
                        window.location = "portal/franchise-login";
                    }, 2000);

                } else {
                    $('.franchise').empty();
                    $('.franchise').append(json.responseMessage);
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".franchise").html("");
                    });
                  
                }

            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.franchise').empty();
                 $('.franchise').append(errorThrown);
                $(".modal").on("hidden.bs.modal", function() {
                $(".franchise").html("");
                });
              
            }
        });
    
    }
    </script>
 <script>
    //Add Individual Customer
    function addCustomer() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addcustomer').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.Action == 1) {
                    $('.indCustomer').empty(); //clear apend
                    $('.indCustomer').append("Customer registered successfully");
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".indCustomer").html("");
                    });
                        setTimeout(function() {
                        window.location = "portal/customer-login";
                    }, 2000);
                } else {
                    $('.indCustomer').empty();
                    $('.indCustomer').append(json.Message);
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".indCustomer").html("");
                    });
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.indCustomer').empty();
                 $('.indCustomer').append(errorThrown);
                $(".modal").on("hidden.bs.modal", function() {
                $(".indCustomer").html("");
                });
            }
        });
    }
    </script>
       <script>
    //Add Co Customer
    function addCoCustomer() {
        $('#spinner').show();
        $.ajax({
            type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#addcocustomer').serialize(),
            cache: false,
            success: function(response) {
                var json = $.parseJSON(response);
                if (json.Action == 1) {
                    $('.coCustomer').empty(); //clear apend
                    $('.coCustomer').append("Customer registered successfully");
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".coCustomer").html("");
                    });
                    setTimeout(function() {
                        window.location = "portal/customer-login";
                    }, 2000);
                } else {
                    $('.coCustomer').empty();
                    $('.coCustomer').append(json.Message);
                    $(".modal").on("hidden.bs.modal", function() {
                    $(".coCustomer").html("");
                    });
                    
                }
            },
            error: function(jqXHR, textStatus, errorThrown) {
                $('.coCustomer').empty();
                 $('.coCustomer').append(errorThrown);
                $(".modal").on("hidden.bs.modal", function() {
                $(".coCustomer").html("");
                });
            }
        });
    }
    </script>

</html>