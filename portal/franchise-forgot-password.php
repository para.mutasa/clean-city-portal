

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>VAYA TECHNOLOGIES</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <style>
        .btn-primary {
    color: #fff;
    background-color: #8dc63b;
    border-color: #8dc63b;
}
    </style>
</head>
<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-split nk-split-page nk-split-md">
                        <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container bg-white w-lg-45">
                            <div class="absolute-top-right d-lg-none p-3 p-sm-5">
                                <a href="#" class="toggle btn btn-white btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
                            </div>
                            <div class="nk-block nk-block-middle nk-auth-body">
                                <div class="brand-logo pb-5">
                                    <a href="portal/index" class="logo-link">
                                        <img class="logo-light logo-img logo-img-md" src="./images/vaya_logo.png" srcset="./images/vaya_logo.png" alt="logo">
                                        <img class="logo-dark logo-img logo-img-md" src="./images/vaya_logo.png" srcset="./images/vaya_logo.png" alt="logo-dark">
                                    </a>
                                </div>
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <h5 class="nk-block-title">Forgot password</h5>
                                        <div class="nk-block-des">
                                            <p>If you forgot your password, well, then we’ll email you instructions to reset your password.</p>
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <div  style ="color: #1C7ACD; text-align: center;" class='forgot-password'></div>
                                <form  id ="forgotpassword" class="form-validate">
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email</label>
                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="email" class="form-control form-control-lg" name="email" id="default-01" placeholder="Enter your email address" required>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                    <input type="hidden" class="form-control" name="franchise-forgot-password">
                                     <button type="button" class="btn btn-lg btn-primary btn-block" name="franchise-forgot-password" onClick="forgotPassword()">Send Reset Token</button>
                          
                                    </div>
                                </form><!-- form -->
                                <div class="form-note-s2 pt-5">
                                    <a href="portal/franchise-login"><strong>Return to login</strong></a>
                                </div>
                            </div><!-- .nk-block -->
                            <div class="nk-block nk-auth-footer">
                                <!-- <div class="nk-block-between">
                                    <ul class="nav nav-sm">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Terms & Condition</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Privacy Policy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Help</a>
                                        </li>
                                  
                                    </ul>
                                </div> -->
                                <div class="mt-3">
                                    <p>&copy; 2022 VAYA TECHNOLOGIES. All Rights Reserved.</p>
                                </div>
                            </div><!-- .nk-block -->
                        </div><!-- .nk-split-content -->
                        <div class="nk-split-content nk-split-stretch bg-abstract"></div><!-- .nk-split-content -->
                    </div><!-- .nk-split -->
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->
    <!-- JavaScript -->
    <script src="./assets/js/bundle.js?ver=2.9.0"></script>
 <script src="./assets/js/scripts.js?ver=2.9.0"></script>
     <!-- BEGIN: AJAX CALLS-->
     <script>
  //Forgot Password
function forgotPassword(){
    if ($("#forgotpassword").valid()) {
  $.ajax({
    type: "POST",
            url: "portal/admin/controller/process.php",
            data: $('form#forgotpassword').serialize(),
            cache: false,
			success: function(response){
        var json = $.parseJSON(response);
        // console.log(json.Action);
        if (json.responseStatus == "SUCCESS")  {
        $('.forgot-password').empty(); //clear apend
         $('.forgot-password').append(json.responseMessage);
              $(".modal").on("hidden.bs.modal", function() {
              $("#forgot-password").html("");
            });
          setTimeout(function(){ window.location="portal/franchise-reset-password"; },2000);
        }else {
            $('.forgot-password').empty(); //clear apend
          $('.forgot-password').append(json.responseMessage);
               $(".modal").on("hidden.bs.modal", function() {
                $("#forgot-password").html("");
            });

        }
        
			},
      error: function(jqXHR, textStatus, errorThrown) {
          //  console.log(jqXHR, textStatus, errorThrown);
          $('#forgot-password').empty(); //clear apen
          $('.forgot-password').append(errorThrown);
               $(".modal").on("hidden.bs.modal", function() {
                $("#forgot-password").h
            });
        }
		});
    }
}
</script>

</html>