<?php
   session_start();
    include_once('../utils/VayaCleanCityUtility.php');

    if(isset($_POST['login']))
    {
        if(!empty($_POST['username']) && !empty($_POST['vPassword']))
        {
            $username 		 = trim($_POST['username']);
            $vPassword 	 = trim($_POST['vPassword']);
            $userType  = "Customer";
            $type = 'userLogin';

            if(!isset($_POST['g-recaptcha-response']) || empty($_POST['g-recaptcha-response'])) {
                $errorMsg = 'reCAPTHCA verification failed, please try again.';
            }else {

                $ch = curl_init();
                curl_setopt($ch, CURLOPT_URL, 'https://www.google.com/recaptcha/api/siteverify?secret='.$secret.'&response='.$_POST['g-recaptcha-response']);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                $response = curl_exec($ch);
                curl_close($ch);
                $response = json_decode($response);

                if($response->success) {
                 $getUser = customerLogin($type,$username,$vPassword,$userType);

                //  var_dump($getUser);
                //  exit;

                if ($getUser['Action'] == 1){
                    if($getUser['result']['partnerType'] == "Corporate"){
                        $_SESSION["customer_id"] = $getUser['result']['iuserId'];
                        $_SESSION["customer_vphone"] = $getUser['result']['vphone'];
                        $_SESSION["vemail"] = $getUser['result']['vemail'];
                        $_SESSION["vfirstName"] = $getUser['result']['companyName'];
                        $_SESSION["vlastName"] = "";
                    }else {
                        $_SESSION["customer_id"] = $getUser['result']['iuserId'];
                        $_SESSION["customer_vphone"] = $getUser['result']['vphone'];
                        $_SESSION["vemail"] = $getUser['result']['vemail'];
                        $_SESSION["vfirstName"] = $getUser['result']['vfirstName'];
                        $_SESSION["vlastName"] = $getUser['result']['vlastName'];
                    }
                    if(isset($_SESSION["customer_id"])) {
                        header('location: customer/bookings');
                        exit;
                        
                    } else {
                        header("location: portal/customer-login");
                        exit;
                    }
                    
                } else {
                    session_destroy();
                    $errorMsg = $getUser['Message'];
                } }else {
                    $errorMsg = 'reCAPTHCA verification failed, please try again.';
                }
            }
    
            if(isset($_GET['logout']) && $_GET['logout'] == true)
            {
                session_destroy();
                header("location: portal/customer-login");
                exit;
            }

            if(isset($_GET['lmsg']) && $_GET['lmsg'] == true)
            {
                $errorMsg = "Login required to access dashboard";
            }
        }
    }

?>

<!DOCTYPE html>
<html lang="zxx" class="js">

<head>
    <base href="../">
    <meta charset="utf-8">
    <meta name="author" content="Softnio">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="clean city.">
    <!-- Fav Icon  -->
    <link rel="shortcut icon" href="./images/favicon.png">
    <!-- Page Title  -->
    <title>VAYA TECHNOLOGIES</title>
    <!-- StyleSheets  -->
    <link rel="stylesheet" href="./assets/css/dashlite.css?ver=2.9.0">
    <link id="skin-default" rel="stylesheet" href="./assets/css/theme.css?ver=2.9.0">
    <style>
        .btn-primary {
    color: #fff;
    background-color: #8dc63b;
    border-color: #8dc63b;
}
    </style>
</head>

<body class="nk-body bg-white npc-general pg-auth">
    <div class="nk-app-root">
        <!-- main @s -->
        <div class="nk-main ">
            <!-- wrap @s -->
            <div class="nk-wrap nk-wrap-nosidebar">
                <!-- content @s -->
                <div class="nk-content ">
                    <div class="nk-split nk-split-page nk-split-md">
                        <div class="nk-split-content nk-block-area nk-block-area-column nk-auth-container bg-white">
                            <div class="absolute-top-right d-lg-none p-3 p-sm-5">
                                <a href="#" class="toggle btn-white btn btn-icon btn-light" data-target="athPromo"><em class="icon ni ni-info"></em></a>
                            </div>
                            <div class="nk-block nk-block-middle nk-auth-body">
                                <div class="brand-logo pb-5">
                                    <a href="portal/customer-login" class="logo-link">
                                        <img class="logo-light logo-img logo-img-md" src="./images/vaya_logo.png" srcset="./images/vaya_logo.png" alt="logo">
                                        <img class="logo-dark logo-img logo-img-md" src="./images/logo-dark.png" srcset="./images/vaya_logo.png" alt="logo-dark">
                                    </a>
                                </div>
                                <div class="nk-block-head">
                                    <div class="nk-block-head-content">
                                        <!-- <h5 class="nk-block-title">Sign-In</h5> -->
                                        <div class="nk-block-des">
                                        <?php 
                                    if(isset($errorMsg))
                                    {
                                        echo '<span style ="margin-left: -1%;"class="alert alert-danger">';
                                            echo $errorMsg;
                                        echo '</span>';
                                        unset($errorMsg);
                                    }
                                ?>       
                                        </div>
                                    </div>
                                </div><!-- .nk-block-head -->
                                <form action="portal/customer-login" method="post">
                                    <div class="form-group">
                                      
                                        <div class="form-label-group">
                                            <label class="form-label" for="default-01">Email</label>
                                            <!-- <a class="link link-primary link-sm" tabindex="-1" href="#">Need Help?</a> -->
                                        </div>
                                        <div class="form-control-wrap">
                                            <input type="email" name ="username" class="form-control form-control-lg" id="default-01" pattern="[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$"  placeholder="Enter your email address" required>
                                        </div>
                                    </div><!-- .form-group -->
                                    <div class="form-group">
                                        <div class="form-label-group">
                                            <label class="form-label" for="password">Password</label>
                                            <a class="link link-primary link-sm" tabindex="-1" href="portal/customer-forgot-password">Forgot Password?</a>
                                        </div>
                                        <div class="form-control-wrap">
                                            <a tabindex="-1" href="#" class="form-icon form-icon-right passcode-switch lg" data-target="password">
                                                <em class="passcode-icon icon-show icon ni ni-eye"></em>
                                                <em class="passcode-icon icon-hide icon ni ni-eye-off"></em>
                                            </a>
                                            <input type="password" class="form-control form-control-lg" name="vPassword" id="password" placeholder="Enter your password" required>
                                        </div><br />
                                        <div class="g-recaptcha" data-sitekey="6LeUkr8ZAAAAAMC0ZmiMkM2tRZ_1sucyFY1WV_71"></div>
                                    </div><!-- .form-group -->
                                    <div class="form-group">
                                        <button type="submit"  class="btn btn-lg btn-primary btn-block" name="login" >Sign in</button>
                                    </div>
                                </form><!-- form -->
                                <div class="form-note-s2 pt-4"> New on our platform? <a href="portal/register-customer">Create an account</a>
                                </div>
                                <!-- <div class="text-center pt-4 pb-3">
                                    <h6 class="overline-title overline-title-sap"><span>OR</span></h6>
                                </div>
                                <ul class="nav justify-center gx-4">
                                    <li class="nav-item"><a class="nav-link" href="#">Facebook</a></li>
                                    <li class="nav-item"><a class="nav-link" href="#">Google</a></li>
                                </ul>
                                <div class="text-center mt-5">
                                    <span class="fw-500">I don't have an account? <a href="portal/pages/auths/create-booking">Create a booking</a></span>
                                </div> -->
                            </div>
                            <!-- .nk-block -->
                            <div class="nk-block nk-auth-footer">
                                <div class="nk-block-between">
                                    <!-- <ul class="nav nav-sm">
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Terms & Condition</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Privacy Policy</a>
                                        </li>
                                        <li class="nav-item">
                                            <a class="nav-link" href="#">Help</a>
                                        </li>
                                        <li class="nav-item dropup">
                                            <a class="dropdown-toggle dropdown-indicator has-indicator nav-link" data-toggle="dropdown" data-offset="0,10"><small>English</small></a>
                                            <div class="dropdown-menu dropdown-menu-sm dropdown-menu-right">
                                                <ul class="language-list">
                                                    <li>
                                                        <a href="#" class="language-item">
                                                            <img src="./images/flags/english.png" alt="" class="language-flag">
                                                            <span class="language-name">English</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="language-item">
                                                            <img src="./images/flags/spanish.png" alt="" class="language-flag">
                                                            <span class="language-name">Español</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="language-item">
                                                            <img src="./images/flags/french.png" alt="" class="language-flag">
                                                            <span class="language-name">Français</span>
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a href="#" class="language-item">
                                                            <img src="./images/flags/turkey.png" alt="" class="language-flag">
                                                            <span class="language-name">Türkçe</span>
                                                        </a>
                                                    </li>
                                                </ul>
                                            </div>
                                        </li>
                                    </ul> -->
                                    <!-- .nav -->
                                </div>
                                <div class="mt-3">
                                    <p>&copy; 2022 VAYA TECHNOLOGIES. All Rights Reserved.</p>
                                </div>
                            </div><!-- .nk-block -->
                        </div><!-- .nk-split-content -->
                        <div class="nk-split-content nk-split-stretch bg-abstract"></div><!-- .nk-split-content -->
                    </div><!-- .nk-split -->
                </div>
                <!-- wrap @e -->
            </div>
            <!-- content @e -->
        </div>
        <!-- main @e -->
    </div>
    <!-- app-root @e -->

  <!-- JavaScript -->
  <script src="./assets/js/bundle.js?ver=2.9.0"></script>
 <script src="./assets/js/scripts.js?ver=2.9.0"></script>
 <!-- Google reCaptcha -->
<script src="https://www.google.com/recaptcha/api.js" async defer></script>


</html>