<?php
    // error_reporting(E_ALL);
    error_reporting(0);
    include_once('curl_url.php');
    $GOOGLE_API_KEY  = "AIzaSyDQTpHIDA-oUPXuJhXW7PHxRo_9OWUnoMs";
    $secret = '6LeUkr8ZAAAAAPLjUQfjU8takt8g7rxonze_GVIO';
 
    function getAuthToken(){
        global $base_url_cb;

        $url = $base_url_cb.'oauth/token';

        $params = array(
            "scope" => 'read',
            "username" => 'admin',
            "password" => 'password',
            "client_id" => 'client_id',
            "client_secret" => 'client_secret',
            "redirect_uri" => 'http://' . $_SERVER["HTTP_HOST"] . $_SERVER["PHP_SELF"],
            "grant_type" => "password"
        );

        $ch = curl_init();
        curl_setopt($ch, constant("CURLOPT_" . 'URL'), $url);
        curl_setopt($ch, constant("CURLOPT_" . 'POST'), true);
        curl_setopt($ch, constant("CURLOPT_" . 'POSTFIELDS'), $params);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $output = curl_exec($ch);
        $info = curl_getinfo($ch);
        curl_close($ch);

        if ($info['http_code'] === 200) {
            header('Content-Type: ' . $info['content_type']);

            $res_arr = json_decode($output,false);
            return $res_arr->access_token;
        } else {
            return 'An error happened';
        }
    }

  

    function callWebApiPost($jsonData, $curl_url){
        $ch = curl_init($curl_url);

        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);

        // $ACC_TOKEN = getAuthToken();
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        return $result;
    }

    function callWebBiddingApiPost($jsonData){
        global $bidding_url;
        $curl = curl_init();
        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt_array($curl, [
         CURLOPT_URL => $bidding_url,
        CURLOPT_RETURNTRANSFER => true,
        CURLOPT_ENCODING => "",
        CURLOPT_MAXREDIRS => 10,
        CURLOPT_TIMEOUT => 30,
        CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
        CURLOPT_CUSTOMREQUEST => "POST",
        CURLOPT_POSTFIELDS => $jsonDataEncoded,
        CURLOPT_HTTPHEADER => [
            "Content-Type: application/json"
        ],
        ]);

        $response = curl_exec($curl);
        curl_close($curl);
        return json_decode($response,true);
     }

    //  function callWebFileDownloadBiddingApiGet($userId,$document_type,$userType){
    //     $curl = curl_init();
    //        curl_setopt_array($curl, [
    //     CURLOPT_URL =>  str_replace ( ' ', '%20', "https://vayaafrica.com/vbp-backend/v1/vaya-bidding-platform-service/downloadFile?userId=".$userId."&docType=".$document_type."&userType=".$userType ),
    //     CURLOPT_RETURNTRANSFER => true,
    //       CURLOPT_ENCODING => "",
    //       CURLOPT_MAXREDIRS => 10,
    //       CURLOPT_TIMEOUT => 30,
    //       CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //       CURLOPT_CUSTOMREQUEST => "GET",
    //       CURLOPT_POSTFIELDS => "",
    //       CURLOPT_HTTPHEADER => [
    //         "Content-Type: application/json"
    //       ],
    //     ]);
        
    //     $response = curl_exec($curl);

    //     echo $response;
    //     exit;
    //     curl_close($curl);
    //     return json_decode($response,true);
    //  }
//      function callWebFileDownloadBiddingApiGet($userId,$document_type,$userType){
//         $url = "https://vayaafrica.com/vbp-backend/v1/vaya-bidding-platform-service/downloadFile?userId=".$userId."&docType=".$document_type."&userType=".$userType ;
//         $ch = curl_init(str_replace(" ","%20",$url));

//   // Initialize directory name where
//   // file will be save
//   $dir = './';
  
//   // Use basename() function to return
//   // the base name of file
//   $file_name = basename($url);
  
//   // Save file into file location
//   $save_file_loc = $dir . $file_name;
  
//   // Open file
//   $fp = fopen($save_file_loc, 'wb');
  
//   // It set an option for a cURL transfer
//   curl_setopt($ch, CURLOPT_FILE, $fp);
//   curl_setopt($ch, CURLOPT_HEADER, 0);
//   curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
  
//   // Perform a cURL session
//   curl_exec($ch);
  
//   // Closes a cURL session and frees all resources
//   curl_close($ch);
  
//   // Close file
//   fclose($fp);
        
    
//      }

     function callWebFileDownloadBiddingApiGet($userId,$document_type,$userType){
        $url = "https://vayaafrica.com/vbp-backend/v1/vaya-bidding-platform-service/downloadFile?userId=".$userId."&docType=".$document_type."&userType=".$userType ;
        $file = str_replace(" ","%20",$url);
        $ch = curl_init($file);
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_BINARYTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        $rawdata = curl_exec ($ch);
        curl_close ($ch);
        // header('Content-type: image/jpeg');
        return $rawdata;

     }


     function getSubTypesById(){
        $curl = curl_init();
        curl_setopt_array($curl, [
          CURLOPT_URL => "https://vayaafrica.com/clean_city_java_backend/collectionSubtype/get-collection-sub-type-by-id/18",
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => "",
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => "GET",
          CURLOPT_POSTFIELDS => "",
          CURLOPT_HTTPHEADER => [
            "Content-Type: application/json"
          ],
        ]);
        
        $response = curl_exec($curl);
        curl_close($curl);
        
        return $response;
        
             }

     function callWebFileUploadApiPost($jsonData, $curl_url){
        $curl = curl_init();

        curl_setopt_array($curl, array(
          CURLOPT_URL => $curl_url,
          CURLOPT_RETURNTRANSFER => true,
          CURLOPT_ENCODING => '',
          CURLOPT_MAXREDIRS => 10,
          CURLOPT_TIMEOUT => 30,
          CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
          CURLOPT_CUSTOMREQUEST => 'POST',
          CURLOPT_POSTFIELDS => $jsonData,
          CURLOPT_HTTPHEADER => [
            "Content-Type: multipart/form-data;"
          ],
          
        ));
        
        $result = curl_exec($curl);
        curl_close($curl);
        return $result;
    }

    function callWebApiGet($curl_url){
        // global $token;
        $ch = curl_init($curl_url);

        // $ACC_TOKEN = getAuthToken();
        // curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json', 'authorization: Bearer '.$ACC_TOKEN));
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);
        curl_close($ch);
        return $result;
    }


   
    function callWebApiPut($jsonData, $curl_url){
        $ch = curl_init($curl_url);

        // $ACC_TOKEN = getAuthToken();
        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        return $result;
    }


    function callWebApiDelete($jsonData, $curl_url){
        $ch = curl_init($curl_url);

        // $ACC_TOKEN = getAuthToken();
        $jsonDataEncoded = json_encode($jsonData);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "DELETE");
        curl_setopt($ch, CURLOPT_POSTFIELDS, $jsonDataEncoded);
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type: application/json'));
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $result = curl_exec($ch);

        return $result;
    }

 /* Waste Types */

    //add waste
    function addWaste($bagColor,$capacity,$description,$variableCost,$variableCostDescription,$wasteTypeName){

        $jsonData = array(
            'bagColor' => $bagColor,
            'capacity' => $capacity,
            'description' => $description,
            'variableCost' => $variableCost,
            'variableCostDescription' => $variableCostDescription,
            'wasteTypeName' => $wasteTypeName
        );

        global $base_url;
        global $create_waste_type;
        return callWebApiPost($jsonData,$base_url.$create_waste_type);

    }


    //edit waste type
    function editWasteType($bagColor,$capacity,$description,$id,$variableCost,$variableCostDescription,$wasteTypeName){

        $jsonData = array(
            'bagColor' => $bagColor,
            'capacity' => $capacity,
            'description' => $description,
            'id' => $id,
            'variableCost' => $variableCost,
            'variableCostDescription' => $variableCostDescription,
            'wasteTypeName' => $wasteTypeName
        );

        global $base_url;
        global $update_waste_type;
        return callWebApiPost($jsonData,$base_url.$update_waste_type);

    }
    //get all waste types
         function getAllWaste(){
           global $base_url;
           global $all_waste_types;

           return callWebApiGet($base_url.$all_waste_types);
        }
        //Find waste type by ID
        function getWasteTypeById($wasteTypeId){

            global $base_url;
            global $find_waste_type_by_id;
            return callWebApiGet($base_url.$find_waste_type_by_id.$wasteTypeId);
        
        }

        //update role
        function updateRole($authorized,$checker,$lastModifiedBy,$maker,$name,$id,$created){

            $jsonData = array(
                'authorized' => $authorized,
                'checker' => $checker,
                'lastModifiedBy' => $lastModifiedBy,
                'maker' => $maker,
                'name' => $name,
                'recordStatus' => $created,
                'id' => $id
            );

            global $base_url;
            global $update_role;
            return callWebApiPut($jsonData,$base_url.$update_role);

        }

        //delete role
        function deleteRole($id,$lastModifiedBy){

            $jsonData = array(
                'id' => $id,
                'lastModifiedBy' => $lastModifiedBy
            );

            global $base_url;
            global $delete_role;
            return callWebApiDelete($jsonData,$base_url.$delete_role);

        }
        /*end User Roles*/

        /**Collection Types */

           //Get All Collection Types
           function getAllCollectionTypes(){
            global $base_url;
            global $all_collection_types;
 
            return callWebApiGet($base_url.$all_collection_types);
         }
 
         //add collection types
    function addCollectionType($collectionType,$unit,$status){

        $jsonData = array(
            'collectionType' => $collectionType,
            'unit' => $unit,
            'status' => $status
        );

        global $base_url;
        global $create_colletion_type;
        return callWebApiPost($jsonData,$base_url.$create_colletion_type);

    }


             //edit collection types
             function updateCollectionType($collectionType,$id,$status,$unit){

                $jsonData = array(
                    'collectionType' => $collectionType,
                    'id' => $id,
                    'status' => $status,
                    'unit' => $unit
                );
        
                global $base_url;
                global $update_collection_type;
                return callWebApiPost($jsonData,$base_url.$create_colletion_type);
        
            }
        

     
    
//approve user
    function approveUser($id,$lastModifiedBy,$checkerEmail){

        $jsonData = array(
            'id' => $id,
            'lastModifiedBy' => $lastModifiedBy,
            'checkerEmail' => $checkerEmail
        );

        global $base_url;
        global $approve_user;
        return callWebApiPut($jsonData,$base_url.$approve_user);

    }

     //get all users
   function getAllUsers(){
    global $base_url;
    global $all_users;

    return callWebApiGet($base_url.$all_users);
}


        //update user
        function  updateUser($firstName,$lastModifiedBy,$lastName,$oxygenPlantId,$phoneNumber,$userType,$id){

            $jsonData = array(
                'firstName' => $authorized,
                'lastModifiedBy' => $lastModifiedBy,
                'lastName' => $lastName,
                'oxygenPlantId' => $oxygenPlantId,
                'phoneNumber' => $phoneNumber,
                'userType' => $userType,
                'id' => $idl
            );

            global $base_url;
            global $update_user;
            return callWebApiPut($jsonData,$base_url.$update_user);

        }

         //delete user
         function deleteUser($id,$lastModifiedBy){

            $jsonData = array(
                'id' => $id,
                'lastModifiedBy' => $lastModifiedBy
            );

            global $base_url;
            global $delete_user;
            return callWebApiDelete($jsonData,$base_url.$delete_user);

        }
 //get one user
        function GetUser($id){


            global $base_url;
            global $user_by_Id;
            return callWebApiGet($base_url.$user_by_Id.$id);

        }
        /**end Users */
       /**Collection Sub Types*/
    //Get All Collection Sub Types
    function getAllCollectionSubTypes(){
        global $base_url;
        global $all_collection_sub_types;

        return callWebApiGet($base_url.$all_collection_sub_types);
     }

     //Get Sub Type By Collection Type
function getSubTypeByCollectionType($collectionType){

    global $base_url;
    global $get_sub_type_by_collection_type;
    return callWebApiGet($base_url.$get_sub_type_by_collection_type.$collectionType);

}


              //add collection sub type
              function addSubType($message,$quantity,$costPerUnitUSD,$costPerUnitZWL,$collectionType,$imaxQty,$ratePerKm,$vshow,$vsubType,$vunit,$quotationValidityDays){

                $jsonData = array(
                    'message' => $message,
                    'quantity' => $quantity,
                    'collectionType' => $collectionType,
                    'costPerUnitUSD' => $costPerUnitUSD,
                    'costPerUnitZWL' => $costPerUnitZWL,
                    'imaxQty' => $imaxQty,
                    'ratePerKm' => $ratePerKm,
                    'vshow' => $vshow,
                    'vsubType' => $vsubType,
                    'vunit' => $vunit,
                    'quotationValidityDays' => $quotationValidityDays
                );
        
                global $base_url;
                global $create_colletion_sub_type;
                return callWebApiPost($jsonData,$base_url.$create_colletion_sub_type);
        
            }
        
              //edit collection sub type
              function editSubType($message,$quantity,$costPerUnitUSD,$costPerUnitZWL,$estatus,$id,$imaxQty,$ratePerKm,$collectionType,$vshow,$vsubType,$vunit,$quotationValidityDays){

                $jsonData = array(
                    'message' => $message,
                    'quantity' => $quantity,
                    'costPerUnitUSD' => $costPerUnitUSD,
                    'costPerUnitZWL' => $costPerUnitZWL,
                    'status' => $estatus,
                    'id' => $id,
                    'imaxQty' => $imaxQty,
                    'ratePerKm' => $ratePerKm,
                    'collectionType' => $collectionType,
                    'vshow' => $vshow,
                    'vsubType' => $vsubType,
                    'vunit' => $vunit,
                    'quotationValidityDays' => $quotationValidityDays
                );
        
                global $base_url;
                global $update_collection_sub_type;
                return callWebApiPost($jsonData,$base_url.$update_collection_sub_type);
        
            }

    
  
         /**Partners API */
  
           //add Franchise
        function addPartner($address,$address2,$city,$code,$company,$country,$email,$language,$lattitude,$longitude,$password,$phoneNumber,$province,$state,$zimraBpNumber){

            $jsonData = array(
                
                'address' => $address,
                 'address2' => $address2,
                'city' => $city,
                'code' => $code,
                'company' => $company,
                'country' => $country,
                'email' => $email,
                'language' => $language,
                'lattitude' => $lattitude,
                'longitude' => $longitude,
                'password' => $password,
                'phoneNumber' => $phoneNumber,
                'province' => $province,
                'state' => $state,
                'zimraBpNumber' => $zimraBpNumber
         
            );

            global $base_url;
            global $create_partner;
            return callWebApiPost($jsonData,$base_url.$create_partner);

    }

    function getFranchiseById($FranchiseeId){

        global $base_url;
        global $get_partner_by_id;
        return callWebApiGet($base_url.$get_partner_by_id.$FranchiseeId);
    
    }

    function getAllPartners(){

        global $base_url;
        global $all_partners;
        return callWebApiGet($base_url.$all_partners);
    
    }
    //edit Partner
    function editPartner($address,$address2,$city,$code,$company,$country,$email,$franchiseId,$language,$latitude,$longitude,$password,$phoneNumber,$province,$ratePerKM,$registrationDate,$state,$status,$zimra_BP_Number){

    $jsonData = array(
        'address' => $address,
        'address2' => $address2,
        'city' => $city,
        'code' => $code,
        'company' => $company,
        'country' => $country,
        'email' => $email,
        'franchiseId' => $franchiseId,
        'language' => $language,
        'latitude' => $latitude,
        'longitude' => $longitude,
        // 'password' => $password,
        'phoneNumber' => $phoneNumber,
        'province' => $province,
        'ratePerKM' => $ratePerKM,
        'registrationDate' => $registrationDate,
        'state' => $state,
        'status' => $status,
        'zimra_BP_Number' => $zimra_BP_Number
    );
    
    global $base_url;
    global $update_partner;
    return callWebApiPost($jsonData,$base_url.$update_partner);

}


        //update  Cylinder checks
        function updateCylinderCheckUp($franchiseeId,$country,$authorized,$checker,$cylinderId,$cylinderStandardWeight,$engineer,$issuesWithWeight,$lastModifiedBy,$maker,$plantManager,$purityCheck,$purityIssues,$purityLevel,$recordStatus,$weightChecked,$id){

            $jsonData = array(
                
                 'franchiseeId' => $franchiseeId,
                 'country' => $country,
                'authorized' => $authorized,
                'checker' => $checker,
                'cylinderId' => $cylinderId,
                'cylinderStandardWeight' => $cylinderStandardWeight,
                'engineer' => $engineer,
                'issuesWithWeight' => $issuesWithWeight,
                'lastModifiedBy' => $lastModifiedBy,
                'maker' => $maker,
                'plantManager' => $plantManager,
                'purityCheck' => $purityCheck,
                'purityIssues' => $purityIssues,
                'purityLevel' => $purityLevel,
                'recordStatus' => $recordStatus,
                'weightChecked' => $weightChecked,
                'id' => $id

            );

            global $base_url;
            global $update_cylinder_checks;
            return callWebApiPut($jsonData,$base_url.$update_cylinder_checks);

        }
        //get all cylinder checks
  function getAllCylinderChecks(){
    global $base_url;
    global $all_cylinder_checks;

    return callWebApiGet($base_url.$all_cylinder_checks);
}
             //delete Cylinder Check
             function deleteCylinderCheck($id,$lastModifiedBy){

                    $jsonData = array(
                        'id' => $id,
                        'lastModifiedBy' => $lastModifiedBy
                    );

                    global $base_url;
                    global $delete_cylinder_checks;
                    return callWebApiDelete($jsonData,$base_url.$delete_cylinder_checks);

                }


            /**End Cylinder Checks */
         /**Drivers */

     //Get All Drivers
      function getAllDrivers(){
        global $base_url;
        global $all_drivers;

        return callWebApiGet($base_url.$all_drivers);
     }
     function getDriversByFranchiseId($driverId){

        global $base_url;
        global $get_drivers_by_franchiseid;
        return callWebApiGet($base_url.$get_drivers_by_franchiseid.$driverId);
    
    }
    function getFranchiseDriversById($franchiseDriverId){

        global $base_url;
        global $get_drivers_by_Id;
        return callWebApiGet($base_url.$get_drivers_by_Id.$franchiseDriverId);
    
    }


    
          //add Driver
          function  addDriver($type,$vfirstName,$vlastName,$ifranchiseId,$vemail,$vphone,$caddress,$caddress2,$egender,$phoneCode,$countryCode,$vpassword){

            $jsonData = array(
                'type' => $type,
                'vfirstName' => $vfirstName,
                'vlastName' => $vlastName,
                'ifranchiseId' => $ifranchiseId,
                'vemail' => $vemail,
                'vphone' => $vphone,
                'caddress' => $caddress,
                'caddress2' => $caddress2,
                'egender' => $egender,
                'phoneCode' => $phoneCode,
                'countryCode' => $countryCode,
                'vpassword' => $vpassword
              
            );

            global $base_url;
            global $create_driver;
            return callWebApiPost($jsonData,$base_url.$create_driver);

        }
               //edit Driver
               function   editDriver($driverId,$vname,$vlastName,$vemail,$vphone,$vcaddress,$vcadress2,$egender,$franchiseId,$status,$vpassword){

                $jsonData = array(
                    'driverId' => $driverId,
                    'vname' => $vname,
                    'vlastName' => $vlastName,
                    'vemail' => $vemail,
                    'vphone' => $vphone,
                    'vcaddress' => $vcaddress,
                    'vcadress2' => $vcadress2,
                    'egender' => $egender,
                    'franchiseId' => $franchiseId,
                    'status' => $status,
                    'vpassword' => $vpassword
                );
    
                global $base_url;
                global $update_driver;
                return callWebApiPost($jsonData,$base_url.$update_driver);
    
            }
  //update Cylinder
  function  updateCylinder($franchiseeId,$country,$authorized,$batchNumber,$checker,$color,$conditionStatus,$cylinderSerialNumber,$cylinderStandardWeight,$cylinderStatus,$lastModifiedBy,$maker,$recordStatus,$id){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'batchNumber' => $batchNumber,
        'checker' => $checker,
        'color' => $color,
        'conditionStatus' => $conditionStatus,
        'cylinderSerialNumber' => $cylinderSerialNumber,
        'cylinderStandardWeight' => $cylinderStandardWeight,
        'cylinderStatus' => $cylinderStatus,
        'lastModifiedBy' => $lastModifiedBy,
        'maker' => $maker,
        'recordStatus' => $recordStatus,
        'id' => $id

    );

    global $base_url;
    global $update_cylinder;
    return callWebApiPut($jsonData,$base_url.$update_cylinder);

}

  //get all cylinders
  function getAllCylinders(){
    global $base_url;
    global $all_cylinders;

    return callWebApiGet($base_url.$all_cylinders);
}

           //delete cylinder
           function deleteCylinder($id,$lastModifiedBy){

            $jsonData = array(
                'id' => $id,
                'lastModifiedBy' => $lastModifiedBy
            );

            global $base_url;
            global $delete_cylinder;
            return callWebApiDelete($jsonData,$base_url.$delete_cylinder);

        }
         /**End Cylinders */





      /**Vehicle */

     //Get All Vehicles
     function getAllVehicles(){
        global $base_url;
        global $all_vehicles;

        return callWebApiGet($base_url.$all_vehicles);
     }
     function getVehiclesByFranchiseId($franId){

        global $base_url;
        global $get_vehicles_by_franchiseid;
        return callWebApiGet($base_url.$get_vehicles_by_franchiseid.$franId);
    
    }
         //add vehicle
         function addVehicle($capacity,$franchiseId,$licensePlateNumber,$make,$model,$vehicleStatus,$year){

            $jsonData = array(
                'capacity' => $capacity,
                 'franchiseId' => $franchiseId,
                'licensePlateNumber' => $licensePlateNumber,
                'make' => $make,
                'model' => $model,
                'vehicleStatus' => $vehicleStatus,
                'year' => $year    
            );

            global $base_url;
            global $create_vehicle;
            return callWebApiPost($jsonData,$base_url.$create_vehicle);

    }
      //edit vehicle
      function  editVehicle($capacity,$franchiseId,$licensePlateNumber,$make,$model,$vehicleId,$vehicleStatus,$year){

        $jsonData = array(
            'capacity' => $capacity,
             'franchiseId' => $franchiseId,
            'licensePlateNumber' => $licensePlateNumber,
            'make' => $make,
            'model' => $model,
            'vehicleId' => $vehicleId,
            'vehicleStatus' => $vehicleStatus,
            'year' => $year    
        );

        global $base_url;
        global $update_vehicle;
        return callWebApiPost($jsonData,$base_url.$update_vehicle);

}

//edit cylinder Tracking
        function editCylinderTracking($franchiseeId,$country,$authorized,$checkedIn,$checkedOut,$checker,$customerId,$cylindersDelivered,$cylindersReturned,$estimatedReturnDate,$invoicingId,$lastModifiedBy,$maker,$recordStatus,$id){

            $jsonData = array(
                'franchiseeId' => $franchiseeId,
                 'country' => $country,
                'authorized' => $authorized,
                'checkedIn' => $checkedIn,
                'checkedOut' => $checkedOut,
                'checker' => $checker,
                'customerId' => $customerId,
                'cylindersDelivered' => $cylindersDelivered,
                'cylindersReturned' => $cylindersReturned,
                'estimatedReturnDate' => $estimatedReturnDate,
                'invoicingId' => $invoicingId,
                'lastModifiedBy' => $lastModifiedBy,
                'maker' => $maker,
                'purityLevel' => $purityLevel,
                'recordStatus' => $recordStatus,
                'id' => $id


            );

            global $base_url;
            global $update_cylinder_tracking;
            return callWebApiPut($jsonData,$base_url.$update_cylinder_tracking);

        }

          //delete cylinder tracking
  function deleteCylinderTracking($id,$lastModifiedBy){

    $jsonData = array(
        'id' => $id,
        'lastModifiedBy' => $lastModifiedBy
    );

    global $base_url;
    global $delete_cylinder_tracking;
    return callWebApiDelete($jsonData,$base_url.$delete_cylinder_tracking);

}


  //get all cylinder tracking
  function getAllCylinderTracking(){
    global $base_url;
    global $all_cylinder_tracking;

    return callWebApiGet($base_url.$all_cylinder_tracking);
}

/**End Cylinder Tracking */

   /**Bookings */
     //Get All Bookings
     function getBookings(){
        global $base_url;
        global $all_bookings;

        return callWebApiGet($base_url.$all_bookings);
     }
     function getBookingsByStatus($eStatus){

        global $base_url;
        global $all_bookings_by_status;
        return callWebApiGet($base_url.$all_bookings_by_status.$eStatus);
    
    }
    function getBookingsByPaymentStatus($paymentStatus){

        global $base_url;
        global $admin_reports;
        return callWebApiGet($base_url.$admin_reports.$paymentStatus);
    
    }

    function getBookingsByPaymentStatusAndFranchiseId($franchiseId,$paymentStatus){

        global $base_url;
        global $franchise_reports;
        return callWebApiGet($base_url.$franchise_reports.'/'.$franchiseId.'/'.$paymentStatus);
    
    }

    function downloadDriverFile($userId,$document_type,$userType){

        // var_dump($userId);
        // var_dump($document_type);
        // var_dump($userType);
        // exit;
        return callWebFileDownloadBiddingApiGet($userId,$document_type,$userType);
    
    }

    
    function reAssignFranchise($bookingId,$iFranchiseId){

        global $base_url;
        global $re_assign_franchise;
        return callWebApiPost($base_url.$re_assign_franchise.'/'.$bookingId.'/'.$iFranchiseId);
    
    }


     function getBookingsByUserId($iuserId){

        global $base_url;
        global $get_bookings_by_userid;
        return callWebApiGet($base_url.$get_bookings_by_userid.$iuserId);
    
    }
    function getDocumentsByCustomerId($iuserId){

        global $base_url;
        global $get_documents_by_userid;
        return callWebApiGet($base_url.$get_documents_by_userid.$iuserId);
    
    }
    function getBookingsByFranchiseId($franchiseId){

        global $base_url;
        global $get_bookings_by_franchiseid;
        return callWebApiGet($base_url.$get_bookings_by_franchiseid.$franchiseId);
    
    }
    function assignDriver($bookingId,$iDriverId,$iFranchiseId){

        global $base_url;
        global $assign_booking;
        return callWebApiGet($base_url.$assign_booking.$bookingId.'/'.$iDriverId.'/'.$iFranchiseId);

    }
    function assignDriverToFranchise($franchiseId,$idriverId){

        global $base_url;
        global $assign_driver_to_franchise;
        return callWebApiGet($base_url.$assign_driver_to_franchise.'/'.$franchiseId.'/'.$idriverId);

    }
    function reAssignDriver($ibookingId,$driverId){

        global $base_url;
        global $re_assign_booking;
        return callWebApiGet($base_url.$re_assign_booking.'/'.$ibookingId.'/'.$driverId);
    
    }
    
    function  startTrip($ibookingId){

        global $base_url;
        global $start_trip;
        return callWebApiPost("",$base_url.$start_trip.'/'.$ibookingId);

}
function  endTrip($ibookingId){

    global $base_url;
    global $end_trip;
    return callWebApiPost("",$base_url.$end_trip.'/'.$ibookingId);

}
   //approve booking cancellation by driver
   function approveBookingCancellation($bookingId,$userId){

    $jsonData = array(
        'bookingId' => $bookingId,
        'userId' => $userId
    );

    global $base_url;
    global $admin_approve_cancel_booking;
    return callWebApiPost($jsonData,$base_url.$admin_approve_cancel_booking);

}



    //create House Hold Booking
    function addHouseBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType){

    $jsonData = array(
        
        'collectionDate' => $collectionDate,
        'currencyCode' => $currencyCode,
        'collectionType' => $collectionType,
        'iquantity' => $iquantity,
        'isubCollectionTypeId' => $isubCollectionTypeId,
        'iuserId' => $iuserId,
        'vcollectionsRequestAddress' => $vcollectionsRequestAddress,
        'vcollectionsRequestLat' => $vcollectionsRequestLat,
        'vcollectionsRequestLong' => $vcollectionsRequestLong,
        'wasteType' => $wasteType
    );

    global $base_url;
    global $create_booking;
    return callWebApiPost($jsonData,$base_url.$create_booking);
    
     }

       //update House Hold Booking
    function editHouseBooking($ibookingId,$collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType){

        $jsonData = array(
            'ibookingId' => $ibookingId,
            'collectionDate' => $collectionDate,
            'currencyCode' => $currencyCode,
            'collectionType' => $collectionType,
            'iquantity' => $iquantity,
            'isubCollectionTypeId' => $isubCollectionTypeId,
            'iuserId' => $iuserId,
            'vcollectionsRequestAddress' => $vcollectionsRequestAddress,
            'vcollectionsRequestLat' => $vcollectionsRequestLat,
            'vcollectionsRequestLong' => $vcollectionsRequestLong,
            'wasteType' => $wasteType
        );
    
        global $base_url;
        global $update_booking;
        return callWebApiPost($jsonData,$base_url.$update_booking);
        
         }
       //create Commercial Booking
    function addComBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType){

        $jsonData = array(
            
            'collectionDate' => $collectionDate,
            'currencyCode' => $currencyCode,
            'collectionType' => $collectionType,
            'iquantity' => $iquantity,
            'isubCollectionTypeId' => $isubCollectionTypeId,
            'iuserId' => $iuserId,
            'vcollectionsRequestAddress' => $vcollectionsRequestAddress,
            'vcollectionsRequestLat' => $vcollectionsRequestLat,
            'vcollectionsRequestLong' => $vcollectionsRequestLong,
            'wasteType' => $wasteType
        );
    
        global $base_url;
        global $create_booking;
        return callWebApiPost($jsonData,$base_url.$create_booking);
        
         }

                //update Commercial Booking
    function editComBooking($ibookingId,$collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType){

        $jsonData = array(
            'ibookingId' => $ibookingId,
            'collectionDate' => $collectionDate,
            'currencyCode' => $currencyCode,
            'collectionType' => $collectionType,
            'iquantity' => $iquantity,
            'isubCollectionTypeId' => $isubCollectionTypeId,
            'iuserId' => $iuserId,
            'vcollectionsRequestAddress' => $vcollectionsRequestAddress,
            'vcollectionsRequestLat' => $vcollectionsRequestLat,
            'vcollectionsRequestLong' => $vcollectionsRequestLong,
            'wasteType' => $wasteType
        );
    
        global $base_url;
        global $update_booking;
        return callWebApiPost($jsonData,$base_url.$update_booking);
        
         }
    //create HouseHold Adhoc Booking
    function addHouseAdBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType){

        $jsonData = array(
            
            'collectionDate' => $collectionDate,
            'currencyCode' => $currencyCode,
            'collectionType' => $collectionType,
            'iquantity' => $iquantity,
            'isubCollectionTypeId' => $isubCollectionTypeId,
            'iuserId' => $iuserId,
            'vcollectionsRequestAddress' => $vcollectionsRequestAddress,
            'vcollectionsRequestLat' => $vcollectionsRequestLat,
            'vcollectionsRequestLong' => $vcollectionsRequestLong,
            'wasteType' => $wasteType
        );
    
        global $base_url;
        global $create_booking;
        return callWebApiPost($jsonData,$base_url.$create_booking);
        
         }

            //update HouseHold Adhoc Booking
    function editHouseAdBooking($ibookingId,$collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType){

        $jsonData = array(
            
            'ibookingId' => $ibookingId,
            'collectionDate' => $collectionDate,
            'currencyCode' => $currencyCode,
            'collectionType' => $collectionType,
            'iquantity' => $iquantity,
            'isubCollectionTypeId' => $isubCollectionTypeId,
            'iuserId' => $iuserId,
            'vcollectionsRequestAddress' => $vcollectionsRequestAddress,
            'vcollectionsRequestLat' => $vcollectionsRequestLat,
            'vcollectionsRequestLong' => $vcollectionsRequestLong,
            'wasteType' => $wasteType
        );
    
        global $base_url;
        global $update_booking;
        return callWebApiPost($jsonData,$base_url.$update_booking);
        
         }
            //create Commercial Adhoc Booking
    function addComAdBooking($collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType){

        $jsonData = array(
            
            'collectionDate' => $collectionDate,
            'currencyCode' => $currencyCode,
            'collectionType' => $collectionType,
            'iquantity' => $iquantity,
            'isubCollectionTypeId' => $isubCollectionTypeId,
            'iuserId' => $iuserId,
            'vcollectionsRequestAddress' => $vcollectionsRequestAddress,
            'vcollectionsRequestLat' => $vcollectionsRequestLat,
            'vcollectionsRequestLong' => $vcollectionsRequestLong,
            'wasteType' => $wasteType
        );
    
        global $base_url;
        global $create_booking;
        return callWebApiPost($jsonData,$base_url.$create_booking);
        
         }


                  //update Commercial Adhoc Booking
    function editComAdBooking($ibookingId,$collectionDate,$currencyCode,$collectionType,$iquantity,$isubCollectionTypeId,$iuserId,$vcollectionsRequestAddress,$vcollectionsRequestLat,$vcollectionsRequestLong,$wasteType){

        $jsonData = array(
            
            'ibookingId' => $ibookingId,
            'collectionDate' => $collectionDate,
            'currencyCode' => $currencyCode,
            'collectionType' => $collectionType,
            'iquantity' => $iquantity,
            'isubCollectionTypeId' => $isubCollectionTypeId,
            'iuserId' => $iuserId,
            'vcollectionsRequestAddress' => $vcollectionsRequestAddress,
            'vcollectionsRequestLat' => $vcollectionsRequestLat,
            'vcollectionsRequestLong' => $vcollectionsRequestLong,
            'wasteType' => $wasteType
        );
    
        global $base_url;
        global $update_booking;
        return callWebApiPost($jsonData,$base_url.$update_booking);
        
         }


    //add Oxy Plant
    function addOxyPlant($franchiseeId,$country,$authorized,$checker,$email,$lastModifiedBy,$location,$maker,$phoneNumber,$plantName,$recordStatus){

        $jsonData = array(
            
           'franchiseeId' => $franchiseeId,
            'country' => $country,
            'authorized' => $authorized,
            'checker' => $checker,
            'email' => $email,
            'lastModifiedBy' => $lastModifiedBy,
            'location' => $location,
            'maker' => $maker,
            'phoneNumber' => $phoneNumber,
            'plantName' => $plantName,
            'recordStatus' => $recordStatus
        );

        global $base_url;
        global $add_oxy_plant;
        return callWebApiPost($jsonData,$base_url.$add_oxy_plant);

    }

    
    //edit Oxy Plant
    function editOxyPlant($franchiseeId,$country,$authorized,$checker,$email,$lastModifiedBy,$location,$maker,$phoneNumber,$plantName,$recordStatus,$id){

        $jsonData = array(
            
           'franchiseeId' => $franchiseeId,
            'country' => $country,
            'authorized' => $authorized,
            'checker' => $checker,
            'email' => $email,
            'lastModifiedBy' => $lastModifiedBy,
            'location' => $location,
            'maker' => $maker,
            'phoneNumber' => $phoneNumber,
            'plantName' => $plantName,
            'recordStatus' => $recordStatus,
            'id' => $id
        );

        global $base_url;
        global $update_oxy_plant;
        return callWebApiPut($jsonData,$base_url.$update_oxy_plant);

    }

  //get all Oxy Plant
  function getAllOxyPlant(){
    global $base_url;
    global $all_oxy_plant;

    return callWebApiGet($base_url.$all_oxy_plant);
}

/**End Oxygen Plant */


/**Oxygen production */

//add new oxygen production
function addNewOxyProduction($franchiseeId,$country,$authorized,$checker,$batchNumber,$lastModifiedBy,$numberOfCylindersFilled,$maker,$oxygenProduction,$shiftTime,$startTime,$shutdownTime,$engineer,$plantManager,$recordStatus){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'checker' => $checker,
        'batchNumber' => $batchNumber,
        'lastModifiedBy' => $lastModifiedBy,
        'numberOfCylindersFilled' => $numberOfCylindersFilled,
        'maker' => $maker,
        'oxygenProduction' => $oxygenProduction,
        'shiftTime' => $shiftTime,
        'startTime' => $startTime,
        'shutdownTime' => $shutdownTime,
        'engineer'=>$engineer,
        'plantManager'=>$plantManager,
        'recordStatus' => $recordStatus
    );

    global $base_url;
    global $add_oxy_prod;
    return callWebApiPost($jsonData,$base_url.$add_oxy_prod);

}
//Edit oxygen production
function editOxyProduction($franchiseeId,$country,$authorized,$checker,$batchNumber,$lastModifiedBy,$numberOfCylindersFilled,$maker,$oxygenProduction,$shiftTime,$startTime,$shutdownTime,$engineer,$plantManager,$recordStatus,$id){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'checker' => $checker,
        'batchNumber' => $batchNumber,
        'lastModifiedBy' => $lastModifiedBy,
        'numberOfCylindersFilled' => $numberOfCylindersFilled,
        'maker' => $maker,
        'oxygenProduction' => $oxygenProduction,
        'shiftTime' => $shiftTime,
        'startTime' => $startTime,
        'shutdownTime' => $shutdownTime,
        'engineer'=>$engineer,
        'plantManager'=>$plantManager,
        'recordStatus' => $recordStatus,
        'id'=>$id
    );

    global $base_url;
    global $update_oxy_prod;
    return callWebApiPut($jsonData,$base_url.$update_oxy_prod);

}
  //get all Oxy Prod
  function getAllOxyProd(){
    global $base_url;
    global $all_oxy_prod;

    return callWebApiGet($base_url.$all_oxy_prod);
}
function getProdById($id){
    global $base_url;
    global $id_oxy_prod;

    return callWebApiGet($base_url.$id_oxy_prod.$id);
}
  //delete oxygen production
  function deleteOxyProduction($id,$lastModifiedBy){

    $jsonData = array(
        'id' => $id,
        'lastModifiedBy' => $lastModifiedBy
    );

    global $base_url;
    global $delete_oxy_prod;
    return callWebApiDelete($jsonData,$base_url.$delete_oxy_prod);

}

/**End Oxygen Production */
/**Quotations */
   //Get All Quotations
   function getQuotations(){
    global $base_url;
    global $all_quotations;

    return callWebApiGet($base_url.$all_quotations);
 }
 function getQuotationById($quotationId){

    global $base_url;
    global $get_quotation_by_id;
    return callWebApiGet($base_url.$get_quotation_by_id.$quotationId);

}
function getQuotationByBookingId($bookingId){

    global $base_url;
    global $get_quotation_by_bookingid;
    return callWebApiGet($base_url.$get_quotation_by_bookingid.$bookingId);

}
function getBookingByBookingId($bookingId){

    global $base_url;
    global $get_booking_by_bookingId;
    return callWebApiGet($base_url.$get_booking_by_bookingId.$bookingId);

}

function getDriverDocumentsById($IDriverId){

    global $base_url;
    global $get_driver_documents_by_idriverId;
    return callWebApiGet($base_url.$get_driver_documents_by_idriverId.$IDriverId);

}


function getBookingByUserId($UserId){

    global $base_url;
    global $get_booking_by_userId;
    return callWebApiGet($base_url.$get_booking_by_userId.$UserId);

}
/**Quotations */

/**Currencies */
   //Get All Currencies
   function getCurrencies(){
    global $base_url;
    global $all_currencies;

    return callWebApiGet($base_url.$all_currencies);
 }

   //Get All Currency By Id
 function getCurrencyById($currencyId){

    global $base_url;
    global $get_currency_by_Id;
    return callWebApiGet($base_url.$get_currency_by_Id.$currencyId);

}

     //add Currency
     function addCurrency($code,$name,$symbol){

        $jsonData = array(
            'code' => $code,
            'name' => $name,
            'symbol' => $symbol
        );

        global $base_url;
        global $create_currency;
        return callWebApiPost($jsonData,$base_url.$create_currency);

    }
         //edit customer email
         function editCustomerEmail($oldEmail,$newEmail){

            $jsonData = array(
                'oldEmail' => $oldEmail,
                'newEmail' => $newEmail
            );
    
            global $base_url;
            global $customer_edit_email;
            return callWebApiPost($jsonData,$base_url.$customer_edit_email);
    
        }
         //edit Currency
         function editCurrency($code,$name,$symbol,$currencyId,$status){

            $jsonData = array(
                'code' => $code,
                'name' => $name,
                'symbol' => $symbol,
                'currencyId' => $currencyId,
                'status' => $status
            );
    
            global $base_url;
            global $update_currency;
            return callWebApiPost($jsonData,$base_url.$update_currency);
    
        }

//add new Customer
function addNewCustomer($franchiseeId,$country,$customerName,$address,
$agreedPrice,
$authorization,
$authorized,
$checker,
$contactName,
$contractType,
$countryCode,
$dateEstablished,
$email,
$expDeliverySchedule,
$financeContactEmail,
$financeContactName,
$financeContactNumber,
$lastModifiedBy,
$maker,
$natureOfBusiness,
$numberWk,
$o2ContactEmail,
$o2ContactName,
$o2ContactNumber,
$phoneCode,
$phoneNumber,
$pinNumber,
$recordStatus,
$salesOfficer,
$vatNumber,
$vcurrency,
$vlang,
$locationLatitude,
$locationLongitude
){
    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
          'customerName'=>$customerName,
        'address' => $address,
        'agreedPrice' => $agreedPrice,
        'authorization' => $authorization,
        'authorized' => $authorized,
        'checker' => $checker,
        'contactName' => $contactName,
        'contractType' => $contractType,
        'countryCode' => $countryCode,
        'dateEstablished' => $dateEstablished,
        'email' => $email,
        'expDeliverySchedule'=>$expDeliverySchedule,
        'financeContactEmail'=>$financeContactEmail,
        'financeContactName'=>$financeContactName,
        'financeContactNumber'=>$financeContactNumber,
        'lastModifiedBy'=>$lastModifiedBy,
        'maker'=>$maker,
        'natureOfBusiness'=>$natureOfBusiness,
        'numberWk'=>$numberWk,
        'o2ContactEmail'=>$o2ContactEmail,
        'o2ContactName'=>$o2ContactName,
        'o2ContactNumber'=>$o2ContactNumber,
        'phoneCode'=>$phoneCode,
        'phoneNumber'=>$phoneNumber,
        'pinNumber'=>$pinNumber,
        'recordStatus'=>$recordStatus,
        'salesOfficer'=>$salesOfficer,
        'vatNumber'=>$vatNumber,
        'vcurrency'=>$vcurrency,
        'vlang' => $vlang,
       'locationLatitude'=> $locationLatitude,
       'locationLongitude'=> $locationLongitude
    );

    global $base_url;
    global $add_customer;
    return callWebApiPost($jsonData,$base_url.$add_customer);

}
//add new Customer
function editCustomer($customerName,$address,
$agreedPrice,
$authorization,
$authorized,
$checker,
$contactName,
$contractType,
$countryCode,
$dateEstablished,
$email,
$expDeliverySchedule,
$financeContactEmail,
$financeContactName,
$financeContactNumber,
$lastModifiedBy,
$maker,
$natureOfBusiness,
$numberWk,

$phoneCode,
$phoneNumber,
$pinNumber,
$recordStatus,
$salesOfficer,
$vatNumber,
$vcurrency,
$vlang,
$id,
$locationLatitude,
$locationLongitude
){
    $jsonData = array(
        'customerName'=>$customerName,
        'address' => $address,
        'agreedPrice' => $agreedPrice,
        'authorization' => $authorization,
        'authorized' => $authorized,
        'checker' => $checker,
        'contactName' => $contactName,
        'contractType' => $contractType,
        'countryCode' => $countryCode,
        'dateEstablished' => $dateEstablished,
        'email' => $email,
        'expDeliverySchedule'=>$expDeliverySchedule,
        'financeContactEmail'=>$financeContactEmail,
        'financeContactName'=>$financeContactName,
        'financeContactNumber'=>$financeContactNumber,
        'lastModifiedBy'=>$lastModifiedBy,
        'maker'=>$maker,
        'natureOfBusiness'=>$natureOfBusiness,
        'numberWk'=>$numberWk,
        'o2ContactEmail'=>"tg",
        'o2ContactName'=>"tg",
        'o2ContactNumber'=>"tg",
        'phoneCode'=>$phoneCode,
        'phoneNumber'=>$phoneNumber,
        'pinNumber'=>$pinNumber,
        'recordStatus'=>$recordStatus,
        'salesOfficer'=>$salesOfficer,
        'vatNumber'=>$vatNumber,
        'vcurrency'=>$vcurrency,
        'vlang' => $vlang,
        'id' => $id,
       'locationLatitude'=> $locationLatitude,
       'locationLongitude'=> $locationLongitude
    );

    global $base_url;
    global $update_customer;
    return callWebApiPut($jsonData,$base_url.$update_customer);

}
//get Customers
function getAllLogisticsViaBiddingCustomers(){

    $jsonData = array(
        'type' => "getAllCustomers"
    );

 return callWebBiddingApiPost($jsonData);

}

//Customer Forgot Password
function customerForgotPassword($type,$vEmail,$userType){

    $jsonData = array(
        'type' => $type,
        'vEmail' => $vEmail,
        'userType' => $userType
    );

 return callWebBiddingApiPost($jsonData);

}

//Customer Reset Password
function customerResetPassword($type,$token,$newPassword,$userType){

    $jsonData = array(
        'type' => $type,
        'token' => $token,
        'newPassword' => $newPassword,
        'userType' => $userType
    );

 return callWebBiddingApiPost($jsonData);

}


//Admin Forgot Password
function adminForgotPassword($type,$vEmail,$userType){

    $jsonData = array(
        'type' => $type,
        'vEmail' => $vEmail,
        'userType' => $userType
    );

 return callWebBiddingApiPost($jsonData);

}

//Admin Reset Password
function adminResetPassword($type,$token,$newPassword,$userType){

    $jsonData = array(
        'type' => $type,
        'token' => $token,
        'newPassword' => $newPassword,
        'userType' => $userType
    );

 return callWebBiddingApiPost($jsonData);

}

//Franchise forgot password
function franchiseForgotPassword($email){

    $jsonData = array(
        'email' => $email
    );

    global $base_url;
    global $franchise_forgot_password;
    return callWebApiPost($jsonData,$base_url.$franchise_forgot_password);

}


//Franchise reset password
function franchiseResetPassword($email,$resetToken,$newpassword){

    $jsonData = array(
        'email' => $email,
        'resetToken' => $resetToken,
        'newpassword' => $newpassword
    );

    global $base_url;
    global $franchise_reset_password;
    return callWebApiPost($jsonData,$base_url.$franchise_reset_password);

}

//Delete Ind Customer 
function  deleteCustomer($type,$iUserId){

    $jsonData = array(
        'type' => $type,
        'iUserId' => $iUserId
    );

 return callWebBiddingApiPost($jsonData);

}

//Delete Co Customer 
function  deleteCoCustomer($type,$iUserId){

    $jsonData = array(
        'type' => $type,
        'iUserId' => $iUserId
    );

 return callWebBiddingApiPost($jsonData);

}




//Add Admin
function addAdmin($type,$firstname,$lastname,$VEmail,$phoneNumber,$password){

    $jsonData = array(
        'type' => $type,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'VEmail' => $VEmail,
        'phoneNumber' => $phoneNumber,
        'password' => $password
    );

 return callWebBiddingApiPost($jsonData);

}


//Add Individual Customer
function addCustomer($type,$vEmail,$vFirstName,$vPhone,$vInviteCode,$vPassword,$vLastName,$phoneCode,$countryCode,$vLang,$userType,$address){

    $jsonData = array(
        'type' => $type,
        'vEmail' => $vEmail,
        'vFirstName' => $vFirstName,
        'vPhone' => $vPhone,
        'vInviteCode' => $vInviteCode,
        'vPassword' => $vPassword,
        'vLastName' => $vLastName,
        'phoneCode' => $phoneCode,
        'countryCode' => $countryCode,
        'vLang' => $vLang,
        'userType' => $userType,
        'address' => $address
    );

 return callWebBiddingApiPost($jsonData);

}
//Edit Individual Customer
function editIndiCustomer($vCurrency,$customerId,$type,$vEmail,$vFirstName,$vPhone,$vInviteCode,$vPassword,$vLastName,$phoneCode,$countryCode,$vLang,$userType,$address){

    $jsonData = array(

        'vCurrency' => $vCurrency,
        'customerId' => $customerId,
        'type' => $type,
        'vEmail' => $vEmail,
        'vFirstName' => $vFirstName,
        'vPhone' => $vPhone,
        'vInviteCode' => $vInviteCode,
        'vPassword' => $vPassword,
        'vLastName' => $vLastName,
        'phoneCode' => $phoneCode,
        'countryCode' => $countryCode,
        'vLang' => $vLang,
        'userType' => $userType,
        'address' => $address
    );

 return callWebBiddingApiPost($jsonData);

}
//Add Co Customer
function addCoCustomer($type,$name,$vCaddress,$vEmail,$vPhone,$vPassword,$phoneCode,$countryCode,$zimraBPNumber,$userType){

    $jsonData = array(
        'type' => $type,
        'name' => $name,
        'vCaddress' => $vCaddress,
        'vEmail' => $vEmail,
        'vPhone' => $vPhone,
        'vPassword' => $vPassword,
        'phoneCode' => $phoneCode,
        'countryCode' => $countryCode,
        'zimraBPNumber' => $zimraBPNumber,
        'userType' => $userType
    );

 return callWebBiddingApiPost($jsonData);

}

//Edit Co Customer
function editCoCustomer($customerId,$type,$name,$vCaddress,$vEmail,$vPhone,$phoneCode,$countryCode,$zimraBPNumber,$userType){

    $jsonData = array(
        'userId' => $customerId,
        'type' => $type,
        'name' => $name,
        'vCaddress' => $vCaddress,
        'vEmail' => $vEmail,
        'vPhone' => $vPhone,
        'phoneCode' => $phoneCode,
        'countryCode' => $countryCode,
        'zimraBPNumber' => $zimraBPNumber,
        'userType' => $userType
    );

 return callWebBiddingApiPost($jsonData);

}
//Edit Admin
function editAdmin($type,$id,$firstname,$lastname,$VEmail,$phoneNumber){

    $jsonData = array(
        'type' => $type,
        'id' => $id,
        'firstname' => $firstname,
        'lastname' => $lastname,
        'VEmail' => $VEmail,
        'phoneNumber' => $phoneNumber
    );

 return callWebBiddingApiPost($jsonData);

}

//get Customers via Clean City
function getAllLogisticsViaCleanCityCustomers(){

    global $base_url;
    global $find_all_elogistics_customers;
    return callWebApiGet($base_url.$find_all_elogistics_customers);

}
//get Drivers via Clean City
function getAllLogisticsViaCleanCityDrivers(){

    global $base_url;
    global $find_all_elogistics_drivers;
    return callWebApiGet($base_url.$find_all_elogistics_drivers);

}

//get Drivers from elogistics
function getAllElogisticsDrivers(){

    $jsonData = array(
        'type' => "getAllPartners"
    );

 return callWebBiddingApiPost($jsonData);

}

//Upload Proof Of Payment
function uploadProofOfPayment($reference,$bookingId,$filedata,$filetype,$file_name){

    $jsonData = array(
        'reference' => $reference,
        'bookingId' => $bookingId,
        'file' => new CURLFILE($filedata,$filetype,$file_name)
    );

    global $base_url;
    global $upload_proof_of_payment;
    return callWebFileUploadApiPost($jsonData,$base_url.$upload_proof_of_payment);

}

//Upload Proof Of Payment
function uploadDocuments(
    $userId,
    $nationalId_data,$nationalId_type,$nationalId_name,
    $proofOfResidence_data,$proofOfResidence_type,$proofOfResidence_name,
    $bankStatement_data,$bankStatement_type,$bankStatement_name,
    $tradeLicense_data,$tradeLicense_type,$tradeLicense_name
 ){

    $jsonData = array(
        'userId' => $userId,
        'nationalId ' => new CURLFILE($nationalId_data,$nationalId_type,$nationalId_name),
        'proofOfResidence ' => new CURLFILE($proofOfResidence_data,$proofOfResidence_type,$proofOfResidence_name),
        'bankStatement' => new CURLFILE($bankStatement_data,$bankStatement_type,$bankStatement_name),
        'tradeLicense' => new CURLFILE($tradeLicense_data,$tradeLicense_type,$tradeLicense_name)
    );

    global $base_url;
    global $upload_documents;
    return callWebFileUploadApiPost($jsonData,$base_url.$upload_documents);

}


  
  function getAllLogisticsDrivers()
{
  $jsonData = array(
    'type' => "getAllPartners"
  );
  return callWebBiddingApiPost($jsonData);
}

  
function getAllLogisticsAdmins()
{
  $jsonData = array(
    'type' => "getAllUsers"
  );
  return callWebBiddingApiPost($jsonData);
}

function biddingCustomers()
{
  $jsonData = array(
    'type' => "getAllCustomers"
  );
  return callWebBiddingApiPost($jsonData);
}

// function getAllLogisticsAdmins()
// {
//   $jsonData = array(
//     'type' => "getAllUsers"
//   );
//   return callWebBiddingApiPost($jsonData);
// }
function adminLogin($username,$vPassword)
{
  $jsonData = array(
    'username' => $username,
    'password' => $vPassword
  );
  global $base_url;
  global $admin_login;
  return callWebApiPost($jsonData,$base_url.$admin_login);
}

function customerLogin($type,$username,$vPassword,$userType)
{
  $jsonData = array(
    'type' => $type,
    'username' => $username,
    'vPassword' => $vPassword,
    'userType' => $userType
  );
  return callWebBiddingApiPost($jsonData);
}

function franchiseLogin($username,$vPassword){
    $jsonData = array(
        'email' => $username,
        'password' => $vPassword
    );
    global $base_url;
    global $franchise_login;
    return callWebApiPost($jsonData,$base_url.$franchise_login);

}

//get all elogistics customers
function getCleanCityCustomers(){
    global $base_url;
    global $find_all_customers;

    return callWebApiGet($base_url.$find_all_customers);
}

//get all elogistics customers details by id
function getElogisticsCustomersById($elogisticsId){
    global $base_url;
    global $find_customer_by_Id;

    return callWebApiGet($base_url.$find_customer_by_Id.$elogisticsId);
}



/**Payment Methods */

function getAllPayments(){
    global $base_url;
    global $get_all_payments;
    return callWebApiGet($base_url.$get_all_payments);
}


   //Get All Payment Methods
   function getPaymentMethods(){
    global $base_url;
    global $all_payment_methods;

    return callWebApiGet($base_url.$all_payment_methods);
 }
//add payment method
function addPaymentMethod($description){

    $jsonData = array(
        'description' => $description
    );

    global $base_url;
    global $create_payment_method;
    return callWebApiPost($jsonData,$base_url.$create_payment_method);

}


//edit payment method
function editPaymentMethod($description,$paymentMethodId,$status){

    $jsonData = array(
        'description' => $description,
        'paymentMethodId' => $paymentMethodId,
        'status' => $status
    );

    global $base_url;
    global $update_payment_method;
    return callWebApiPost($jsonData,$base_url.$update_payment_method);

}

//eco ZWL payment
function ecoZWLPayment($currencyCode,$ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId){

    $jsonData = array(
        'currencyCode' => $currencyCode,
        'ecocashNumber' => $ecocashNumber,
        'amount' => $amount,
        'paidBy' => $paidBy,
        'paymentMethod' => $paymentMethod,
        'bookingId' => $bookingId
    );

    global $base_url;
    global $ecocash_payment;
    return callWebApiPost($jsonData,$base_url.$ecocash_payment);

}
//eco USD payment
function ecoUSDPayment($currencyCode,$ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId){

    $jsonData = array(
        'currencyCode' => $currencyCode,
        'ecocashNumber' => $ecocashNumber,
        'amount' => $amount,
        'paidBy' => $paidBy,
        'paymentMethod' => $paymentMethod,
        'bookingId' => $bookingId
    );

    global $base_url;
    global $ecocash_payment;
    return callWebApiPost($jsonData,$base_url.$ecocash_payment);

}
//cash ZWL payment
function cashZWLPayment($currencyCode,$ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId){

    $jsonData = array(
        'currencyCode' => $currencyCode,
        'ecocashNumber' => $ecocashNumber,
        'amount' => $amount,
        'paidBy' => $paidBy,
        'paymentMethod' => $paymentMethod,
        'bookingId' => $bookingId
    );

    global $base_url;
    global $ecocash_payment;
    return callWebApiPost($jsonData,$base_url.$ecocash_payment);

}
//cash USD payment
function cashUSDPayment($currencyCode,$ecocashNumber,$amount,$paidBy,$paymentMethod,$bookingId){

    $jsonData = array(
        'currencyCode' => $currencyCode,
        'ecocashNumber' => $ecocashNumber,
        'amount' => $amount,
        'paidBy' => $paidBy,
        'paymentMethod' => $paymentMethod,
        'bookingId' => $bookingId
    );

    global $base_url;
    global $ecocash_payment;
    return callWebApiPost($jsonData,$base_url.$ecocash_payment);

}

//approve payment
function approvePayment($paymentId,$paymentStatus){

    $jsonData = array(
        'paymentId' => $paymentId,
        'paymentStatus' => $paymentStatus
    );

    global $base_url;
    global $approve_payment;
    return callWebApiPost($jsonData,$base_url.$approve_payment);

}
//approve Customer Account

function approveCustomer($userId,$status){

    global $base_url;
    global $approve_customer;
    return callWebApiGet($base_url.$approve_customer.'/'.$userId.'/'.$status);

}

// get Payments By payment Method
function getpaymentsByMethod($paymentMethod){

    global $base_url;
    global $payments_by_method;
    return callWebApiGet($base_url.$payments_by_method.$paymentMethod);

}

// approve driver
function approveDriver($IDriverId){

    global $base_url;
    global $approve_driver_documents;
    return callWebApiGet($base_url.$approve_driver_documents.$IDriverId);

}
//Edit Equipment Maintanance
function EditEquipmentMaintanance($authorized,$checker,$maintenanceType,$lastModifiedBy,$equipmentMaintained,$maker,$maintenanceDescription,$partsUsed,$maintenancePerformedBy,$maintenanceTeamName,$maintenanceTeamContact,$engineer,$plantManager,$remarks,$recordStatus,$id){

    $jsonData = array(
        'authorized' => $authorized,
        'checker' => $checker,
        'maintenanceType' => $maintenanceType,
        'lastModifiedBy' => $lastModifiedBy,
        'equipmentMaintained' => $equipmentMaintained,
        'maker' => $maker,
        'maintenanceDescription' => $maintenanceDescription,
        'partsUsed' => $partsUsed,
        'maintenancePerformedBy' => $maintenancePerformedBy,
        'maintenanceTeamName' => $maintenanceTeamName,
        'maintenanceTeamContact'=>$maintenanceTeamContact,
        'engineer'=>$engineer,
        'plantManager'=>$plantManager,
        'remarks'=>$remarks,
        'recordStatus' => $recordStatus,
        'id'=>$id
    );

    global $base_url;
    global $update_equip_main;
    return callWebApiPut($jsonData,$base_url.$update_equip_main);

}
  //get all Equipment Maintenance
  function getAllEquipMain(){
    global $base_url;
    global $all_equip_main;

    return callWebApiGet($base_url.$all_equip_main);
}
//delete equipment maintance
function deleteEquipmentMaintance($id,$lastModifiedBy){

    $jsonData = array(
        'authorized' => $authorized,
        'checker' => $checker,
        'engineer' => $engineer,
        'equipmentMaintained' => $equipmentMaintained,
        'lastModifiedBy' => $lastModifiedBy,
        'maintenanceDescription' => $maintenanceDescription,
        'maintenancePerformedBy' => $maintenancePerformedBy,
        'maintenanceTeamContact' => $maintenanceTeamContact,

        'maintenanceTeamName' => $maintenanceTeamName,
        'maintenanceType' => $maintenanceType,
        'maker' => $maker,

        'partsUsed' => $partsUsed,
        'plantManager' => $plantManager,
        'recordStatus' => $recordStatus,
        'remarks' => $remarks
    );

    global $base_url;
    global $add_equip_main;
    return callWebApiPost($jsonData,$base_url.$add_equip_main);

}


   //edit Equip
   function updateEquip($franchiseeId,$country,$authorized,$checker,$engineer,$equipmentMaintained,$lastModifiedBy,$maintenanceDescription,$maintenancePerformedBy,$maintenanceTeamContact,$maintenanceTeamName,$maintenanceType,$maker,$partsUsed,$plantManager,$recordStatus,$remarks,$id){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'checker' => $checker,
        'engineer' => $engineer,
        'equipmentMaintained' => $equipmentMaintained,
        'lastModifiedBy' => $lastModifiedBy,
        'maintenanceDescription' => $maintenanceDescription,
        'maintenancePerformedBy' => $maintenancePerformedBy,
        'maintenanceTeamContact' => $maintenanceTeamContact,

        'maintenanceTeamName' => $maintenanceTeamName,
        'maintenanceType' => $maintenanceType,
        'maker' => $maker,

        'partsUsed' => $partsUsed,
        'plantManager' => $plantManager,
        'recordStatus' => $recordStatus,
        'remarks' => $remarks,
        'id' => $id
    );

    global $base_url;
    global $update_equip_main;
    return callWebApiPut($jsonData,$base_url.$update_equip_main);

}
/**End Equipment Maintance */
/**Incident reports */

  //get all Incident Reports
  function getAllIncidentReport(){
    global $base_url;
    global $all_incident_reports;

    return callWebApiGet($base_url.$all_incident_reports);
}

        //add Incident Report
        function addNewReport($franchiseeId,$country,$authorized,$businessHead,$engineer,$followUpRecommendation,$incidentCause,$incidentDetails,$lastModifiedBy,$locationOfIncident,$maker,$plantManager,$recordStatus,$resolution,$serviceRequired,$shift,$typeOfService){

            $jsonData = array(
                'franchiseeId' => $franchiseeId,
                'country' => $country,
                'authorized' => $authorized,
                'businessHead' => $businessHead,
                'engineer' => $engineer,
                'followUpRecommendation' => $followUpRecommendation,
                'incidentCause' => $incidentCause,
                'incidentDetails' => $incidentDetails,
                'lastModifiedBy' => $lastModifiedBy,
                'locationOfIncident' => $locationOfIncident,

                'maker' => $maker,
                'plantManager' => $plantManager,
                'recordStatus' => $recordStatus,

                'resolution' => $resolution,
                'serviceRequired' => $serviceRequired,
                'shift' => $shift,
                'typeOfService' => $typeOfService
            );

            global $base_url;
            global $add_incident_report;
            return callWebApiPost($jsonData,$base_url.$add_incident_report);

        }

           //edit Incident Report
           function editReport($franchiseeId,$country,$authorized,$businessHead,$engineer,$followUpRecommendation,$incidentCause,$incidentDetails,$lastModifiedBy,$locationOfIncident,$maker,$plantManager,$recordStatus,$resolution,$serviceRequired,$shift,$typeOfService,$id){

            $jsonData = array(
                'franchiseeId' => $franchiseeId,
                'country' => $country,
                'authorized' => $authorized,
                'businessHead' => $businessHead,
                'engineer' => $engineer,
                'followUpRecommendation' => $followUpRecommendation,
                'incidentCause' => $incidentCause,
                'incidentDetails' => $incidentDetails,
                'lastModifiedBy' => $lastModifiedBy,
                'locationOfIncident' => $locationOfIncident,

                'maker' => $maker,
                'plantManager' => $plantManager,
                'recordStatus' => $recordStatus,

                'resolution' => $resolution,
                'serviceRequired' => $serviceRequired,
                'shift' => $shift,
                'typeOfService' => $typeOfService,
                'id' => $id
            );

            global $base_url;
            global $update_incident_report;
            return callWebApiPut($jsonData,$base_url.$update_incident_report);

        }


        //delete report
        function deleteReport($id,$lastModifiedBy){

            $jsonData = array(
                'id' => $id,
                'lastModifiedBy' => $lastModifiedBy
            );

            global $base_url;
            global $delete_incident_report;
            return callWebApiDelete($jsonData,$base_url.$delete_incident_report);

        }

/**End incident reports */






/**Reports */
  //get all Incident Reports
  function getAllInvoices(){
    global $base_url;
    global $all_invoices;

    return callWebApiGet($base_url.$all_invoices);
}


      //add Invoice
      function addInvoice($customerOrderId,$franchiseeId,$country,$authorized,$checker,$description,$lastModifiedBy,$maker,$paymentMethod,$quantity,$recordStatus,$total,$transactionNumber,$unitPrice){

        $jsonData = array(
            'customerOrderId' => $customerOrderId,
            'franchiseeId' => $franchiseeId,
            'country' => $country,
            'authorized' => $authorized,
            'checker' => $checker,
            'description' => $description,
            'lastModifiedBy' => $lastModifiedBy,
            'maker' => $maker,
            'paymentMethod' => $paymentMethod,
            'quantity' => $quantity,
            'recordStatus' => $recordStatus,
            'total' => $total,
            'transactionNumber' => $transactionNumber,
            'unitPrice' => $unitPrice
        );

        global $base_url;
        global $add_invoice;
        return callWebApiPost($jsonData,$base_url.$add_invoice);

    }

      //delete invoice
      function deleteInvoice($id,$lastModifiedBy){

        $jsonData = array(
            'id' => $id,
            'lastModifiedBy' => $lastModifiedBy
        );

        global $base_url;
        global $delete_invoice;
        return callWebApiDelete($jsonData,$base_url.$delete_invoice);

    }

          //edit Invoice
          function editInvoice($customerOrderId,$deliveryLogId,$franchiseeId,$country,$authorized,$checker,$description,$lastModifiedBy,$maker,$paymentMethod,$quantity,$recordStatus,$total,$transactionNumber,$unitPrice,$id){

            $jsonData = array(
                'customerOrderId' => $customerOrderId,
                'deliveryLogId' => $deliveryLogId,
                'franchiseeId' => $franchiseeId,
                'country' => $country,
                'authorized' => $authorized,
                'checker' => $checker,
             
                'description' => $description,
                'lastModifiedBy' => $lastModifiedBy,
                'maker' => $maker,
                'paymentMethod' => $paymentMethod,
                'quantity' => $quantity,

                'recordStatus' => $recordStatus,
                'total' => $total,
                'transactionNumber' => $transactionNumber,

                'unitPrice' => $unitPrice,
                'id' => $id

            );

            global $base_url;
            global $update_invoice;
            return callWebApiPut($jsonData,$base_url.$update_invoice);

        }

/**End invoices */

/**Cities */
   //Get All Cities
   function getAllCities(){
    global $base_url;
    global $all_cities;

    return callWebApiGet($base_url.$all_cities);
 }

 //add City
 function addCity($cityName,$country,$province,$countryCode){

    $jsonData = array(
        'cityName' => $cityName,
        'country' => $country,
        'province' => $province,
        'countryCode' => $countryCode
    );

    global $base_url;
    global $create_city;
    return callWebApiPost($jsonData,$base_url.$create_city);

}
 //edit  City
 function editCity($cityName,$country,$countryCode,$province,$id){

    $jsonData = array(
        'cityName' => $cityName,
        'country' => $country,
        'countryCode' => $countryCode,
        'province' => $province,
        'id' => $id
    );

    global $base_url;
    global $update_city;
    return callWebApiPost($jsonData,$base_url.$update_city);

}
   //add Order
   function addOrder($franchiseeId,$country,
   $authorization,$authorized,$checker,
   $customerId,$cylinderPrice,$cylinderSize,
   $dateDelivered,$lastModifiedBy,$maker,
   $orderChannel,$orderType,$recordStatus,$salesAgent,$unitsOrdered,$plantId){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorization' => $authorization,
        'authorized' => $authorized,
        'checker' => $checker,
        'customerId' => $customerId,
        'cylinderPrice' => $cylinderPrice,
        'cylinderSize' => $cylinderSize,
        'dateDelivered' => $dateDelivered,
        'lastModifiedBy' => $lastModifiedBy,
        'maker' => $maker,
        'orderChannel' => $orderChannel,
        'orderType' => $orderType,
        'recordStatus' => $recordStatus,
        'salesAgent' => $salesAgent,
        'unitsOrdered' => $unitsOrdered,
        'plantId'=>$plantId

    );

    global $base_url;
    global $add_customer_order;
    return callWebApiPost($jsonData,$base_url.$add_customer_order);

}

 //edit Order
 function editOrder($franchiseeId,$country,$authorization,$authorized,$checker,$customerId,$cylinderPrice,$cylinderSize,$dateDelivered,$invoiceNumber,$lastModifiedBy,$maker,$orderChannel,$orderType,$recordStatus,$salesAgent,$unitsOrdered,$id){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorization' => $authorization,
        'authorized' => $authorized,
        'checker' => $checker,
        'customerId' => $customerId,
        'cylinderPrice' => $cylinderPrice,
        'cylinderSize' => $cylinderSize,
        'dateDelivered' => $dateDelivered,
        'invoiceNumber' => $invoiceNumber,

        'lastModifiedBy' => $lastModifiedBy,
        'maker' => $maker,
        'orderChannel' => $orderChannel,

        'orderType' => $orderType,

        'recordStatus' => $recordStatus,
        'salesAgent' => $salesAgent,

        'unitsOrdered' => $unitsOrdered,
        'id' => $id

    );

    global $base_url;
    global $update_customer_order;
    return callWebApiPut($jsonData,$base_url.$update_customer_order);

}


    //delete order
    function deleteOrder($id,$lastModifiedBy){

        $jsonData = array(
            'id' => $id,
            'lastModifiedBy' => $lastModifiedBy
        );

        global $base_url;
        global $delete_customer_order;
        return callWebApiDelete($jsonData,$base_url.$delete_customer_order);

    }

  /**End customer orders */



  /**Zone Pricing  */
     //Get All Zone Pricing
     function getAllZonePricing(){
        global $base_url;
        global $all_zone_prices;
    
        return callWebApiGet($base_url.$all_zone_prices);
     }
    

   //add Customer log
   function addLog($franchiseeId,$country,$authorized,$checker,$contactDetails,$customerDesignation,$dateOfIncident,$dateTimeOfresolution,$description,$issueLoggedBy,$lastModifiedBy,$maker,$recordStatus,$resolution,$signOff,$timeOfIncident){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'checker' => $checker,
        'contactDetails' => $contactDetails,
        'customerDesignation' => $customerDesignation,
        'dateOfIncident' => $dateOfIncident,
        'dateTimeOfresolution' => $dateTimeOfresolution,
        'description' => $description,

        'issueLoggedBy' => $issueLoggedBy,
        'lastModifiedBy' => $lastModifiedBy,
        'maker' => $maker,

        'recordStatus' => $recordStatus,

        'resolution' => $resolution,
        'signOff' => $signOff,

        'timeOfIncident' => $timeOfIncident

    );

    global $base_url;
    global $add_issues_log;
    return callWebApiPost($jsonData,$base_url.$add_issues_log);

}


  //edit  Customer log
  function editLog($franchiseeId,$country,$authorized,$checker,$contactDetails,$customerDesignation,$dateOfIncident,$dateTimeOfresolution,$description,$issueLoggedBy,$lastModifiedBy,$maker,$recordStatus,$resolution,$signOff,$timeOfIncident,$id){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'checker' => $checker,
        'contactDetails' => $contactDetails,
        'customerDesignation' => $customerDesignation,
        'dateOfIncident' => $dateOfIncident,
        'dateTimeOfresolution' => $dateTimeOfresolution,
        'description' => $description,

        'issueLoggedBy' => $issueLoggedBy,
        'lastModifiedBy' => $lastModifiedBy,
        'maker' => $maker,

        'recordStatus' => $recordStatus,

        'resolution' => $resolution,
        'signOff' => $signOff,

        'timeOfIncident' => $timeOfIncident,
        'id' => $id

    );

    global $base_url;
    global $update_issues_log;
    return callWebApiPut($jsonData,$base_url.$update_issues_log);

}



    //delete log
    function deleteLog($id,$lastModifiedBy){

        $jsonData = array(
            'id' => $id,
            'lastModifiedBy' => $lastModifiedBy
        );

        global $base_url;
        global $delete_issues_log;
        return callWebApiDelete($jsonData,$base_url.$delete_issues_log);

    }


   /**End customer logs */




  /**Deliveries */
  //get all delivery logs

  function getAllDeliveryLogs(){
    global $base_url;
    global $all_deliveries_log;

    return callWebApiGet($base_url.$all_deliveries_log);
}

   //add Delivery log
   function addDelivery($customeOrderId,$deliveredCylinderId,$franchiseeId,$country,$authorized,$authorizedBy,$beginningOdometer,$checker,$customerId,$cylinderSize,$cylindersDelivered,$deliveryDate,$driverId,$dropOffLocation,$dropOffTime,$endingOdometer,$invoiceNumber,$lastModifiedBy,$maker,$paymentMethodId,$pickup,$recordStatus,$takeOffLocation,$takeOffTime,$timeOfDelivery){

    $jsonData = array(
        'customeOrderId' => $customeOrderId,
        'deliveredCylinderId' => $deliveredCylinderId,
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'authorizedBy' => $authorizedBy,
        'beginningOdometer' => $beginningOdometer,
        'checker' => $checker,
        'customerId' => $customerId,
        'cylinderSize' => $cylinderSize,
        'cylindersDelivered' => $cylindersDelivered,

        'deliveryDate' => $deliveryDate,
        'driverId' => $driverId,
        'dropOffLocation' => $dropOffLocation,

        'dropOffTime' => $dropOffTime,

        'endingOdometer' => $endingOdometer,
        'invoiceNumber' => $invoiceNumber,

        'lastModifiedBy' => $lastModifiedBy,

        'maker' => $maker,
        'paymentMethodId' => $paymentMethodId,

        'pickup' => $pickup,


        'recordStatus' => $recordStatus,
        'takeOffLocation' => $takeOffLocation,

        'takeOffTime' => $takeOffTime,

        'timeOfDelivery' => $timeOfDelivery


    );

    global $base_url;
    global $add_delivery_log;
    return callWebApiPost($jsonData,$base_url.$add_delivery_log);

}


  //edit Delivery log
  function editDelivery($customeOrderId,$deliveredCylinderId,$franchiseeId,$country,$authorized,$authorizedBy,$beginningOdometer,$checker,$customerId,$cylinderSize,$cylindersDelivered,$deliveryDate,$driverId,$dropOffLocation,$dropOffTime,$endingOdometer,$invoiceNumber,$lastModifiedBy,$maker,$paymentMethodId,$pickup,$recordStatus,$takeOffLocation,$takeOffTime,$timeOfDelivery,$id){

    $jsonData = array(
        'customeOrderId' => $customeOrderId,
        'deliveredCylinderId' => $deliveredCylinderId,
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'authorizedBy' => $authorizedBy,
        'beginningOdometer' => $beginningOdometer,
        'checker' => $checker,
        'customerId' => $customerId,
        'cylinderSize' => $cylinderSize,
        'cylindersDelivered' => $cylindersDelivered,

        'deliveryDate' => $deliveryDate,
        'driverId' => $driverId,
        'dropOffLocation' => $dropOffLocation,

        'dropOffTime' => $dropOffTime,

        'endingOdometer' => $endingOdometer,
        'invoiceNumber' => $invoiceNumber,

        'lastModifiedBy' => $lastModifiedBy,

        'maker' => $maker,
        'paymentMethodId' => $paymentMethodId,

        'pickup' => $pickup,


        'recordStatus' => $recordStatus,
        'takeOffLocation' => $takeOffLocation,

        'takeOffTime' => $takeOffTime,

        'timeOfDelivery' => $timeOfDelivery,

        'id' => $id

    );

    global $base_url;
    global $update_delivery_log;
    return callWebApiPut($jsonData,$base_url.$update_delivery_log);

}



    //delete delivery
    function deleteDelivery($id,$lastModifiedBy){

        $jsonData = array(
            'id' => $id,
            'lastModifiedBy' => $lastModifiedBy
        );

        global $base_url;
        global $delete_delivery_log;
        return callWebApiDelete($jsonData,$base_url.$delete_delivery_log);

    }


     /**End delivery logs */



       /**Start payment method */


  
    //delete delivery
    function deletePayment($id,$lastModifiedBy){

        $jsonData = array(
            'id' => $id,
            'lastModifiedBy' => $lastModifiedBy
        );

        global $base_url;
        global $delete_payment_method;
        return callWebApiDelete($jsonData,$base_url.$delete_payment_method);

    }

      //add  payment method
  function addPayment($franchiseeId,$country,$authorized,$checker,$currency,$lastModifiedBy,$maker,$name,$recordStatus){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'checker' => $checker,
        'currency' => $currency,
        'lastModifiedBy' => $lastModifiedBy,
        'maker' => $maker,
        'name' => $name,
        'recordStatus' => $recordStatus

    );

    global $base_url;
    global $add_payment_method;
    return callWebApiPost($jsonData,$base_url.$add_payment_method);

}


  //edit payment method
  function editPayment($franchiseeId,$country,$authorized,$checker,$currency,$lastModifiedBy,$maker,$name,$recordStatus,$id){

    $jsonData = array(
        'franchiseeId' => $franchiseeId,
        'country' => $country,
        'authorized' => $authorized,
        'checker' => $checker,
        'currency' => $currency,
        'lastModifiedBy' => $lastModifiedBy,
        'maker' => $maker,
        'name' => $name,
        'recordStatus' => $recordStatus,
        'id' => $id

    );

    global $base_url;
    global $update_payment_method;
    return callWebApiPut($jsonData,$base_url.$update_payment_method);

}


         /**End payment method */



         /**Start password */
   //forgot password
   function forgotPassword($email){

    $jsonData = array(
        'email' => $email
    );

    global $base_url;
    global $forgot_password;
    return callWebApiPost($jsonData,$base_url.$forgot_password);

}


   //reset password
   function resetPassword($resetToken,$password){

    $jsonData = array(
        'resetToken' => $resetToken,
        'password' => $password
    );

    global $base_url;
    global $reset_password;
    return callWebApiPost($jsonData,$base_url.$reset_password);

}

         /**End password */
  /**Queries */

  //Get All Queries
  function getAllQueries(){
    global $base_url;
    global $all_queries;

    return callWebApiGet($base_url.$all_queries);
 }

      //add  Franchise 
      function addFranchise($franchiseeId,$franchiseeName,$address,$country,$email,$lastModifiedBy,$maker,$phoneNumber,$recordStatus){

        $jsonData = array(
            'franchiseeId' => $franchiseeId,
            'franchiseeName' => $franchiseeName,
            'address' => $address,
            'country' => $country,
            'email' => $email,
            'lastModifiedBy' => $lastModifiedBy,
            'maker' => $maker,
            'phoneNumber' => $phoneNumber,
            'recordStatus' => $recordStatus
    
        );
    
        global $base_url;
        global $add_franchise;
        return callWebApiPost($jsonData,$base_url.$add_franchise);
    
    }

          //edit  Franchise
          function  updateFranchise($authorized,$checker,$franchiseeId,$franchiseeName,$address,$country,$email,$lastModifiedBy,$maker,$phoneNumber,$recordStatus,$id){

            $jsonData = array(
                'authorized' => $authorized,
                'checker' => $checker,
                'franchiseeId' => $franchiseeId,
                'franchiseeName' => $franchiseeName,
                'address' => $address,
                'country' => $country,
                'email' => $email,
                'lastModifiedBy' => $lastModifiedBy,
                'maker' => $maker,
                'phoneNumber' => $phoneNumber,
                'recordStatus' => $recordStatus,
                'id' => $id
        
            );
        
            global $base_url;
            global $update_franchise;
            return callWebApiPut($jsonData,$base_url.$update_franchise);
        
        }
        
        //delete Franchise
        function deleteFranchise($id,$lastModifiedBy){

            $jsonData = array(
                'id' => $id,
                'lastModifiedBy' => $lastModifiedBy
            );
    
            global $base_url;
            global $delete_franchise;
            return callWebApiDelete($jsonData,$base_url.$delete_franchise);
    
        }
    
           /**End End Franchise */

             /**Start Generate Qr */
 
//    function generateCode($cylinderSerialNumber){

//     $jsonData = array(
//         'cylinderSerialNumber' => $cylinderSerialNumber
//     );

//     global $base_url;
//     global $generate_qr;
//     return callWebApiGetQRCode($jsonData,$base_url.$generate_qr);

// }

function callWebApiGetBarCode($cocylinderMSerialNumberde){
    // global $token;

    $curl = curl_init();

    curl_setopt_array($curl, array(
    CURLOPT_URL => 'https://vayaafrica.com/oxygen-hub-dummy/v1/barcodes/generate/code128/'.$cocylinderMSerialNumberde,
    CURLOPT_RETURNTRANSFER => true,
    CURLOPT_ENCODING => '',
    CURLOPT_MAXREDIRS => 10,
    CURLOPT_TIMEOUT => 0,
    CURLOPT_FOLLOWLOCATION => true,
    CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    CURLOPT_CUSTOMREQUEST => 'GET',
  ));
  
  $response = curl_exec($curl);
  
  curl_close($curl);

  return $response;

  
 
}

           /**End Generate Qr */

 /** Start Get All By FranchiseeId */

 //Users
 function GetUserByFranchiseeId($FranchiseeId){


    global $base_url;
    global $user_by_franchiseeId;
    return callWebApiGet($base_url.$user_by_franchiseeId.$FranchiseeId);

}

//Customers
function GetCustomerByFranchiseeId($FranchiseeId){


    global $base_url;
    global $customer_by_franchiseeId;
    return callWebApiGet($base_url.$customer_by_franchiseeId.$FranchiseeId);

}


//Customer Issues Log
function GetCustomerIssuesLogByFranchiseeId($FranchiseeId){


    global $base_url;
    global $issues_log_franchiseeId;
    return callWebApiGet($base_url.$issues_log_franchiseeId.$FranchiseeId);

}

//Customer Order
function GetCustomerOrderByFranchiseeId($FranchiseeId){


    global $base_url;
    global $customer_by_franchiseeId;
    return callWebApiGet($base_url.$customer_by_franchiseeId.$FranchiseeId);

}
//Cylinder Checks 
function GetCylinderChecksByFranchiseeId($FranchiseeId){


    global $base_url;
    global $cylinder_checks_by_franchiseeId;
    return callWebApiGet($base_url.$cylinder_checks_by_franchiseeId.$FranchiseeId);

}

//Cylinders
function GetCylindersByFranchiseeId($FranchiseeId){


    global $base_url;
    global $cylinder_by_franchiseeId;
    return callWebApiGet($base_url.$cylinder_by_franchiseeId.$FranchiseeId);

}

//Cylinder Tracking
function GetCylinderTrackingByFranchiseeId($FranchiseeId){

    global $base_url;
    global $cylinder_tracking_by_franchiseeId;
    return callWebApiGet($base_url.$cylinder_tracking_by_franchiseeId.$FranchiseeId);

}

//Delivery Log
function GetDeliveryLogByFranchiseeId($FranchiseeId){

    global $base_url;
    global $cylinder_tracking_by_franchiseeId;
    return callWebApiGet($base_url.$cylinder_tracking_by_franchiseeId.$FranchiseeId);

}

//Eq
function GetEqByFranchiseeId($FranchiseeId){

    global $base_url;
    global $eq_by_franchiseeId;
    return callWebApiGet($base_url.$eq_by_franchiseeId.$FranchiseeId);

}

//Incident Report
function GetIncidentReportByFranchiseeId($FranchiseeId){

    global $base_url;
    global $incident_rep_by_franchiseeId;
    return callWebApiGet($base_url.$incident_rep_by_franchiseeId.$FranchiseeId);

}

//Invoice
function GetInvoiceByFranchiseeId($FranchiseeId){

    global $base_url;
    global $invoice_by_franchiseeId;
    return callWebApiGet($base_url.$invoice_by_franchiseeId.$FranchiseeId);

}

//Plant
function GetPlantByFranchiseeId($FranchiseeId){

    global $base_url;
    global $plant_by_franchiseeId;
    return callWebApiGet($base_url.$plant_by_franchiseeId.$FranchiseeId);

}

//Oxy
function GetOxyByFranchiseeId($FranchiseeId){

    global $base_url;
    global $oxy_by_franchiseeId;
    return callWebApiGet($base_url.$oxy_by_franchiseeId.$FranchiseeId);

}

//Payment
function GetPaymentByFranchiseeId($FranchiseeId){

    global $base_url;
    global $payment_by_franchiseeId;
    return callWebApiGet($base_url.$payment_by_franchiseeId.$FranchiseeId);

}
  /**End  Get All By FranchiseeId  */

    /**Start  Get Franchise By Id  */
 
       /**End  Get Franchise By Id  */

          /**Start  Get Plant By Id  */
    function getPlantByID($FranchiseeId){

        global $base_url;
        global $by_id_oxy_plant;
        return callWebApiGet($base_url.$by_id_oxy_plant.$FranchiseeId);
    
    }
       /**End  Get Franchise By Id  */

  /**Start Delivered Cylinders */

//Get all Delivered Cylinders
function getAllDeliveredCylinders(){
    global $base_url;
    global $all_d_cylinders;

    return callWebApiGet($base_url.$all_d_cylinders);
}

//Get Delivered Cylinders By FranchiseId
function GetDeliveredCylindersByFranchiseeId($FranchiseeId){

    global $base_url;
    global $dcylinders_by_franchiseeId;
    return callWebApiGet($base_url.$dcylinders_by_franchiseeId.$FranchiseeId);

}

//add  Delivered Cylinders
function addDeliveredC($authorized,$checker,$country,$cylinderSerialNumber,$deliveryLogId,$franchiseeId,$lastModifiedBy,$maker,$recordStatus){

$jsonData = array(
    'authorized' => $authorized,
    'checker' => $checker,
    'country' => $country,
    'cylinderSerialNumber' => $cylinderSerialNumber,
    'deliveryLogId' => $deliveryLogId,
    'franchiseeId' => $franchiseeId,
    'lastModifiedBy' => $lastModifiedBy,
    'maker' => $maker,
    'recordStatus' => $recordStatus

);

global $base_url;
global $add_d_cylinder;
return callWebApiPost($jsonData,$base_url.$add_d_cylinder);

}

  //update Delivered Cylinders
  function  updateDeliveredC($authorized,$checker,$country,$cylinderSerialNumber,$deliveryLogId,$franchiseeId,$lastModifiedBy,$maker,$recordStatus,$id){

    $jsonData = array(
        'authorized' => $authorized,
        'checker' => $checker,
        'country' => $country,
        'cylinderSerialNumber' => $cylinderSerialNumber,
        'deliveryLogId' => $deliveryLogId,
        'franchiseeId' => $franchiseeId,
        'lastModifiedBy' => $lastModifiedBy,
        'maker' => $maker,
        'recordStatus' => $recordStatus,
        'id' => $id

    );

    global $base_url;
    global $update_d_cylinder;
    return callWebApiPut($jsonData,$base_url.$update_d_cylinder);

}

//delete Delivered Cylinder
function deleteDeliveredC($id,$lastModifiedBy){

    $jsonData = array(
        'id' => $id,
        'lastModifiedBy' => $lastModifiedBy
    );

    global $base_url;
    global $delete_d_cylinder;
    return callWebApiDelete($jsonData,$base_url.$delete_d_cylinder);

}

   /**End End Delivered Cylinders */
     
?>
