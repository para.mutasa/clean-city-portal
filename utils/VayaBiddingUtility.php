<?php
    // error_reporting(E_ALL);
    error_reporting(0);
include_once('curl_url.php');

    function callVayaBiddingWebApiPost($jsonData)
    {
        global $bidding_url;
  
        $jsonDataEncoded = json_encode($jsonData);

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $bidding_url,
        
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 0,
            CURLOPT_FOLLOWLOCATION => true,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonDataEncoded,
            CURLOPT_HTTPHEADER => array(
            "Content-Type: application/json"
            ),
        ));
        
        $response = curl_exec($curl);
        
        curl_close($curl);
        return json_decode($response,true);
    }


    ?>
