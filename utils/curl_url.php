<?php
$base_url = "https://vayaafrica.com/clean_city_java_backend/";
$bidding_url = "https://vayaafrica.com/vbp-backend/v1/vaya-bidding-platform-service/";
$bidding_file_url = "https://vayaafrica.com/vbp-backend/v1/vaya-bidfind_all_elogistics_driversding-platform-service/downloadFile?";

//Reports  API
$franchise_reports = 'bookings/find-by-payment-status-and-franchise-id';
$admin_reports = 'bookings/find-by-payment-status/';

//Admin  API
$admin_login = 'users/login';

//Driver  API
$find_all_elogistics_drivers = 'users/find-all-elogistics-drivers';
$get_driver_documents_by_idriverId = 'franchise-driver/find-franchise-driver-documents/';
$approve_driver_documents = 'franchise-driver/approve-franchise-driver/';

//Upload File  API
$upload_proof_of_payment = 'documentStorage/uploadFile';

//Customer  API
$find_all_customers = 'customer/find-all';
$find_customer_by_Id = 'customer/find-by-elogistics-id/';
$find_all_elogistics_customers = 'customer/find-all-elogistics';
$customer_edit_email = 'customer/update-email';
$upload_documents = 'customer/upload-customer-documents';
$approve_customer = 'customer/approve-customer';
$get_documents_by_userid = 'customer/find-by-elogistics-id/';

//Waste Type API
$all_waste_types = 'wasteTypes/find-waste-types';
$create_waste_type = 'wasteTypes/create-wasteType';
$update_waste_type = 'wasteTypes/update-waste-type';
$find_waste_type_by_id = 'wasteTypes/find-by-id/';

//Collection Type API
$all_collection_types = 'collectionTypes/find-collection-types';
$create_colletion_type = 'collectionTypes/create-collection-type';
$update_collection_type = 'collectionTypes/update-collection-type';

//Collection Sub Type API
$all_collection_sub_types = 'collectionSubtype/get-all-collection-sub-types';
$create_colletion_sub_type = 'collectionSubtype/create-collection-sub-type';
$update_collection_sub_type = 'collectionSubtype/update-collection-sub-type';
$get_sub_type_by_collection_type = 'collectionSubtype/get-all-collectionsubtype-By-CollectionType/';

//Franchise  API
$all_partners = 'franchise/get-All-Franchises';
$create_partner = 'franchise/create-franchise';
$update_partner = 'franchise/update-franchise';
$get_partner_by_id = 'franchise/get-Franchise-By-Id/';
$franchise_login = 'franchise/franchise-login/';
$franchise_forgot_password = 'franchise/franchise-forgot-password';
$franchise_reset_password = 'franchise/franchise-reset-password';

//Franchise Driver  API
$all_drivers = 'franchise-driver/find-all-franchise-drivers';
$create_driver = 'franchise-driver/create-franchise-driver';
$update_driver= 'franchise-driver/update-franchise-driver';
$get_drivers_by_franchiseid = 'franchise-driver/find-driver-by-franchise-id/';
$get_drivers_by_Id = 'franchise-driver/find-franchise-driver-by-id/';
$assign_driver_to_franchise = 'franchise-driver/assign-franchise/';


//Vehicle  API
$all_vehicles = 'vehicles/find-vehicles';
$create_vehicle = 'vehicles/create-vehicle';
$update_vehicle= 'vehicles/update-vehicle';
$get_vehicles_by_franchiseid = 'vehicles/find-by-franchise-id/';

//Payment API
$get_all_payments = 'payment/find-all-payments';
$create_payment = 'payment/create-payments';
$update_payment = 'payment/update-payment';  // not used 
$ecocash_payment = 'payment/make-ecocash-payment';
$approve_payment = 'payment/update-payment-status';
$payments_by_method = 'payment/find-payments-by-payment-method-id/';



//Booking  API
$all_bookings = 'bookings/find-all-bookings';
$create_booking = 'bookings/create-booking';
$update_booking = 'bookings/update-booking';
$get_bookings_by_userid = 'bookings/get-bookings-by-user-id/';
$get_bookings_by_franchiseid = 'bookings/find-by-booking-franchise-id/';
$assign_booking = 'bookings/accept-booking/';
$all_bookings_by_status = 'bookings/get-bookings-by-status/';
$re_assign_booking = 'bookings/reassign-driver/';
$get_booking_by_bookingId = 'bookings/find-by-booking-id/';
$get_booking_by_userId = 'bookings/get-bookings-by-user-id/';
$admin_approve_cancel_booking = 'bookings/admin-approve-cancel';
$re_assign_franchise = 'bookings/reassign-franchise/';



//Trip  API
$start_trip = 'bookings/start-trip/';
$end_trip = 'bookings/end-trip/';

//Currency  API
$all_currencies = 'currency/get-all-currency';
$create_currency = 'currency/create-currency';
$update_currency = 'currency/update';
$get_currency_by_Id = 'currency/get-currency-by-id/';

//Quotations  API
$all_quotations = 'quotations/find-all-quotations';
$create_quotation = 'quotations/create-quotation';
$update_quotation = 'quotations/update-quotation';
$get_quotation_by_id = 'quotations/find-by-id/';
$get_quotation_by_bookingid = 'quotations/find-by-booking-id/';


//Payment Method  API
$all_payment_methods = 'paymentmethod/get-all-payment-methods';
$create_payment_method = 'paymentmethod/create-paymentmethod';
$update_payment_method = 'paymentmethod/update-paymentmethod';

//Cities  API
$all_cities = 'cities/find-cities';
$create_city = 'cities/create-city';
$update_city = 'cities/update-city';


//Zone Pricing  API
$all_zone_prices = 'zonePricing/find-all-zone-pricing';
$create_zone_price = 'zonePricing/create-zone-pricing';
$update_zone_price = 'zonePricing/update-zone-pricing';


//Zones  API(Modification WIP)
$all_zones = 'zone/create-zone';
$create_zone = 'zone/create-zone';
$update_zone = 'zonePricing/update-zone-pricing';

//Queries  API
$all_queries = 'query/find-all-queries';
$create_query = '/query/create-querys';
$update_query = 'query/update-query';


//Users API
$all_users = 'user/all';
$user_by_Id = 'user/';
$add_user = 'user/create';
$update_user = 'user/update';
$delete_user = 'user/delete';
$login_user = 'user/login';
$logins_user = 'user/logins';
$forgot_password = 'user/forgotPasswords';
$reset_password = 'user/resetPasswords';
$approve_user = 'user/approve';
$user_by_franchiseeId = 'user/allByFranchiseeId/';

//Customers API
$all_customers = 'customer/all';
$by_id_customer='customer/';
$add_customer = 'customer/create';
$update_customer = 'customer/update';
$id_customer ='customer/id';
$customer_by_franchiseeId ='customer/allByFranchiseeId/';


?>
